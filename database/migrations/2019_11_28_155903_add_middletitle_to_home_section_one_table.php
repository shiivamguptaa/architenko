<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMiddletitleToHomeSectionOneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_section_one', function (Blueprint $table) {
            if (!Schema::hasColumn('home_section_one', 'middle_section_title'))
              $table->string('middle_section_title')->nullable();          
            if (!Schema::hasColumn('home_section_one', 'middle_section_description'))
              $table->longText('middle_section_description')->nullable();
            if (!Schema::hasColumn('home_section_one', 'middle_section_view_more'))
              $table->string('middle_section_view_more')->nullable();
            if (!Schema::hasColumn('home_section_one', 'youtube_link'))
              $table->string('youtube_link')->nullable();
            if (!Schema::hasColumn('home_section_one', 'youtube_link_title'))
              $table->string('youtube_link_title')->nullable();
			if (!Schema::hasColumn('home_section_one', 'youtube_link_sub_title'))
              $table->string('youtube_link_sub_title')->nullable();
			});			
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home_section_one', function (Blueprint $table) {
            //
        });
    }
}
