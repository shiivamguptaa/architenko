<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MoreInformation extends Authenticatable
{
    use Notifiable;
    protected $table = 'more_information';  
    protected $fillable = [
    	"title",
        "service_title",
        "project_address_title",
        "project_address_input_title",
        "description_question_project_title",
        "personal_contact_title",
        "address_title",
        "mr_title",
        "mrs_title",
        "first_name",
        "middle_name",
        "last_name",
        "address",
        "city",
        "postal_code",
        "country",
        "phone",
        "email",
        "dob",
        "iam_title",
        "enterprise_title",
        "developer_title",
        "governmental_title",
        "private_individual_title",
        "private_individual_address_title",
        "private_individual_mr_title",
        "private_individual_mrs_title",
        "private_individual_first_name",
        "private_individual_middle_name",
        "private_individual_last_name",
        "private_individual_address",
        "private_individual_city",
        "private_individual_postal_code",
        "private_individual_country",
        "private_individual_phone",
        "private_individual_email",
        "private_individual_dob",
        "enterprise_price_title1",
        "enterprise_price_title2",
        "enterprise_address_title",
        "enterprise_city_title",
        "enterprise_postal_code_title",
        "enterprise_country_title",
        "enterprise_phone_title",
        "enterprise_email_title",
        "enterprise_contact_title",
        "enterprise_mr_title",
        "enterprise_mrs_title",
        "enterprise_contact_first_name_title",
        "enterprise_contact_last_name_title",
        "enterprise_contact_personpositionlist_title",
        "submit",
        "language"
	];
    protected $casts = [
        'enterprise_contact_personpositionlist_title' => 'array'
    ];
}
