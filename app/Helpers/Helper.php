<?php
use App\Label;
use App\Service;
use App\MoreInformation;
use App\Country;
use App\Articles;
use App\ErrorMessage;
use App\FormValidaionErrors;

if (!function_exists('getLabel')) {
    function getLabel($label){
        $language = session()->get('locale');
        if(empty($language)){
            $language = "en";
        }
        $labeldata = Label::where("label",$label)->where('language',$language)->orderBy('id',"DESC")->first();
        if(!empty($labeldata)) {
            return $labeldata->name;
        }   
        else{
            return "";
        }
    } 
    
}

if (!function_exists('getServiceList')) {
    function getServiceList() {
        $language = session()->get('locale');
        if(empty($language)){
            $language = "en";
        }
        $servicedata = Service::select('id','title')->where('language',$language)->orderBy('orderby',"ASC")->get();
        if(!empty($servicedata)) {
            return $servicedata->toArray();
        }   
        else{
            return array();
        }
    }     
}

if (!function_exists('getMoreInformation')) {
    function getMoreInformation() {
        $language = session()->get('locale');
        if(empty($language)) {
            $language = "en";
        }
        $moreinformation = MoreInformation::where('language',$language)->orderBy('id',"DESC")->first();
        if(!empty($moreinformation)) {
            return $moreinformation->toArray();
        }   
        else{
            return array();
        }
    }     
}

if (!function_exists('getCountryList')) {
    function getCountryList() {
        $language = session()->get('locale');
        if(empty($language)) {
            $language = "en";
        }
        $getCountryList = Country::where('language',$language)->orderBy('name',"ASC")->get();
        if(!empty($getCountryList)) {
            return $getCountryList->toArray();
        }   
        else{
            return array();
        }
    }     
}
if (!function_exists('getArticalList')) {
    function getArticalList(){
        $language = session()->get('locale');
        if(empty($language)){
            $language = "en";
        }
        $articlesList = Articles::where('language',$language)->orderBy('id',"DESC")->get();
        if(!empty($articlesList)) {
            return $articlesList->toArray();
        }   
        else {
            return array();
        }
    }     
}
if (!function_exists('getEmailEnterMessage')) {
    function getEmailEnterMessage(){
        $language = session()->get('locale');
        if(empty($language)) {
            $language = "en";
        }
        $ErrorMessage = ErrorMessage::where('language',$language)->orderBy('id',"DESC")->first();
        if(!empty($ErrorMessage)) {
            return $ErrorMessage->email_enter_message;           
        }   
        else {
            return "";
        }
    }     
}
if (!function_exists('getEmailEnterInvalidMessage')) {
    function getEmailEnterInvalidMessage(){
        $language = session()->get('locale');
        if(empty($language)) {
            $language = "en";
        }
        $ErrorMessage = ErrorMessage::where('language',$language)->orderBy('id',"DESC")->first();
        if(!empty($ErrorMessage)) {
            return $ErrorMessage->email_invalid_message;           
        }   
        else {
            return "";
        }
    }     
}
if (!function_exists('articalList')) {
    function articalList(){
        $language = session()->get('locale');
        if(empty($language)){
            $language = "en";
        }
        $articlesList = Articles::where('language',$language)->orderBy('id',"DESC")->pluck('title','id');
        if(!empty($articlesList)) {
            return $articlesList->toArray();
        }   
        else {
            return array();
        }
    }     
}
if (!function_exists('getformerrors')) {
    function getformerrors($form){
        $language = session()->get('locale');
        if(empty($language)){
            $language = "en";
        }
        $errors = FormValidaionErrors::where('form_name',$form)->where('language',$language)->first();
    }     
}
if (!function_exists('getimagehtmlbytype')) {
    function getimagehtmlbytype($type,$img){
        if($img != ''){
            // $html = '<div class="row"> <div class="col-md-10"><div><i class="material-icons file-del-btn" data-name="">close</i></div>';
            $html = '<div class="row"> <div class="col-md-10">';
            switch ($type) {
                case 'image':
                    
                    $html .= '<img src="'.URL($img).'" width="100px" style="margin-left:15px">';
                    
                   break;
                case 'youtube':
                    $html .= '<div class="h-c-wrapper"><div class="cont-wrapper"><div class="youtube-video-wrapper"><iframe src="'.$img.'" style="margin-left:15px" width="100px" class="youtube-video" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div></div>';
                   break;
                case 'video':
                    
                    $html .= '<div class="h-c-wrapper"><div class="cont-wrapper"><div class="video-wrapper"><video style="margin-left:15px" width="100px" controls><source src="'.URL($img).'" type="video/mp4"></video></div></div></div> ';
                    
                   break;
                case 'book':
                    $data = json_decode($img);
                    $html .= '<div class="h-c-wrapper"><div class="cont-wrapper"><div class="book-wrapper" style="margin-left:15px">';
                    $index = 0;
                    foreach($data as $image) { 
                        $html.= ($index==0) ? '<img  width="100px"  src="'.URL($image).'" class="active" />' : '<img width="100px" src="'.URL($image).'" />';
                        $index++;   
                    }
                    $html .= '</div></</div></div></div>';
                   break;
                case 'flip':
                    $image_list = json_decode($img);
                    if(count($image_list)>0){
                        $html .= '<div class="h-c-wrapper"><div class="cont-wrapper"><div class="twentytwenty-wrapper twentytwenty-horizontal"><div class="before-after-wrapper twentytwenty-container" style="margin-left:15px">';
                        foreach($image_list as $image){
                            $html .= '<img width="100px"   src="'.URL($image).'" alt="" >';
                        } 
                        $html .= '<div class="twentytwenty-handle"><span class="twentytwenty-left-arrow"></span><span class="twentytwenty-right-arrow"></span></div></div></div></div></div>';
                    }
                   break;
               
                default:
                   # code...
                   break;
            }
            $html .= '</div></div>'; 
        }
       return $html;
    }     
}
