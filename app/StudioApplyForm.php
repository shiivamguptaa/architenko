<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StudioApplyForm extends Authenticatable
{
    use Notifiable;
    protected $table = 'studio_apply_form';  
    protected $fillable = [
    	"privacy_policy_title",
        "privacy_policy_short_description",
        "privacy_policy_accept_title",
        "language_correspondence_title",
        "language_input",
        "position_title",
        "position_option1",
        "position_option2",
        "position_option3",
        "position_option4",
        "personal_contact_title",
        "address_title",
        "mr_title",
        "mrs_title",
        "first_name",
        "middle_name",
        "last_name",
        "address",
        "city",
        "postal_code",
        "country",
        "phone",
        "email",
        "dob",
        "nationality_title",
        "another_nationality_title",
        "education_title",
        "institution_title",
        "degree_title",
        "degree_start_year",
        "another_degree_add_title",
        "language_title",
        "language_list",
        "language_add_another_title",
        "language_add_another_input",
        "profesional_situation_title",
        "profesional_situation_option1",
        "profesional_situation_option2",
        "profesional_situation_option3",
        "profesional_situation_option4",
        "applied_at_architenko_title",
        "yes_title",
        "no_title",
        "aboutus_title",
        "attachments_title",
        "file_validation_message",
        "cover_letter_title",
        "curriculum_vitae_title",
        "portfolio_title",
        "comment_title",
        "apply_title",
        "language",
        "apply_btn",
        "profesional_situation_option2_year_month",
        "profesional_situation_option3_year_month"

	];
    protected $casts = [
        'language_input'=>'array',
        'nationality_list' => 'array',
        'language_list' => 'array'
    ];
}
