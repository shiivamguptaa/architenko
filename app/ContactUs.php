<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ContactUs extends Authenticatable
{
    use Notifiable;
    protected $table = 'contactus';  
    protected $fillable = [
    	"enquiry_title",
    	"enquiry_email",
    	"communication_title",
		"communication_email",
		"image",
		"invoice_information",
		"language",
        "orderby"
	];

}
