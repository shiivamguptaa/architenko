<?php

namespace App\Providers;
use Illuminate\Support\Facades\View;
use App\Configuration;
use App\Languages;
use App\Social;
use App\Logo;
use App\Menu;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $logo=Logo::first();
        //$config=Configuration::first();
		$languages=Languages::get();
		$sociallinks=Social::get();
		//$menus=Menu::where('parent',0)->get();
		//$submenus=Menu::where('parent','!=',0)->get();
		
    //View::share('config', $config);
    View::share('languages', $languages);
    View::share('sociallinks', $sociallinks);
   // View::share('menus', $menus);
   // View::share('submenus', $submenus);
    View::share('logo', $logo);
    }
}
