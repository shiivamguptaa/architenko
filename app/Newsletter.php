<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Newsletter extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'newsletter';
    protected $fillable = [
        'email',
    ];
	
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
}
