<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ProjectDownload extends Authenticatable
{
	protected $table = 'project_download';  
	protected $fillable = [
		'project_id','project_name','email'
	];
}