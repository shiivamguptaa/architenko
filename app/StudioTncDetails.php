<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StudioTncDetails extends Authenticatable
{

	protected $table = 'studio_tnc_details';
    protected $fillable = [
        'tnc_id','studio_id','description','title','image','display_order','show_in_privacy_policy'
    ];
}
