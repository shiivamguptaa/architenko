<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ArticlesCategory extends Authenticatable
{

    protected $table = 'article_category';
    protected $fillable = [
        'category_name','language','orderby'
    ];
	
}
