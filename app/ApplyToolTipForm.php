<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ApplyToolTipForm extends Authenticatable
{
    use Notifiable;
    protected $table = 'apply_tooltip_form';  
    
    protected $fillable = [
    	"title",
        "tooltip_name",
        "you_found_through_title",
        "you_found_through_option1",
        "you_found_through_option2",
        "you_found_through_option3",
        "you_found_through_option4",        
        "choose_toolkit_title",
        "choose_toolkit_option1",
        "choose_toolkit_option2",
        "choose_toolkit_option3",
        "choose_toolkit_option4",      
        "personal_contact_title",
        "i_am_title",
        "enterprise_title",
        "developer_title",
        "governmental_title",
        "private_individual_title",
        "private_individual_address_title",
        "private_individual_mr_title",
        "private_individual_mrs_title",
        "private_individual_first_name",
        "private_individual_middle_name",
        "private_individual_last_name",
        "private_individual_address",
        "private_individual_city",
        "private_individual_postal_code",
        "private_individual_country",
        "private_individual_phone",
        "private_individual_email",
        "private_individual_dob",
        "enterprise_price_title1",
        "enterprise_price_title2",
        "enterprise_address_title",
        "enterprise_city_title",
        "enterprise_postal_code_title",
        "enterprise_country_title",
        "enterprise_phone_title",
        "enterprise_email_title",
        "enterprise_contact_title",
        "enterprise_mr_title",
        "enterprise_mrs_title",
        "enterprise_contact_first_name_title",
        "enterprise_contact_last_name_title",
        "enterprise_contact_personpositionlist_title",
        "apply_title",
        "language"
	];
     protected $casts = [
        'enterprise_contact_personpositionlist_title' => 'array'
    ];
}
