<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StudioDeails extends Authenticatable
{
    use Notifiable;
	protected $table = 'studio_details';
    protected $fillable = [
        'id',
        'studio_id',
        'section_type',
        'description',
        'title',
        'display_order',
        'image',
    ];

}
