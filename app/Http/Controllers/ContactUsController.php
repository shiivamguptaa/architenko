<?php	
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\ContactUs;
use App\ContactUsAddress;
use App\Configuration;
use App\HomeSectionOne;
use App\NewsletterText;
use App\Label;
use App\Menu;
use Validator;
use Session;
use File;
use App;
use DB;

class ContactUsController extends Controller {
	
	public function __construct(){		
		
	}
	
	public function index(){
		$title = 'Contact Us';	
		$locale = App::getLocale();
		$contactus = array();		
		$contactusDetails = array();		
		$contactus = ContactUs::select("id","enquiry_title","enquiry_email","communication_title","communication_email","image","invoice_information","language")->where('language',$locale)->orderBy("orderby","ASC")->first();
		if(!empty($contactus)){
			$contactusDetails = ContactUsAddress::where("contactus_id",$contactus->id)->orderBy("display_order","ASC")->get();
		}
		
		
		$config = Configuration::where('language','=',$locale)->first();
		$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
		$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
		$newslettertext = NewsletterText::where('language','=',$locale)->first();
		$labellist = Label::get();
		$labeldata = array();
		$homedata = HomeSectionOne::where('language','=',$locale)->orderBy("id","DESC")->first();
		foreach($labellist as $label){
			$labeldata[$label->label][$label->language] = $label->name;
		}
		return view('contactus')->with(compact('title','contactus','contactusDetails','locale','menus','submenus','config','newslettertext','labellist','labeldata','homedata'));
	}
}