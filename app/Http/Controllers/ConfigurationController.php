<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuration;
use App\Logo;
use Validator;
use DB;
use File;
use Illuminate\Support\Facades\Input;
use Session;

class ConfigurationController extends Controller
{
    //
	public function __construct()
    {
        $this->middleware('auth');
		$this->messages = [
			'required' => 'The :attribute is required.',
		];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$title = 'Footer Listing';	
		$checkconfig = Configuration::get();
		
        return view('admin.config.listing')->with(compact('checkconfig','title'));
    }
	
	 public function create()
    {
		$title = 'Footer Create';	
		//$checkconfig = Configuration::first();
		
		 
        return view('admin.config.create')->with(compact('title'));
    }
	
	
	public function store(Request $request)
    {
		$title = 'Footer store';	
		
		 $validator = Validator::make($request->all(), [           
          'name' => 'required',
          'email' => 'required',
          'address' => 'required',
          ],$this->messages);
      
	   if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
		 $input = $request->all();
		 
		$logo = 'noimg'; 
		 
		 
        $input['name'] = $input['name'];
        $input['email'] = $input['email'];
        $input['mobile'] = $input['mobile'];
        $input['logo'] = $logo;
        $input['address']=$input['address'];
		$input['description']=$input['description'];
		$input['description2']=$input['description2'];
		$input['language']=$input['lang'];
        
        
        if(Configuration::create($input))
        {
			$request->session()->flash('alert-success', 'Configuration added successfully.');
		}
	  return redirect('footer');
	   
    }
	
	public function edit($configurationid){		
		$title = 'Configuration Edit';		
		$configurationdata = Configuration::find($configurationid);		
		return view('admin.config.edit')->with(compact('title','configurationdata','configurationid'));
    }
	
	public function editlogo($logoid){		
		$title = 'Logo Edit';		
		$logodata = Logo::find($logoid);		
		return view('admin.config.logo')->with(compact('title','logodata','logoid'));
    }
	
	/**
     * Update Configuration info.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){
		
		$validator = Validator::make($request->all(), [           
          'name' => 'required',
          'email' => 'required',
          'address' => 'required',
          ]);
		  
		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->errors());
		}
        
		
		$configurationdata = Configuration::find($request['id']);
		
		if(empty($configurationdata))
		{
			$request->session()->flash('alert-danger', trans('Some problem occured in update footer details.'));
			return redirect('footer');
		}		
		
		$configurationdata->name = ucfirst($request['name']);
		$configurationdata->email = $request['email'];
		$configurationdata->mobile = $request['mobile'];		
		$configurationdata->address = $request['address'];		
		$configurationdata->description = $request['description'];		
		$configurationdata->description2 = $request['description2'];		
		$configurationdata->language = $request['lang'];			
		
		 
		
		if($configurationdata->update()){
			 
			$request->session()->flash('alert-success', trans('Footer details updated successfully.'));
		}
		else{
			$request->session()->flash('alert-danger', trans('Some problem occured in update configuration details.'));			
		}
		
		return redirect('footer');
    }
	
	 public function updatelogo(Request $request){
		
		$validator = Validator::make($request->all(), [           
          
          ]);
		  
		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->errors());
		}
        
		
		$logodata = Logo::find($request['id']);
		
		if(empty($logodata))
		{
			$request->session()->flash('alert-danger', trans('Some problem occured in update logo details.'));
			return redirect()->back();
		}		
		
		 
		if(isset($request->logo) && !empty($request->logo) && $request->logo != NULL){
			$imageName = time().'.'.request()->logo->getClientOriginalExtension();
			$logodata->logo = $imageName;
		} 
		if(isset($request->transparent_logo) && !empty($request->transparent_logo) && $request->transparent_logo != NULL){
			$imageName1 = time().'.'.request()->transparent_logo->getClientOriginalExtension();
			$logodata->transparent_logo = $imageName1;
		} 
		if(isset($request->menutoggle_logo) && !empty($request->menutoggle_logo) && $request->menutoggle_logo != NULL){
			$imageName2 = time().'.'.request()->menutoggle_logo->getClientOriginalExtension();
			$logodata->menutoggle_logo = $imageName2;
		}
		
		if($logodata->update()){
			if(isset($request->logo) && !empty($request->logo) && $request->logo != NULL){				
				$imageName = time().'.'.request()->logo->getClientOriginalExtension();
				$path = public_path('/images');
				request()->logo->move($path, $imageName);				
			}
			if(isset($request->transparent_logo) && !empty($request->transparent_logo) && $request->transparent_logo != NULL){				
				$imageName1 = time().'.'.request()->transparent_logo->getClientOriginalExtension();
				$path = public_path('/images');
				request()->transparent_logo->move($path, $imageName1);				
			}
			if(isset($request->menutoggle_logo) && !empty($request->menutoggle_logo) && $request->menutoggle_logo != NULL){				
				$imageName2 = time().'.'.request()->menutoggle_logo->getClientOriginalExtension();
				$path = public_path('/images');
				request()->menutoggle_logo->move($path, $imageName2);				
			}
			$request->session()->flash('alert-success', trans('Logo details updated successfully.'));
		}
		else{
			$request->session()->flash('alert-danger', trans('Some problem occured in update logo details.'));			
		}
		
		return redirect()->back();
    }
	
	public function destroy($configurationid){
		$configuration = Configuration::where('id','=',$configurationid)->forcedelete();		
		Session::flash('alert-success', 'Footer Deleted Successfully!');		
		return redirect('footer');
    }
}
