<?php
	
	namespace App\Http\Controllers;
	use App;	
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;
	use App\Project;
	use App\Projectdetails;
	use App\ProjectCatagory;
	use App\ProjectDownload;
	use Validator;
	use DB;
	use File;
	use Session;
	use App\Configuration;
	use App\HomeSectionOne;
	use App\NewsletterText;
	use App\Label;
	use App\Menu;
	use App\ProjectInnerContent;
	use Mail;

	
	class ProjectListController extends Controller
	{
		//
		public $limit =4;
		public function __construct()
		{
			// $this->middleware('auth');
			// $this->messages = [
			// 'required' => 'The :attribute is required.',
			// ];
		}
		
		/**
			* Show the project.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			if(isset($_GET['search'])){
				
			}
			$title = 'Project Data Listing';	
			$locale = App::getLocale();
			$projects = array();
			$project = Project::select("id","title","language","subtitle","orderby")->where('language',$locale)->orderBy("orderby","ASC");
			if(isset($_GET['search'])){
				$project->where('title','like','%'.$_GET['search'].'%');
			}
			if(isset($_GET['catagory'])){
				$project->where('catagory_id',$_GET['catagory']);
			}
			$project = $project->paginate(12);
			$pagination_links = $project->links();
			foreach ($project as $key => $value) {
				$projectList = array();
				$projectdetails = Projectdetails::select("id","type","datavalue")->where("projectid",$value->id)->orderBy("display_order","ASC")->first();
					$projectList["projectId"] = $value->id;
					$projectList["title"] = $value->title;
					$projectList["subtitle"] = $value->subtitle;
					$projectList["language"] = $value->language;
					$projectList["orderby"] = $value->orderby;
					$projectList["projectdetailsId"] = $projectdetails->id;
					$projectList["datavalue"] = $projectdetails->datavalue;
					$projectList["type"] = $projectdetails->type;
					$projects[] =$projectList;
			}
			$config = Configuration::where('language','=',$locale)->first();
			$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
			$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
			$newslettertext = NewsletterText::where('language','=',$locale)->first();
			$labellist = Label::get();
			$labeldata = array();
			$homedata = HomeSectionOne::where('language','=',$locale)->orderBy("id","DESC")->first();
			foreach($labellist as $label){
				$labeldata[$label->label][$label->language] = $label->name;
			}
			return view('projectlist')->with(compact('title','projects','locale','menus','submenus','config','newslettertext','labellist','labeldata','homedata','pagination_links'));
		}
		public function getProject(Request $request) {
			$input = $request->all();
			$locale = App::getLocale();
			$offset = $input["offset"];	
			$row_number =1;		
	        $projects = array();
			$project = Project::select("id","title","subtitle","language","orderby")
						->where('language',$locale)
						->orderBy("orderby","ASC")
						->offset($this->limit*$offset)
						->take($this->limit)->get();
			foreach ($project as $key => $value) {
				$projectList = array();
				$projectList["projectId"] = $value->id;
				$projectList["title"] = $value->title;
				$projectList["subtitle"] = $value->subtitle;
				$projectList["language"] = $value->language;
				$projectList["orderby"] = $value->orderby;
				$projectdetails = projectdetails::select("id","type","datavalue")->where("projectid",$value->id)->orderBy("display_order","ASC")->first();
				$projectList["projectdetailsId"] = $projectdetails->id;
				$projectList["datavalue"] = $projectdetails->datavalue;
				$projectList["type"] = $projectdetails->type;
				$projects[] =$projectList;
			}
	        $output = "";
	        $number=1;
	        $next_index_no_two = 2; $next_index_no_three=3;
	        $last_record=false;
			$inner_arr = count($projects);
			foreach($projects as $project)	{
				if($row_number==$next_index_no_two) {
					$output	 .= "<div class='row  row".$number."'>";	
					$output	 .= "<div class='col col-l'>";	
					$number++;			
				}else if($row_number==$next_index_no_three)	{
					$output	 .= "<div class='col col-r'>";	
				}else{
					$output	 .= "<div class='row  row".$number."'>";		
					$output	 .= "<div class='col'>";	
					$number++;			
				}	
				if($project['type']=="image")	{
					$output	 .= "<a href='".url('projectdetails/'.$project['projectId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'>";	
					$output	 .= "<img src=".URL('images/project/'.$project['datavalue'])." />";
					$output	 .= "</div><p class='project-title'>".$project['title']."</p></div></a>";	
				} else if($project['type']=="youtube"){
					$output	 .= "<a href='".url('projectdetails/'.$project['projectId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='youtube-video-wrapper'>";
					$output	 .= "<iframe src='".$project['datavalue']."'  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
					$output	 .= "</div></div><p class='project-title'>".$project['title']."</p></div></a>";	
				}elseif($project['type']=='video'){
					$output	 .="<a href='".url('projectdetails/'.$project['projectId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='video-wrapper'>";
					$output	 .="<video width='100%' controls><source src='".URL('images/project/'.$project['datavalue'])."' type='video/mp4'></video>";
					$output	 .="</div></div><p class='project-title'>{{$project['title']}}</p></div></a>";	
				}
				elseif($project['type']=='book'){
					$image_list = json_decode($project['datavalue']);
					$output	 .="<a href='".url('projectdetails/'.$project['projectId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='book-wrapper'>";
					$index=0; 
					foreach($image_list as $image) { 
						$output	 .=($index==0) ? "<img  width='100%'  src=''.URL('images/project/'.$image).'' class='active' />" : "<img width='100%' src=".URL('images/project/'.$image)." />";
						$index++;	
					}
					$output	.="</div></div><p class='project-title'>{{$project['title']}}</p></div></a>";	
				}
				elseif($project['type']=='flip'){
					$image_list = json_decode($project['datavalue']);
					if(count($image_list)>0){ 
						$output	 .="<a href='".url('projectdetails/'.$project['projectId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='twentytwenty-wrapper twentytwenty-horizontal'><div class='before-after-wrapper twentytwenty-container active'>";
						foreach($image_list as $image) {
							$output	 .="<img width='100%'   src='".URL('images/project/'.$image)."' alt=''>";
						}
						$output	 .="<div class='twentytwenty-handle'><span class='twentytwenty-left-arrow'></span><span class='twentytwenty-right-arrow'></span></div></div></div></div><p class='project-title'>".$project['title']."</p></div></a>";
								
					}
				}
				if($row_number>=$inner_arr && $row_number==$next_index_no_three)		
					$output	 .="</div></div>";
				elseif($row_number!=$next_index_no_two)
					$output	 .="</div>";
				elseif($inner_arr==$row_number)
					$output	 .="</div></div>";
				
				if($row_number==$next_index_no_two)
					$next_index_no_two = $next_index_no_two+4;
				if($row_number==$next_index_no_three)	
					$next_index_no_three = $next_index_no_three+4;	
				
				$output	 .="</div>";
				$row_number++; 
				if($row_number>=5) {
					$row_number=1;
					$next_index_no_two=2;
					$next_index_no_three=3;
					$number=1;
				}
			}	
			$totalRecord = Project::where('language',$locale)->count();
			if($totalRecord>($this->limit*$offset)){
				$isLastRecord = false;	
			}
			else{
				$isLastRecord = true;	
			}
			return response()->json(['success' => true, 'message' => 'Successfully get project list','data'=>$output,'isLastRecord'=>$isLastRecord]);
	}

	public function projectDetails($id){
		if(!empty($id)){			
			$title = 'Project Data Listing';	
			$locale = App::getLocale();
			$projectlist = Project::find($id);
			if(!empty($projectlist)){
				$projects = Projectdetails::where("projectid",$id)->get();
				$config = Configuration::where('language','=',$locale)->first();
				$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
				$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
				$newslettertext = NewsletterText::where('language','=',$locale)->first();
				return view('projectdetails')->with(compact('title','projectlist','projects','locale','menus','submenus','config','newslettertext'));
			}
		}
	}
	public function projectInnerPage($id){
		if(!empty($id)){			
			$title = 'Project Data Listing';	
			$locale = App::getLocale();
			$projectlist = Project::find($id);
			if(!empty($projectlist)){
				$projectInnerContents = ProjectInnerContent::where("projectid",$id)->get();
				$projects = Projectdetails::where("projectid",$id)->get();
				$config = Configuration::where('language','=',$locale)->first();
				$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
				$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
				$newslettertext = NewsletterText::where('language','=',$locale)->first();
				return view('projectinnerpage')->with(compact('title','projectlist','projects','locale','menus','submenus','config','newslettertext','projectInnerContents'));
			}
		}
	}
	public function searchProject(Request $request){
		if($request->ajax()){
			$locale = App::getLocale();
			$projects = Project::select('title','project.id','catagory_id','catagory')->where('project.language',$locale);
			if (isset($request->data)) {
				$projects = $projects->where('title','like','%'.$request->data.'%');
			}
			$projects = $projects->join('project_catagory','project_catagory.id','=','project.catagory_id')->orderby('project_catagory.orderby','ASC')->get();

			$catagory = ProjectCatagory::where('language',$locale)->where('status',1)->orderby('orderby','ASC')->get();

			$data = '<ul class="cat-result">';
			if (!isset($request->data)) {
				foreach ($catagory as $value) {
					$url1 = URL('projectlist?catagory='.$value->id);
					$data.='<li><a href="'.$url1.'">'.$value->catagory.'<span class="hr"></span></a></li>';
				}
			}
			foreach ($projects as $project) {
				$url = URL('projectdetails',$project->id);
				$data .= '<li><a href="'.$url.'">'.$project->title.'<span class="hr"></span></a>'.$project->catagory.'</li>';
			}
			$data .= '</ul>';
			
			return response()->json(['responceCode' => 1, 'data' => $data]);
		}
		else{
			return response()->json(['responceCode' => 0, 'responseMessage' => 'Something went wrong']);
		}
	}

	public function projectDetailsForm(Request $request){
		if($request->ajax()){
			$id = isset($request->projectid) ? $request->projectid : 0;	
			$project = Project::find($id);
			if(!empty($project)){
				if(!empty($project->pdf)){
					$to = $request->userEmail;
					$data = array();
					$path = url('images/project/pdf',$project->pdf);
					Mail::send('email-templates.general', $data, function($message) use ($to,$path) {
						 $message->to($to, 'Architenko');
						 $message->subject('Thanks for download file');
						 $message->setContentType('text/html');
						 $message->from('info@architenko.com','Architenko');
						 $message->attach($path);
					 });
					$data = array();
					$data['project_id'] 	= $request->projectid;
					$data['project_name']	= isset($project->title) ? $project->title : '';
					$data['email']			= isset($request->userEmail) ? $request->userEmail : '';
					ProjectDownload::create($data);
				 	return response()->json(['responceCode' => 1, 'responseMessage' => "Successfully mail send"]);
				}				
				else {
					return response()->json(['responceCode' => 0, 'responseMessage' => "Document not found"]);
				}	
			}
			else {
				return response()->json(['responceCode' => 0, 'responseMessage' => "Something went wrong"]);
			}
		}
		else{
			return response()->json(['responceCode' => 0, 'responseMessage' => 'Something went wrong']);
		}
	}

		/*
		public function create()
		{
			
			$title = 'project Create';	
			
			return view('admin.project.create')->with(compact('title'));
		}
		
		
		public function store(Request $request)
		{
			
			$input = $request->all();
			
			
			$input['title']=$input['title'];			
			$input['subtitle']=$input['subtitle'];			
			$input['descriptionleft']=$input['descriptionleft'];			
			$input['descriptionright']=$input['descriptionright'];			
			$input['language']=$input['lang'];		
			
			$ishome = $input['ishome'];
			
			if($projectadd = project::create($input))
			{				
				foreach($request->addmore as $getvalue){
					//dd($getvalue);
					if($getvalue['type'] == 'image'){
						if(!empty($getvalue['image'])){
							$dataimage = $getvalue['image']->getClientOriginalName();
							$time = strtotime(now());
							$filename = $time.strtolower(str_replace(" ","_",$dataimage));
							$path_image = public_path('/images/project');
							$getvalue['image']->move($path_image, $filename);
							if($ishome == 'image'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'is_home'=>$home,'projectid'=>$projectadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$projectadd->language]);
						}
						else{
							$dataimage ="";	
						}
					}
					if($getvalue['type'] == 'youtube'){
						
						if($ishome == 'youtube'){
							$home = '1';
							}else{
							$home = '0';
						}
						projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$getvalue['youtube'],'is_home'=>$home,'projectid'=>$projectadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$projectadd->language]);
						
					}
					if($getvalue['type'] == 'video'){
						
						if(!empty($getvalue['video'])){
							$datavideo = $getvalue['video']->getClientOriginalName();
							$time = strtotime(now());
							$filename = $time.strtolower(str_replace(" ","_",$datavideo));
							$path_image = public_path('/images/project');
							$getvalue['video']->move($path_image, $filename);
							if($ishome == 'video'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'is_home'=>$home,'projectid'=>$projectadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$projectadd->language]);
						}
						else{
							$datavideo ="";	
						}
					}
					if($getvalue['type'] == 'book'){
						//dd($getvalue);
						$multi_img=array();
						if(!empty($getvalue['book'])){
							foreach($getvalue['book'] as $file){
								$multi_img_name1=$file->getClientOriginalName();
								$time = strtotime(now());
								$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
								$path_multi_img = public_path('/images/project');
								$file->move($path_multi_img,$multi_img_name);
								$multi_img[]=$multi_img_name;
							}
							$bookImages = json_encode($multi_img);
							if($ishome == 'book'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'is_home'=>$home,'projectid'=>$projectadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$projectadd->language]);
						}
						else{
							$bookImages ="";	
						}
					}
					if($getvalue['type'] == 'flip'){
						//dd($getvalue);
						$multi_img=array();
						if(!empty($getvalue['flip'])){
							foreach($getvalue['flip'] as $file){
								$multi_img_name1=$file->getClientOriginalName();
								$time = strtotime(now());
								$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
								$path_multi_img = public_path('/images/project');
								$file->move($path_multi_img,$multi_img_name);
								$multi_img[]=$multi_img_name;
							}
							$bookImages = json_encode($multi_img);
							if($ishome == 'flip'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'is_home'=>$home,'projectid'=>$projectadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$projectadd->language]);
						}
						else{
							$bookImages ="";	
						}
					}
				}
				
				$request->session()->flash('alert-success', 'project added successfully.');
			}
			return redirect('project');
			
		}
		
		
		public function edit($id){		
			$title = 'project Edit';
			
			$projectdata = project::find($id);
			$projectdetails = projectdetails::where('projectid','=',$id)->get();
			return view('admin.project.edit')->with(compact('title','projectdata','id','projectdetails'));
		}
		/**
			* Update Home Section One info.
			*
			* @return \Illuminate\Http\Response
		*/
		/*public function update(Request $request){
			
			$validator = Validator::make($request->all(), [           
			
			]);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			
			$projectdata = project::find($request['id']);
			
			if(empty($projectdata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));
				return redirect('project');
			}			
			
			$projectdata->title = $request['title'];			
			$projectdata->subtitle = $request['subtitle'];			
			$projectdata->descriptionleft = $request['descriptionleft'];			
			$projectdata->descriptionright = $request['descriptionright'];			
			
			if($projectdata->update()){
				
				$request->session()->flash('alert-success', trans('project details updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));			
			}
			
			return redirect('project');
		}

		public function update(Request $request){
			
			// $validator = Validator::make($request->all(), [           
			
			// ]);
			
			// if ($validator->fails())
			// {
			// 	return redirect()->back()->withErrors($validator->errors());
			// }
			
			
			// $projectdata = project::find($request['id']);
			
			// if(empty($projectdata))
			// {
			// 	$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));
			// 	return redirect('project');
			// }			
			
			// $projectdata->title = $request['title'];			
			// $projectdata->subtitle = $request['subtitle'];			
			// $projectdata->descriptionleft = $request['descriptionleft'];			
			// $projectdata->descriptionright = $request['descriptionright'];			
			
			// if($projectdata->update()){
				
			// 	$request->session()->flash('alert-success', trans('project details updated successfully.'));
			// }
			// else{
			// 	$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));			
			// }
			$input = $request->all();
			
			$projectdata = project::find($input['id']);
			if(empty($projectdata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));
				return redirect('project');
			}	
			$projectdata->title = $input['title'];			
			$projectdata->subtitle = $input['subtitle'];			
			$projectdata->descriptionleft = $input['descriptionleft'];
			$projectdata->descriptionright = $input['descriptionright'];		
			$projectdata->language = $input["lang"];		
			$language  = $input["lang"];

			if($projectdata->update()) {
				$projectid = $input['id'];
				$projectdetails  = projectdetails::select('id')->where('projectid','=',$projectid)->get();
				$projectidList = array();
				foreach ($projectdetails as $key => $value) {
					$projectidList[] = $value->id;
				}
				foreach($input["addmore"] as $getvalue){
					if(isset($getvalue['type'])){		
					if($getvalue['type'] == 'image') {						
						if(isset($getvalue["projectId"])){									
							if(in_array($getvalue["projectId"], $projectidList)) {
							
								$projectdetails = projectdetails::find($getvalue["projectId"]);
								if(!empty($projectdetails)){
									if(!empty($getvalue['image'])){
										$dataimage = $getvalue['image']->getClientOriginalName();
										$time = strtotime(now());
										$filename = $time.strtolower(str_replace(" ","_",$dataimage));
										$path_image = public_path('/images/project');
										$getvalue['image']->move($path_image, $filename);
										$projectdetails->datavalue =$filename;
									}	
									$projectdetails->type =$getvalue['type'];
									$projectdetails->projectid =$projectid;
									$projectdetails->moretitle =$getvalue['moretitle'];
									$projectdetails->display_order=$getvalue['display_order'];
									$projectdetails->language=$language;
									$projectdetails->save();
								}
							}					
						}
						if(!isset($getvalue["projectId"]))
						{
							if(!empty($getvalue['image'])){
								$dataimage = $getvalue['image']->getClientOriginalName();
								$time = strtotime(now());
								$filename = $time.strtolower(str_replace(" ","_",$dataimage));
								$path_image = public_path('/images/project');
								$getvalue['image']->move($path_image, $filename);						
								projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
							}						
						}
					}
					if($getvalue['type'] == 'youtube'){					
						if(isset($getvalue["projectId"])){
							if(in_array($getvalue["projectId"], $projectidList)) {
								$projectdetails = projectdetails::find($getvalue["projectId"]);
								if(!empty($projectdetails)){
									$projectdetails->type =$getvalue['type'];
									$projectdetails->datavalue = $getvalue['youtube'];
									$projectdetails->projectid = $projectid;
									$projectdetails->moretitle = $getvalue['moretitle'];
									$projectdetails->display_order=$getvalue['display_order'];
									$projectdetails->language=$language;
									$projectdetails->save();
								}		
							}
						}
						if(!isset($getvalue["projectId"])){
							projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$getvalue['youtube'],'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
						}
						
					}
					if($getvalue['type'] == 'video'){
						if(isset($getvalue["projectId"])){
							if(in_array($getvalue["projectId"], $projectidList)) {
								$projectdetails = projectdetails::find($getvalue["projectId"]);
								if(!empty($projectdetails)){
									if(!empty($getvalue['video'])){
										$datavideo = $getvalue['video']->getClientOriginalName();
										$time = strtotime(now());
										$filename = $time.strtolower(str_replace(" ","_",$datavideo));
										$path_image = public_path('/images/project');
										$getvalue['video']->move($path_image, $filename);
										$projectdetails->datavalue = $filename;
										// projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
									}
									$projectdetails->type =$getvalue['type'];								
									$projectdetails->projectid = $projectid;
									$projectdetails->moretitle = $getvalue['moretitle'];
									$projectdetails->display_order=$getvalue['display_order'];
									$projectdetails->language=$language;
									$projectdetails->save();
								}
							}
						}
						if(!isset($getvalue["projectId"]))
						{
							if(!empty($getvalue['video'])) {
								$datavideo = $getvalue['video']->getClientOriginalName();
								$time = strtotime(now());
								$filename = $time.strtolower(str_replace(" ","_",$datavideo));
								$path_image = public_path('/images/project');
								$getvalue['video']->move($path_image, $filename);
								
								projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
							}
						}
						
					}
					if($getvalue['type'] == 'book'){	
						if(isset($getvalue["projectId"])){	
							if(in_array($getvalue["projectId"], $projectidList)) {	
								$projectdetails = projectdetails::find($getvalue["projectId"]);
								if(!empty($projectdetails)){
									$multi_img=array();
									if(!empty($getvalue['book'])){
										foreach($getvalue['book'] as $file){
											$multi_img_name1=$file->getClientOriginalName();
											$time = strtotime(now());
											$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
											$path_multi_img = public_path('/images/project');
											$file->move($path_multi_img,$multi_img_name);
											$multi_img[]=$multi_img_name;
										}
										$bookImages = json_encode($multi_img);							
										$projectdetails->datavalue =$bookImages;
									}
									$projectdetails->type =$getvalue['type'];								
									$projectdetails->projectid = $projectid;
									$projectdetails->moretitle = $getvalue['moretitle'];
									$projectdetails->display_order=$getvalue['display_order'];
									$projectdetails->language=$language;
									$projectdetails->save();
								}
							}	
						}
						if(!isset($getvalue["projectId"])){		
							$multi_img=array();
							if(!empty($getvalue['book'])){
								foreach($getvalue['book'] as $file){
									$multi_img_name1=$file->getClientOriginalName();
									$time = strtotime(now());
									$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
									$path_multi_img = public_path('/images/project');
									$file->move($path_multi_img,$multi_img_name);
									$multi_img[]=$multi_img_name;
								}
								$bookImages = json_encode($multi_img);							
								projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
							}
						}
						
					}
					if($getvalue['type'] == 'flip'){	
						if(isset($getvalue["projectId"])){					
							if(in_array($getvalue["projectId"], $projectidList)) {	
								$projectdetails = projectdetails::find($getvalue["projectId"]);
								if(!empty($projectdetails)){
									$multi_img=array();
									if(!empty($getvalue['flip'])){
										foreach($getvalue['flip'] as $file){
											$multi_img_name1=$file->getClientOriginalName();
											$time = strtotime(now());
											$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
											$path_multi_img = public_path('/images/project');
											$file->move($path_multi_img,$multi_img_name);
											$multi_img[]=$multi_img_name;										
										}
										$bookImages = json_encode($multi_img);							
										$projectdetails->datavalue =$bookImages;
									}
									$projectdetails->type =$getvalue['type'];								
									$projectdetails->projectid = $projectid;
									$projectdetails->moretitle = $getvalue['moretitle'];
									$projectdetails->display_order=$getvalue['display_order'];
									$projectdetails->language=$language;
									$projectdetails->save();
								}
							}
						}
						if(!isset($getvalue["projectId"])){
							$multi_img=array();
							if(!empty($getvalue['flip'])){
								foreach($getvalue['flip'] as $file){
									$multi_img_name1=$file->getClientOriginalName();
									$time = strtotime(now());
									$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
									$path_multi_img = public_path('/images/project');
									$file->move($path_multi_img,$multi_img_name);
									$multi_img[]=$multi_img_name;
								}
								$bookImages = json_encode($multi_img);							
								projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
							}		
						}				
					}}
				}
				foreach ($projectidList as $key => $value) {
					if(!in_array($value,$request->projectId)){
						projectdetails::where('id','=',$value)->delete();		
					}
				}
			 	$request->session()->flash('alert-success', trans('project details updated successfully.'));
			}
			else {
				$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));			
			}
			return redirect('project');
		}
		public function destroy($id){
			$projectdata = project::where('id','=',$id)->forcedelete();		
			$project1data = projectdetails::where('projectid','=',$id)->forcedelete();		
			Session::flash('alert-success', 'project Deleted Successfully!');		
			return redirect('project');
		}

		public function orderby(Request $request) {			
			$input = $request->all();
			$id=$input['id'];			
			$orderby=$input['orderby'];		
			$project = project::find($id);	
			$project->orderby = $orderby;
			if($project->save()){
				return response()->json(['success' => true, 'message' => "Successfully change orderby"]);
			}	
			else {
				return response()->json(['success' => false, 'message' => "Something went wrong,Please try again later..."]);
			}		
		}*/

	}
