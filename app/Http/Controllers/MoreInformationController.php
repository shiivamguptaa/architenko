<?php	
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\MoreInformationDetails;
use App\Configuration;
use App\HomeSectionOne;
use App\NewsletterText;
use App\Label;
use App\Menu;
use Validator;
use Session;
use File;
use App;	
use DB;
use Mail;
use App\EmailForm;


class MoreInformationController extends Controller {	
	public function __construct() {		
	}		
		
	public function store(Request $request) {
		$locale = App::getLocale();	
		$input = $request->all();	
		$input['language'] = $locale;
		
		$data = array();
		$name = isset($input["first_name"]) ? $input["first_name"] : "";
		$name .= isset($input["middle_name"]) ? $input["middle_name"] : "";
		$data["first_name"] = $name;
		$name .= isset($input["last_name"]) ? $input["last_name"] : "";
		$data["full_name"] = $name;		
		$data["last_name"] = isset($input["last_name"]) ? $input["last_name"] : "";
		$data["city"] = isset($input["city"]) ? $input["city"] : "";
		$data["country"] = isset($input["country"]) ? $input["country"] : "";
		$data["address"] = isset($input["address"]) ? $input["address"] : "";
		$data["phone"] = isset($input["phone"]) ? $input["phone"] : "";
		$data["notes"] = isset($input["description_project"]) ? $input["description_project"] : "";
		$data["email"] = isset($input["email"]) ? $input["email"] : "";
		
		$emailForm = EmailForm::where("formname","moreinformation")->get();

		foreach ($emailForm as $key => $value) {
			$to = $value->email;
			Mail::send('email-templates.form', $data, function($message) use ($to) {
				$message->to($to, 'Architenko');
				$message->subject('More Information Form');
				$message->from('info@architenko.com','Architenko');			
			});
		}	
		if(MoreInformationDetails::create($input)){
			return response()->json(['success' => true, 'message' => "Successfully send message"]);
		}
		else{
			return response()->json(['success' => false, 'message' => "Something went wrong,Please try again later..."]);
		}
	}	
}
