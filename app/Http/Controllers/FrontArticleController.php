<?php
	
	namespace App\Http\Controllers;
	use App;
	use Illuminate\Http\Request;
	use App\Configuration;
	use App\NewsletterText;
	use DB;
	use App\Menu;
	use App\Articles;
	
	
	class FrontArticleController extends Controller
	{
		/**
			* Create a new controller instance.
			*
			* @return void
		*/
		public function __construct()
		{
			//$this->middleware('auth');
		}
		
		/**
			* Show the application dashboard.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		public function index()
		{
			$locale = App::getLocale();
			$title = 'Articles';
			
			$articles = Articles::where('language','=',$locale)->paginate(12);
			
			$config = Configuration::where('language','=',$locale)->first();
			$menus = Menu::where('language','=',$locale)->where('parent',0)->get();
			$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->get();
			$newslettertext = NewsletterText::where('language','=',$locale)->first();
			
			return view('articles')->with(compact('articles','config','title','locale','menus','submenus','newslettertext'));
			
		}
	}
