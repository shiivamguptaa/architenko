<?php
	
	namespace App\Http\Controllers;
	use App;
	use Illuminate\Http\Request;
	use App\Configuration;
	use App\NewsletterText;
	use DB;
	use App\Menu;
	
	
	class FrontProjectController extends Controller
	{
		/**
			* Create a new controller instance.
			*
			* @return void
		*/
		public function __construct()
		{
			//$this->middleware('auth');
		}
		
		/**
			* Show the application dashboard.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		public function index()
		{
			$locale = App::getLocale();
			$title = 'Projects';
			
			$projects = DB::table('project')
            ->join('projectdetails', 'project.id', '=', 'projectdetails.projectid')
            ->select('projectdetails.*', 'project.title')
            ->where('projectdetails.language',$locale)			
			->orderBy('projectdetails.id', 'asc')
            ->paginate(12);
			
			$config = Configuration::where('language','=',$locale)->first();
			$menus = Menu::where('language','=',$locale)->where('parent',0)->get();
			$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->get();
			$newslettertext = NewsletterText::where('language','=',$locale)->first();
			
			return view('projects')->with(compact('projects','config','title','locale','menus','submenus','newslettertext'));
			
		}
	}
