<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use App\Configuration;
use App\HomeSectionOne;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$locale = App::getLocale();
		$title = 'Configuration Listing';	
		$checkconfig = Configuration::where('language','=',$locale)->first();
		
        return view('admin.config.listing')->with(compact('checkconfig','title'));
    }
}
