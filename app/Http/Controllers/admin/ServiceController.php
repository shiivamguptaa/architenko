<?php
	
	namespace App\Http\Controllers\admin;
	use App;	
	use Illuminate\Http\Request;
	use App\Service;
	use App\Servicedetails;
	use Validator;
	use DB;
	use File;
	use Illuminate\Support\Facades\Input;
	use Session;
	
	class ServiceController extends Controller
	{
		//
		public function __construct()
		{
			$this->middleware('auth');
			$this->messages = [
			'required' => 'The :attribute is required.',
			];
		}
		
		/**
			* Show the Service.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			$title = 'Service Data Listing';	
			$locale = App::getLocale();
			$services = array();
			$service = Service::select("id","title","language","orderby",'is_home')->orderBy("orderby","ASC")->get();
			foreach ($service as $key => $value) {
				
				$servicedetails = Servicedetails::select("id","type","datavalue")->where("serviceid",$value->id)->orderBy("display_order","ASC")->first();
				if(!empty($servicedetails)) {
					$serviceList = array();
					$serviceList["serviceId"] = $value->id;
					$serviceList["title"] = $value->title;
					$serviceList["language"] = $value->language;
					$serviceList["orderby"] = $value->orderby;
					$serviceList["is_home"] = $value->is_home;
					$serviceList["servicedetailsId"] = $servicedetails->id;
					$serviceList["datavalue"] = $servicedetails->datavalue;
					$serviceList["type"] = $servicedetails->type;
					$services[] = $serviceList;
				}				
			}
			return view('admin.service.listing')->with(compact('title','services'));
		}
		
		public function create()
		{
			
			$title = 'Service Create';	
			
			return view('admin.service.create')->with(compact('title'));
		}
		
		
		public function store(Request $request)
		{
								
			
			$input = $request->all();			
			$input['title']=$input['title'];			
			$input['subtitle']=$input['subtitle'];			
			$input['descriptionleft']=$input['descriptionleft'];			
			$input['descriptionright']=$input['descriptionright'];			
			$input['language']=$input['lang'];			
			$ishome = $input['ishome'];			
			if($serviceadd = Service::create($input))
			{				
				foreach($request->addmore as $getvalue){							
					if($getvalue['type'] == 'image'){
						if(!empty($getvalue['image'])){
							$dataimage = $getvalue['image']->getClientOriginalName();
							$time = strtotime(now());
							$filename = $time.strtolower(str_replace(" ","_",$dataimage));
							$path_image = public_path('/images/service');
							$getvalue['image']->move($path_image, $filename);
							if($ishome == 'image'){
								$home = '1';
							}
							else{
								$home = '0';
							}						
							$display_order = isset($getvalue['display_order']) ? $getvalue['display_order'] : 0;	
							Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'is_home'=>$home,'serviceid'=>$serviceadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$serviceadd->language]);
						}
						else{
							$dataimage ="";	
						}
					}
					if($getvalue['type'] == 'youtube'){
						
						if($ishome == 'youtube'){
							$home = '1';
							}else{
							$home = '0';
						}
						$display_order = isset($getvalue['display_order']) ? $getvalue['display_order'] : 0;
						Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$getvalue['youtube'],'is_home'=>$home,'serviceid'=>$serviceadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$serviceadd->language]);
						
					}
					if($getvalue['type'] == 'video'){
						
						if(!empty($getvalue['video'])){
							$datavideo = $getvalue['video']->getClientOriginalName();
							$time = strtotime(now());
							$filename = $time.strtolower(str_replace(" ","_",$datavideo));
							$path_image = public_path('/images/service');
							$getvalue['video']->move($path_image, $filename);
							if($ishome == 'video'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							$display_order = isset($getvalue['display_order']) ? $getvalue['display_order'] : 0;
							Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'is_home'=>$home,'serviceid'=>$serviceadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$serviceadd->language]);
						}
						else{
							$datavideo ="";	
						}
					}
					if($getvalue['type'] == 'book'){
						//dd($getvalue);
						$multi_img=array();
						if(!empty($getvalue['book'])){
							foreach($getvalue['book'] as $file){
								$multi_img_name1=$file->getClientOriginalName();
								$time = strtotime(now());
								$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
								$path_multi_img = public_path('/images/service');
								$file->move($path_multi_img,$multi_img_name);
								$multi_img[]=$multi_img_name;
							}
							$bookImages = json_encode($multi_img);
							if($ishome == 'book'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							$display_order = isset($getvalue['display_order']) ? $getvalue['display_order'] : 0;
							Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'is_home'=>$home,'serviceid'=>$serviceadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$serviceadd->language]);
						}
						else{
							$bookImages ="";	
						}
					}
					if($getvalue['type'] == 'flip'){						
						$multi_img=array();
						if(!empty($getvalue['flip'])){
							foreach($getvalue['flip'] as $file){
								$multi_img_name1=$file->getClientOriginalName();
								$time = strtotime(now());
								$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
								$path_multi_img = public_path('/images/service');
								$file->move($path_multi_img,$multi_img_name);
								$multi_img[]=$multi_img_name;
							}
							$bookImages = json_encode($multi_img);
							if($ishome == 'flip'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							$display_order = isset($getvalue['display_order']) ? $getvalue['display_order'] : 0;
							Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'is_home'=>$home,'serviceid'=>$serviceadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$serviceadd->language]);
						}
						else{
							$bookImages ="";	
						}
					}
				}
				
				$request->session()->flash('alert-success', 'Service added successfully.');
			}
			return redirect('service');
			
		}
		
		
		public function edit($id){		
			$title = 'Service Edit';			
			$servicedata = Service::find($id);
			$servicedetails = Servicedetails::where('serviceid','=',$id)->get();
			return view('admin.service.edit')->with(compact('title','servicedata','id','servicedetails'));
		}
		/**
			* Update Home Section One info.
			*
			* @return \Illuminate\Http\Response
		*/
		/*public function update(Request $request){
			
			$validator = Validator::make($request->all(), [           
			
			]);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			
			$servicedata = Service::find($request['id']);
			
			if(empty($servicedata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update service link details.'));
				return redirect('service');
			}			
			
			$servicedata->title = $request['title'];			
			$servicedata->subtitle = $request['subtitle'];			
			$servicedata->descriptionleft = $request['descriptionleft'];			
			$servicedata->descriptionright = $request['descriptionright'];			
			
			if($servicedata->update()){
				
				$request->session()->flash('alert-success', trans('Service details updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update service link details.'));			
			}
			
			return redirect('service');
		}*/

		public function update(Request $request){
			
			// $validator = Validator::make($request->all(), [           
			
			// ]);
			
			// if ($validator->fails())
			// {
			// 	return redirect()->back()->withErrors($validator->errors());
			// }
			
			
			// $projectdata = Project::find($request['id']);
			
			// if(empty($projectdata))
			// {
			// 	$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));
			// 	return redirect('project');
			// }			
			
			// $projectdata->title = $request['title'];			
			// $projectdata->subtitle = $request['subtitle'];			
			// $projectdata->descriptionleft = $request['descriptionleft'];			
			// $projectdata->descriptionright = $request['descriptionright'];			
			
			// if($projectdata->update()){
				
			// 	$request->session()->flash('alert-success', trans('Project details updated successfully.'));
			// }
			// else{
			// 	$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));			
			// }
			$input = $request->all();
			
			$servicedata = Service::find($input['id']);
			if(empty($servicedata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update service link details.'));
				return redirect('service');
			}	
			$servicedata->title = $input['title'];			
			$servicedata->subtitle = $input['subtitle'];			
			$servicedata->descriptionleft = $input['descriptionleft'];
			$servicedata->descriptionright = $input['descriptionright'];		
			$servicedata->language = $input["lang"];		
			$language  = $input["lang"];

			if($servicedata->update()) {
				$serviceid = $input['id'];
				$servicedetails  = Servicedetails::select('id')->where('serviceid','=',$serviceid)->get();
				$serviceidList = array();
				foreach ($servicedetails as $key => $value) {
					$serviceidList[] = $value->id;
				}
				foreach($input["addmore"] as $getvalue){
					$display_order = isset($getvalue['display_order']) ? $getvalue['display_order'] : 0;
					if(isset($getvalue['type'])){		
					if($getvalue['type'] == 'image') {						
						if(isset($getvalue["serviceId"])){									
							if(in_array($getvalue["serviceId"], $serviceidList)) {
							
								$servicedetails = Servicedetails::find($getvalue["serviceId"]);
								if(!empty($servicedetails)){
									if(!empty($getvalue['image'])){
										$dataimage = $getvalue['image']->getClientOriginalName();
										$time = strtotime(now());
										$filename = $time.strtolower(str_replace(" ","_",$dataimage));
										$path_image = public_path('/images/service');
										$getvalue['image']->move($path_image, $filename);
										$servicedetails->datavalue = $filename;
										$servicedetails->type = $getvalue['type'];
									}									
									$servicedetails->serviceid = $serviceid;
									$servicedetails->moretitle = $getvalue['moretitle'];
									$servicedetails->display_order = $display_order;
									$servicedetails->language=$language;
									$servicedetails->save();
								}
							}					
						}
						if(!isset($getvalue["serviceId"]))
						{
							if(!empty($getvalue['image'])){
								$dataimage = $getvalue['image']->getClientOriginalName();
								$time = strtotime(now());
								$filename = $time.strtolower(str_replace(" ","_",$dataimage));
								$path_image = public_path('/images/service');
								$getvalue['image']->move($path_image, $filename);						
								Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$language]);
							}						
						}
					}
					if($getvalue['type'] == 'youtube'){					
						if(isset($getvalue["serviceId"])){
							if(in_array($getvalue["serviceId"], $serviceidList)) {
								$servicedetails = Servicedetails::find($getvalue["serviceId"]);
								if(!empty($servicedetails)){
									$servicedetails->type =$getvalue['type'];
									$servicedetails->datavalue = $getvalue['youtube'];
									$servicedetails->serviceid = $serviceid;
									$servicedetails->moretitle = $getvalue['moretitle'];
									$servicedetails->display_order=$display_order;
									$servicedetails->language=$language;
									$servicedetails->save();
								}		
							}
						}
						if(!isset($getvalue["serviceId"])){
							Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$getvalue['youtube'],'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$language]);
						}
						
					}
					if($getvalue['type'] == 'video'){
						if(isset($getvalue["serviceId"])){
							if(in_array($getvalue["serviceId"], $serviceidList)) {
								$servicedetails = Servicedetails::find($getvalue["serviceId"]);
								if(!empty($servicedetails)){
									if(!empty($getvalue['video'])){
										$datavideo = $getvalue['video']->getClientOriginalName();
										$time = strtotime(now());
										$filename = $time.strtolower(str_replace(" ","_",$datavideo));
										$path_image = public_path('/images/service');
										$getvalue['video']->move($path_image, $filename);
										$servicedetails->datavalue = $filename;
										// servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
										$servicedetails->type =$getvalue['type'];	
									}
																
									$servicedetails->serviceid = $serviceid;
									$servicedetails->moretitle = $getvalue['moretitle'];
									$servicedetails->display_order = $display_order;
									$servicedetails->language=$language;
									$servicedetails->save();
								}
							}
						}
						if(!isset($getvalue["serviceId"]))
						{
							if(!empty($getvalue['video'])) {
								$datavideo = $getvalue['video']->getClientOriginalName();
								$time = strtotime(now());
								$filename = $time.strtolower(str_replace(" ","_",$datavideo));
								$path_image = public_path('/images/service');
								$getvalue['video']->move($path_image, $filename);
								
								Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$language]);
							}
						}
						
					}
					if($getvalue['type'] == 'book'){	
						if(isset($getvalue["serviceId"])){	
							if(in_array($getvalue["serviceId"], $serviceidList)) {	
								$servicedetails = Servicedetails::find($getvalue["serviceId"]);
								if(!empty($servicedetails)){
									$multi_img=array();
									if(!empty($getvalue['book'])){
										foreach($getvalue['book'] as $file){
											$multi_img_name1=$file->getClientOriginalName();
											$time = strtotime(now());
											$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
											$path_multi_img = public_path('/images/service');
											$file->move($path_multi_img,$multi_img_name);
											$multi_img[]=$multi_img_name;
										}
										$bookImages = json_encode($multi_img);							
										$servicedetails->datavalue =$bookImages;
										$servicedetails->type =$getvalue['type'];
									}																	
									$servicedetails->serviceid = $serviceid;
									$servicedetails->moretitle = $getvalue['moretitle'];
									$servicedetails->display_order=$display_order;
									$servicedetails->language=$language;
									$servicedetails->save();
								}
							}	
						}
						if(!isset($getvalue["serviceId"])){		
							$multi_img=array();
							if(!empty($getvalue['book'])){
								foreach($getvalue['book'] as $file){
									$multi_img_name1=$file->getClientOriginalName();
									$time = strtotime(now());
									$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
									$path_multi_img = public_path('/images/service');
									$file->move($path_multi_img,$multi_img_name);
									$multi_img[]=$multi_img_name;
								}
								$bookImages = json_encode($multi_img);							
								Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$language]);
							}
						}
						
					}
					if($getvalue['type'] == 'flip'){	
						if(isset($getvalue["serviceId"])){					
							if(in_array($getvalue["serviceId"], $serviceidList)) {	
								$servicedetails = servicedetails::find($getvalue["serviceId"]);
								if(!empty($servicedetails)){
									$multi_img=array();
									if(!empty($getvalue['flip'])){
										foreach($getvalue['flip'] as $file){
											$multi_img_name1=$file->getClientOriginalName();
											$time = strtotime(now());
											$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
											$path_multi_img = public_path('/images/service');
											$file->move($path_multi_img,$multi_img_name);
											$multi_img[]=$multi_img_name;										
										}
										$bookImages = json_encode($multi_img);							
										$servicedetails->datavalue =$bookImages;
										$servicedetails->type =$getvalue['type'];								
									}
									$servicedetails->serviceid = $serviceid;
									$servicedetails->moretitle = $getvalue['moretitle'];
									$servicedetails->display_order=$display_order;
									$servicedetails->language=$language;
									$servicedetails->save();
								}
							}
						}
						if(!isset($getvalue["serviceId"])){
							$multi_img=array();
							if(!empty($getvalue['flip'])){
								foreach($getvalue['flip'] as $file){
									$multi_img_name1=$file->getClientOriginalName();
									$time = strtotime(now());
									$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
									$path_multi_img = public_path('/images/service');
									$file->move($path_multi_img,$multi_img_name);
									$multi_img[]=$multi_img_name;
								}
								$bookImages = json_encode($multi_img);							
								Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$language]);
							}		
						}				
					}}
				}
				foreach ($serviceidList as $key => $value) {
					if(!in_array($value,$request->serviceId)){
						Servicedetails::where('id','=',$value)->delete();		
					}
				}
			 	$request->session()->flash('alert-success', trans('Service details updated successfully.'));
			}
			else {
				$request->session()->flash('alert-danger', trans('Some problem occured in update service link details.'));			
			}
			return redirect('service');
		}
		public function destroy($id){
			$servicedata = Service::where('id','=',$id)->forcedelete();		
			$service1data = Servicedetails::where('serviceid','=',$id)->forcedelete();		
			Session::flash('alert-success', 'Service Deleted Successfully!');		
			return redirect('service');
		}

		public function orderby(Request $request) {			
			$input = $request->all();
			$id=$input['id'];			
			$orderby=$input['orderby'];		
			$service = Service::find($id);	
			$service->orderby = $orderby;
			if($service->save()){
				return response()->json(['success' => true, 'message' => "Successfully change orderby"]);
			}	
			else {
				return response()->json(['success' => false, 'message' => "Something went wrong,Please try again later..."]);
			}		
		}
		public function showServiceInHome(Request $request){
			$id = isset($request->id) ? $request->id : 0;
			$is_home = isset($request->is_home) ? $request->is_home : 0;
			if(!empty($id)) {
				$service = Service::find($id);
				if(!empty($service)) {
					$service->is_home = $is_home;
					$service->save();
					return response()->json(['responceCode' => 1 ,'message' => 'Successfully update']);
				}
			}
		}
		public function onChangeServiceLanguage(Request $request){
			// $language = isset($request->language) ? $request->language : "en";
			// $service = Service::select("id","title","language","orderby",'is_home')->where("language",$language)->orderBy("orderby","ASC")->get();
			// $data = "";		   
			// $counter = 1;
			// foreach ($service as $key => $value) {				
			// 	$servicedetails = Servicedetails::select("id","type","datavalue")->where("serviceid",$value->id)->orderBy("display_order","ASC")->first();
			// 	$data.= "<tr>";
			// 	$data.= "<td>".$counter++."</td>";
			// 	$data.= "<td>".$value->title."</td>";
			// 	if($servicedetails->type == "image"){
			// 		$data.= "<td><img src='".asset('images/service/'.$servicedetails->datavalue)."' class='width-100'></td>";
			// 	}
			// 	elseif($servicedetails->type == "book" || $servicedetails->type == "flip"){
			// 		$data.= "<td>";
			// 		$booking = json_decode($servicedetails->datavalue);
			// 		foreach($booking as $key => $book) :
			// 			$data.= "<td><img src='".asset('images/project/'.$book)."' class='width-100'></td>";
			// 		endforeach; 
			// 		$data.= "<td>";
			// 	}
			// 	elseif($servicedetails->type == "youtube"){
			// 		$data.= "<td>".$servicedetails->datavalue."</td>";
			// 	}
			// 	else{
			// 		$data.= "<td>".$servicedetails->datavalue."</td>";
			// 	}
			// 	$data.= "<td><input type='text' id='service".$value->id."' class='orderby form-control' value='".$value->orderby."' onfocusout='onChangeOrderBy(".$value->id.")'  /></td>";
			// 	$data.= "<td>".$value->language."</td>";
			// 	$data.= "<td><input type='checkbox' id='checkbox".$value->id."'  class='' ".($value->is_home ? "checked" : "")." onfocusout='showServiceInHome(".$value->id.")'  /></td>";
			// 	$data .="<td class='td-actions text-right'>";				
			// 	$data .="<a href='".URL('service/'.$value->id.'/edit')."' class='btn btn-success'>";
			// 	$data .="<i class='material-icons'>edit</i></a>&nbsp;";
			// 	$data .="<a href='".URL('service/'.$value->id.'/delete')."' class='btn btn-danger'>";
			// 	$data .="<i class='material-icons'>close</i></a>";
			// 	$data .="</td></tr>";
			// }
			$services = array();
			$service = Service::select("id","title","language","orderby",'is_home')->orderBy("orderby","ASC")->where('language',$request->lang)->get();
			foreach ($service as $key => $value) {
				
				$servicedetails = Servicedetails::select("id","type","datavalue")->where("serviceid",$value->id)->orderBy("display_order","ASC")->first();
				if(!empty($servicedetails)) {
					$serviceList = array();
					$serviceList["serviceId"] = $value->id;
					$serviceList["title"] = $value->title;
					$serviceList["language"] = $value->language;
					$serviceList["orderby"] = $value->orderby;
					$serviceList["is_home"] = $value->is_home;
					$serviceList["servicedetailsId"] = $servicedetails->id;
					$serviceList["datavalue"] = $servicedetails->datavalue;
					$serviceList["type"] = $servicedetails->type;
					$services[] = $serviceList;
				}				
			}
			return response()->json(['responseCode' => 1 ,'responseMessage' => 'Successfully retrived','data'=>$services]);
		}
	}
