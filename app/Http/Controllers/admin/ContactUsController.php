<?php	
namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Configuration;
use App\HomeSectionOne;
use App\NewsletterText;
use App\ContactUs;
use App\ContactUsAddress;
use App\Label;
use App\Menu;
use Validator;
use Session;
use File;
use App;
use DB;

class ContactUsController extends Controller {
	
	public function __construct() {	
	}
	
	public function index() {		
		$title = 'Contact Listing';	
		$locale = App::getLocale();
		$contactus = ContactUs::orderBy("orderby","ASC")->get();	
		$config = Configuration::where('language','=',$locale)->first();
		$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
		$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
		$newslettertext = NewsletterText::where('language','=',$locale)->first();
		$labellist = Label::get();
		$labeldata = array();
		$homedata = HomeSectionOne::where('language','=',$locale)->orderBy("id","DESC")->first();
		foreach($labellist as $label){
			$labeldata[$label->label][$label->language] = $label->name;
		}
		return view('admin.contactus.listing')->with(compact('title','contactus','locale','menus','submenus','config','newslettertext','labellist','labeldata','homedata'));
	}

	public function create(){
		$title = 'Contact Us Create';				
		return view('admin.contactus.create')->with(compact('title'));
	}

	public function store(Request $request) {
		$input = $request->all();
		$contactusData = array();
		$contactusData["enquiry_title"]  = isset($request->enquiry_title) ? $request->enquiry_title : ""; 
		$contactusData["enquiry_email"]  = isset($request->enquiry_email) ? $request->enquiry_email : ""; 
		$contactusData["communication_title"]  = isset($request->communication_title) ? $request->communication_title : ""; 
		$contactusData["communication_email"]  = isset($request->communication_email) ? $request->communication_email : ""; 
		$contactusData["invoice_information"]  = isset($request->invoice_information) ? $request->invoice_information : ""; 
		$contactusData["language"]  = isset($request->lang) ? $request->lang : "en"; 
		if($request->hasFile('image')){
			$file = $request->image; 
			$dataimage = $file->getClientOriginalName();
			$time = rand(1,1000000).strtotime(now());
			$filename = $time.strtolower(str_replace(" ","_",$dataimage));
			$path_image = public_path('/images/contactus');
			$file->move($path_image, $filename);
			$contactusData["image"] =$filename;
		}

		$addressList = array();
		if($contactus = ContactUs::create($contactusData)){
			foreach ($request->address as $value) {
				$popup_image = '';
				$direction = '';
				if(isset($value['popup_image'])){
					$file = $value['popup_image']; 
					$dataimage = $file->getClientOriginalName();
					$time = rand(1,1000000).strtotime(now());
					$filename = $time.strtolower(str_replace(" ","_",$dataimage));
					$path_image = public_path('/images/contactus');
					$file->move($path_image, $filename);
					$popup_image = $filename;
				}
				if(isset($value['direction'])){
					$file = $value['direction']; 
					$dataimage = $file->getClientOriginalName();
					$time = rand(1,1000000).strtotime(now());
					$filename = $time.strtolower(str_replace(" ","_",$dataimage));
					$path_image = public_path('/images/contactus');
					$file->move($path_image, $filename);
					$direction = $filename;
				}

				$addressList = array();
				$addressList["contactus_id"] = isset($contactus->id) ? $contactus->id : 0;
				$addressList["title"] = isset($value['title']) ? $value['title'] : "";
				$addressList["address"] = isset($value['address']) ? $value['address'] : "";
				$addressList["phone_number"] = isset($value['phone_number']) ? $value['phone_number'] : "";
				$addressList["description"] = isset($value['description']) ? $value['description'] : "";
				$addressList["direction"] = $direction;
				$addressList["pop_image"] = $popup_image;
				$addressList["display_order"] = isset($value['display_order']) ? $value['display_order'] : 0;
				ContactUsAddress::create($addressList);
			}
			$request->session()->flash('alert-success', 'Contact us added successfully.');
		}	
		return redirect('contact');
	}
	public function edit($id){
		$title = "Edit ContactUs";
		$contactus = ContactUs::where('id',$id)->first();
		$conatactusaddress = ContactUsAddress::where('contactus_id',$id)->get();
		return view('admin.contactus.edit')->with(compact('title','contactus','conatactusaddress'));
	}
	public function update(Request $request){
		
		$input = $request->all();
		$contactusData = array();
		$contactusData["enquiry_title"]  = isset($request->enquiry_title) ? $request->enquiry_title : ""; 
		$contactusData["enquiry_email"]  = isset($request->enquiry_email) ? $request->enquiry_email : ""; 
		$contactusData["communication_title"]  = isset($request->communication_title) ? $request->communication_title : ""; 
		$contactusData["communication_email"]  = isset($request->communication_email) ? $request->communication_email : ""; 
		$contactusData["invoice_information"]  = isset($request->invoice_information) ? $request->invoice_information : ""; 
		$contactusData["language"]  = isset($request->lang) ? $request->lang : "en"; 
		if($request->hasFile('image')){
			$file = $request->image; 
			$dataimage = $file->getClientOriginalName();
			$time = rand(1,1000000).strtotime(now());
			$filename = $time.strtolower(str_replace(" ","_",$dataimage));
			$path_image = public_path('/images/contactus');
			$file->move($path_image, $filename);
			$contactusData["image"] =$filename;
		}

		$addressList = array();
		if(ContactUs::where('id',$request->contact_id)->update($contactusData)){
			$adds = ContactUsAddress::where('contactus_id',$request->contact_id)->get();
			$id = array();
			foreach ($request->address as $value) {
				$addressList = array();
				
				if(!empty($value['popup_image'])){
					$file = $value['popup_image']; 
					$dataimage = $file->getClientOriginalName();
					$time = rand(1,1000000).strtotime(now());
					$filename = $time.strtolower(str_replace(" ","_",$dataimage));
					$path_image = public_path('/images/contactus');
					$file->move($path_image, $filename);
					$addressList["pop_image"] = $filename;
				}
				if(!empty($value['direction'])){
					$file = $value['direction']; 
					$dataimage = $file->getClientOriginalName();
					$time = rand(1,1000000).strtotime(now());
					$filename = $time.strtolower(str_replace(" ","_",$dataimage));
					$path_image = public_path('/images/contactus');
					$file->move($path_image, $filename);
					$addressList["direction"] = $filename;
				}
				$addressList["contactus_id"] = isset($request->contact_id) ? $request->contact_id : 0;
				$addressList["title"] = isset($value['title']) ? $value['title'] : "";
				$addressList["address"] = isset($value['address']) ? $value['address'] : "";
				$addressList["phone_number"] = isset($value['phone_number']) ? $value['phone_number'] : "";
				$addressList["description"] = isset($value['description']) ? $value['description'] : "";
				$addressList["display_order"] = isset($value['display_order']) ? $value['display_order'] : 0;

				if(isset($value['id'])){
					$id[] = $value['id'];
					ContactUsAddress::where('id',$value['id'])->update($addressList);
				}
				else{
					ContactUsAddress::create($addressList);
				}
			}
			if (!empty($id) && !empty($adds)) {
				foreach ($adds as $add) {
					if (!in_array($add->id, $id)) {
						ContactUsAddress::where('id',$add->id)->forcedelete();
					}
				}
			}
		}	
		$request->session()->flash('alert-success', 'Contact us update successfully.');
		return redirect('contact');
	}
	public function delete($id){
		ContactUs::where('id',$id)->forcedelete();
		ContactUsAddress::where('contactus_id',$id)->forcedelete();
		Session::flash('alert-success', 'ContactUs Deleted Successfully!');		
		return redirect('contact');
	}
	public function orderby(Request $request){
		$id = isset($request->id) ? $request->id : 0;
		$orderby = isset($request->orderby) ? $request->orderby : 0;
		if(!empty($id)) {
			$contactus = ContactUs::find($id);
			if(!empty($contactus)) {
				$contactus->orderby = $orderby;
				$contactus->save();
				return response()->json(['responceCode' => 1 ,'message' => 'Successfully update order']);
			}
		}
	}
	public function onchnagelanguage(Request $request){
		$contactus = ContactUs::orderBy("orderby","ASC")->where('language',$request->lang)->get();	
		return response()->json(['responseCode' => 1, 'responseMessage' => 'Contact Successfully', 'data'=> $contactus]);
	}
}