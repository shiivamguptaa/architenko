<?php	
namespace App\Http\Controllers\admin;	
use Illuminate\Http\Request;
use App\EmailForm;
use Validator;
use DB;
use File;
use Illuminate\Support\Facades\Input;
use Session;
	
class EmailFormController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->messages = [
		'required' => 'The :attribute is required.',
		];
	}
	public function index()
	{
		$title = 'Email Form Listing';	
		$emailForm = EmailForm::get();			
		return view('admin.emailform.listing')->with(compact('emailForm','title'));
	}
	
	public function create()
	{
		$title = 'Email Form Create';				
		return view('admin.emailform.create')->with(compact('title'));
	}	
	
	public function store(Request $request){		
		$input = $request->all();
		if(EmailForm::create($input)){
			$request->session()->flash('alert-success', 'Email added successfully.');
		}
		return redirect('emailform');
	}	
	
	public function edit($id){		
		$title = 'Email Form Edit';		
		$emailform = EmailForm::find($id);		
		return view('admin.emailform.edit')->with(compact('title','emailform','id'));
	}
	
	public function update(Request $request) {
		$emailformdata = EmailForm::find($request->id);		
		if(empty($emailformdata)){
			$request->session()->flash('alert-danger', trans('Some problem occured in update email form details.'));
			return redirect('emailform');
		}							
		$emailformdata->formname = $request->formname;			
		$emailformdata->email = $request->email;			
		if($emailformdata->update()){			 
			$request->session()->flash('alert-success', trans('Email details updated successfully.'));
		}
		else{
			$request->session()->flash('alert-danger', trans('Some problem occured in update email form details.'));			
		}		
		return redirect('emailform');
	}

	public function destroy($id){
		$emailformdata = EmailForm::where('id','=',$id)->forcedelete();		
		Session::flash('alert-success', 'Email Deleted Successfully!');		
		return redirect('emailform');
	}	
}
