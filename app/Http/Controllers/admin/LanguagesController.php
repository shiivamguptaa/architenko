<?php
	
	namespace App\Http\Controllers\admin;
	
	use Illuminate\Http\Request;
	use App\Languages;
	use Validator;
	use DB;
	use File;
	use Illuminate\Support\Facades\Input;
	use Session;
	
	class LanguagesController extends Controller
	{
		//
		public function __construct()
		{
			$this->middleware('auth');
			$this->messages = [
			'required' => 'The :attribute is required.',
			];
		}
		
		/**
			* Show the Home Section One.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			$title = 'Language Listing';	
			$languages = Languages::get();
			
			return view('admin.language.listing')->with(compact('languages','title'));
		}
		
		public function create()
		{
			$title = 'Language Create';	
			
			return view('admin.language.create')->with(compact('title'));
		}
		
		
		public function store(Request $request)
		{
			
			$input = $request->all();
			$input['name']=$input['name'];			
			
			if(Languages::create($input))
			{
				$request->session()->flash('alert-success', 'Language added successfully.');
			}
			return redirect('languages');
			
		}
		
		
		public function edit($id){		
			$title = 'Language Edit';
			
			$languagedata = Languages::find($id);		
			return view('admin.language.edit')->with(compact('title','languagedata','id'));
		}
		/**
			* Update Home Section One info.
			*
			* @return \Illuminate\Http\Response
		*/
		public function update(Request $request){
			
			$validator = Validator::make($request->all(), [           
			
			]);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			
			$languagedata = Languages::find($request['id']);
			
			if(empty($languagedata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update language details.'));
				return redirect('languages');
			}			
					
			$languagedata->name = $request['name'];			
			
			if($languagedata->update()){
				 
				$request->session()->flash('alert-success', trans('Language details updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update language details.'));			
			}
			
			return redirect('languages');
		}
		public function destroy($id){
			$languagedata = Languages::where('id','=',$id)->forcedelete();		
			Session::flash('alert-success', 'Language Deleted Successfully!');		
			return redirect('languages');
		}
	}
