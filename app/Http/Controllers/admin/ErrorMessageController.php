<?php
	
namespace App\Http\Controllers\admin;	
use Illuminate\Http\Request;
use App\ErrorMessage;
use Validator;
use DB;
use File;
use Illuminate\Support\Facades\Input;
use Session;

class ErrorMessageController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->messages = [
		'required' => 'The :attribute is required.',
		];
	}
				
	public function index()
	{
		$title = 'Error Message Listing';	
		$errors = ErrorMessage::orderBy("id","DESC")->get();
		return view('admin.errormessage.listing')->with(compact('errors','title'));
	}
	
	
	public function create()
	{
		$title = 'Error Message Create';				
		return view('admin.errormessage.create')->with(compact('title'));
	}
		
	public function store(Request $request)
	{
		$input = $request->all();
		if(ErrorMessage::create($input))
		{
			$request->session()->flash('alert-success', 'Error Message added successfully.');			
		}
		return redirect('errormessage');		
	}
		
	public function edit($id){		
		$title = 'Error Message Edit';		
		$error = ErrorMessage::find($id);		
		return view('admin.errormessage.edit')->with(compact('title','error','id'));
	}
	
	public function update(Request $request){
		$error = ErrorMessage::find($request['id']);
		if(empty($error))
		{
			$request->session()->flash('alert-danger', trans('Some problem occured in update error message details.'));
			return redirect('errormessage');
		}			
		$error->email_invalid_message = isset($request['email_invalid_message']) ? $request['email_invalid_message'] : "" ;			
		$error->email_enter_message = isset($request['email_enter_message']) ? $request['email_enter_message'] : "" ;			
		$error->language = $request['language'];	
		if($error->update()){			 
			$request->session()->flash('alert-success', trans('error message details updated successfully.'));
		}
		else{
			$request->session()->flash('alert-danger', trans('Some problem occured in update error message details.'));			
		}		
		return redirect('errormessage');
	}	

	public function destroy($id){
		$labeldata = ErrorMessage::where('id','=',$id)->forcedelete();		
		Session::flash('alert-success', 'Error message Deleted Successfully!');		
		return redirect('errormessage');
	}
	public function onchangelanguage(Request $request){
		$errors = ErrorMessage::where('language',$request->lang)->orderBy("id","DESC")->get();
		return response()->json(['responseCode' => 1, 'responseMessage' => 'successfully', 'data' => $errors]);
	}
}
