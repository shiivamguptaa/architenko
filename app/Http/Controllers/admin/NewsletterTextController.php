<?php
	
	namespace App\Http\Controllers\admin;
	
	use Illuminate\Http\Request;
	use App\NewsletterText;
	use Validator;
	use DB;
	use File;
	use Illuminate\Support\Facades\Input;
	use Session;
	
	class NewsletterTextController extends Controller
	{
		//
		public function __construct()
		{
			$this->middleware('auth');
			$this->messages = [
			'required' => 'The :attribute is required.',
			];
		}
		
		/**
			* Show the Newsletter text.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			$title = 'Newsletter Text Listing';	
			$newslettertext = NewsletterText::get();
			
			return view('admin.newsletter.textlisting')->with(compact('newslettertext','title'));
		}
		
		public function create()
		{
			$title = 'Newsletter Text Create';	
			
			return view('admin.newsletter.create')->with(compact('title'));
		}
		
		
		public function store(Request $request)
		{
			$validator = Validator::make($request->all(), [           
			'description' => 'required',
			],$this->messages);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			$input = $request->all();
			$input['description']=$input['description'];			
			$input['language']=$input['lang'];			
			
			if(NewsletterText::create($input))
			{
				$request->session()->flash('success', 'Newsletter text added successfully.');
			}
			return redirect('newslettertext');
			
		}
		
		
		public function edit($id){		
			$title = 'Newsletter text Edit';
			
			$newslettertext = NewsletterText::find($id);		
			return view('admin.newsletter.edit')->with(compact('title','newslettertext','id'));
		}
		/**
			* Update newsletter text info.
			*
			* @return \Illuminate\Http\Response
		*/
		public function update(Request $request){
			
			$validator = Validator::make($request->all(), [           
			'description' => 'required',
			],$this->messages);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			
			$newslettertext = NewsletterText::find($request['id']);
			
			if(empty($newslettertext))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update newsletter text details.'));
				return redirect('newslettertext');
			}			
					
			$newslettertext->description = $request['description'];			
			$newslettertext->language = $request['lang'];			
			
			if($newslettertext->update()){
				 
				$request->session()->flash('alert-success', trans('Newsletter text updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update newsletter text details.'));			
			}
			
			return redirect('newslettertext');
		}
		public function destroy($id){
			$newslettertext = NewsletterText::where('id','=',$id)->forcedelete();		
			Session::flash('alert-success', 'Newsletter text Deleted Successfully!');		
			return redirect('newslettertext');
		}
	}
