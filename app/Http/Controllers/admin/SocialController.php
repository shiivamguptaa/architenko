<?php
	
	namespace App\Http\Controllers\admin;
	
	use Illuminate\Http\Request;
	use App\Social;
	use Validator;
	use DB;
	use File;
	use Illuminate\Support\Facades\Input;
	use Session;
	
	class SocialController extends Controller
	{
		//
		public function __construct()
		{
			$this->middleware('auth');
			$this->messages = [
			'required' => 'The :attribute is required.',
			];
		}
		
		/**
			* Show the Home Section One.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			$title = 'Social Links Listing';	
			$social = Social::get();
			
			return view('admin.social.listing')->with(compact('social','title'));
		}
		
		public function create()
		{
			$title = 'Social Link Create';	
			
			return view('admin.social.create')->with(compact('title'));
		}
		
		
		public function store(Request $request)
		{
			
			$input = $request->all();
			$input['name']=$input['name'];			
			$input['link']=$input['link'];			
			
			if(Social::create($input))
			{
				$request->session()->flash('alert-success', 'Social link added successfully.');
			}
			return redirect('social');
			
		}
		
		
		public function edit($id){		
			$title = 'Social Link Edit';
			
			$socialdata = Social::find($id);		
			return view('admin.social.edit')->with(compact('title','socialdata','id'));
		}
		/**
			* Update Home Section One info.
			*
			* @return \Illuminate\Http\Response
		*/
		public function update(Request $request){
			
			$validator = Validator::make($request->all(), [           
			
			]);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			
			$socialdata = Social::find($request['id']);
			
			if(empty($socialdata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update social link details.'));
				return redirect('social');
			}			
					
			$socialdata->name = $request['name'];			
			$socialdata->link = $request['link'];			
			
			if($socialdata->update()){
				 
				$request->session()->flash('alert-success', trans('Social details updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update social link details.'));			
			}
			
			return redirect('social');
		}
		public function destroy($id){
			$socialdata = Social::where('id','=',$id)->forcedelete();		
			Session::flash('alert-success', 'Social Deleted Successfully!');		
			return redirect('social');
		}
	}
