<?php
	namespace App\Http\Controllers\admin;
	
	use Illuminate\Http\Request;
	use App\Project;
	use App\Projectdetails;
	use App\ProjectInnerContent;
	use App\ProjectCatagory;
	use Validator;
	use DB;
	use File;
	use Illuminate\Support\Facades\Input;
	use Session;
	
	class ProjectController extends Controller
	{
		//
		public function __construct()
		{
			$this->middleware('auth');
			$this->messages = [
			'required' => 'The :attribute is required.',
			];
		}
		
		/**
			* Show the Home Section One.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			$title = 'Project Data Listing';	
			//$project = Project::get();
			// $projects = DB::table('project')
   //          ->join('projectdetails', 'project.id', '=', 'projectdetails.projectid')
   //          ->select('projectdetails.*', 'project.id as projectId','project.title','project.orderby')            
   //          ->get();

			$projects = array();
			$project = Project::select("project.id","catagory","title","project.language","project.orderby",'project.is_home')
					   ->leftJoin('project_catagory','project_catagory.id',"=",'project.catagory_id')->orderBy("orderby","ASC")->get();
			foreach ($project as $key => $value) {
				$projectList = array();
				$projectList["projectId"] = $value->id;
				$projectList["catagory"] = $value->catagory;
				$projectList["title"] = $value->title;
				$projectList["language"] = $value->language;
				$projectList["orderby"] = $value->orderby;
				$projectList["is_home"] = $value->is_home;
				$projectdetails = Projectdetails::select("id","moretitle","type","datavalue")->where("projectid",$value->id)->orderBy("display_order","ASC")->first();

				$projectList["projectdetailsId"] = $projectdetails['id'];
				$projectList["moretitle"] = $projectdetails['moretitle'];
				$projectList["datavalue"] = $projectdetails['datavalue'];
				$projectList["type"] = $projectdetails['type'];
				$projects[] =$projectList;
			}
			return view('admin.project.listing')->with(compact('title','projects'));
		}
		
		public function create()
		{
			
			$title = 'Project Create';	
			
			return view('admin.project.create')->with(compact('title'));
		}
		
		
		public function store(Request $request)
		{
			$input = $request->all();			
			$data = array();
			$filename = "";
			if($request->hasFile("pdf")){
				$file = $request->file('pdf');
				$dataimage = $file->getClientOriginalName();
				$time = strtotime(now()).rand(100000,100000000);
				$filename = $time.strtolower(str_replace(" ","_",$dataimage));
				$path_image = public_path('/images/project/pdf/');
				$file->move($path_image, $filename);						
			}
			$data['title']=$input['title'];			
			$data['pdf']=$filename;
			$data['subtitle']=$input['subtitle'];			
			$data['descriptionleft']=$input['descriptionleft'];			
			$data['descriptionright']=$input['descriptionright'];			
			$data['language']=$input['lang'];		
			$data['catagory_id']=$input['catagory_id'];		
			$data['innerpagetitle']=$input['innerpagetitle'];		
			$data['download_text']=$input['downloadText'];		
			$orderby =Project::where("language",$input['lang'])->count();
			$data['orderby'] = $orderby + 1; 		
			if($projectadd = Project::create($data))
			{				
				foreach($request->addmore as $getvalue){		
				$display_order = isset($getvalue['display_order']) ? $getvalue['display_order'] : 0;					
					if($getvalue['type'] == 'image'){
						if(!empty($getvalue['image'])){
							$dataimage = $getvalue['image']->getClientOriginalName();
							$time = strtotime(now());
							$filename = $time.strtolower(str_replace(" ","_",$dataimage));
							$path_image = public_path('/images/project');
							$getvalue['image']->move($path_image, $filename);			

							Projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'projectid'=>$projectadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$projectadd->language]);
						}
						else{
							$dataimage ="";	
						}
					}
					if($getvalue['type'] == 'youtube'){		
						Projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$getvalue['youtube'],'projectid'=>$projectadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$projectadd->language]);
						
					}
					if($getvalue['type'] == 'video'){
						
						if(!empty($getvalue['video'])){
							$datavideo = $getvalue['video']->getClientOriginalName();
							$time = strtotime(now());
							$filename = $time.strtolower(str_replace(" ","_",$datavideo));
							$path_image = public_path('/images/project');
							$getvalue['video']->move($path_image, $filename);
							Projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'projectid'=>$projectadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$projectadd->language]);
						}
						else{
							$datavideo ="";	
						}
					}
					if($getvalue['type'] == 'book'){					
						$multi_img=array();
						if(!empty($getvalue['book'])){
							foreach($getvalue['book'] as $file){
								$multi_img_name1=$file->getClientOriginalName();
								$time = strtotime(now());
								$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
								$path_multi_img = public_path('/images/project');
								$file->move($path_multi_img,$multi_img_name);
								$multi_img[]=$multi_img_name;
							}
							$bookImages = json_encode($multi_img);							
							Projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'projectid'=>$projectadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$projectadd->language]);
						}
						else{
							$bookImages ="";	
						}
					}
					if($getvalue['type'] == 'flip'){						
						$multi_img=array();
						if(!empty($getvalue['flip'])){
							foreach($getvalue['flip'] as $file){
								$multi_img_name1=$file->getClientOriginalName();
								$time = strtotime(now());
								$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
								$path_multi_img = public_path('/images/project');
								$file->move($path_multi_img,$multi_img_name);
								$multi_img[]=$multi_img_name;
							}
							$bookImages = json_encode($multi_img);							
							Projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'projectid'=>$projectadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$projectadd->language]);
						}
						else{
							$bookImages ="";	
						}
					}
				}
				
				if (!empty($request->innerPage)) {
					foreach($request->innerPage as $value){	
						if(!empty($value['image'])){
							$dataimage = $value['image']->getClientOriginalName();
							$time = strtotime(now());
							$filename = $time.strtolower(str_replace(" ","_",$dataimage));
							$path_image = public_path('/images/project');
							$value['image']->move($path_image, $filename);		
							$display_order = isset($value['display_order']) ? $value['display_order'] : 0;							
							ProjectInnerContent::create(['description'=>$value['description'],'image'=>$filename,'projectid'=>$projectadd->id,'title'=>$value['title'],'display_order'=>$display_order,'language'=>$projectadd->language]);
						}
					}
				}
				$request->session()->flash('alert-success', 'Project added successfully.');
			}
			return redirect('project');
			
		}
		
		
		public function edit($id){		
			$title = 'Project Edit';			
			$projectdata = Project::find($id);
			$catagoryList = array();
			if(!empty($projectdata)){
				$catagoryList = ProjectCatagory::where("language",$projectdata->language)->where("status",1)->get();
			}
			$projectdetails = Projectdetails::where('projectid','=',$id)->get();
			$projectinnercontents = ProjectInnerContent::where('projectid','=',$id)->get();
			return view('admin.project.edit')->with(compact('title','projectdata','id','projectdetails','projectinnercontents','catagoryList'));
		}
		/**
			* Update Home Section One info.
			*
			* @return \Illuminate\Http\Response
		*/
		public function update(Request $request){
			// $validator = Validator::make($request->all(), [           
			
			// ]);
			
			// if ($validator->fails())
			// {
			// 	return redirect()->back()->withErrors($validator->errors());
			// }
			
			
			// $projectdata = Project::find($request['id']);
			
			// if(empty($projectdata))
			// {
			// 	$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));
			// 	return redirect('project');
			// }			
			
			// $projectdata->title = $request['title'];			
			// $projectdata->subtitle = $request['subtitle'];			
			// $projectdata->descriptionleft = $request['descriptionleft'];			
			// $projectdata->descriptionright = $request['descriptionright'];			
			
			// if($projectdata->update()){
				
			// 	$request->session()->flash('alert-success', trans('Project details updated successfully.'));
			// }
			// else{
			// 	$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));			
			// }
			$input = $request->all();					
			$projectdata = Project::find($input['id']);
			if(empty($projectdata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));
				return redirect('project');
			}	
			if($request->hasFile("pdf")){
				$file = $request->file('pdf');
				$dataimage = $file->getClientOriginalName();
				$time = strtotime(now()).rand(100000,100000000);
				$filename = $time.strtolower(str_replace(" ","_",$dataimage));
				$path_image = public_path('/images/project/pdf/');
				$file->move($path_image, $filename);	
				$projectdata->pdf = $filename;		
			}
			$projectdata->title = $input['title'];			
			$projectdata->subtitle = $input['subtitle'];			
			$projectdata->descriptionleft = $input['descriptionleft'];
			$projectdata->descriptionright = $input['descriptionright'];		
			$projectdata->language = $input["lang"];		
			$projectdata->innerpagetitle=$input['innerpagetitle'];		
			$projectdata->catagory_id=$input['catagory_id'];		
			$projectdata->download_text=$input['downloadText'];		
			$language = $input["lang"];
			if($projectdata->update()) {
				$projectid = $input['id'];
				$projectdetails  = Projectdetails::select('id')->where('projectid','=',$projectid)->get();
				$projectidList = array();
				foreach ($projectdetails as $key => $value) {
					$projectidList[] = $value->id;
				}
				foreach($input["addmore"] as $getvalue){
					$display_order = isset($getvalue['display_order']) ? $getvalue['display_order'] : 0;	
					if(isset($getvalue['type'])){		
					if($getvalue['type'] == 'image') {						
						if(isset($getvalue["projectId"])){									
							if(in_array($getvalue["projectId"], $projectidList)) {
							
								$projectdetails = Projectdetails::find($getvalue["projectId"]);
								if(!empty($projectdetails)){
									if(!empty($getvalue['image'])){
										$dataimage = $getvalue['image']->getClientOriginalName();
										$time = strtotime(now());
										$filename = $time.strtolower(str_replace(" ","_",$dataimage));
										$path_image = public_path('/images/project');
										$getvalue['image']->move($path_image, $filename);
										$projectdetails->datavalue =$filename;
										$projectdetails->type =$getvalue['type'];
									}	
									$projectdetails->projectid =$projectid;
									$projectdetails->moretitle =$getvalue['moretitle'];
									$projectdetails->display_order=$getvalue['display_order'];
									$projectdetails->language=$language;
									$projectdetails->save();
								}
							}					
						}
						if(!isset($getvalue["projectId"]))
						{
							if(!empty($getvalue['image'])){
								$dataimage = $getvalue['image']->getClientOriginalName();
								$time = strtotime(now());
								$filename = $time.strtolower(str_replace(" ","_",$dataimage));
								$path_image = public_path('/images/project');
								$getvalue['image']->move($path_image, $filename);						
								Projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$language]);
							}						
						}
					}
					if($getvalue['type'] == 'youtube'){					
						if(isset($getvalue["projectId"])){
							if(in_array($getvalue["projectId"], $projectidList)) {
								$projectdetails = Projectdetails::find($getvalue["projectId"]);
								if(!empty($projectdetails)){
									$projectdetails->type =$getvalue['type'];
									$projectdetails->datavalue = $getvalue['youtube'];
									$projectdetails->projectid = $projectid;
									$projectdetails->moretitle = $getvalue['moretitle'];
									$projectdetails->display_order=$display_order;
									$projectdetails->language=$language;
									$projectdetails->save();
								}		
							}
						}
						if(!isset($getvalue["projectId"])){
							Projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$getvalue['youtube'],'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$language]);
						}
						
					}
					if($getvalue['type'] == 'video'){
						if(isset($getvalue["projectId"])){
							if(in_array($getvalue["projectId"], $projectidList)) {
								$projectdetails = Projectdetails::find($getvalue["projectId"]);
								if(!empty($projectdetails)){
									if(!empty($getvalue['video'])){
										$datavideo = $getvalue['video']->getClientOriginalName();
										$time = strtotime(now());
										$filename = $time.strtolower(str_replace(" ","_",$datavideo));
										$path_image = public_path('/images/project');
										$getvalue['video']->move($path_image, $filename);
										$projectdetails->datavalue = $filename;										
										$projectdetails->type =$getvalue['type'];								
									}
									$projectdetails->projectid = $projectid;
									$projectdetails->moretitle = $getvalue['moretitle'];
									$projectdetails->display_order=$display_order;
									$projectdetails->language=$language;
									$projectdetails->save();
								}
							}
						}
						if(!isset($getvalue["projectId"]))
						{
							if(!empty($getvalue['video'])) {
								$datavideo = $getvalue['video']->getClientOriginalName();
								$time = strtotime(now());
								$filename = $time.strtolower(str_replace(" ","_",$datavideo));
								$path_image = public_path('/images/project');
								$getvalue['video']->move($path_image, $filename);
								
								Projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$language]);
							}
						}
						
					}
					if($getvalue['type'] == 'book'){	
						if(isset($getvalue["projectId"])){	
							if(in_array($getvalue["projectId"], $projectidList)) {	
								$projectdetails = Projectdetails::find($getvalue["projectId"]);
								if(!empty($projectdetails)){
									$multi_img=array();
									if(!empty($getvalue['book'])){
										foreach($getvalue['book'] as $file){
											$multi_img_name1=$file->getClientOriginalName();
											$time = strtotime(now());
											$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
											$path_multi_img = public_path('/images/project');
											$file->move($path_multi_img,$multi_img_name);
											$multi_img[]=$multi_img_name;
										}
										$bookImages = json_encode($multi_img);							
										$projectdetails->datavalue =$bookImages;
										$projectdetails->type =$getvalue['type'];	
									}
								//	$projectdetails->type =$getvalue['type'];								
									$projectdetails->projectid = $projectid;
									$projectdetails->moretitle = $getvalue['moretitle'];
									$projectdetails->display_order=$display_order;
									$projectdetails->language=$language;
									$projectdetails->save();
								}
							}	
						}
						if(!isset($getvalue["projectId"])){		
							$multi_img=array();
							if(!empty($getvalue['book'])){
								foreach($getvalue['book'] as $file){
									$multi_img_name1=$file->getClientOriginalName();
									$time = strtotime(now());
									$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
									$path_multi_img = public_path('/images/project');
									$file->move($path_multi_img,$multi_img_name);
									$multi_img[]=$multi_img_name;									
								}
								$bookImages = json_encode($multi_img);							
								Projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$language]);
							}
						}
						
					}
					if($getvalue['type'] == 'flip'){	
						if(isset($getvalue["projectId"])){					
							if(in_array($getvalue["projectId"], $projectidList)) {	
								$projectdetails = Projectdetails::find($getvalue["projectId"]);
								if(!empty($projectdetails)){
									$multi_img=array();
									if(!empty($getvalue['flip'])){
										foreach($getvalue['flip'] as $file){
											$multi_img_name1=$file->getClientOriginalName();
											$time = strtotime(now());
											$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
											$path_multi_img = public_path('/images/project');
											$file->move($path_multi_img,$multi_img_name);
											$multi_img[]=$multi_img_name;										
										}
										$bookImages = json_encode($multi_img);							
										$projectdetails->datavalue=$bookImages;
										$projectdetails->type =$getvalue['type'];
									}																	
									$projectdetails->projectid = $projectid;
									$projectdetails->moretitle = $getvalue['moretitle'];
									$projectdetails->display_order=$display_order;
									$projectdetails->language=$language;
									$projectdetails->save();
								}
							}
						}
						if(!isset($getvalue["projectId"])){
							$multi_img=array();
							if(!empty($getvalue['flip'])){
								foreach($getvalue['flip'] as $file){
									$multi_img_name1=$file->getClientOriginalName();
									$time = strtotime(now());
									$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
									$path_multi_img = public_path('/images/project');
									$file->move($path_multi_img,$multi_img_name);
									$multi_img[]=$multi_img_name;
								}
								$bookImages = json_encode($multi_img);							
								Projectdetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'projectid'=>$projectid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$display_order,'language'=>$language]);
							}		
						}				
					}}
				}
				foreach ($projectidList as $key => $value) {
					if(!in_array($value,$request->projectId)){
						Projectdetails::where('id','=',$value)->delete();		
					}
				}

				$projectInnerContentDetails  = ProjectInnerContent::select('id')->where('projectid','=',$projectid)->get();
				$projectInnerContentList = array();
				foreach ($projectInnerContentDetails as $key => $value) {
					$projectInnerContentList[] = $value->id;
				}
				
				if (!empty($request->innerPage)) {
					foreach($input["innerPage"] as $value){					
						$display_order = isset($value['display_order']) ? $value['display_order'] : 0;	
						if(isset($value["projectInnerContentid"])){									
							if(in_array($value["projectInnerContentid"], $projectInnerContentList)) {							
								$projectInnerContentDetails = ProjectInnerContent::find($value["projectInnerContentid"]);

								if(!empty($projectInnerContentDetails)){
									if(!empty($value['image'])){
										$dataimage = $value['image']->getClientOriginalName();
										$time = strtotime(now());
										$filename = $time.strtolower(str_replace(" ","_",$dataimage));
										$path_image = public_path('/images/project');
										$value['image']->move($path_image, $filename);
										$projectInnerContentDetails->datavalue =$filename;
									}	
									$projectInnerContentDetails->title =$value['title'];									
									$projectInnerContentDetails->description =$value['description'];
									$projectInnerContentDetails->display_order=$display_order;
									$projectInnerContentDetails->language=$language;
									$projectInnerContentDetails->save();
								}
							}					
						}
						if(!isset($value["projectInnerContentid"])){
							if(!empty($value['image'])){
								$dataimage = $value['image']->getClientOriginalName();
								$time = strtotime(now());
								$filename = $time.strtolower(str_replace(" ","_",$dataimage));
								$path_image = public_path('/images/project');
								$value['image']->move($path_image, $filename);						
								ProjectInnerContent::create(['description'=>$value['description'],'image'=>$filename,'projectid'=>$projectid,'title'=>$value['title'],'display_order'=>$display_order,'language'=>$language]);
							}	
						}					
					}
					foreach ($projectInnerContentList as $key => $value) {
						if(!in_array($value,$input['projectInnerContentid'])){
							ProjectInnerContent::where('id','=',$value)->delete();		
						}
					}
				}
			 	$request->session()->flash('alert-success', trans('Project details updated successfully.'));
			}
			else {
				$request->session()->flash('alert-danger', trans('Some problem occured in update project link details.'));			
			}
			return redirect('project');
		}
		public function destroy($id){
			$projectdata = Project::where('id','=',$id)->delete();		
			$projectdata = Projectdetails::where('projectid','=',$id)->delete();		
			Session::flash('alert-success', 'Project Deleted Successfully!');		
			return redirect('project');
		}
		public function orderby(Request $request) {			
			$input = $request->all();
			$id=$input['id'];			
			$orderby=$input['orderby'];		
			$menudata = Project::find($id);	
			$menudata->orderby = $orderby;
			if($menudata->save()){
				return response()->json(['success' => true, 'message' => "Successfully change orderby"]);
			}	
			else {
				return response()->json(['success' => false, 'message' => "Something went wrong,Please try again later..."]);
			}		
		}
		public function Catagory(){
			// echo 'test';
			// exit;
			$caragory = ProjectCatagory::where('status',1)->orderBy('orderby',"ASC")->get();
			return view('admin.projectcatagory.listing')->with(compact('caragory'));
		}
		public function CatagoryCreate(){
			return view('admin.projectcatagory.create');
		}
		public function CatagoryStore(Request $request){
			$validator = Validator::make($request->all(), [           
				'catagory' => 'required'
			]);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			$data = array(
				'catagory' 	=> isset($request->catagory) ? $request->catagory : '',
				'language' 	=> isset($request->lang) ? $request->lang : '',
				'status'	=> 1
			);
			ProjectCatagory::create($data);
			Session::flash('alert-success', 'Catagory Added Successfully!');
			return redirect('projectcatagory');
		}
		public function CatagoryEdit($id){
			$catagory = ProjectCatagory::find($id);
			return view('admin.projectcatagory.edit')->with(compact('catagory'));
		}
		public function CatagoryUpdate(Request $request){
			$validator = Validator::make($request->all(), [           
				'catagory' => 'required'
			]);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			ProjectCatagory::where('id',$request->id)->update(['catagory' => $request->catagory, 'language' => $request->lang]);
			Session::flash('alert-success', 'Catagory Updated Successfully!');
			return redirect('projectcatagory');
		}
		public function CatagoryDelete($id){
			$pcatagory = ProjectCatagory::findOrfail($id);
			$pcatagory->status = 0;
			$pcatagory->touch();
			$pcatagory->save();
			Session::flash('alert-success', 'Catagory Deleted Successfully!');
			return redirect('projectcatagory');
		}
		public function CatagoryByLang(Request $request){
			$catagory = ProjectCatagory::where('language',$request->lang)->where('status',1)->get();
			$data = '<option value="" selected hidden disabled>Select Catagory...</option>';
			foreach ($catagory as $value) {
				$data .= '<option value="'.$value->id.'">'.$value->catagory.'</option>';
			}
			return response()->json(['responceCode' => 1 ,'data' => $data]);
		}
		public function projectcategoryorderby(Request $request){
			$id = isset($request->id) ? $request->id : 0;
			$orderby = isset($request->orderby) ? $request->orderby : 0;
			if(!empty($id)) {
				$projectcatagory = ProjectCatagory::find($id);
				if(!empty($projectcatagory)) {
					$projectcatagory->orderby = $orderby;
					$projectcatagory->save();
					return response()->json(['responceCode' => 1 ,'message' => 'Successfully update order']);
				}
			}
		}
		public function showProjectInHome(Request $request){
			$id = isset($request->id) ? $request->id : 0;
			$is_home = isset($request->is_home) ? $request->is_home : 0;
			if(!empty($id)) {
				$project = Project::find($id);
				if(!empty($project)) {
					$project->is_home = $is_home;
					$project->save();
					return response()->json(['responceCode' => 1 ,'message' => 'Successfully update']);
				}
			}
		}
		public function onChangeProjectLanguage(Request $request){
			$projects = array();
			$project = Project::select("project.id","catagory","title","project.language","project.orderby",'project.is_home')->where('project.language',$request->lang)
					   ->leftJoin('project_catagory','project_catagory.id',"=",'project.catagory_id')->orderBy("orderby","ASC")->get();
			foreach ($project as $key => $value) {
				$projectList = array();
				$projectList["projectId"] = $value->id;
				$projectList["catagory"] = $value->catagory;
				$projectList["title"] = $value->title;
				$projectList["language"] = $value->language;
				$projectList["orderby"] = $value->orderby;
				$projectList["is_home"] = $value->is_home;
				$projectdetails = Projectdetails::select("id","moretitle","type","datavalue")->where("projectid",$value->id)->orderBy("display_order","ASC")->first();

				$projectList["projectdetailsId"] = $projectdetails['id'];
				$projectList["moretitle"] = $projectdetails['moretitle'];
				$projectList["datavalue"] = $projectdetails['datavalue'];
				$projectList["type"] = $projectdetails['type'];
				$projects[] =$projectList;
			}
			return response()->json(['success' => true ,'message' => 'Successfully get project list','data'=>$projects]);
		}
		public function pojectcategorybylang(Request $request){
			$caragory = ProjectCatagory::where('status',1)->where('language',$request->lang)->orderBy('orderby',"ASC")->get();
			return response()->json(['success' => true ,'message' => 'Successfully get catagory list','data'=>$caragory]);
		}
	}
