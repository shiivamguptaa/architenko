<?php	
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\StudioApplyForm;
use App\Configuration;
use App\HomeSectionOne;
use App\NewsletterText;
use App\StudioApplyDetails;
use App\Country;
use App\Menu;
use Validator;
use Session;
use File;
use App;	
use DB;


class StudioApplyFormController extends Controller 
{	
	public function __construct() 
	{		
	}		
		
	public function index()
	{
		$title = 'Studio Apply Form Data Listing';	
		$studioapplyform = StudioApplyForm::get();		
		return view('admin.studioapplyform.listing')->with(compact('studioapplyform','title'));
	}

	public function create() 
	{
		$title = 'Studio Apply Form Create';	
		return view('admin.studioapplyform.create')->with(compact('title'));
	}	

	public function store(Request $request)
	{
		
		$input = $request->all();
		if(StudioApplyForm::create($input))
		{
			$request->session()->flash('alert-success', 'Studio Apply Form added successfully.');
			
		}
		return redirect('studioapplyform');		
	}	
	public function edit($id){		
		$title = 'Studio Apply Form Edit';
		$studioapplyform = StudioApplyForm::find($id);		
		return view('admin.studioapplyform.edit')->with(compact('title','studioapplyform','id'));
	}	
	public function update(Request $request) {
		
		$studioApplyForm = StudioApplyForm::find($request['id']);
		if(empty($studioApplyForm)) {
			$request->session()->flash('alert-danger', trans('Some problem occured in update studio apply form details.'));
			return redirect('studioapplyform');
		}		
		$input = $request->all();	
		if(StudioApplyForm::findOrFail($request['id'])->update($input)) {				 
			$request->session()->flash('alert-success', trans('Studio Apply Form details updated successfully.'));
		}
		else{
			$request->session()->flash('alert-danger', trans('Some problem occured in update studio apply form details.'));		
		}			
		return redirect('studioapplyform');
	}
	

	public function destroy($id) {
		StudioApplyForm::where('id','=',$id)->forcedelete();		
		Session::flash('alert-success', 'Studio Apply Form Deleted Successfully!');		
		return redirect('studioapplyform');
	}

	public function studioApplyFormDetailsList() {
		$title = "Studio Apply Form Details Listing";
		$studioApplyDetails = StudioApplyDetails::orderBy("id","DESC")->get();		
		return view('admin.studioapplydetails.listing')->with(compact('title','studioApplyDetails'));
	}
	public function studioApplyFormDetailById($id) {
		$title = "Studio Apply Form Detail";
		$studioApplyDetail = StudioApplyDetails::select("studio_apply_details.*","country1.name as country1","country2.name as country2")->leftJoin("country as country1","country1.id","studio_apply_details.country1")->leftJoin("country as country2","country2.id","studio_apply_details.country2")->orderBy("studio_apply_details.id","DESC")->first();	
		return view('admin.studioapplydetails.view')->with(compact('title','studioApplyDetail'));
	}
	public function onchnagelanguage(Request $request){
		$studioApplyDetails = StudioApplyDetails::where('language',$request->lang)->orderBy("id","DESC")->get();
		return response()->json(['responseCode' => 1, 'responseMessage' => 'success', 'data' => $studioApplyDetails]);
	}
	public function delete($id){
		$studioapplydetails = StudioApplyDetails::find($id);
		$studioapplydetails->delete();
	}

}
