<?php

namespace App\Http\Controllers\admin;	
use Illuminate\Http\Request;
use App\Project;
use App\Projectdetails;
use App\ProjectInnerContent;
use App\Studio;
use App\StudioDeails;
use App\StudioPosition;
use App\StudioTnC;
use App\StudioTncDetails;
use Validator;
use DB;
use File;
use Illuminate\Support\Facades\Input;
use Session;

class StudioController extends Controller
{
	
	public function __construct()
	{
		$this->middleware('auth');
		$this->messages = [
		'required' => 'The :attribute is required.',
		];
	}
	

	public function index()
	{
		$title = 'Studio Data Listing';		
		$studio = Studio::get();
		return view('admin.studio.listing')->with(compact('title','studio'));
	}
	
	public function create()
	{
		
		$title = 'Studio Create';				
		return view('admin.studio.create')->with(compact('title'));
	}
	
	
	public function store(Request $request)
	{
		// print_r($request->all());
		// exit();
		$input = $request->all();
		$data = array();
		$data['title_img_type'] 				= isset($input['main_image_type']) ? $input['main_image_type'] : '';
		$data['title_img'] 						= $this->get_Image($input['main_image_type'],$input['img_left']);
		$data['profile_title'] 					= isset($input['profiletitle']) ? $input['profiletitle'] : '';
		$data['profile_description'] 			= isset($input['profile_section_description']) ? $input['profile_section_description'] : '';
		$data['profile_right_img_type'] 		= isset($input['profile_image_type']) ? $input['profile_image_type'] : '';
		$data['profile_right_img'] 				= $this->get_Image($input['profile_image_type'],$input['profile_img_left']);
		$data['founder_title'] 					= isset($input['foundertitle']) ? $input['foundertitle'] : '';
		$data['founder_right_img_type'] 		= isset($input['founder_image_type']) ? $input['founder_image_type'] : '';
		$data['founder_right_img'] 				= $this->get_Image($input['founder_image_type'],$input['founder_img_left']);
		$data['founder_description'] 			= isset($input['founder_section_description']) ? $input['founder_section_description'] : '';
		$data['join_architenko_title'] 			= isset($input['joinarchitenkotitle']) ? $input['joinarchitenkotitle'] : '';
		$data['join_architenko_description'] 	= isset($input['joinarchitenko_section_description']) ? $input['joinarchitenko_section_description'] : '';
		$data['opened_positions_title'] 		= isset($input['openedpositionstitle']) ? $input['openedpositionstitle'] : '';
		$data['terms_and_conditions'] 			= isset($input['termsandconditions']) ? $input['termsandconditions'] : '';
		$data['award_title'] 					= isset($input['awardstitle']) ? $input['awardstitle'] : '';
		$data['award_description'] 				= isset($input['awards_section_description']) ? $input['awards_section_description'] : '';
		$data['language'] 						= isset($input['lang']) ? $input['lang'] : '';
		
		$slider_images = array();
		if(!empty($input['addmore'][0]['image'])){
			foreach ($input['addmore'] as $img) {
				if(!empty($img['image'])){
					$slider_img = $this->get_Image('singleimage',$img['image']);
				}
				else{
					$slider_img = '';
				}
				$slider_images[] = array(
					'display_order' => $img['display_order'],
					'image' => $slider_img
				);
			}
		}
		$data['award_slider'] 					= json_encode($slider_images);
		if($studio = Studio::create($data)){
			if(!empty($input['profilPopup'])){	
				foreach ($input['profilPopup'] as $profiledata) {
					if(!empty($profiledata['image'])){
						$image = $this->get_Image('singleimage',$profiledata['image']);
					}
					else{
						$image = '';
					}
					$profiledata = array(
								'studio_id' => $studio->id,
								'section_type' => 'profilepopup',
								'description' => isset($profiledata['description']) ? $profiledata['description'] : '',
								'title' => isset($profiledata['title']) ? $profiledata['title'] : '',
								'display_order' => isset($profiledata['display_order']) ? $profiledata['display_order'] : 0,
								'image' => $image

							);
					StudioDeails::create($profiledata);
				}
			}
			if(!empty($input['founderPopup'])){	
				foreach ($input['founderPopup'] as $founderPopup) {
					if(!empty($founderPopup['image'])){
						$image = $this->get_Image('singleimage',$founderPopup['image']);
					}
					else{
						$image = '';
					}
					$founderdata = array(
								'studio_id' => $studio->id,
								'section_type' => 'founderpopup',
								'description' => isset($founderPopup['description']) ? $founderPopup['description'] : '',
								'title' => isset($founderPopup['title']) ? $founderPopup['title'] : '',
								'display_order' => isset($founderPopup['display_order']) ? $founderPopup['display_order'] : 0,
								'image' => $image

							);
					StudioDeails::create($founderdata);
				}
			}
			if(!empty($input['awardrPopup'])){	
				foreach ($input['awardrPopup'] as $awardrPopup) {
					if(!empty($awardrPopup['image'])){
						$image = $this->get_Image('singleimage',$awardrPopup['image']);
					}
					else{
						$image = '';
					}
					$awarddata = array(
								'studio_id' => $studio->id,
								'section_type' => 'awardpopup',
								'description' => isset($awardrPopup['description']) ? $awardrPopup['description'] : '',
								'title' => isset($awardrPopup['title']) ? $awardrPopup['title'] : '',
								'display_order' => isset($awardrPopup['display_order']) ? $awardrPopup['display_order'] : 0,
								'image' => $image

							);
					StudioDeails::create($awarddata);
				}
			}
			if(!empty($input['position'])){	
				foreach ($input['position'] as $position) {
					if(!empty($position['image'])){
						$image = $this->get_Image('singleimage',$position['image']);
					}
					else{
						$image = '';
					}
					$positiondata = array(
								'studio_id' => $studio->id,
								'description' => isset($position['description']) ? $position['description'] : '',
								'title' => isset($position['title']) ? $position['title'] : '',
								'image_title' => isset($position['imagetitle']) ? $position['imagetitle'] : '',
								'display_order' => isset($position['display_order']) ? $position['display_order'] : 0,
								'image' => $image

							);
					StudioPosition::create($positiondata);
				}
			}
			if (!empty($input['termandcondition'])) {
				foreach ($input['termandcondition'] as $tncsection) {
					$Studiotnc = StudioTnC::create(['studio_id' => $studio->id,'title' => isset($tncsection['title']) ? $tncsection['title'] : '','orderby' => isset($tncsection['orderby']) ? $tncsection['orderby'] : '']);
					unset($tncsection['title']);
					unset($tncsection['orderby']);
					foreach ($tncsection as $tncsubsection) {				
						if(!empty($tncsubsection['image'])){
							$tncimg = $this->get_Image('singleimage',$tncsubsection['image']);
						}
						else{
							$tncimg = '';
						}
						$data = array(
								'tnc_id' 			=> $Studiotnc->id,
								'studio_id' 		=> $studio->id,
								'description' 		=> isset($tncsubsection['description']) ? $tncsubsection['description'] : '', 
								'show_in_privacy_policy' => isset($tncsubsection['show_in_privacy_policy']) ? 1 : 0, 
								'title' 			=> isset($tncsubsection['title']) ? $tncsubsection['title'] : '', 
								'image' 			=> $tncimg, 
								'display_order' 	=> isset($tncsubsection['display_order']) ? $tncsubsection['display_order'] : 0
							);
						StudioTncDetails::create($data);
					}
				}
			}
		
		}
		return redirect("studio");
		
	}
	public function get_Image($type,$files){
		if($type == 'image' || $type == 'video' || $type == 'book' || $type == 'flip' || $type == 'singleimage'){
			if (!empty($files)) {
				if($type == 'image' || $type == 'video'){
					foreach($files as $file){
						$dataimage = $file->getClientOriginalName();
						$time = strtotime(now());
						$filename = rand(1,9999).$time.strtolower(str_replace(" ","_",$dataimage));
						$path_image = public_path('/images/studio');
						$file->move($path_image, $filename);
					}
				}
				if($type == 'book' || $type == 'flip'){					
					$multi_img=array();
					foreach($files as $file){
						$multi_img_name=$file->getClientOriginalName();
						$time = strtotime(now());
						$multi_img_name = rand(1,9999).$time.strtolower(str_replace(" ","_",$multi_img_name));
						$path_multi_img = public_path('/images/studio');
						$file->move($path_multi_img,$multi_img_name);
						$multi_img[]=$multi_img_name;
					}
					$filename = json_encode($multi_img);
				}
				if($type == 'singleimage'){
						$dataimage = $files->getClientOriginalName();
						$time = strtotime(now());
						$filename = rand(1,9999).$time.strtolower(str_replace(" ","_",$dataimage));
						$path_image = public_path('/images/studio');
						$files->move($path_image, $filename);
				}
				return $filename;
			}
			else{
				return '';
			}
		}
		else{
			return $files;
		}
	}
	public function delete($id){
		Studio::where('id',$id)->forcedelete();
		StudioDeails::where('studio_id',$id)->forcedelete();
		StudioPosition::where('studio_id',$id)->forcedelete();
		StudioTnC::where('studio_id',$id)->forcedelete();
		StudioTncDetails::where('studio_id',$id)->forcedelete();
		Session::flash('alert-success', 'Studio Deleted Successfully!');		
		return redirect('studio');
	}
	public function edit($id) {		
		$title = 'Studio Edit';
		$studio = Studio::where('id',$id)->first();
		$studio_details = StudioDeails::where('studio_id',$id)->get();
		$studio_position = StudioPosition::where('studio_id',$id)->get();
		$studio_tnc = StudioTnC::where('studio_id',$id)->get();
		$studio_tnc_details = StudioTncDetails::where('studio_id',$id)->get();
		return view('admin.studio.edit')->with(compact('title','studio','studio_details','studio_position','studio_tnc','studio_tnc_details'));
	}

	public function update(Request $request){
		// echo "<pre>";	
		// print_r($request->all());
		// die;
		$input = $request->all();
		$data = array();
		if(!empty($request->img_left)){
			$data['title_img_type'] 				= isset($input['main_image_type']) ? $input['main_image_type'] : '';
			$data['title_img'] 						= $this->get_Image($input['main_image_type'],$input['img_left']);
		}	
		$data['profile_title'] 						= isset($input['profiletitle']) ? $input['profiletitle'] : '';
		$data['profile_description'] 				= isset($input['profile_section_description']) ? $input['profile_section_description'] : '';
		if(!empty($request->profile_img_left)){
			$data['profile_right_img_type'] 		= isset($input['profile_image_type']) ? $input['profile_image_type'] : '';
			$data['profile_right_img'] 				= $this->get_Image($input['profile_image_type'],$input['profile_img_left']);
		}
		$data['founder_title'] 						= isset($input['foundertitle']) ? $input['foundertitle'] : '';
		if(!empty($request->founder_img_left)){
			$data['founder_right_img_type'] 		= isset($input['founder_image_type']) ? $input['founder_image_type'] : '';
			$data['founder_right_img'] 				= $this->get_Image($input['founder_image_type'],$input['founder_img_left']);
		}
		$data['founder_description'] 			= isset($input['founder_section_description']) ? $input['founder_section_description'] : '';
		$data['join_architenko_title'] 			= isset($input['joinarchitenkotitle']) ? $input['joinarchitenkotitle'] : '';
		$data['join_architenko_description'] 	= isset($input['joinarchitenko_section_description']) ? $input['joinarchitenko_section_description'] : '';
		$data['opened_positions_title'] 		= isset($input['openedpositionstitle']) ? $input['openedpositionstitle'] : '';
		$data['terms_and_conditions'] 			= isset($input['termsandconditions']) ? $input['termsandconditions'] : '';
		$data['award_title'] 					= isset($input['awardstitle']) ? $input['awardstitle'] : '';
		$data['award_description'] 				= isset($input['awards_section_description']) ? $input['awards_section_description'] : '';
		$data['language'] 						= isset($input['lang']) ? $input['lang'] : 'en';
		// print_r($data);
		// exit();
		$slider_images = array();
		if(!empty($request->addmore)){
			foreach ($request->addmore as $img) {
				//print_r($img);
				if(!empty($img['image'])){
					$slider_img = $this->get_Image('singleimage',$img['image']);
				}
				else{
					$slider_img = isset($img['exist_img']) ? $img['exist_img'] : '';
				}
				$slider_images[] = array(
					'display_order' => $img['display_order'],
					'image' => $slider_img
				);
			}
		}
		$data['award_slider'] = json_encode($slider_images);



		Studio::where('id',$request->studio_id)->update($data);
		if(!empty($input['profilPopup'])){
			$profilepopuplist = StudioDeails::where('studio_id',$request->studio_id)->where('section_type','profilepopup')->get();	
			$id = array();
			
			foreach ($input['profilPopup'] as $profilPopup) {
				$profiledata = array();
				if(!empty($profilPopup['image'])){
					$profiledata['image'] = $this->get_Image('singleimage',$profilPopup['image']);
				}
				$profiledata['description'] 	= isset($profilPopup['description']) ? $profilPopup['description'] : '';
				$profiledata['title']			= isset($profilPopup['title']) ? $profilPopup['title'] : '';
				$profiledata['display_order'] 	= isset($profilPopup['display_order']) ? $profilPopup['display_order'] : 0;

				if(isset($profilPopup['id'])){
					$id[] = $profilPopup['id'];
					StudioDeails::where('id',$profilPopup['id'])->update($profiledata);
				}
				else{
					$profiledata['studio_id'] = $request->studio_id;
					$profiledata['section_type'] = 'profilepopup';
					$profiledata['image'] = isset($profiledata['image']) ? $profiledata['image'] : '';
					StudioDeails::create($profiledata);
				}
			}
			if(!empty($id)){
				foreach ($profilepopuplist as $value) {
					if(!in_array($value->id, $id)){
						StudioDeails::where('id',$value->id)->forcedelete();
					}
				}
			}
		}
		if(!empty($input['founderPopup'])){
			$founderpopuplist = StudioDeails::where('studio_id',$request->studio_id)->where('section_type','founderpopup')->get();	
			$id = array();
			
			foreach ($input['founderPopup'] as $founderPopup) {
				$founderData = array();
				if(!empty($founderPopup['image'])){
					$founderData['image'] = $this->get_Image('singleimage',$founderPopup['image']);
				}
				$founderData['description'] 	= isset($founderPopup['description']) ? $founderPopup['description'] : '';
				$founderData['title']			= isset($founderPopup['title']) ? $founderPopup['title'] : '';
				$founderData['display_order'] 	= isset($founderPopup['display_order']) ? $founderPopup['display_order'] : 0;

				if(isset($founderPopup['id'])){
					$id[] = $founderPopup['id'];
					StudioDeails::where('id',$founderPopup['id'])->update($founderData);
				}
				else{
					$founderData['studio_id'] = $request->studio_id;
					$founderData['section_type'] = 'founderpopup';
					$founderData['image'] = isset($founderData['image']) ? $founderData['image'] : '';
					StudioDeails::create($founderData);
				}
			}
			if(!empty($id)){
				foreach ($founderpopuplist as $value) {
					if(!in_array($value->id, $id)){
						StudioDeails::where('id',$value->id)->forcedelete();
					}
				}
			}
		}
		if(!empty($input['awardrPopup'])){
			$awardpopulist = StudioDeails::where('studio_id',$request->studio_id)->where('section_type','awardpopup')->get();	
			$id = array();
			foreach ($input['awardrPopup'] as $awardrPopup) {
				$awardData = array();
				if(!empty($awardrPopup['image'])){
					$awardData['image'] = $this->get_Image('singleimage',$awardrPopup['image']);
				}
				$awardData['description'] 	= isset($awardrPopup['description']) ? $awardrPopup['description'] : '';
				$awardData['title']			= isset($awardrPopup['title']) ? $awardrPopup['title'] : '';
				$awardData['display_order'] 	= isset($awardrPopup['display_order']) ? $awardrPopup['display_order'] : 0;
				if(isset($awardrPopup['id'])){
					$id[] = $awardrPopup['id'];
					StudioDeails::where('id',$awardrPopup['id'])->update($awardData);
				}
				else{
					$awardData['studio_id'] = $request->studio_id;
					$awardData['section_type'] = 'awardpopup';
					$awardData['image'] = isset($awardData['image']) ? $awardData['image'] : '';
					StudioDeails::create($awardData);
				}
			}
			if(!empty($id)){
				foreach ($awardpopulist as $value) {
					if(!in_array($value->id, $id)){
						StudioDeails::where('id',$value->id)->forcedelete();
					}
				}
			}
		}
		if(!empty($input['position'])){
			$studioPositionList = StudioPosition::where('studio_id',$request->studio_id)->get();
			$id = array();
			foreach ($input['position'] as $position) {
				$positionData =array();
				if(!empty($position['image'])){
					$positionData['image'] = $this->get_Image('singleimage',$position['image']);
				}
				$positionData['description'] 	= isset($position['description']) ? $position['description'] : '';
				$positionData['title']			= isset($position['title']) ? $position['title'] : '';
				$positionData['display_order'] 	= isset($position['display_order']) ? $position['display_order'] : 0;
				$positionData['image_title'] 	= isset($position['imagetitle']) ? $position['imagetitle'] : 0;
				if(isset($position['id'])){
					$id[] = $position['id'];
					StudioPosition::where('id',$position['id'])->update($positionData);
				}
				else{
					$positionData['studio_id'] = $request->studio_id;
					$positionData['section_type'] = 'awardpopup';
					$positionData['image'] = isset($positionData['image']) ? $positionData['image'] : '';
					StudioPosition::create($positionData);
				}
			}
			if(!empty($id)){
				foreach ($studioPositionList as $value) {
					if(!in_array($value->id, $id)){
						StudioPosition::where('id',$value->id)->forcedelete();
					}
				}
			}
		}
		if(!empty($input['termandcondition'])){
			$Studiotnc = StudioTnC::where('studio_id',$request->studio_id)->get();
			$StudiotncDetail = StudioTncDetails::where('studio_id',$request->studio_id)->get();
			$id = array();
			$tnc_deatail_id = array();
			foreach ($input['termandcondition'] as $tnc) {
				$title = isset($tnc['title']) ? $tnc['title'] : '';
				$orderby = isset($tnc['orderby']) ? $tnc['orderby'] : '';
				$curruntTncId = 0;
				if(isset($tnc['id'])){
					$id[] = $tnc['id'];
					$curruntTncId = $tnc['id'];
					StudioTnC::where('id',$tnc['id'])->update(['title' => $title,'orderby' => $orderby]);
				}
				else{
					$temp = StudioTnC::create(['studio_id' => $request->studio_id,'title' => $title,'orderby' => $orderby]);
					$curruntTncId = $temp->id; 
				}
				unset($tnc['title']);
				unset($tnc['orderby']);
				if(isset($tnc['id'])){
					unset($tnc['id']);
				}
				foreach ($tnc as $value) {
					$tncDetailsData = array();
					if(!empty($value['image'])){
						$tncDetailsData['image'] = $this->get_Image('singleimage',$value['image']);
					}
					$tncDetailsData['show_in_privacy_policy'] = isset($value['show_in_privacy_policy']) ? 1 : 0;
					$tncDetailsData['description'] 		= isset($value['description']) ? $value['description'] : '';
					$tncDetailsData['title'] 			= isset($value['title']) ? $value['title'] : '';
					$tncDetailsData['display_order'] 	= isset($value['display_order']) ? $value['display_order'] : 0;
					if(isset($value['id'])){
						$tnc_deatail_id[] = $value['id'];
						StudioTncDetails::where('id',$value['id'])->update($tncDetailsData);
					}
					else{
						$tncDetailsData['tnc_id'] 		= $curruntTncId;
						$tncDetailsData['studio_id'] 	= $request->studio_id;
						StudioTncDetails::create($tncDetailsData);
					}
				}
			}
			if(!empty($id)){
				foreach ($Studiotnc as $value) {
					if(!in_array($value->id, $id)){
						StudioTnC::where('id',$value->id)->forcedelete();
					}
				}
			}

			if(isset($tnc_deatail_id) && !empty($tnc_deatail_id)){
				foreach ($StudiotncDetail as $value) {
					if(!in_array($value->id, $tnc_deatail_id)){
						StudioTncDetails::where('id',$value->id)->forcedelete();
					}
				}
			}
		}
		Session::flash('alert-success', 'Studio Update Successfully!');
		return redirect("studio");
	}
	public function listByLang(Request $request){
		// print_r($request->all());
		$studio = Studio::where('language',$request->lang)->get();
		return response()->json(['data'=> $studio]);
	}
	
}