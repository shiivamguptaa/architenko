<?php	
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Session;
use App\FormValidaionErrors;

class MoreInformationValidationController extends Controller 
{	
	public function __construct() 
	{		
	}
	public function index(){

	}	
	public function create(){
		$title = "MoreInformation Validation";
		return view('admin.moreinformationvalidation.create',compact('title'));
	}
	public function store(Request $request){
		$input = array();
		$input = $request->all();
		unset($input['_token']);
		unset($input['language']);
		$data = array();
		foreach ($input as $key => $value) {
			$data[] = array(
					$key	=> $value
				);
		}
		$dataArray = array(
					'form_name' => 'moreinformation',
					'errors'	=> json_encode($data),
					'language' => $request->language
				);
		FormValidaionErrors::create($dataArray);
		print_r($data);
		print_r($dataArray);
	}
}