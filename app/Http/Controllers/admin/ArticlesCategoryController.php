<?php

namespace App\Http\Controllers\admin;	
use Illuminate\Http\Request;
use App\ArticlesCategory;	
use Validator;
use DB;
use File;
use Illuminate\Support\Facades\Input;
use Session;
	
class ArticlesCategoryController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
		$this->messages = [
		'required' => 'The :attribute is required.',
		];
	}
			
	public function index() {
		$category = ArticlesCategory::where('status',1)->orderBy("orderby","ASC")->get();
		return view('admin.articlescategory.listing')->with(compact('category'));
	}
	
	public function create() {			
		$title = 'Articles Category Create';
		return view('admin.articlescategory.create')->with(compact('title'));
	}
	
	
	public function store(Request $request) {		
		$validator = Validator::make($request->all(), [           
			'category_name' => 'required'
		]);		
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}
		$data = array(
			'category_name' 	=> isset($request->category_name) ? $request->category_name : '',
			'language' 	=> isset($request->lang) ? $request->lang : '',
		);
		ArticlesCategory::create($data);
		Session::flash('alert-success', 'Category Added Successfully!');
		return redirect('articlescategory');
	}
	
	
	public function edit($id){		
		$category = ArticlesCategory::find($id);
		return view('admin.articlescategory.edit')->with(compact('category'));

	}

	public function update(Request $request) {
		$validator = Validator::make($request->all(), [           
			'category_name' => 'required'
		]);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}
		ArticlesCategory::where('id',$request->id)->update(['category_name'=> $request->category_name, 'language' => $request->lang]);
		Session::flash('alert-success', 'Category Updated Successfully!');
		return redirect('articlescategory');
	}

	public function destroy($id){
		$category = ArticlesCategory::find($id);
		if(!empty($category)){	
			$category->status = 0;
			$category->save();
			Session::flash('alert-success', 'Catagory Deleted Successfully!');
		}
		return redirect('articlescategory');
	}
	public function orderby(Request $request){
			$id = isset($request->id) ? $request->id : 0;
			$orderby = isset($request->orderby) ? $request->orderby : 0;
			if(!empty($id)) {
				$articlescategory = ArticlesCategory::find($id);
				if(!empty($articlescategory)) {
					$articlescategory->orderby = $orderby;
					$articlescategory->save();
					return response()->json(['responceCode' => 1 ,'message' => 'Successfully update order']);
				}
			}
	}
	public function onchnagelanguage(Request $request){
		$category = ArticlesCategory::where('status',1)->where('language',$request->lang)->orderBy("orderby","ASC")->get();
		return response()->json(['responseCode' => 1, 'responseMessage' => 'Catagory retrive Successfully', 'data' => $category]);
	}
}
