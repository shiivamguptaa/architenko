<?php	
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\ApplyToolTipDetails;
use App\ApplyToolTipForm;
use App\Configuration;
use App\HomeSectionOne;
use App\NewsletterText;
use App\Menu;
use Validator;
use Session;
use File;
use App;	
use DB;


class ApplyToolTipFormController extends Controller 
{	
	public function __construct() {		
	}		
		
	public function index()
	{
		$title = 'Apply Tool Kit Form Listing';	
		$applytooltipform = ApplyToolTipForm::get();		
		return view('admin.applytooltipform.listing')->with(compact('applytooltipform','title'));
	}


	public function create() 
	{
		$title = 'Apply Tool Kit Form Create';	
		return view('admin.applytooltipform.create')->with(compact('title'));
	}	

	public function store(Request $request)
	{		
		$input = $request->all();
		if(ApplyToolTipForm::create($input)) {
			$request->session()->flash('alert-success', 'Apply Tool Tip Form added successfully.');			
		}
		return redirect('applytooltipform');		
	}	
	public function edit($id){		
		$title = 'Apply Tool Kit Form Edit';
		$applytooltipform = ApplyToolTipForm::find($id);	
		//print_r($applytooltipform);die;	
		return view('admin.applytooltipform.edit')->with(compact('title','applytooltipform','id'));
	}		
	public function update(Request $request) {
		
		$applytooltipform = ApplyToolTipForm::find($request['id']);
		if(empty($applytooltipform)) {
			$request->session()->flash('alert-danger', trans('Some problem occured in update apply tool kit form details.'));
			return redirect('applytooltipform');
		}	
		$input = $request->all();	
		// echo "<pre>";
		// print_r($input);
		// die;
		if(ApplyToolTipForm::findOrFail($request['id'])->update($input)) {		
			$request->session()->flash('alert-success', trans('Apply Tool Kit Form details updated successfully.'));
		}
		else{
			$request->session()->flash('alert-danger', trans('Some problem occured in update apply tool kit form details.'));		
		}			
		return redirect('applytooltipform');
	}
	public function destroy($id) {
		ApplyToolTipForm::where('id','=',$id)->forcedelete();		
		Session::flash('alert-success', 'Apply Tool Kit Form Deleted Successfully!');		
		return redirect('applytooltipform');
	}

	public function applyToolkitFormDetails() {
		$title = "Apply Tool Kit Form Details Listing";
		$applytooltipformdetails = ApplyToolTipDetails::orderBy("id","DESC")->get();		
		return view('admin.applytooltipdetails.listing')->with(compact('title','applytooltipformdetails'));
	}
	public function applyToolkitFormDetailById($id) {
		$title = "Apply Tool Kit Form Detail";
		$studioApplyDetail = ApplyToolTipDetails::select("apply_tooltip_details.*","article.title as article_title")->leftJoin("article","article.id","=","apply_tooltip_details.article_id")->where("apply_tooltip_details.id",$id)->first();	
		//echo "<pre>";print_r($applytooltipformdetail->toArray());die;	
		return view('admin.applytooltipdetails.view')->with(compact('title','studioApplyDetail'));
	}
	public function onchnagelanguage(Request $request){
		$applytooltipformdetails = ApplyToolTipDetails::where('language',$request->lang)->orderBy("id","DESC")->get();
		return response()->json(['responseCode' => 1, 'responseMessage' => 'successfully', 'data' => $applytooltipformdetails]);
	}
	public function delete($id){
		$applyDetails = ApplyToolTipDetails::find($id);
		$applyDetails->delete();
	}
}
