<?php
	
	namespace App\Http\Controllers\admin;	
	use Illuminate\Http\Request;
	use App\Newsletter;
	use App\HomeSectionOne;
	use App\Menu;
	use App\Configuration;
	use App;
	use Excel;
	use App\Exports\UsersExport;
	
	class NewsletterController extends Controller
	{
		public function index()
		{
			$title = 'Newsletter';	
			$newletter = Newsletter::get();
			
			return view('admin.newsletter.listing')->with(compact('newletter','title'));
		}
		
		public function create()
		{
			$title = 'Newsletter';
			
			return view('welcome')->with(compact('title'));
		}
		
		
		public function store(Request $request)
		{
			
			$input = $request->all();
			
			$input['email']=$input['email'];		
			$newsletter = Newsletter::where('email',$input['email'])->first();
			if($newsletter['email'] == '' || empty($newsletter['email']) || $newsletter['email'] == Null){
				if(Newsletter::create($input))
				{
					return response()->json(['responseCode' => 1, 'responseMessage' => getLabel('subscribealert') ]);
				}
			}
			// return redirect('/')->with('failure', 'Sorry! You have already subscribed ');
			return response()->json(['responseCode' => 0, 'responseMessage' =>  getLabel('subscribealerterror') ]);
			
		}
		public function delete($id){
			$news = Newsletter::find($id);
			$news->delete();
		}
		public function download(){
			$data = Newsletter::get();
			$excelData[] = array('#','Email','Date');

			foreach ($data as $key => $value) {
				$newDate = date("m-d-Y", strtotime($value->created_at));  
			 	$excelData[] = array(
			 				'#' => $key+1,
			 				'Email' => $value->email,
			 				'Date' => $newDate
			 			);
			 } 
			return Excel::download(new UsersExport,'Subscriber.csv');
		}
		
	}
