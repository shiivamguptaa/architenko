<?php
	
	namespace App\Http\Controllers\admin;	
	use Illuminate\Http\Request;
	use App\Label;
	use Validator;
	use DB;
	use File;
	use Illuminate\Support\Facades\Input;
	use Session;
	
	class LabelController extends Controller
	{
		//
		public function __construct()
		{
			$this->middleware('auth');
			$this->messages = [
			'required' => 'The :attribute is required.',
			];
		}
		
		/**
			* Show the Home Section One.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			$title = 'Label Listing';	
			$labels = Label::orderBy("label","ASC")->get();			
			// print_r($labels);
			// exit();
			return view('admin.label.listing')->with(compact('labels','title'));
		}
		
		
		public function create()
		{
			$title = 'Label Create';				
			return view('admin.label.create')->with(compact('title'));
		}
		
		
		
		
		public function store(Request $request)
		{
			// print_r($request->all());
			// exit();
			$input = $request->all();
			if(Label::create($input))
			{
				$request->session()->flash('alert-success', 'label added successfully.');
				
			}
			return redirect('label');
			
		}
		
		
		
		public function edit($id){		
			$title = 'Label Edit';
			
			$label = Label::find($id);		
			return view('admin.label.edit')->with(compact('title','label','id'));
		}
		
		
	
		/**
			* Update Home Section One info.
			*
			* @return \Illuminate\Http\Response
		*/
		public function update(Request $request){
			
			
			
			$labeldata = Label::find($request['id']);
			
			if(empty($labeldata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update label details.'));
				return redirect('label');
			}			
					
			$labeldata->name = $request['name'];			
			$labeldata->label = $request['label'];			
			$labeldata->language = $request['language'];		
			
			if($labeldata->update()){
				 
				$request->session()->flash('alert-success', trans('label details updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update label details.'));			
			}
			Session::flash('alert-success', 'Lable Update Successfully!');
			return redirect('label');
		}
		
	
		public function destroy($id){
			$labeldata = Label::where('id','=',$id)->forcedelete();		
			Session::flash('alert-success', 'Label Deleted Successfully!');		
			return redirect('label');
		}


		public function onChangeLabelLanguage(Request $request){
			$language = isset($request->lang) ? $request->lang : "en";
			$labels = Label::where("language",$language)->orderBy("label","ASC")->get();			
			return response()->json(['responseCode' => true ,'responseMessage' => 'Successfully get project list','data'=>$labels]);
		}
}
