<?php
	
	namespace App\Http\Controllers\admin;
	
	use Illuminate\Http\Request;
	use App\ServiceTopdescription;
	use Validator;
	use DB;
	use File;
	use Illuminate\Support\Facades\Input;
	use Session;
	
	class ServiceTopdescriptionController extends Controller
	{
		//
		public function __construct()
		{
			$this->middleware('auth');
			$this->messages = [
			'required' => 'The :attribute is required.',
			];
		}
		
		/**
			* Show the Home Section One.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			$title = 'Service Top Description Listing';	
			$servicetopdescription = ServiceTopdescription::orderBy("orderby","ASC")->get();
			
			return view('admin.servicetop.listing')->with(compact('servicetopdescription','title'));
		}
		
		public function create()
		{
			$title = 'Service Top Description Create';	
			
			return view('admin.servicetop.create')->with(compact('title'));
		}
		
		
		public function store(Request $request)
		{
			$validator = Validator::make($request->all(), [			
			'description' => 'required',
			],$this->messages);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			$input = $request->all();
			$input['description']=$input['description'];			
			$input['language']=$input['lang'];			
			
			if(ServiceTopdescription::create($input))
			{
				$request->session()->flash('alert-success', 'Service Top Description added successfully.');
			}
			return redirect('servicetopdescription');
			
		}
		
		
		public function edit($id){		
			$title = 'Service Top Description Edit';
			
			$servicetopdescription = ServiceTopdescription::find($id);		
			return view('admin.servicetop.edit')->with(compact('title','servicetopdescription','id'));
		}
		/**
			* Update Home Section One info.
			*
			* @return \Illuminate\Http\Response
		*/
		public function update(Request $request){
			
			$validator = Validator::make($request->all(), [			
			'description' => 'required',
			],$this->messages);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			
			$servicetopdescription = ServiceTopdescription::find($request['id']);
			
			if(empty($servicetopdescription))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update service top description details.'));
				return redirect('servicetopdescription');
			}			
					
			$servicetopdescription->description = $request['description'];			
			$servicetopdescription->language = $request['lang'];			
			
			if($servicetopdescription->update()){
				 
				$request->session()->flash('alert-success', trans('Service Top Description details updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update service top description details.'));			
			}
			
			return redirect('servicetopdescription');
		}
		public function destroy($id){
			$servicetopdescription = ServiceTopdescription::where('id','=',$id)->forcedelete();		
			Session::flash('alert-success', 'Service Top Description Deleted Successfully!');		
			return redirect('servicetopdescription');
		}

		public function orderby(Request $request){
			$id = isset($request->id) ? $request->id : 0;
			$orderby = isset($request->orderby) ? $request->orderby : 0;
			if(!empty($id)) {
				$servicetopdescription = ServiceTopdescription::find($id);
				if(!empty($servicetopdescription)) {
					$servicetopdescription->orderby = $orderby;
					$servicetopdescription->save();
					return response()->json(['responceCode' => 1 ,'message' => 'Successfully update order']);
				}
			}
		}
	}
