<?php
	
	namespace App\Http\Controllers\admin;
	
	use Illuminate\Http\Request;
	use App\Menu;
	use Validator;
	use DB;
	use File;
	use Illuminate\Support\Facades\Input;
	use Session;
	
	class MenuController extends Controller
	{
		//
		public function __construct()
		{
			$this->middleware('auth');
			$this->messages = [
			'required' => 'The :attribute is required.',
			];
		}
		
		/**
			* Show the Home Section One.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			$title = 'Menu Listing';	
			$menu = Menu::where('parent',0)->get();
			
			return view('admin.menu.listing')->with(compact('menu','title'));
		}
		
		public function subindex()
		{
			$title = 'Sub Menu Listing';	
			//$menu = Menu::where('parent','!=',0)->get();	
			$menu = DB::table('menu as submenu')
			->join('menu as parentmenu','submenu.parent','=','parentmenu.id')
			->select("submenu.id as id","parentmenu.name as parentmenuname","submenu.name as submenuname","submenu.language as language","submenu.orderby as orderby","submenu.link as link")
          	->where('submenu.parent','!=',0)->get();
          	
            return view('admin.menu.sublisting')->with(compact('menu','title'));
		}
		
		public function create()
		{
			$title = 'Menu Create';				
			return view('admin.menu.create')->with(compact('title'));
		}
		
		public function subcreate()
		{
			$title = 'Sub Menu Create';	
			$menus = Menu::where('parent',0)->get();
			return view('admin.menu.subcreate')->with(compact('title','menus'));
		}
		
		
		public function store(Request $request)
		{
			
			$input = $request->all();
			$input['name']=$input['name'];			
			$input['link']=$input['link'];			
			$orderby = Menu::where("parent","0")->where("language",$input["lang"])->max("orderby");	
			$input['orderby']=$orderby+1;
			if(Menu::create($input))
			{
				$request->session()->flash('alert-success', 'Menu added successfully.');
				
			}
			return redirect('menu');
			
		}
		
		public function substore(Request $request)
		{
			
			$input = $request->all();
			$input['name']=$input['name'];			
			$input['link']=$input['link'];			
			$input['parent']=$input['parentmenu'];			
			$orderby = Menu::where("parent","!=","0")->where("language",$input["lang"])->max("orderby");	
			$input['orderby']=$orderby+1;
			
			if(Menu::create($input))
			{
				$request->session()->flash('alert-success', 'Sub Menu added successfully.');
			}
			return redirect('submenu');
			
		}
		
		
		public function edit($id){		
			$title = 'Menu Edit';
			
			$menudata = Menu::find($id);		
			return view('admin.menu.edit')->with(compact('title','menudata','id'));
		}
		
		public function subedit($id){		
			$title = 'Sub Menu Edit';
			$menus = Menu::where('parent',0)->get();
			$menudata = Menu::find($id);		
			return view('admin.menu.subedit')->with(compact('title','menudata','id','menus'));
		}
		public function orderby(Request $request) {			
			$input = $request->all();
			$id=$input['id'];			
			$orderby=$input['orderby'];		
			$menudata = Menu::find($id);	
			$menudata->orderby = $orderby;
			if($menudata->save()){
				return response()->json(['success' => true, 'message' => "Successfully change orderby"]);
			}	
			else {
				return response()->json(['success' => false, 'message' => "Something went wrong,Please try again later..."]);
			}
		
		}
		/**
			* Update Home Section One info.
			*
			* @return \Illuminate\Http\Response
		*/
		public function update(Request $request){
				
			$validator = Validator::make($request->all(), [           
			
			]);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			
			$menudata = Menu::find($request['id']);
			
			if(empty($menudata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update menu details.'));
				return redirect('menu');
			}			
					
			$menudata->name = $request['name'];			
			$menudata->link = $request['link'];			
			$menudata->language = $request['lang'];							
			
			if($menudata->update()){
				 
				$request->session()->flash('alert-success', trans('Menu details updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update menu details.'));			
			}
			
			return redirect('menu');
		}
		
		public function subupdate(Request $request){
			
			$validator = Validator::make($request->all(), [           
			
			]);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			
			$menudata = Menu::find($request['id']);
			
			if(empty($menudata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update menu details.'));
				return redirect('submenu');
			}			
					
			$menudata->name = $request['name'];			
			$menudata->link = $request['link'];			
			$menudata->parent = $request['parentmenu'];		
			$menudata->language = $request['lang'];		
			
			if($menudata->update()){
				 
				$request->session()->flash('alert-success', trans('Submenu details updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update submenu details.'));			
			}
			
			return redirect('submenu');
		}
		
		public function destroy($id){
			$menudata = Menu::where('id','=',$id)->forcedelete();		
			Session::flash('alert-success', 'Menu Deleted Successfully!');		
			return redirect('menu');
		}
		public function subdestroy($id){
			$menudata = Menu::where('id','=',$id)->forcedelete();		
			Session::flash('alert-success', 'Sub Menu Deleted Successfully!');		
			return redirect('submenu');
		}
		public function manubylang(Request $request){
			$menu = Menu::where('parent',0)->where('language',$request->lang)->get();
			return response()->json(['responseCode' => 1 , 'responseMessage' => 'Menu retrive Successfully', 'data' => $menu]);
		}
		public function submanubylang(Request $request){
			$menu = DB::table('menu as submenu')
			->join('menu as parentmenu','submenu.parent','=','parentmenu.id')
			->select("submenu.id as id","parentmenu.name as parentmenuname","submenu.name as submenuname","submenu.language as language","submenu.orderby as orderby","submenu.link as link")
          	->where('submenu.parent','!=',0)->where('submenu.language',$request->lang)->get();
			return response()->json(['responseCode' => 1 , 'responseMessage' => 'Menu retrive Successfully', 'data' => $menu]);
		}
	}
