<?php
	
	namespace App\Http\Controllers\admin;	
	use Illuminate\Http\Request;
	use App\Articles;
	use App\ArticlesDetails;
	use App\ArticlesCategory;
	use Validator;
	use DB;
	use File;
	use Illuminate\Support\Facades\Input;
	use Session;
	
	class ArticlesController extends Controller
	{
		//
		public function __construct()
		{
			$this->middleware('auth');
			$this->messages = [
			'required' => 'The :attribute is required.',
			];
		}
		
		/**
			* Show the Articles.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			$title = 'Articles Listing';		
			$articles = Articles::select("article.id","article_category.category_name","title","article.language","article.orderby")
					   ->leftJoin('article_category','article_category.id',"=",'article.category_id')->orderBy("article.orderby","ASC")->get();		
			return view('admin.articles.listing')->with(compact('title','articles'));
		}
		
		public function create() {			
			$title = 'Article Create';			
			return view('admin.articles.create')->with(compact('title'));
		}
		
		public function articleCategoryByLanguage(Request $request) {		
			$data = "";	
			if(!empty($request->lang)){
				$catagory = ArticlesCategory::where('language',$request->lang)->where('status',1)->get();
				$data = '<option value="" selected  disabled>Select Catagory...</option>';
				foreach ($catagory as $value) {
					$data .= '<option value="'.$value->id.'">'.$value->category_name.'</option>';
				}
			}
			else {
				$data = "<option value='' selected disabled>Select Catagory</option>";
			}
			return response()->json(['responceCode' => 1 ,'data' => $data]);
		}
		
		public function store(Request $request)
		{

			$data['language'] = isset($request->language) ? $request->language : "";
			$data['category_id'] = isset($request->category_id) ? $request->category_id : 0;
			$data['title'] = isset($request->title) ? $request->title : "";
			$data['main_image_type'] = isset($request->main_image_type) ? $request->main_image_type : "";
			$data['short_description'] = isset($request->short_description) ? $request->short_description : "";
			$data['readmore'] = isset($request->readmore) ? 1 : 0;
			$data['applyfortooltip'] = isset($request->applyfortooltip) ? 1 : 0;
			if(!empty($request->main_image) && $request->main_image_type!="youtube") {
				$multi_img = array();
				foreach($request->main_image as $file){
					$multi_img_name1=$file->getClientOriginalName();
					$time = rand(1,1000000).strtotime(now());
					$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
					$path_multi_img = public_path('/images/articles');
					$file->move($path_multi_img,$multi_img_name);
					$multi_img[]=$multi_img_name;
				}
				$image = json_encode($multi_img);
				$data['main_image'] =$image;
			}
			else {
				$data['main_image'] = isset($request->main_image) ? $request->main_image : "";
			}
			if($articlesadd = Articles::create($data)) {	
				if(isset($request->readmore)) {

					foreach($request->innerPage as $value){	
						$image = array();
						if(!empty($value['image']) && $value['image_type']!="youtube") {
							$multi_img = array();
							foreach($value['image'] as $file){
								$multi_img_name1=$file->getClientOriginalName();
								$time = rand(1,1000000).strtotime(now());
								$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
								$path_multi_img = public_path('/images/articles');
								$file->move($path_multi_img,$multi_img_name);
								$multi_img[]=$multi_img_name;
							}
							$image = json_encode($multi_img);
						}
						else if($value['image_type']=="youtube"){
							$image = isset($value['image']) ? $value['image'] : "";
						}	
						$display_order = isset($value['display_order']) ? $value['display_order'] : 0;							
						ArticlesDetails::create(['image_title'=>$value['image_title'],'description'=>$value['description'],'image'=>$image,'article_id'=>$articlesadd->id,'display_order'=>$display_order,'language'=>$articlesadd->language,'image_type'=>$value['image_type']]);
					}
					
				}
			}
			return redirect('article');
		
 
			/*$title = 'Article Store';
			$validator = Validator::make($request->all(), [           
				'main_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
				'left_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
				'right_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
				'title' => 'required',
				'slug' => 'required',
				'short_description' => 'required',
				'left_description' => 'required',
				'right_description' => 'required',
			],$this->messages);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			$input = $request->all();
			
			if(!empty($input['main_image']) && !empty($input['left_image']) && !empty($input['right_image'])){				
				$main_image = $input['main_image']->getClientOriginalName();
				$time = strtotime(now());
				$filenameMain = $time.strtolower(str_replace(" ","_",$main_image));	
				
				$left_image = $input['left_image']->getClientOriginalName();				
				$filenameLeft = $time.strtolower(str_replace(" ","_",$left_image));	
				
				$right_image = $input['right_image']->getClientOriginalName();				
				$filenameRight = $time.strtolower(str_replace(" ","_",$right_image));	
				
				$path = public_path('/images/articles');	
				
				$input['main_image']->move($path, $filenameMain);
				$input['left_image']->move($path, $filenameLeft);
				$input['right_image']->move($path, $filenameRight);				
			}else{
				$filenameMain = ''; 
				$filenameLeft = ''; 				
				$filenameRight = '';				
			}
			
			$input['main_image'] = $filenameMain;
			$input['left_image'] = $filenameLeft;
			$input['right_image'] = $filenameRight;
			$input['language']=$input['lang'];
			$input['title']= $input['title'];
			$input['slug']= str_slug($input['slug']);
			$input['short_description']= $input['short_description'];			
			$input['left_description']= $input['left_description'];			
			$input['right_description']= $input['right_description'];			
			
			if(Articles::create($input))
			{
				$request->session()->flash('alert-success', 'Article added successfully.');
			}*/
			
			
		}
		
		
		public function edit($articleid){		
			$title = 'Edit Articles';			
			$articles =Articles::where('id','=',$articleid)->first();	
			$articlescategory = array();	
			if(!empty($articles)){
				$articlescategory = ArticlesCategory::where('language','=',$articles["language"])->get();	
			}
			$articlesdetails  = ArticlesDetails::where('article_id','=',$articleid)->get();		
			// print_r($articlesdetails);
			// exit();
			return view('admin.articles.edit')->with(compact('title','articles','articlesdetails','articlescategory','articleid'));			
		}
		
		public function update(Request $request){

			$articleid = $request->articleid;		
			$input = $request->all();
			$articles =Articles::find($articleid);	
			if(!empty($articles)){
				$articles->language = isset($request->language) ? $request->language : "";
				$articles->category_id= isset($request->category_id) ? $request->category_id : 0;
				$articles->title = isset($request->title) ? $request->title : "";
				$articles->short_description = isset($request->short_description) ? $request->short_description : "";
				$articles->readmore = isset($request->readmore) ? 1 : 0;
				$articles->applyfortooltip = isset($request->applyfortooltip) ? 1 : 0;				
				if(!empty($request->main_image) && $request->main_image_type!="youtube") {
					$multi_img = array();
					foreach($request->main_image as $file){
						$multi_img_name1=$file->getClientOriginalName();
						$time = rand(1,1000000).strtotime(now());
						$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
						$path_multi_img = public_path('/images/articles');
						$file->move($path_multi_img,$multi_img_name);
						$multi_img[]=$multi_img_name;
					}
					$articles->main_image_type = isset($request->main_image_type) ? $request->main_image_type : "";
					$image = json_encode($multi_img);
					$articles->main_image = $image;
				}
				else if($request->main_image_type=="youtube") {
					$articles->main_image_type = isset($request->main_image_type) ? $request->main_image_type : "";
					$articles->main_image = isset($request->main_image) ? $request->main_image : "";
				}
				$articles->update();
				
				$articlesdetails  = ArticlesDetails::select('id')->where('article_id','=',$articleid)->get();
				$articlesidList = array();
				foreach ($articlesdetails as $key => $value) {
					$articlesidList[] = $value->id;
				}
				if(isset($input["innerPage"])){
					foreach($input["innerPage"] as $value){					
						$display_order = isset($value['display_order']) ? $value['display_order'] : 0;	
						if(isset($value["ariclesInnerContentid"])){									
							if(in_array($value["ariclesInnerContentid"], $articlesidList)) {							
								$articlesdetails = ArticlesDetails::find($value["ariclesInnerContentid"]);
								if(!empty($articlesdetails)){
									if(!empty($value['image']) && $value['image_type']!="youtube") {
										$multi_img = array();
										foreach($value['image'] as $file){
											$multi_img_name1=$file->getClientOriginalName();
											$time = rand(1,1000000).strtotime(now());
											$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
											$path_multi_img = public_path('/images/articles');
											$file->move($path_multi_img,$multi_img_name);
											$multi_img[]=$multi_img_name;
										}
										$articlesdetails->image = json_encode($multi_img);
										$articlesdetails->image_type = $value['image_type'];
									}
									else if($value['image_type']=="youtube"){
										$articlesdetails->image = isset($value['image']) ? $value['image'] : "";
										$articlesdetails->image_type = $value['image_type'];
									}		
									$articlesdetails->description =$value['description'];
									$articlesdetails->image_title =$value['image_title'];
									$articlesdetails->display_order=$display_order;
									$articlesdetails->save();
								}
							}					
						}
						if(!isset($value["ariclesInnerContentid"])){
							if(!empty($value['image'])){
								$image = array();
								if(!empty($value['image']) && $value['image_type']!="youtube") {
									$multi_img = array();
									foreach($value['image'] as $file){
										$multi_img_name1=$file->getClientOriginalName();
										$time = rand(1,1000000).strtotime(now());
										$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
										$path_multi_img = public_path('/images/articles');
										$file->move($path_multi_img,$multi_img_name);
										$multi_img[]=$multi_img_name;
									}
									$image = json_encode($multi_img);
								}
								else if($value['image_type']=="youtube"){
									$image = isset($value['image']) ? $value['image'] : "";
								}												
								ArticlesDetails::create(['image_title' =>$value['image_title'],'description'=>$value['description'],'image'=>$image,'image_type'=>$value['image_type'],'article_id'=>$articleid,'display_order'=>$display_order]);
							}	
						}					
					}
				}
				foreach ($articlesidList as $key => $value) {
					if(!in_array($value,$input['ariclesInnerContentid'])){
						ArticlesDetails::where('id','=',$value)->delete();		
					}
				}
		 		$request->session()->flash('alert-success', trans('Articles details updated successfully.'));
			
			}
			/*
			$validator = Validator::make($request->all(), [				
				'title' => 'required',
				'slug' => 'required',
				'short_description' => 'required',
				'left_description' => 'required',
				'right_description' => 'required',
			],$this->messages);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			
			$articles = Articles::find($request['id']);
			
			if(empty($articles))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update article details.'));
				return redirect('article');
			}			
					
			$articles->language = $request['lang'];
			$articles->title = $request['title'];
			$articles->slug = str_slug($request['slug']);
			$articles->short_description = $request['short_description'];			
			$articles->left_description = $request['left_description'];			
			$articles->right_description = $request['right_description'];			
			
			
						
			if(isset($request->main_image) && !empty($request->main_image) && $request->main_image != NULL){				
				$main_image = request()->main_image->getClientOriginalName();
				$time = strtotime(now());
				$filenameMain = $time.strtolower(str_replace(" ","_",$main_image));				
				$articles->main_image = $filenameMain;
			}
			if(isset($request->left_image) && !empty($request->left_image) && $request->left_image != NULL){
				$left_image = request()->left_image->getClientOriginalName();
				$time = strtotime(now());
				$filenameLeft = $time.strtolower(str_replace(" ","_",$left_image));
				$articles->left_image = $filenameLeft;
			}
			if(isset($request->right_image) && !empty($request->right_image) && $request->right_image != NULL){
				$right_image = request()->right_image->getClientOriginalName();
				$time = strtotime(now());
				$filenameRight = $time.strtolower(str_replace(" ","_",$right_image));
				$articles->right_image = $filenameRight;
			}
			
			
			
			if($articles->update()){
				if(isset($request->main_image) && !empty($request->main_image) && $request->main_image != NULL){				
					$main_image = request()->main_image->getClientOriginalName();
					$time = strtotime(now());
					$filenameMain = $time.strtolower(str_replace(" ","_",$main_image));	
					$path = public_path('/images/articles');
					request()->main_image->move($path, $filenameMain);					
				}
				if(isset($request->left_image) && !empty($request->left_image) && $request->left_image != NULL){				
					$left_image = request()->left_image->getClientOriginalName();
					$time = strtotime(now());
					$filenameLeft = $time.strtolower(str_replace(" ","_",$left_image));
					$path = public_path('/images/articles');
					request()->left_image->move($path, $filenameLeft);					
				}
				if(isset($request->right_image) && !empty($request->right_image) && $request->right_image != NULL){				
					$right_image = request()->right_image->getClientOriginalName();
					$time = strtotime(now());
					$filenameRight = $time.strtolower(str_replace(" ","_",$right_image));
					$path = public_path('/images/articles');
					request()->right_image->move($path, $filenameRight);					
				}
				
				$request->session()->flash('alert-success', trans('Article details updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update article details.'));			
			}
			*/
			return redirect('article');
		}
		public function destroy($articleid){
			Articles::where('id','=',$articleid)->forcedelete();		
			ArticlesDetails::where('article_id','=',$articleid)->forcedelete();		
			Session::flash('alert-success', 'Article Deleted Successfully!');		
			return redirect('article');
		}
		public function orderby(Request $request){
			$id = isset($request->id) ? $request->id : 0;
			$orderby = isset($request->orderby) ? $request->orderby : 0;
			if(!empty($id)) {
				$articles = Articles::find($id);
				if(!empty($articles)) {
					$articles->orderby = $orderby;
					$articles->save();
					return response()->json(['responceCode' => 1 ,'message' => 'Successfully update order']);
				}
			}
		}
			public function onChangeArticlesLanguage(Request $request){
			// $language = isset($request->language) ? $request->language : "en";
			// $articles = Articles::select("article.id","article_category.category_name","title","article.language","article.orderby")
			// 		   ->leftJoin('article_category','article_category.id',"=",'article.category_id')
			// 		   ->where("article.language",$language)->orderBy("article.orderby","ASC")->get();	
			// $data = "";		   
			// $counter = 1;
			// foreach ($articles as $key => $value) {				
			// 	$data.= "<tr>";
			// 	$data.= "<td>".$counter++."</td>";
			// 	$data.= "<td>".$value->category_name."</td>";
			// 	$data.= "<td>".$value->title."</td>";
			// 	$data.= "<td><input type='text' id='category".$value->id."' class='orderby form-control' value='".$value->orderby."' onfocusout='onChangeOrderBy(".$value->id.")'  /></td>";
			// 	$data.= "<td>".$value->language."</td>";
			// 	$data .="<td class='td-actions text-right'>";				
			// 	$data .="<a href='".URL('article/edit/'.$value->id)."' class='btn btn-success'>";
			// 	$data .="<i class='material-icons'>edit</i></a>&nbsp;";
			// 	$data .="<a href='".URL('article/delete/'.$value->id)."' class='btn btn-danger'>";
			// 	$data .="<i class='material-icons'>close</i></a>";
			// 	$data .="</td></tr>";
			// }
			$language = isset($request->lang) ? $request->lang : "en";
			$articles = Articles::select("article.id","article_category.category_name","title","article.language","article.orderby")->where('article.language',$language)
					   ->leftJoin('article_category','article_category.id',"=",'article.category_id')->orderBy("article.orderby","ASC")->get();		
			return response()->json(['responseCode' => 1 ,'responseMessage' => 'Successfully get article list','data'=>$articles]);
		}
	}
