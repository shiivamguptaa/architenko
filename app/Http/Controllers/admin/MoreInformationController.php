<?php	
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\MoreInformation;
use App\MoreInformationDetails;
use App\Configuration;
use App\HomeSectionOne;
use App\NewsletterText;
use App\Menu;
use Validator;
use Session;
use File;
use App;	
use DB;


class MoreInformationController extends Controller 
{	
	public function __construct() 
	{		
	}		
		
	public function index()
	{
		$title = 'More Information Data Listing';	
		$moreinformations = MoreInformation::get();
		return view('admin.moreinformation.listing')->with(compact('moreinformations','title'));
	}

	public function create() 
	{
		$title = 'More Information Create';	
		return view('admin.moreinformation.create')->with(compact('title'));
	}	

	public function store(Request $request)
	{
		
		$input = $request->all();
		if(MoreInformation::create($input))
		{
			$request->session()->flash('alert-success', 'More Information added successfully.');
			
		}
		return redirect('moreinformation');		
	}	
	public function edit($id){		
		$title = 'More Information Edit';
		$moreInformation = MoreInformation::find($id);		
		return view('admin.moreinformation.edit')->with(compact('title','moreInformation','id'));
	}		
	public function update(Request $request) {
		
		$moreInformation = MoreInformation::find($request['id']);
		if(empty($moreInformation)) {
			$request->session()->flash('alert-danger', trans('Some problem occured in update moreinformation details.'));
			return redirect('moreinformation');
		}	
		$input = $request->all();	
		if(MoreInformation::findOrFail($request['id'])->update($input)) {		
			$request->session()->flash('alert-success', trans('MoreInformation details updated successfully.'));
		}
		else{
			$request->session()->flash('alert-danger', trans('Some problem occured in update MoreInformation details.'));		
		}			
		return redirect('moreinformation');
	}
	public function destroy($id) {
		MoreInformation::where('id','=',$id)->forcedelete();		
		Session::flash('alert-success', 'MoreInformation Deleted Successfully!');		
		return redirect('moreinformation');
	}

	public function moreInformationDetailsList() {
		$title = "More Information Details Listing";
		$moreInformationDetails = MoreInformationDetails::orderBy("id","DESC")->get();		
		return view('admin.moreinformationdetails.listing')->with(compact('title','moreInformationDetails'));
	}
	public function moreInformationDetailsById($id) {
		$title = "More Information Details";
		$moreInformationDetail = MoreInformationDetails::select("more_information_details.*","service.title")->leftJoin("service","service.id","more_information_details.service_id")->where("more_information_details.id",$id)->first();		
		return view('admin.moreinformationdetails.view')->with(compact('title','moreInformationDetail'));
	}
	public function onchanagelanguage(Request $request){
		$moreinformations = MoreInformationDetails::where('language',$request->lang)->orderBy("id","DESC")->get();		
		return response()->json(['responseCode' => 1, 'responseMessage' => 'success', 'data' => $moreinformations]);
	}
	public function delete($id){
		$moreinfodetail = MoreInformationDetails::find($id);
		$moreinfodetail->delete();
	}
}
