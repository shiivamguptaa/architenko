<?php
namespace App\Http\Controllers\admin;	
use Illuminate\Http\Request;
use App\Country;
use Validator;
use DB;
use File;
use Illuminate\Support\Facades\Input;
use Session;
	
class CountryController extends Controller
{
	
	public function __construct() {
		$this->middleware('auth');
		$this->messages = [
		'required' => 'The :attribute is required.',
		];
	}
	
	public function index() {
		$title = 'Country Listing';	
		$countrys = Country::orderBy("name","ASC")->get();		
		return view('admin.country.listing')->with(compact('countrys','title'));
	}
	
	
	public function create() {
		$title = 'Add New Country';				
		return view('admin.country.create')->with(compact('title'));
	}

	public function store(Request $request) {			
		$input = $request->all();
		if(Country::create($input)) {
			$request->session()->flash('alert-success', 'Country added successfully');				
		}
		return redirect('country');			
	}
	
	public function edit($id) {		
		$title = 'Country Edit';
		$country = Country::find($id);		
		return view('admin.country.edit')->with(compact('title','country','id'));
	}
	
	public function update(Request $request) 
	{	
		$country = Country::find($request['id']);		
		if(empty($country)) {
			$request->session()->flash('alert-danger', trans('Some problem occured in update country details.'));
			return redirect('country');
		}							
		$country->name = $request['name'];			
		$country->language = $request['language'];			
		if($country->update()) {			 
			$request->session()->flash('alert-success', trans('country details updated successfully.'));
		}
		else {
			$request->session()->flash('alert-danger', trans('Some problem occured in update country details.'));			
		}		
		return redirect('country');
	}	

	public function destroy($id){
		Country::where('id','=',$id)->forcedelete();		
		Session::flash('alert-success', 'Country Deleted Successfully!');		
		return redirect('country');
	}	
	public function onchnagelanguage(Request $request){
		$countrys = Country::where('language',$request->lang)->orderBy("name","ASC")->get();
		return response()->json(['responseCode' => 1, 'responseMessage' => 'success', 'data' => $countrys]);
	}	
}
