<?php
	
	namespace App\Http\Controllers\admin;
	
	use Illuminate\Http\Request;
	use Validator;
	use DB;
	use File;
	use Illuminate\Support\Facades\Input;
	use Session;
	use App\ProjectDownload;
	
	class ProjectDownloadController extends Controller
	{
		public function index(){
			$downloads = ProjectDownload::get();
			return view('admin.projectdownload.list',compact('downloads'));
		}
		public function delete($id){
			$dl = ProjectDownload::find($id);
			$dl->delete();
		}
	}	
