<?php
	
	namespace App\Http\Controllers;
	use App;	
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;
	use App\Service;
	use App\Servicedetails;
	use App\ProjectInnerContent;
	use App\ServiceTopdescription;
	use Validator;
	use DB;
	use File;
	use Session;
	use App\Configuration;
	use App\HomeSectionOne;
	use App\NewsletterText;
	use App\Label;
	use App\Menu;

	
	class ServiceListController extends Controller
	{
		//
		public $limit =4;
		public function __construct()
		{
			// $this->middleware('auth');
			// $this->messages = [
			// 'required' => 'The :attribute is required.',
			// ];
		}
		
		/**
			* Show the Service.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		
		public function index()
		{
			$title = 'Service Data Listing';	
			$locale = App::getLocale();
			$services = array();		
			$service = Service::select("id","title","language","subtitle","orderby")->where('language',$locale)->orderBy("orderby","ASC")->get();
			$serviceTopDescription = ServiceTopdescription::where('language',$locale)->orderBy("orderby","ASC")->first();
			foreach ($service as $key => $value) {				
				$servicedetails = Servicedetails::select("id","type","datavalue")->where("serviceid",$value->id)->orderBy("display_order","ASC")->first();
				if(!empty($servicedetails)){
					$serviceList = array();
					$serviceList["serviceId"] = $value->id;
					$serviceList["title"] = $value->title;
					$serviceList["subtitle"] = $value->subtitle;
					$serviceList["language"] = $value->language;
					$serviceList["orderby"] = $value->orderby;
					$serviceList["servicedetailsId"] = $servicedetails->id;
					$serviceList["datavalue"] = $servicedetails->datavalue;
					$serviceList["type"] = $servicedetails->type;
					$services[] =$serviceList;
				}
			}
			$config = Configuration::where('language','=',$locale)->first();
			$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
			$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
			$newslettertext = NewsletterText::where('language','=',$locale)->first();
			$labellist = Label::get();
			$labeldata = array();
			$homedata = HomeSectionOne::where('language','=',$locale)->orderBy("id","DESC")->first();
			foreach($labellist as $label){
				$labeldata[$label->label][$label->language] = $label->name;
			}
			return view('servicelist')->with(compact('title','services','locale','menus','submenus','config','newslettertext','labellist','labeldata','homedata','serviceTopDescription'));
		}
	// 	public function getServiceList(Request $request) {
	// 		$input = $request->all();
	// 		$locale = App::getLocale();

	// 		$offset = $input["offset"];	
	// 		$row_number =1;		
	//         $services = array();
	// 		$service = Service::select("id","title","subtitle","language","orderby")
	// 					->where('language',$locale)
	// 					->orderBy("orderby","ASC")
	// 					->offset($this->limit*$offset)
	// 					->take($this->limit)->get();
	// 		foreach ($service as $key => $value) {
	// 			$serviceList = array();
	// 			$serviceList["serviceId"] = $value->id;
	// 			$serviceList["title"] = $value->title;
	// 			$serviceList["subtitle"] = $value->subtitle;
	// 			$serviceList["language"] = $value->language;
	// 			$serviceList["orderby"] = $value->orderby;
	// 			$servicedetails = Servicedetails::select("id","type","datavalue")->where("serviceid",$value->id)->orderBy("display_order","ASC")->first();
	// 			$serviceList["servicedetailsId"] = $servicedetails->id;
	// 			$serviceList["datavalue"] = $servicedetails->datavalue;
	// 			$serviceList["type"] = $servicedetails->type;
	// 			$services[] =$serviceList;
	// 		}
	//         $output = "";
	//         $number=1;
	//         $next_index_no_two = 2; $next_index_no_three=3;
	//         $last_record=false;
	// 		$inner_arr = count($services);
	// 		foreach($services as $service)	{
	// 			if($row_number==$next_index_no_two) {
	// 				$output	 .= "<div class='row  row".$number."'>";	
	// 				$output	 .= "<div class='col col-l'>";	
	// 				$number++;			
	// 			}else if($row_number==$next_index_no_three)	{
	// 				$output	 .= "<div class='col col-r'>";	
	// 			}else{
	// 				$output	 .= "<div class='row  row".$number."'>";		
	// 				$output	 .= "<div class='col'>";	
	// 				$number++;			
	// 			}	
	// 			if($service['type']=="image")	{
	// 				$output	 .= "<a href='".url('servicedetails/'.$service['serviceId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'>";	
	// 				$output	 .= "<img src=".URL('images/service/'.$service['datavalue'])." />";
	// 				$output	 .= "</div><h2 class='r-title'>".$service['title']."</h2></div><div class='text-b'><p>".$service['subtitle']."</p></div></a>";	
	// 			} else if($service['type']=="youtube"){
	// 				$output	 .= "<a href='".url('servicedetails/'.$service['serviceId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='youtube-video-wrapper'>";
	// 				$output	 .= "<iframe src='".$service['datavalue']."'  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
	// 				$output	 .= "</div></div><h2 class='r-title'>".$service['title']."</h2></div><div class='text-b'><p>".$service['subtitle']."</p></div></a>";	
	// 			}elseif($service['type']=='video'){
	// 				$output	 .="<a href='".url('servicedetails/'.$service['serviceId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='video-wrapper'>";
	// 				$output	 .="<video width='100%' controls><source src='".URL('images/service/'.$service['datavalue'])."' type='video/mp4'></video>";
	// 				$output	 .="</div></div><h2 class='r-title'>".$service['title']."</h2></div><div class='text-b'><p>".$service['subtitle']."</p></div></a>";	
	// 			}
	// 			elseif($service['type']=='book'){
	// 				$image_list = json_decode($service['datavalue']);
	// 				$output	 .="<a href='".url('servicedetails/'.$service['serviceId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='book-wrapper'>";
	// 				$index=0; 
	// 				foreach($image_list as $image) { 
	// 					$output	 .=($index==0) ? "<img  width='100%'  src=''.URL('images/service/'.$image).'' class='active' />" : "<img width='100%' src=".URL('images/service/'.$image)." />";
	// 					$index++;	
	// 				}
	// 				$output	.="</div></div><h2 class='r-title'>".$service['title']."</h2></div><div class='text-b'><p>".$service['subtitle']."</p></div></a>";	
	// 			}
	// 			elseif($service['type']=='flip'){
	// 				$image_list = json_decode($service['datavalue']);
	// 				if(count($image_list)>0){ 
	// 					$output	 .="<a href='".url('servicedetails/'.$service['serviceId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='twentytwenty-wrapper twentytwenty-horizontal'><div class='before-after-wrapper twentytwenty-container active'>";
	// 					foreach($image_list as $image) {
	// 						$output	 .="<img width='100%'   src='".URL('images/service/'.$image)."' alt=''>";
	// 					}
	// 					$output	 .="<div class='twentytwenty-handle'><span class='twentytwenty-left-arrow'></span><span class='twentytwenty-right-arrow'></span></div></div></div></div><h2 class='r-title'>".$service['title']."</h2></div><div class='text-b'><p>".$service['subtitle']."</p></div></a>";
								
	// 				}
	// 			}
	// 			if($row_number>=$inner_arr && $row_number==$next_index_no_three)		
	// 				$output	 .="</div></div>";
	// 			elseif($row_number!=$next_index_no_two)
	// 				$output	 .="</div>";
	// 			elseif($inner_arr==$row_number)
	// 				$output	 .="</div></div>";
				
	// 			if($row_number==$next_index_no_two)
	// 				$next_index_no_two = $next_index_no_two+4;
	// 			if($row_number==$next_index_no_three)	
	// 				$next_index_no_three = $next_index_no_three+4;	
				
	// 			$output	 .="</div>";
	// 			$row_number++; 
	// 			if($row_number>=5) {
	// 				$row_number=1;
	// 				$next_index_no_two=2;
	// 				$next_index_no_three=3;
	// 				$number=1;
	// 			}
	// 		}	
	// 		$totalRecord = Service::where('language',$locale)->count();
	// 		if($totalRecord>($this->limit*$offset)){
	// 			$isLastRecord = false;	
	// 		}
	// 		else{
	// 			$isLastRecord = true;	
	// 		}
	// 		return response()->json(['success' => true, 'message' => 'Successfully get service list','data'=>$output,'isLastRecord'=>$isLastRecord]);
	// }
	public function serviceDetails($id){
		if(!empty($id)){			
			$title = 'Service Data Listing';	
			$locale = App::getLocale();
			$servicelist = Service::find($id);
			if(!empty($servicelist)){
				$services = Servicedetails::where('language','=',$locale)->where('serviceid','=',$id)->get();
				$config = Configuration::where('language','=',$locale)->first();
				$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
				$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
				$newslettertext = NewsletterText::where('language','=',$locale)->first();
				return view('servicedetails')->with(compact('title','servicelist','services','locale','menus','submenus','config','newslettertext'));
			}
		}
	}
		/*
		public function create()
		{
			
			$title = 'Service Create';	
			
			return view('admin.service.create')->with(compact('title'));
		}
		
		
		public function store(Request $request)
		{
			
			$input = $request->all();
			
			
			$input['title']=$input['title'];			
			$input['subtitle']=$input['subtitle'];			
			$input['descriptionleft']=$input['descriptionleft'];			
			$input['descriptionright']=$input['descriptionright'];			
			$input['language']=$input['lang'];		
			
			$ishome = $input['ishome'];
			
			if($serviceadd = Service::create($input))
			{				
				foreach($request->addmore as $getvalue){
					//dd($getvalue);
					if($getvalue['type'] == 'image'){
						if(!empty($getvalue['image'])){
							$dataimage = $getvalue['image']->getClientOriginalName();
							$time = strtotime(now());
							$filename = $time.strtolower(str_replace(" ","_",$dataimage));
							$path_image = public_path('/images/service');
							$getvalue['image']->move($path_image, $filename);
							if($ishome == 'image'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'is_home'=>$home,'serviceid'=>$serviceadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$serviceadd->language]);
						}
						else{
							$dataimage ="";	
						}
					}
					if($getvalue['type'] == 'youtube'){
						
						if($ishome == 'youtube'){
							$home = '1';
							}else{
							$home = '0';
						}
						Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$getvalue['youtube'],'is_home'=>$home,'serviceid'=>$serviceadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$serviceadd->language]);
						
					}
					if($getvalue['type'] == 'video'){
						
						if(!empty($getvalue['video'])){
							$datavideo = $getvalue['video']->getClientOriginalName();
							$time = strtotime(now());
							$filename = $time.strtolower(str_replace(" ","_",$datavideo));
							$path_image = public_path('/images/service');
							$getvalue['video']->move($path_image, $filename);
							if($ishome == 'video'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'is_home'=>$home,'serviceid'=>$serviceadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$serviceadd->language]);
						}
						else{
							$datavideo ="";	
						}
					}
					if($getvalue['type'] == 'book'){
						//dd($getvalue);
						$multi_img=array();
						if(!empty($getvalue['book'])){
							foreach($getvalue['book'] as $file){
								$multi_img_name1=$file->getClientOriginalName();
								$time = strtotime(now());
								$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
								$path_multi_img = public_path('/images/service');
								$file->move($path_multi_img,$multi_img_name);
								$multi_img[]=$multi_img_name;
							}
							$bookImages = json_encode($multi_img);
							if($ishome == 'book'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'is_home'=>$home,'serviceid'=>$serviceadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$serviceadd->language]);
						}
						else{
							$bookImages ="";	
						}
					}
					if($getvalue['type'] == 'flip'){
						//dd($getvalue);
						$multi_img=array();
						if(!empty($getvalue['flip'])){
							foreach($getvalue['flip'] as $file){
								$multi_img_name1=$file->getClientOriginalName();
								$time = strtotime(now());
								$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
								$path_multi_img = public_path('/images/service');
								$file->move($path_multi_img,$multi_img_name);
								$multi_img[]=$multi_img_name;
							}
							$bookImages = json_encode($multi_img);
							if($ishome == 'flip'){
								$home = '1';
							}
							else{
								$home = '0';
							}
							Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'is_home'=>$home,'serviceid'=>$serviceadd->id,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$serviceadd->language]);
						}
						else{
							$bookImages ="";	
						}
					}
				}
				
				$request->session()->flash('alert-success', 'Service added successfully.');
			}
			return redirect('service');
			
		}
		
		
		public function edit($id){		
			$title = 'Service Edit';
			
			$servicedata = Service::find($id);
			$servicedetails = Servicedetails::where('serviceid','=',$id)->get();
			return view('admin.service.edit')->with(compact('title','servicedata','id','servicedetails'));
		}
		/**
			* Update Home Section One info.
			*
			* @return \Illuminate\Http\Response
		*/
		/*public function update(Request $request){
			
			$validator = Validator::make($request->all(), [           
			
			]);
			
			if ($validator->fails())
			{
				return redirect()->back()->withErrors($validator->errors());
			}
			
			
			$servicedata = Service::find($request['id']);
			
			if(empty($servicedata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update service link details.'));
				return redirect('service');
			}			
			
			$servicedata->title = $request['title'];			
			$servicedata->subtitle = $request['subtitle'];			
			$servicedata->descriptionleft = $request['descriptionleft'];			
			$servicedata->descriptionright = $request['descriptionright'];			
			
			if($servicedata->update()){
				
				$request->session()->flash('alert-success', trans('Service details updated successfully.'));
			}
			else{
				$request->session()->flash('alert-danger', trans('Some problem occured in update service link details.'));			
			}
			
			return redirect('service');
		}

		public function update(Request $request){
			
			// $validator = Validator::make($request->all(), [           
			
			// ]);
			
			// if ($validator->fails())
			// {
			// 	return redirect()->back()->withErrors($validator->errors());
			// }
			
			
			// $servicedata = service::find($request['id']);
			
			// if(empty($servicedata))
			// {
			// 	$request->session()->flash('alert-danger', trans('Some problem occured in update service link details.'));
			// 	return redirect('service');
			// }			
			
			// $servicedata->title = $request['title'];			
			// $servicedata->subtitle = $request['subtitle'];			
			// $servicedata->descriptionleft = $request['descriptionleft'];			
			// $servicedata->descriptionright = $request['descriptionright'];			
			
			// if($servicedata->update()){
				
			// 	$request->session()->flash('alert-success', trans('service details updated successfully.'));
			// }
			// else{
			// 	$request->session()->flash('alert-danger', trans('Some problem occured in update service link details.'));			
			// }
			$input = $request->all();
			
			$servicedata = Service::find($input['id']);
			if(empty($servicedata))
			{
				$request->session()->flash('alert-danger', trans('Some problem occured in update service link details.'));
				return redirect('service');
			}	
			$servicedata->title = $input['title'];			
			$servicedata->subtitle = $input['subtitle'];			
			$servicedata->descriptionleft = $input['descriptionleft'];
			$servicedata->descriptionright = $input['descriptionright'];		
			$servicedata->language = $input["lang"];		
			$language  = $input["lang"];

			if($servicedata->update()) {
				$serviceid = $input['id'];
				$servicedetails  = Servicedetails::select('id')->where('serviceid','=',$serviceid)->get();
				$serviceidList = array();
				foreach ($servicedetails as $key => $value) {
					$serviceidList[] = $value->id;
				}
				foreach($input["addmore"] as $getvalue){
					if(isset($getvalue['type'])){		
					if($getvalue['type'] == 'image') {						
						if(isset($getvalue["serviceId"])){									
							if(in_array($getvalue["serviceId"], $serviceidList)) {
							
								$servicedetails = Servicedetails::find($getvalue["serviceId"]);
								if(!empty($servicedetails)){
									if(!empty($getvalue['image'])){
										$dataimage = $getvalue['image']->getClientOriginalName();
										$time = strtotime(now());
										$filename = $time.strtolower(str_replace(" ","_",$dataimage));
										$path_image = public_path('/images/service');
										$getvalue['image']->move($path_image, $filename);
										$servicedetails->datavalue =$filename;
									}	
									$servicedetails->type =$getvalue['type'];
									$servicedetails->serviceid =$serviceid;
									$servicedetails->moretitle =$getvalue['moretitle'];
									$servicedetails->display_order=$getvalue['display_order'];
									$servicedetails->language=$language;
									$servicedetails->save();
								}
							}					
						}
						if(!isset($getvalue["serviceId"]))
						{
							if(!empty($getvalue['image'])){
								$dataimage = $getvalue['image']->getClientOriginalName();
								$time = strtotime(now());
								$filename = $time.strtolower(str_replace(" ","_",$dataimage));
								$path_image = public_path('/images/service');
								$getvalue['image']->move($path_image, $filename);						
								Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
							}						
						}
					}
					if($getvalue['type'] == 'youtube'){					
						if(isset($getvalue["serviceId"])){
							if(in_array($getvalue["serviceId"], $serviceidList)) {
								$servicedetails = Servicedetails::find($getvalue["serviceId"]);
								if(!empty($servicedetails)){
									$servicedetails->type =$getvalue['type'];
									$servicedetails->datavalue = $getvalue['youtube'];
									$servicedetails->serviceid = $serviceid;
									$servicedetails->moretitle = $getvalue['moretitle'];
									$servicedetails->display_order=$getvalue['display_order'];
									$servicedetails->language=$language;
									$servicedetails->save();
								}		
							}
						}
						if(!isset($getvalue["serviceId"])){
							Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$getvalue['youtube'],'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
						}
						
					}
					if($getvalue['type'] == 'video'){
						if(isset($getvalue["serviceId"])){
							if(in_array($getvalue["serviceId"], $serviceidList)) {
								$servicedetails = Servicedetails::find($getvalue["serviceId"]);
								if(!empty($servicedetails)){
									if(!empty($getvalue['video'])){
										$datavideo = $getvalue['video']->getClientOriginalName();
										$time = strtotime(now());
										$filename = $time.strtolower(str_replace(" ","_",$datavideo));
										$path_image = public_path('/images/service');
										$getvalue['video']->move($path_image, $filename);
										$servicedetails->datavalue = $filename;
										// servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
									}
									$servicedetails->type =$getvalue['type'];								
									$servicedetails->serviceid = $serviceid;
									$servicedetails->moretitle = $getvalue['moretitle'];
									$servicedetails->display_order=$getvalue['display_order'];
									$servicedetails->language=$language;
									$servicedetails->save();
								}
							}
						}
						if(!isset($getvalue["serviceId"]))
						{
							if(!empty($getvalue['video'])) {
								$datavideo = $getvalue['video']->getClientOriginalName();
								$time = strtotime(now());
								$filename = $time.strtolower(str_replace(" ","_",$datavideo));
								$path_image = public_path('/images/service');
								$getvalue['video']->move($path_image, $filename);
								
								Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$filename,'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
							}
						}
						
					}
					if($getvalue['type'] == 'book'){	
						if(isset($getvalue["serviceId"])){	
							if(in_array($getvalue["serviceId"], $serviceidList)) {	
								$servicedetails = Servicedetails::find($getvalue["serviceId"]);
								if(!empty($servicedetails)){
									$multi_img=array();
									if(!empty($getvalue['book'])){
										foreach($getvalue['book'] as $file){
											$multi_img_name1=$file->getClientOriginalName();
											$time = strtotime(now());
											$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
											$path_multi_img = public_path('/images/service');
											$file->move($path_multi_img,$multi_img_name);
											$multi_img[]=$multi_img_name;
										}
										$bookImages = json_encode($multi_img);							
										$servicedetails->datavalue =$bookImages;
									}
									$servicedetails->type =$getvalue['type'];								
									$servicedetails->serviceid = $serviceid;
									$servicedetails->moretitle = $getvalue['moretitle'];
									$servicedetails->display_order=$getvalue['display_order'];
									$servicedetails->language=$language;
									$servicedetails->save();
								}
							}	
						}
						if(!isset($getvalue["serviceId"])){		
							$multi_img=array();
							if(!empty($getvalue['book'])){
								foreach($getvalue['book'] as $file){
									$multi_img_name1=$file->getClientOriginalName();
									$time = strtotime(now());
									$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
									$path_multi_img = public_path('/images/service');
									$file->move($path_multi_img,$multi_img_name);
									$multi_img[]=$multi_img_name;
								}
								$bookImages = json_encode($multi_img);							
								Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
							}
						}
						
					}
					if($getvalue['type'] == 'flip'){	
						if(isset($getvalue["serviceId"])){					
							if(in_array($getvalue["serviceId"], $serviceidList)) {	
								$servicedetails = servicedetails::find($getvalue["serviceId"]);
								if(!empty($servicedetails)){
									$multi_img=array();
									if(!empty($getvalue['flip'])){
										foreach($getvalue['flip'] as $file){
											$multi_img_name1=$file->getClientOriginalName();
											$time = strtotime(now());
											$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
											$path_multi_img = public_path('/images/service');
											$file->move($path_multi_img,$multi_img_name);
											$multi_img[]=$multi_img_name;										
										}
										$bookImages = json_encode($multi_img);							
										$servicedetails->datavalue =$bookImages;
									}
									$servicedetails->type =$getvalue['type'];								
									$servicedetails->serviceid = $serviceid;
									$servicedetails->moretitle = $getvalue['moretitle'];
									$servicedetails->display_order=$getvalue['display_order'];
									$servicedetails->language=$language;
									$servicedetails->save();
								}
							}
						}
						if(!isset($getvalue["serviceId"])){
							$multi_img=array();
							if(!empty($getvalue['flip'])){
								foreach($getvalue['flip'] as $file){
									$multi_img_name1=$file->getClientOriginalName();
									$time = strtotime(now());
									$multi_img_name = $time.strtolower(str_replace(" ","_",$multi_img_name1));
									$path_multi_img = public_path('/images/service');
									$file->move($path_multi_img,$multi_img_name);
									$multi_img[]=$multi_img_name;
								}
								$bookImages = json_encode($multi_img);							
								Servicedetails::create(['type'=>$getvalue['type'],'datavalue'=>$bookImages,'serviceid'=>$serviceid,'moretitle'=>$getvalue['moretitle'],'display_order'=>$getvalue['display_order'],'language'=>$language]);
							}		
						}				
					}}
				}
				foreach ($serviceidList as $key => $value) {
					if(!in_array($value,$request->serviceId)){
						Servicedetails::where('id','=',$value)->delete();		
					}
				}
			 	$request->session()->flash('alert-success', trans('Service details updated successfully.'));
			}
			else {
				$request->session()->flash('alert-danger', trans('Some problem occured in update service link details.'));			
			}
			return redirect('service');
		}
		public function destroy($id){
			$servicedata = Service::where('id','=',$id)->forcedelete();		
			$service1data = Servicedetails::where('serviceid','=',$id)->forcedelete();		
			Session::flash('alert-success', 'Service Deleted Successfully!');		
			return redirect('service');
		}

		public function orderby(Request $request) {			
			$input = $request->all();
			$id=$input['id'];			
			$orderby=$input['orderby'];		
			$service = Service::find($id);	
			$service->orderby = $orderby;
			if($service->save()){
				return response()->json(['success' => true, 'message' => "Successfully change orderby"]);
			}	
			else {
				return response()->json(['success' => false, 'message' => "Something went wrong,Please try again later..."]);
			}		
		}*/

	}
