<?php	
namespace App\Http\Controllers;
use App;	
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\ArticlesDetails;
use App\ArticlesCategory;
use App\ApplyToolTipForm;
use App\ApplyToolTipDetails; 
use App\Articles;
use Validator;
use DB;
use File;
use Session;
use App\Configuration;
use App\HomeSectionOne;
use App\NewsletterText;
use App\Label;
use App\Menu;
use App\ProjectInnerContent;
use Mail;
use App\EmailForm;

class ArticleListController extends Controller {	
	public function __construct() {
		
	}		
		
	public function index()
	{
		$title = 'Articles Data Listing';	
		$locale = App::getLocale();
		$articles = array();
		$articles =Articles::where('language',$locale)->orderBy("orderby","ASC");	
		if(isset($_GET['search'])){
			$articles = $articles->where('title','like','%'.$_GET['search'].'%');
		}
		if(isset($_GET['catagory'])){
			$articles = $articles->where('category_id',$_GET['catagory']);
		}	
		$articles = $articles->paginate(12);

		$pagination_links = $articles->links();
		$config = Configuration::where('language','=',$locale)->first();
		$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
		$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
		$newslettertext = NewsletterText::where('language','=',$locale)->first();
		$labellist = Label::get();
		$labeldata = array();
		$homedata = HomeSectionOne::where('language','=',$locale)->orderBy("id","DESC")->first();
		foreach($labellist as $label){
			$labeldata[$label->label][$label->language] = $label->name;
		}
		return view('articlelist')->with(compact('title','locale','menus','submenus','config','newslettertext','labellist','labeldata','homedata','pagination_links','articles'));
	}
	
	public function articleInnerContent($id){

		$title = 'Articles Inner Page';	
		$locale = App::getLocale();
		$articles =Articles::select('id','title','applyfortooltip')->where('language',$locale)->where('id',$id)->orderBy("orderby","ASC")->first();	
		$articlesDetails =ArticlesDetails::select('image_type','image_title','image','description')->where('article_id',$id)->orderBy("display_order","ASC")->get();

		$applytooltipform =ApplyToolTipForm::where("language",$locale)->orderBy("id","DESC")->first();
		$config = Configuration::where('language','=',$locale)->first();
		$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
		$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
		$newslettertext = NewsletterText::where('language','=',$locale)->first();
		$homedata = HomeSectionOne::where('language','=',$locale)->orderBy("id","DESC")->first();
		return view('articlesinnerpage')->with(compact('title','locale','menus','submenus','config','newslettertext','homedata','articles','articlesDetails','applytooltipform'));
		
	}
	public function articleSearch(Request $request){
		if($request->ajax()){
			$locale = App::getLocale();
			$catagory = ArticlesCategory::where('language',$locale)->where('status',1)->orderby('orderby','ASC')->get();

			$articles =Articles::select('title','article.id','article.category_id','category_name')->where('article.language',$locale)->where('readmore',1);
			if (isset($request->data)) {
				$articles = $articles->where('title','like','%'.$request->data.'%');
			}
			$articles = $articles->join('article_category','article_category.id','=','article.category_id')->orderby('article_category.orderby','ASC')->get();
			
			$data = '<ul class="cat-result">';
			if(!isset($request->data)) {
				foreach ($catagory as $value) {
					$url1 = URL('articlelist?catagory='.$value->id);
					$data .= '<li><a href="'.$url1.'">'.$value->category_name.'<span class="hr"></span></a></li>';
				}
			}
			foreach ($articles as $article) {
				$url = URL('articleinner',$article->id);
				$data .= '<li><a href="'.$url.'">'.$article->title.'<span class="hr"></span></a>'.$article->category_name.'</li>';
			}
			$data .= '</ul>';

			return response()->json(['responceCode' => 1, 'data' => $data]);
		}
		else{
			return response()->json(['responceCode' => 0, 'responseMessage' => 'Something went wrong']);
		}
	}

	public function storeApplyToolkitForm(Request  $request){
		$input = $request->all();
		$data = array();
		$locale = App::getLocale();
		$data["language"] = $locale;

		$data["article_id"] = isset($input["article_id"]) ?  $input["article_id"] : 0;
		$data["you_found_through"] = isset($input["you_found_through"]) ?  $input["you_found_through"] : "";
		$data["you_found_through_other"] = isset($input["you_found_through_other"]) ?  $input["you_found_through_other"] : "";
		$data["choose_toolkit"] = isset($input["choose_toolkit"]) ?  $input["choose_toolkit"] : "";
		$data["choose_toolkit_other"] = isset($input["choose_toolkit_other"]) ?  $input["choose_toolkit_other"] : "";
		$data["position"] = isset($input["position"]) ?  $input["position"] : "";

		$data["private_individual_gender"] = isset($input["private_individual_gender"]) ?  $input["private_individual_gender"] : "";
		$data["private_individual_first_name"] = isset($input["private_individual_first_name"]) ?  $input["private_individual_first_name"] : "";
		$data["private_individual_middle_name"] = isset($input["private_individual_middle_name"]) ?  $input["private_individual_middle_name"] : "";
		$data["private_individual_last_name"] = isset($input["private_individual_last_name"]) ?  $input["private_individual_last_name"] : "";
		$data["private_individual_address"] = isset($input["private_individual_address"]) ?  $input["private_individual_address"] : "";
		$data["private_individual_city"] = isset($input["private_individual_city"]) ?  $input["private_individual_city"] : "";
		$data["private_individual_country"] = isset($input["private_individual_country"]) ?  $input["private_individual_country"] : "";
		$data["private_individual_phone"] = isset($input["private_individual_phone"]) ?  $input["private_individual_phone"] : "";
		$data["private_individual_email"] = isset($input["private_individual_email"]) ?  $input["private_individual_email"] : "";
		$data["private_individual_dob"] = isset($input["private_individual_dob"]) ?  $input["private_individual_dob"] : "";




		$data["enterprise_price_title1"] = isset($input["enterprise_price_title1"]) ?  $input["enterprise_price_title1"] : "";
		$data["enterprise_price_title2"] = isset($input["enterprise_price_title2"]) ?  $input["enterprise_price_title2"] : "";
		$data["enterprise_address_title"] = isset($input["enterprise_address_title"]) ?  $input["enterprise_address_title"] : "";
		$data["enterprise_city_title"] = isset($input["enterprise_city_title"]) ?  $input["enterprise_city_title"] : "";
		$data["enterprise_postal_code_title"] = isset($input["enterprise_postal_code_title"]) ?  $input["enterprise_postal_code_title"] : "";
		$data["enterprise_country_title"] = isset($input["enterprise_country_title"]) ?  $input["enterprise_country_title"] : "";
		$data["enterprise_phone_title"] = isset($input["enterprise_phone_title"]) ?  $input["enterprise_phone_title"] : "";
		$data["enterprise_email_title"] = isset($input["enterprise_email_title"]) ?  $input["enterprise_email_title"] : "";
		$data["enterprise_gender"] = isset($input["enterprise_gender"]) ?  $input["enterprise_gender"] : "";
		$data["enterprise_contact_first_name_title"] = isset($input["enterprise_contact_first_name_title"]) ?  $input["enterprise_contact_first_name_title"] : "";
		$data["enterprise_contact_last_name_title"] = isset($input["enterprise_contact_last_name_title"]) ?  $input["enterprise_contact_last_name_title"] : "";
		$data["enterprise_contact_personpositionlist_title"] = isset($input["enterprise_contact_personpositionlist_title"]) ?  $input["enterprise_contact_personpositionlist_title"] : "";


		$clientdata = array();
		if($data["position"]=="Private individual")	{
			$clientdata["full_name"] = $data["private_individual_first_name"]." ".$data["private_individual_middle_name"];		
			$clientdata["last_name"] = $data["private_individual_last_name"];
			$clientdata["country"]   = $data["private_individual_country"];
			$clientdata["address"]   = $data["private_individual_address"];
			$clientdata["phone"]     = $data["private_individual_phone"];
			$clientdata["email"]     = $data["private_individual_email"];
			$clientdata["city"] 	 = $data["private_individual_city"];
		}
		else {
			$clientdata["full_name"] = $data["enterprise_price_title1"];		
			$clientdata["last_name"] = $data["enterprise_price_title2"];
			$clientdata["country"]   = $data["enterprise_country_title"];
			$clientdata["address"]   = $data["enterprise_address_title"];
			$clientdata["phone"]     = $data["enterprise_phone_title"];
			$clientdata["email"]     = $data["enterprise_email_title"];
			$clientdata["city"] 	 = $data["enterprise_city_title"];
		}

		$emailForm = EmailForm::where("formname","applytoolkit")->get();
		foreach ($emailForm as $key => $value) {
			$to = $value->email;
			Mail::send('email-templates.form', $clientdata, function($message) use ($to) {				
				$message->to($to, 'Architenko');
				$message->subject('Apply Toolkit Form');
				$message->from('info@architenko.com','Architenko');				
			});
		}	
		
		if(ApplyToolTipDetails::create($data)){
			return response()->json(['success' => true, 'message' =>"Successfully send message"]);
		}
		else{
			return response()->json(['success' => false, 'message' =>"Something went wrong,Please try again later..."]);
		}
		
	}
}
