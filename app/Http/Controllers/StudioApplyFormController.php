<?php	
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\StudioApplyForm;
use App\Configuration;
use App\HomeSectionOne;
use App\NewsletterText;
use App\Label;
use App\Menu;
use Validator;
use Session;
use File;
use App;	
use DB;


class StudioApplyFormController extends Controller {	
	public function __construct() {		
	}		
		
	public function store(Request $request) {
		$locale = App::getLocale();	
		$input = $request->all();	
		$input['language'] = $locale;
		if(StudioApplyForm::create($input)){
			return response()->json(['success' => true, 'message' => "Successfully send message"]);
		}
		else{
			return response()->json(['success' => false, 'message' => "Something went wrong,Please try again later..."]);
		}
	}	
}
