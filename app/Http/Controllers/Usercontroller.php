<?php
	
	namespace App\Http\Controllers;
	use App;	
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;
	use Validator;
	use File;
	use Session;
	use Mail;
	use App\User;
	use Auth;
	use Hash;
	
	class Usercontroller extends Controller
	{
		public function __construct()
		{
		}
		public function index(){
			if(isset(Auth::user()->role)){
				if(Auth::user()->role != 1){	
					return redirect('/menu');
				}
			}
			else{
				return redirect('/menu');
			}
			$title = 'User Listing';
			$users = User::get();
			return view('admin/users/index',compact('users','title'));
		}
		public function create(){
			$title = 'Create User';
			return view('admin/users/create',compact('title'));
		}
		public function store(Request $request){
			$validatedData = $request->validate([
		        'name' => 'required',
		        'email' => 'required|email|unique:users',
		        'password' => 'required'
		    ]);
		    $data = array();
		    $data['name'] = $request->name;
		    $data['email'] = $request->email;
		    $data['password'] = Hash::make($request->password);
		    $data['role'] = 2;
		    User::create($data);
		    $request->session()->flash('alert-success', 'User Create successfully.');
		    return redirect('users');
		}
		public function edit($id){
			$user = User::find($id);
			return view('admin/users/edit',compact('user'));
		}
		public function update(Request $request){
			$validatedData = $request->validate([
		        'name' => 'required',
		        'email' => 'required|email|unique:users,email,'.$request->id
		    ]);
		    $data = array();
		    $data['name'] = $request->name;
		    $data['email'] = $request->email;
		    $pass = trim($request->password);
		    if (isset($request->password)) {
		    	$data['password'] = Hash::make($request->password);
		    }
		    User::where('id',$request->id)->update($data);
		    $request->session()->flash('alert-success', 'User update successfully.');
		    return redirect('users');
		}
		public function delete($id){
			User::where('id',$id)->forcedelete();
			session()->flash('alert-success', 'User delete successfully.');
		    return redirect('users');
		}
	}