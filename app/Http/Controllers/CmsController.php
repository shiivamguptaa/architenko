<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use App\Configuration;
use App\HomeSectionOne;
use App\NewsletterText;
use App\Label;
use DB;
use App\Menu;
use App\Service;
use App\Servicedetails;
use App\Project;
use App\Projectdetails;


class CmsController extends Controller
{
	/**
		* Create a new controller instance.
		*
		* @return void
	*/
	public $limit =4;
	public function __construct()
	{
		//$this->middleware('auth');
	}
	
	/**
		* Show the application dashboard.
		*
		* @return \Illuminate\Contracts\Support\Renderable
	*/
	public function index()
	{
		$locale = App::getLocale();
		$title = 'Home Page';
		$ishomepage = 'home';
		
	
        $projects = array();
		$project = Project::select("id","title","language","subtitle","orderby")->where('language',$locale)->where('is_home',1)->orderBy("orderby","ASC")->offset(0)->take($this->limit)->get();
		foreach ($project as $key => $value) {			
			$projectdetails = Projectdetails::select("id","type","datavalue")->where("projectid",$value->id)->orderBy("display_order","ASC")->first();
			if(!empty($projectdetails)) {
				$projectList = array();
				$projectList["projectId"] = $value->id;
				$projectList["title"] = $value->title;
				$projectList["subtitle"] = $value->subtitle;
				$projectList["language"] = $value->language;
				$projectList["orderby"] = $value->orderby;
				$projectList["servicedetailsId"] = $projectdetails->id;
				$projectList["datavalue"] = $projectdetails->datavalue;
				$projectList["type"] = $projectdetails->type;
				$projects[] =$projectList;
			}
		}

		$services = array();
		$service = Service::select("id","title","language","subtitle","orderby")->where('language',$locale)->where('is_home',1)->orderBy("orderby","ASC")->get();
		foreach ($service as $key => $value) {
			$servicedetails = Servicedetails::select("id","type","datavalue")->where("serviceid",$value->id)->orderBy("display_order","ASC")->first();
			if(!empty($servicedetails)) {
				$serviceList = array();
				$serviceList["serviceId"] = $value->id;
				$serviceList["title"] = $value->title;
				$serviceList["subtitle"] = $value->subtitle;
				$serviceList["language"] = $value->language;
				$serviceList["orderby"] = $value->orderby;
				$serviceList["servicedetailsId"] = $servicedetails->id;
				$serviceList["datavalue"] = $servicedetails->datavalue;
				$serviceList["type"] = $servicedetails->type;
				$services[] =$serviceList;
			}
		}
		
		
		
		$homedata = HomeSectionOne::where('language','=',$locale)->orderBy("id","DESC")->first();			
		$left_image = "";
		$right_image = "";
		if($homedata["left_item_type"]=="image")	{			
			$image_list = json_decode($homedata["img_left"]);
			$image_url = isset($image_list[0]) ? URL($image_list[0]) : "";
			if(!empty($image_url)){
				$left_image = "<img src='".$image_url."' width='100%'   />";
			}	
		}
		else if($homedata["left_item_type"]=="youtube")	{
			if(!empty($homedata["img_left"])) {
				$left_image = "<iframe src='".$homedata["img_left"]."'  width='350px'  height='350px'  class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
			}
		}	
		else if($homedata["left_item_type"]=="video")	{		
			$image_list = json_decode($homedata["img_left"]);
			$video_url = isset($image_list[0]) ? URL($image_list[0]) : "";
			if(!empty($video_url)){
				$left_image = '<video  width="350px" height="350px" controls><source src="'.$video_url.'" type="video/mp4"></video>';
			}					
		}
		else if($homedata["left_item_type"]=="book")	{			
			$image_list = json_decode($homedata["img_left"]);			
			$index=0;
			$left_image = '<div class="h-c-wrapper"><div class="cont-wrapper"><div class="book-wrapper">';
			foreach($image_list as $image) { 
				$left_image .= ($index==0) ? '<img  width="350px"  height="350px" src="'.URL($image).'" class="active" />' : '<img width="350px"  height="350px" src="'.URL($image).'" />';
				$index++;	
			}
			$left_image .='</div></div></div>';
		}
		else if($homedata["left_item_type"]=="flip")	{		
			$image_list = json_decode($homedata["img_left"]);
			if(count($image_list)>0){
				$left_image = '<div class="h-c-wrapper"><div class="cont-wrapper"><div class="twentytwenty-wrapper twentytwenty-horizontal"><div class="before-after-wrapper twentytwenty-container active">';
				foreach($image_list as $image){
					$left_image .= '<img width="350px"  height="350px"  src="'.URL($image).'" alt="">';
				}
				$left_image.='<div class="twentytwenty-handle"><span class="twentytwenty-left-arrow"></span><span class="twentytwenty-right-arrow"></span></div></div></div></div></div>';
			}
		}


		if($homedata["right_item_type"]=="image")	{			
			$image_list = json_decode($homedata["img_right"]);
			$image_url = isset($image_list[0]) ? URL($image_list[0]) : "";
			if(!empty($image_url)){
				$right_image = "<img src='".$image_url."' width='100%' />";
			}	
		}	
		else if($homedata["right_item_type"]=="youtube")	{	
			if(!empty($homedata["img_right"])) {
				$right_image = "<iframe src='".$homedata["img_right"]."'  width='350px'  height='350px'  class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
			}				
		}	
		else if($homedata["right_item_type"]=="video")	{		
			$image_list = json_decode($homedata["img_right"]);
			$video_url = isset($image_list[0]) ? URL($image_list[0]) : "";
			if(!empty($video_url)){
				$right_image = '<video  width="350px" height="350px" controls><source src="'.$video_url.'" type="video/mp4"></video>';
			}	
		}
		else if($homedata["right_item_type"]=="book")	{			
			$image_list = json_decode($homedata["img_right"]);
			$right_image = '<div class="h-c-wrapper"><div class="cont-wrapper"><div class="book-wrapper">';
			$index=0;
			foreach($image_list as $image){				
				$right_image .= ($index==0) ? '<img  width="350px"  height="350px" src="'.URL($image).'" class="active" />' : '<img  width="350px"  height="350px" src="'.URL($image).'" />';
				$index++;	
			}
			$right_image .='</div></div></div>';
		}
		else if($homedata["right_item_type"]=="flip")	{		
			$image_list = json_decode($homedata["img_right"]);
			if(count($image_list)>0){				
				$right_image = '<div class="h-c-wrapper"><div class="cont-wrapper"><div class="twentytwenty-wrapper twentytwenty-horizontal"><div class="before-after-wrapper twentytwenty-container active" style="height: 442.016px;">';
				foreach($image_list as $image){
					$right_image .= '<img width="350px"  height="350px" src="'.URL($image).'" alt="" style="clip: rect(0px, 140.016px, 442.016px, 0px);">';
				}
				$right_image.='<div class="twentytwenty-handle" style="left: 27.0156px;"><span class="twentytwenty-left-arrow"></span><span class="twentytwenty-right-arrow"></span></div></div></div></div></div>';
			}
		}
	
		$config = Configuration::where('language','=',$locale)->first();
		$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
		$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
		$newslettertext = NewsletterText::where('language','=',$locale)->first();
		$labellist = Label::get();
		$labeldata = array();
		foreach($labellist as $label){
			$labeldata[$label->label][$label->language] = $label->name;
		}
		return view('welcome')->with(compact('projects','services','title','homedata','locale','menus','submenus','config','newslettertext','ishomepage','labeldata','left_image','right_image'));
	}

	public function getProjectList(Request $request) {
		$input = $request->all();
		$locale = App::getLocale();
		$offset = $input["offset"];	
		$row_number =1;		
        $projects = array();
		$project = Project::select("id","title","subtitle","language","orderby")
					->where('language',$locale)
					->where('is_home',1)
					->orderBy("orderby","ASC")
					->offset($this->limit*$offset)
					->take($this->limit)->get();
		foreach ($project as $key => $value) {
			
			$projectdetails = Projectdetails::select("id","type","datavalue")->where("projectid",$value->id)->orderBy("display_order","ASC")->first();
			if(!empty($projectdetails)) {
				$projectList = array();
				$projectList["projectId"] = $value->id;
				$projectList["title"] = $value->title;
				$projectList["subtitle"] = $value->subtitle;
				$projectList["language"] = $value->language;
				$projectList["orderby"] = $value->orderby;
				$projectList["projectdetailsId"] = $projectdetails->id;
				$projectList["datavalue"] = $projectdetails->datavalue;
				$projectList["type"] = $projectdetails->type;
				$projects[] =$projectList;
			}
		}

        $output = "";
        $number=1;
        $next_index_no_two = 2; $next_index_no_three=3;
        $last_record=false;
		$inner_arr = count($projects);
		foreach($projects as $project)	{
			if($row_number==$next_index_no_two) {
				$output	 .= "<div class='row row".$number."'>";	
				$output	 .= "<div class='col col-l'>";	
				$number++;			
			}else if($row_number==$next_index_no_three)	{
				$output	 .= "<div class='col col-r'>";	
			}else{
				$output	 .= "<div class='row row".$number."'>";		
				$output	 .= "<div class='col'>";	
				$number++;			
			}	
			if($project['type']=="image")	{
				$output	 .= "<a href='".url('projectdetails/'.$project['projectId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'>";	
				$output	 .= "<img src=".URL('images/project/'.$project['datavalue'])." />";
				$output	 .= "</div><p class='project-title'>".$project['title']."</p></div></a>";	
			} else if($project['type']=="youtube"){
				$output	 .= "<a href='".url('projectdetails/'.$project['projectId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='youtube-video-wrapper'>";
				$output	 .= "<iframe src='".$project['datavalue']."'  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
				$output	 .= "</div></div><p class='project-title'>".$project['title']."</p></div></a>";	
			}elseif($project['type']=='video'){
				$output	 .="<a href='".url('projectdetails/'.$project['projectId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='video-wrapper'>";
				$output	 .="<video width='100%' controls><source src='".URL('images/project/'.$project['datavalue'])."' type='video/mp4'></video>";
				$output	 .="</div></div><p class='project-title'>{{$project['title']}}</p></div></a>";	
			}
			elseif($project['type']=='book'){
				$image_list = json_decode($project['datavalue']);
				$output	 .="<a href='".url('projectdetails/'.$project['projectId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='book-wrapper'>";
				$index=0; 
				foreach($image_list as $image) { 
					$output	 .=($index==0) ? "<img  width='100%'  src=''.URL('images/project/'.$image).'' class='active' />" : "<img width='100%' src=".URL('images/project/'.$image)." />";
					$index++;	
				}
				$output	.="</div></div><p class='project-title'>{{$project['title']}}</p></div></a>";	
			}
			elseif($project['type']=='flip'){
				$image_list = json_decode($project['datavalue']);
				if(count($image_list)>0){ 
					$output	 .="<a href='".url('projectdetails/'.$project['projectId'])."'><div class='h-c-wrapper'><div class='cont-wrapper'><div class='twentytwenty-wrapper twentytwenty-horizontal'><div class='before-after-wrapper twentytwenty-container active' style='height: 442.016px;'>";
					foreach($image_list as $image) {
						$output	 .="<img width='100%'   src='".URL('images/project/'.$image)."' alt='' style='clip: rect(0px, 140.016px, 442.016px, 0px);'>";
					}
					$output	 .="<div class='twentytwenty-handle' style='left: 27.0156px;'><span class='twentytwenty-left-arrow'></span><span class='twentytwenty-right-arrow'></span></div></div></div></div><p class='project-title'>".$project['title']."</p></div></a>";
							
				}}
			if($row_number>=$inner_arr && $row_number==$next_index_no_three)		
				$output	 .="</div></div>";
			elseif($row_number!=$next_index_no_two)
				$output	 .="</div>";
			elseif($inner_arr==$row_number)
				$output	 .="</div></div>";
			
			if($row_number==$next_index_no_two)
				$next_index_no_two = $next_index_no_two+4;
			if($row_number==$next_index_no_three)	
				$next_index_no_three = $next_index_no_three+4;	
			
			$output	 .="</div>";
			$row_number++; 
			if($row_number>=5) {
				$row_number=1;
				$next_index_no_two=2;
				$next_index_no_three=3;
				$number=1;
			}
		}	
		$totalRecord = Project::where('language',$locale)->count();
		if($totalRecord>($this->limit*$offset)){
			$isLastRecord = false;	
		}
		else{
			$isLastRecord = true;	
		}
		return response()->json(['success' => true, 'message' => 'successfully get project list','data'=>$output,'isLastRecord'=>$isLastRecord]);
	}
}


