<?php
	
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\HomeSectionOne;
use Validator;
use Session;
use File;
use DB;

class FrontController extends Controller
{	
	public function __construct()
	{
		$this->middleware('auth');
		$this->messages = [
		'required' => 'The :attribute is required.',
		];
	}	
	
	public function index()
	{
		$title = 'Home Page Listing';	
		$sectiononedata = HomeSectionOne::all();		
		return view('admin.home.listing')->with(compact('sectiononedata','title'));
	}
	
	public function create()
	{
		$title = 'Home Page Create';			
		return view('admin.home.create')->with(compact('title'));
	}
		
	public function store(Request $request)
	{
		
		$input = $request->all();
		$data = array();
		$data['language']=$input['lang'];
		$data['project_service_title']= $input['project_service_title'];
		$data['left_item_type']= $input['left_item_type'];
		$data['right_item_type']= $input['right_item_type'];
		$data['middle_section_title']= $input['middle_section_title'];
		$data['middle_section_description']= $input['middle_section_description'];
		$data['middle_section_view_more']= $input['middle_section_view_more'];			
		$data['project_main_title']= $input['project_main_title'];
		$rightImageList  = array();
		$leftImageList = array();
		if ($request->hasFile('img_left')) {
			foreach ($request->file('img_left') as $file) {
				$destinationPath = 'images/home/';
				$publicDestinationPath = public_path('images/home/');
				$fileExtension   = $file->getClientOriginalExtension();
				$fileName        = 'image' . rand() . "_" . date("Y_m_d") . "." . $fileExtension;
				$imagePath       = $destinationPath . $fileName;
				$file->move($publicDestinationPath, $fileName);
				$leftImageList[] = $imagePath;
			}
			$data['img_left'] = json_encode($leftImageList);
		}
		else{
			$data['img_left'] = $input["img_left"];
		}
		if ($request->hasFile('img_right')) {
			foreach ($request->file('img_right') as $file) {
				$destinationPath = 'images/home/';
				$publicDestinationPath = public_path('images/home/');
				$fileExtension   = $file->getClientOriginalExtension();
				$fileName        = 'image' . rand() . "_" . date("Y_m_d") . "." . $fileExtension;
				$imagePath       = $destinationPath . $fileName;
				$file->move($publicDestinationPath, $fileName);
				$rightImageList[] = $imagePath;
			}
			$data['img_right'] = json_encode($rightImageList);
		}
		else{
			$data['img_right'] = $input["img_right"];
		}
		if(HomeSectionOne::create($data))
		{
			$request->session()->flash('alert-success', 'Home page added successfully.');
		}
		return redirect('front/home');		
	}

	public function edit($sectiononeid){		
		$title = 'Home Page Edit';		
		$sectiononedata = HomeSectionOne::find($sectiononeid);					
		return view('admin.home.edit')->with(compact('title','sectiononedata','sectiononeid'));
	}

	public function update(Request $request){		
		$input = $request->all();
		$sectiononedata = HomeSectionOne::find($request['id']);		
		if(empty($sectiononedata))
		{
			$request->session()->flash('alert-danger', trans('Some problem occured in update home page details.'));
			return redirect('front/home');
		}	
		$data = array();
		$sectiononedata->language=$input['lang'];
		$sectiononedata->project_service_title= $input['project_service_title'];
		$sectiononedata->left_item_type= $input['left_item_type'];
		$sectiononedata->right_item_type= $input['right_item_type'];
		$sectiononedata->middle_section_title= $input['middle_section_title'];
		$sectiononedata->middle_section_description= $input['middle_section_description'];
		$sectiononedata->middle_section_view_more= $input['middle_section_view_more'];			
		$sectiononedata->project_main_title= $input['project_main_title'];
		$rightImageList  = array();
		$leftImageList = array();
		if ($request->hasFile('img_left')) {
			foreach ($request->file('img_left') as $file) {
				$destinationPath = 'images/home/';
				$publicDestinationPath = public_path('images/home/');
				$fileExtension   = $file->getClientOriginalExtension();
				$fileName        = 'image' . rand() . "_" . date("Y_m_d") . "." . $fileExtension;
				$imagePath       = $destinationPath . $fileName;
				$file->move($publicDestinationPath, $fileName);
				$leftImageList[] = $imagePath;
			}
			$sectiononedata->img_left = json_encode($leftImageList);
		}
		else if(!empty($input["img_left"])){
			$sectiononedata->img_left = $input["img_left"];
		}

		if ($request->hasFile('img_right')) {
			foreach ($request->file('img_right') as $file) {
				$destinationPath = 'images/home/';
				$publicDestinationPath = public_path('images/home/');
				$fileExtension   = $file->getClientOriginalExtension();
				$fileName        = 'image' . rand() . "_" . date("Y_m_d") . "." . $fileExtension;
				$imagePath       = $destinationPath . $fileName;
				$file->move($publicDestinationPath, $fileName);
				$rightImageList[] = $imagePath;
			}
			$sectiononedata->img_right = json_encode($rightImageList);
		}
		else if(!empty($input["img_right"])){			
			$sectiononedata->img_right  = $input["img_right"];
		}
		if($sectiononedata->update()){
			$request->session()->flash('alert-success', trans('Home page details updated successfully.'));
		}
		return redirect('front/home');		
	}

	public function destroy($sectiononeid){
		$sectiononedata = HomeSectionOne::where('id','=',$sectiononeid)->forcedelete();		
		Session::flash('alert-success', 'Home Page Deleted Successfully!');		
		return redirect('front/home');
	}
}
