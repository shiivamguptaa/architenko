<?php	
namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Studio;
use App\StudioDeails;
use App\StudioPosition;
use App\StudioTnC;
use App\StudioTncDetails;
use App\StudioApplyDetails;
use Validator;
use DB;
use File;
use Session;
use App\Configuration;
use App\HomeSectionOne;
use App\NewsletterText;
use App\Label;
use App\Menu;
use App\StudioApplyForm;
use App\EmailForm;
use Mail;

class StudioListController extends Controller {
	
	public function __construct(){		
		
	}
	
	public function index(){
		
		$title = 'Studio Data Listing';	
		$locale = App::getLocale();
		$studiolist = Studio::where('language',$locale)->orderBy("id","DESC")->first();			
		$studiodetailslist = array(); 
		$studiopositionlist = array();
		$studioTermConditionList = array();
		$studioTermConditionDetailsList = array();
		$termconditionList = array();
		if(!empty($studiolist)){
			$termconditionList = StudioTncDetails::select("studio_tnc.title as title","studio_tnc_details.title as imageTitle","studio_tnc_details.image as image","studio_tnc_details.description as description")->join("studio_tnc","studio_tnc.id","studio_tnc_details.tnc_id")->where('show_in_privacy_policy',1)->orderBy("studio_tnc_details.id","DESC")->first();	

			$studiodetailslist = StudioDeails::where('studio_id',$studiolist->id)->orderBy("id","ASC")->get();		
			$studiopositionlist = StudioPosition::where('studio_id',$studiolist->id)->orderBy("display_order","ASC")->get();
			$studioTermConditionList = StudioTnC::where('studio_id',$studiolist->id)->orderBy("orderby","ASC")->get();	
			foreach ($studioTermConditionList as $key => $value) {
				$studioTermConditionDetailsList[] = StudioTncDetails::where('tnc_id',$value->id)->orderBy("id","ASC")->get();
			}				
		}
		$studioapplyform = StudioApplyForm::where('language',$locale)->orderBy("id","DESC")->first();	
		$config = Configuration::where('language','=',$locale)->first();
		$menus = Menu::where('language','=',$locale)->where('parent',0)->orderBy("orderby","ASC")->get();
		$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->orderBy("orderby","ASC")->get();
		$newslettertext = NewsletterText::where('language','=',$locale)->first();
		$homedata = HomeSectionOne::where('language','=',$locale)->orderBy("id","DESC")->first();		
		return view('studiolist')->with(compact('title','studiolist','studiodetailslist','studiopositionlist','studioTermConditionList','studioTermConditionDetailsList','locale','menus','submenus','config','newslettertext','homedata','studioapplyform','termconditionList'));
	}	
	public function storeStudioApplyForm(Request  $request){
		$input = $request->all();
		$data = array();
		$locale = App::getLocale();
		$data["language"] = $locale;
		$data["privacy_policy_accept_title"] = isset($input["privacy_policy_accept_title"]) ?  "YES" : "NO";
		$data["language_input"] = isset($input["language_input"]) ?  $input["language_input"] : "";
		$data["position_title"] = isset($input["position_title"]) ?  $input["position_title"] : "";
		$data["position1"] = isset($input["position1"]) ?  $input["position1"] : "";
		$data["position2"] = isset($input["position2"]) ?  $input["position2"] : "";
		$data["position3"] = isset($input["position3"]) ?  $input["position3"] : "";
		$data["position4"] = isset($input["position4"]) ?  $input["position4"] : "";
		$data["gender"] = isset($input["gender"]) ?  $input["gender"] : "";
		$data["first_name"] = isset($input["first_name"]) ?  $input["first_name"] : "";
		$data["middle_name"] = isset($input["middle_name"]) ?  $input["middle_name"] : "";
		$data["last_name"] = isset($input["last_name"]) ?  $input["last_name"] : "";
		$data["address"] = isset($input["address"]) ?  $input["address"] : "";
		$data["city"] = isset($input["city"]) ?  $input["city"] : "";
		$data["postal_code"] = isset($input["postal_code"]) ?  $input["postal_code"] : "";
		$data["country"] = isset($input["country"]) ?  $input["country"] : "";
		$data["phone"] = isset($input["phone"]) ?  $input["phone"] : "";
		$data["email"] = isset($input["email"]) ?  $input["email"] : "";
		$data["dob"] = isset($input["dob"]) ?  $input["dob"] : NULL;
		$data["country1"] = isset($input["nationality1"]) ?  $input["nationality1"] : "";
		$data["country2"] = isset($input["nationality2"]) ?  $input["nationality2"] : "";
		$data["institution"] = isset($input["institution"]) ?  $input["institution"] : "";
		$data["degree"] = isset($input["degree"]) ?  $input["degree"] : "";
		$data["start_year"] = isset($input["start_year"]) ?  $input["start_year"] : "";		
		$data["end_year"] = isset($input["end_year"]) ?  $input["end_year"] : "";
		$data["language_list"] = isset($input["language_list"]) ?  $input["language_list"] : "";
		$data["other_language"] = isset($input["other_language"]) ?  $input["other_language"] : "";		
		$data["experience_years"] = isset($input["experience_years"]) ?  $input["experience_years"] : "";
		$data["experience_months"] = isset($input["experience_months"]) ?  $input["experience_months"] : "";
		$data["experience_countries"] = isset($input["experience_countries"]) ?  $input["experience_countries"] : "";
		$data["applied_at_architenko"] = isset($input["applied_at_architenko"]) ?  $input["applied_at_architenko"] : "";
		$data["aboutus"] = isset($input["aboutus"]) ?  $input["aboutus"] : "";
		$data["comments"] = isset($input["comments"]) ?  $input["comments"] : "";
		$data["profesional_situation"] = isset($input["profesional_situation"]) ?  $input["profesional_situation"] : "";












		if($request->hasFile('cover_letter')) {
			$file = $request->cover_letter;
			$dataimage = $file->getClientOriginalName();
			$time = strtotime(now()).rand(1,10000000);
			$filename = $time.strtolower(str_replace(" ","_",$dataimage));
			$path_image = public_path('/images/studio');
			$file->move($path_image, $filename);	
			$data["cover_letter"]  = $filename;
		}	
		if($request->hasFile('curriculum_vitae')) {
			$file = $request->curriculum_vitae;
			$dataimage = $file->getClientOriginalName();
			$time = strtotime(now()).rand(1,10000000);
			$filename = $time.strtolower(str_replace(" ","_",$dataimage));
			$path_image = public_path('/images/studio');
			$file->move($path_image, $filename);	
			$data["curriculum_vitae"]  = $filename;
		}
		if($request->hasFile('portfolio')) {
			$file = $request->portfolio;
			$dataimage = $file->getClientOriginalName();
			$time = strtotime(now()).rand(1,10000000);
			$filename = $time.strtolower(str_replace(" ","_",$dataimage));
			$path_image = public_path('/images/studio');
			$file->move($path_image, $filename);
			$data["portfolio"]  = $filename;
		}
  	






		$clientdata = array();
		$clientdata["full_name"] = $data["first_name"]." ".$data["middle_name"];		
		$clientdata["last_name"] = $data["last_name"];
		$clientdata["country"]   = $data["country"];
		$clientdata["address"]   = $data["address"];
		$clientdata["phone"]     = $data["phone"];
		$clientdata["email"]     = $data["email"];
		$clientdata["city"] 	 = $data["city"];

  		$emailForm = EmailForm::where("formname","privacypolicy")->get();
		foreach ($emailForm as $key => $value) {
			$to = $value->email;
			Mail::send('email-templates.form', $clientdata, function($message) use ($to) {
				$message->to($to, 'Architenko');
				$message->subject('Studio Apply Form');
				$message->from('info@architenko.com','Architenko');			
			});
		}	

  		if(StudioApplyDetails::create($data)) {
			return response()->json(['success' => true, 'message' => "Successfully send message"]);
		}
		else {
			return response()->json(['success' => false, 'message' => "Something went wrong,Please try again later..."]);
		}	
	}	
}