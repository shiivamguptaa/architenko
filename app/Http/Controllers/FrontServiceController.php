<?php
	
	namespace App\Http\Controllers;
	use App;
	use Illuminate\Http\Request;
	use App\Configuration;
	use App\NewsletterText;
	use DB;
	use App\Menu;
	use App\ServiceTopdescription;
	
	
	class FrontServiceController extends Controller
	{
		/**
			* Create a new controller instance.
			*
			* @return void
		*/
		public function __construct()
		{
			//$this->middleware('auth');
		}
		
		/**
			* Show the application dashboard.
			*
			* @return \Illuminate\Contracts\Support\Renderable
		*/
		public function index()
		{
			$locale = App::getLocale();
			$title = 'Services';
			
			$services = DB::table('service')
            ->join('servicedetails', 'service.id', '=', 'servicedetails.serviceid')
            ->select('servicedetails.*', 'service.*')
            ->where('is_home',0)
            ->where('servicedetails.language',$locale)
			->orderBy('servicedetails.id', 'asc')
            ->get();
			
			$config = Configuration::where('language','=',$locale)->first();
			$menus = Menu::where('language','=',$locale)->where('parent',0)->get();
			$submenus = Menu::where('language','=',$locale)->where('parent','!=',0)->get();
			$newslettertext = NewsletterText::where('language','=',$locale)->first();
			$servicetopdescription = ServiceTopdescription::where('language','=',$locale)->first();
			
			return view('services')->with(compact('services','config','title','locale','menus','submenus','newslettertext','servicetopdescription'));
			
		}
	}
