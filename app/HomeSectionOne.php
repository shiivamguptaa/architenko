<?php	
	namespace App;	
	use Illuminate\Notifications\Notifiable;
	use Illuminate\Contracts\Auth\MustVerifyEmail;
	use Illuminate\Foundation\Auth\User as Authenticatable;
	
	class HomeSectionOne extends Authenticatable
	{
		use Notifiable;
		
		/**
			* The attributes that are mass assignable.
			*
			* @var array
		*/
		protected $table = 'home_section_one';
		protected $primaryKey = 'id';
		protected $fillable = ['left_item_type','right_item_type','project_service_title',
        'img_left', 'img_right','language','middle_section_title','middle_section_description','middle_section_view_more','project_main_title'
		];
		
		
		/**
			* The attributes that should be hidden for arrays.
			*
			* @var array
		*/
		
	}
