<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ProjectInnerContent extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'projectinnercontent';
    protected $fillable = [
        'title','projectid','description','image','display_order','language'
    ];
	
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
}
