<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ContactUsAddress extends Authenticatable
{
    use Notifiable;
    protected $table = 'contactus_address';  
    protected $fillable = [
    	"contactus_id",
		"title",		
		"address",
		"phone_number",
		"description",
		"display_order",
		"direction",
		"pop_image",
	];

}
