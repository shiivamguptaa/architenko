<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Studio extends Authenticatable
{
    use Notifiable;
	protected $table = 'studio';
    protected $fillable = [
        'id',
        'language',
        'orderby',
        'title_img_type',
        'title_img',
        'profile_title',
        'profile_description',
        'profile_right_img_type',
        'profile_right_img',
        'founder_title',
        'founder_right_img_type',
        'founder_right_img',
        'founder_description',
        'join_architenko_title',
        'join_architenko_description',
        'opened_positions_title',
        'terms_and_conditions',
        'award_title',
        'award_description',
        'award_slider',
        'language',
        'catagory'
    ];

}
