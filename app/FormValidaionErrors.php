<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class FormValidaionErrors extends Authenticatable
{
    use Notifiable;
	protected $table = 'form_validation_errors';
    protected $fillable = [
        'form_name','errors','language'
    ];
	
}
