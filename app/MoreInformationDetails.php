<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MoreInformationDetails extends Authenticatable
{
    use Notifiable;
    protected $table = 'more_information_details';  
    protected $fillable = [
    	"service_id",
        "project_address",
        "description_project",
        "gender",
        "first_name",
        "middle_name",
        "last_name",
        "address",
        "city",
        "postal_code",
        "country",
        "phone",
        "email",
        "dob",
        "person_position",
        "private_individual_first_name",
        "private_individual_middle_name",
        "private_individual_last_name",
        "private_individual_address",
        "private_individual_city",
        "private_individual_postal_code",
        "private_individual_country",
        "private_individual_phone",
        "private_individual_email",
        "private_individual_dob",
        "enterprise_price_title1",
        "enterprise_price_title2",
        "enterprise_address_title",
        "enterprise_city_title",
        "enterprise_postal_code_title",
        "enterprise_country_title",
        "enterprise_phone_title",
        "enterprise_email_title",
        "enterprise_contact_first_name_title",
        "enterprise_contact_last_name_title",
        "enterprise_position",
        "language"
	];   
}
