<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ProjectCatagory extends Authenticatable
{

    protected $table = 'project_catagory';
    protected $fillable = [
        'catagory','status','language','orderby'
    ];
	
}
