<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Servicedetails extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'servicedetails';
    protected $fillable = [
        'type','datavalue','is_home','serviceid','moretitle','display_order','language' 
    ];
	
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
}
