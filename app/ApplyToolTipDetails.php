<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ApplyToolTipDetails extends Authenticatable
{
    use Notifiable;
    protected $table = 'apply_tooltip_details';  
    protected $fillable = [
    	"article_id",
        "you_found_through",
        "you_found_through_other",
        "choose_toolkit",
        "choose_toolkit_other",
        "position",
        "private_individual_title",
        "private_individual_address_title",
        "private_individual_gender",
        "private_individual_first_name",
        "private_individual_middle_name",
        "private_individual_last_name",
        "private_individual_address",
        "private_individual_city",
        "private_individual_postal_code",
        "private_individual_country",
        "private_individual_phone",
        "private_individual_email",
        "private_individual_dob",
        "enterprise_price_title1",
        "enterprise_price_title2",
        "enterprise_address_title",
        "enterprise_city_title",
        "enterprise_postal_code_title",
        "enterprise_country_title",
        "enterprise_phone_title",
        "enterprise_email_title",
        "enterprise_gender",
        "enterprise_contact_first_name_title",
        "enterprise_contact_last_name_title",
        "enterprise_contact_personpositionlist_title",
        "language"
	];   
}
