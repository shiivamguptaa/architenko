<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ErrorMessage extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'error_message';
    protected $fillable = [
        'email_enter_message','email_invalid_message','language'
    ];
	
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
}
