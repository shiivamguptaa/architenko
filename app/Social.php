<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Social extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	 protected $table = 'sociallinks';
    protected $fillable = [
        'name','link',
    ];
	
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
}
