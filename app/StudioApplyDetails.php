<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StudioApplyDetails extends Authenticatable
{
    use Notifiable;
    protected $table = 'studio_apply_details';  
    protected $fillable = [
    	"privacy_policy_accept_title",
        "language_input",
        "position_title",
        "position1",
        "position2",
        "position3",
        "position4",
        "gender",
        "first_name",
        "middle_name",
        "last_name",
        "address",
        "city",
        "postal_code",
        "country",
        "phone",
        "email",
        "dob",
        "country1",
        "country2",
        "institution",
        "degree",
        "start_year",
        "end_year",
        "language_list",
        "other_language",
        "profesional_situation",
        "experience_years",
        "experience_months",
        "experience_countries",
        "applied_at_architenko",
        "aboutus",
        "comments",
        "cover_letter",
        "curriculum_vitae",
        "portfolio",
        "language"

	];
    protected $casts = [
        'institution'=>'array',
        'degree' => 'array',
        'start_year' => 'array',
        'end_year' => 'array'
    ];
}
