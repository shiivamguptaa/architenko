<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StudioPosition extends Authenticatable
{
    use Notifiable;

	protected $table = 'studio_position';
    protected $fillable = [
        'studio_id','description','image','title','image_title','display_order'
    ];
}
