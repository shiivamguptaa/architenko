$(document).ready(function() {





        $('.book-wrapper').mousemove(function(e) {
            var ww = $(this).width();
            var wl = $(this).find('img').length;
            var interval = ww/wl;
            var showImg;
            var rect = e.target.getBoundingClientRect();
            var x = e.clientX - rect.left;
            showImg = Math.round(x/interval)-1;
            $(this).find('img').removeClass('active');            
            if (showImg>0) {
                $(this).find('img').eq(showImg).addClass('active');
            }else{
                $(this).find('img').eq(0).addClass('active');
            }
        })

        $('.book-wrapper').mouseout(function(e) {
            $(this).find('img').removeClass('active');
            $(this).find('img').eq(0).addClass('active');
        })

});
