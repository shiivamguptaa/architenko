$(document).ready(function() {

    $('.inner-slider').slick({
        infinite: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        // autoplay: true,
        autoplaySpeed: 2000,
        speed:1000,
    });

    $('.img-slider').slick({
        infinite: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        // autoplay: true,
        autoplaySpeed: 2000,
        asNavFor: '.quote-slider',
    });
 

    $('.quote-slider').slick({
        asNavFor: '.img-slider',
    });

    if ( $(window).width() > 992) {
        $('.project-slider-wrapper .read-more').on('click', function(){
            // $(this).parent().next().slideToggle( "slow", function() {});

            $(".project-info").slideToggle( "slow", function() {});
            $('.project-slider-wrapper .read-more').css('display','none')
            $('.project-slider-wrapper .hide').css('display','inline-block')
        });
        $('.project-slider-wrapper .hide').on('click', function(){
            $(".project-info").slideToggle( "slow", function() {});
            $('.project-slider-wrapper .hide').css('display','none')
            $('.project-slider-wrapper .read-more').css('display','inline-block')
        });
    }

 });
