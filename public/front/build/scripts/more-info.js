$(document).ready(function() {
    $('#service-select').select2({
        placeholder: 'Select from the list',
        allowClear: true,
        width: '100%'
    });
    $('#nationality-select').select2({
        placeholder: 'Nationality',
        width: '100%'
    });
    $('#person-position').select2({
        placeholder: 'select from the list',
        width: '100%'
    });
    $('#enterprise-position').select2({
        placeholder: 'select from the list',
        width: '100%'
    });


    $("#date_of_birth").datepicker({
       dateFormat: "dd-mm-yy",
       changeMonth: true,
       changeYear: true,
       yearRange: '-115:+10',
       beforeShow: function () {
           setTimeout(function (){
           $('.ui-datepicker').css('z-index', 99999999999999);

           }, 0);
       },

   }).on('change', function() {
       if($('#date_of_birth').valid()){
           $('#date_of_birth').removeClass('errRed');
       }

   });

   $("#more-info-wrapper .add-input").on('click', function(){
       $('#nationality-select-2').select2({
           placeholder: $('#nationality-select-2').attr("placeholder"),
           width: '100%'
       });
       $("#more-info-wrapper .add-input").hide();
   });

    $(".add-input1").on('click', function(){
       $('#nationality-select-app-2').select2({
           placeholder: $('#nationality-select-app-2').attr("placeholder"),
           width: '100%'
       });
       $(".add-input1").hide();
   });

   $(".pop-up-big .hide").on('click', function(){
         $(this).parents(".pop-up-big").removeClass('active')
         $('body').removeClass('over')
   });

   $(document).keyup(function(e) {
        if (e.key === "Escape") { // escape key maps to keycode `27`
            $(".pop-up-big").removeClass('active')
            $('body').removeClass('over')
        }
    });

   $(".subscribe-form .more-info").on('click', function(){
       $("#more-info-wrapper").addClass('active')
        $('body').addClass('over')
   });

   $(document).keydown(function(e) {
       if (e.key === "ArrowUp" || e.key === "ArrowDown") {
            if ($( ".pop-up-big" ).hasClass('active')) {
                $('.pop-up-big.active').attr("tabindex",-1).focus()
            }
        }
    });
});
