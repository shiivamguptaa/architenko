$(document).ready(function() {

    var $slickElement = $('.inner-slider');

    $slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){ 
        $('#pic-description').html($('.slick-current').find('.inner').attr('data-description'))
        $('iframe')[0].contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*')
    });
    var observer = lozad();
    observer.observe();
    // var totalOffset;
    // function setHeightOfSlider(){
    //     if ( $(window).width() < 992) {
    //         totalOffset=130
    //     }
    //     $('.inner-slider-wrapper').height($(window).height()-(totalOffset))
    // }
    //
    // $(window).resize(function(){
    //     setHeightOfSlider();
    // });
    //
    // $(window).on('load', function(){
    //     setHeightOfSlider();
    // })
    // setHeightOfSlider();

    $slickElement.slick({
        infinite: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        // autoplay: true,
        autoplaySpeed: 2000,
        speed:1000,
    });

    $(document).keydown(function(e) {
         if (e.key === "ArrowRight") {
             $(".next").click()
         }
         if (e.key === "ArrowLeft") {
             $(".prev").click()
         }
     });

 });

$(function() {

    var $status = $('.project-n');
    var $slickElement = $('.img-slider');

    $slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
        $('.project-slider-wrapper').find('.pic-description').html($('.slick-current').find('.inner').attr('data-description'))
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $status.text(i + '/' + slick.slideCount);
    });
    var observer = lozad();
    observer.observe();

    $slickElement.slick({
        lazyLoad: 'ondemand',
        infinite: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        // autoplay: true,
        autoplaySpeed: 2000,
        speed:1000,

    });


    $(document).keydown(function(e) {
         if (e.key === "ArrowRight") {
             $(".next").click()
         }
         if (e.key === "ArrowLeft") {
             $(".prev").click()
         }
     });

    // if ( $(window).width() > 992) {
    // var totalOffset;
    // function setHeightOfSlider(){
    //     if ( $(window).width() < 992) {
    //         totalOffset=119
    //     }else{
    //         totalOffset=154
    //     }
    //     $('.inner-slider-wrapper').height($(window).height()-(totalOffset))
    // }
    //
    // $(window).resize(function(){
    //     setHeightOfSlider();
    // });
    //
    // $(window).on('load', function(){
    //     setHeightOfSlider();
    // })
    // setHeightOfSlider();




        $('.project-summary .read-more').on('click', function(){
            // $(this).parent().next().slideToggle( "slow", function() {});

            $(".project-info").slideToggle( "slow", function() {});
            $('.project-summary .read-more').css('display','none')
            $('.project-summary .hide').css('display','inline-block')
        });
        $('.project-slider-wrapper .hide').on('click', function(){
            $(".project-info").slideToggle( "slow", function() {});
            $('.project-summary .hide').css('display','none')
            $('.project-summary .read-more').css('display','inline-block')
        });


    // }

})
$(window).on('load', function(){
    var txt = 'Hi, how are you?';
    var speed = 50;
    var i = 0;
    var textAnimation ="";

    function typeWriter() {
       if (i < txt.length) {
           textAnimation += txt.charAt(i);
           $('.search-input').attr('placeholder',textAnimation)
           i++;
           setTimeout(typeWriter, speed);

       }
       else{
           setTimeout(typeWriterSecond, 2000);
       }
   }

   var txtS = 'Surf through the projects and get inspired!';
   var y = 0;
   var textAnimationS ="";

   function typeWriterSecond() {
        textAnimation="";
       if (y < txtS.length) {
           textAnimationS += txtS.charAt(y);
           $('.search-input').attr('placeholder',textAnimationS)
           y++;
           setTimeout(typeWriterSecond, speed);
       }
   }

   typeWriter()
})


 var addEducation = $(".edu-content").html();
$('.studio-slider').slick({
    infinite: true,
    autoplaySpeed: 2000,
    speed:1000,
    slidesToShow: 1,
    variableWidth: true,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
    responsive: [
      {
        breakpoint: 992,
        settings: {
            variableWidth: false,
            slidesToShow: 1
        }
    }]
});

$("#apply-wrapper .add-input").on('click', function(){
   $('#nationality-select-app-2').select2({
       placeholder: 'Nationality',
       width: '100%'
   });
   $("#apply-wrapper .add-input").hide();
});

$("#apply-wrapper .add-other-lang").on('click', function(){
   $("#apply-wrapper .lang-input").css('display','block');
   $("#apply-wrapper .add-other-lang").hide();
});

$(".pop-up .hide").on('click', function(){
     $(this).parents(".pop-up").removeClass('active');
     $('body').removeClass('over');
});

$(".studio .open-tc").on('click', function(){
    let id = $(this).data("id");
    let title = $(this).data("title");
    $(".term_condition_title").html(title);
    $("#tc-wrapper"+id).addClass('active');
    $('body').addClass('over');
});

$(".studio .open-imprint").on('click', function(){
    $(".imprint-wrapper").addClass('active');
     $('body').addClass('over');
});

// $(".studio .view-position").on('click', function(){
//     $(".position-wrapper").addClass('active')
//      $('body').addClass('over')
// });

$(".open-apply").on('click', function(){
    $(".position-wrapper").removeClass('active');
    $("#apply-wrapper").addClass('active');
     $('body').addClass('over');
});


$("#apply-wrapper .close-icon").on('click', function(){
    $("#apply-wrapper").removeClass('active');
    $('body').removeClass('over');
});
$(".position-wrapper .close-icon").on('click', function(){
    $(".position-wrapper").removeClass('active');
    $('body').removeClass('over');
});



$(".open-pp").on('click', function(){
    $("#apply-wrapper").removeClass("active");
    $(".privacy-poicy-wrapper").addClass('active');
     $('body').addClass('over');
});

$(".privacy-poicy-wrapper .close-icon").on('click', function(){
    $(".privacy-poicy-wrapper").removeClass('active');
    $("#apply-wrapper").addClass('active');
    $('body').addClass('over');
});

$("#open-read-team").on('click', function(){
    $("#view-team-wrapper").addClass('active');
     $('body').addClass('over');
});

$("#read-more-awards").on('click', function(){
    $("#awards-wrapper").addClass('active');
     $('body').addClass('over');
});
$("#open-extended-profile").on('click', function(){
    $(".extended-profile-wrapper").addClass('active');
     $('body').addClass('over');
});


$(".studio .view-position").on('click', function(){
    let id = $(this).data("id");
    $("#position"+id).addClass('active');
    $('body').addClass('over');
});


$(".open-direction").on('click', function(){
    let id = $(this).data("id");
    $("#directions"+id).addClass('active');
    $('body').addClass('over');
});

$(".close-direction").on('click', function(){
     let id = $(this).data("id");
    $("#directions"+id).removeClass('active');
    $('body').removeClass('over');
});
$(".moreinformation").on('click', function(){
    $("#moreInformationForm")[0].reset();
    $("#more-info-wrapper").addClass('active');
    $('body').addClass('over');
});

$('#nationality-select-app').select2({
    placeholder: $('#nationality-select-app').attr("placeholder"),
    width: '100%'
});
$('#nationality-select-app2').select2({
    placeholder: $('#nationality-select-app2').attr("placeholder"),
    width: '100%'
});
$('#nationality-select-app1').select2({
    placeholder: $('#nationality-select-app1').attr("placeholder"),
    width: '100%'
});
$('.start-year').select2({
    placeholder: "Start Year",
    width: '100%'
});
$('.end-year').select2({
    placeholder: "End Year",
    width: '100%'
});


$(".add-education").on("click",function() {
    $('.edu-content').last().append(addEducation);
    $('.start-year').select2({
    placeholder: "Start Year",
    width: '100%'
});
$('.end-year').select2({
    placeholder: "End Year",
    width: '100%'
});
});
$('#experience-years').select2({
    width: '100%',
    minimumResultsForSearch: -1
});
$('#experience-months').select2({
    width: '100%',
    minimumResultsForSearch: -1
});
$('#experience-countries').select2({
    width: '100%',
    minimumResultsForSearch: -1
});
var id1 = $('.person_position').find("option:selected").attr("id");
if (id1 != undefined || id1 != null) {
    if(id1=="privateIndividual"){
        $('.enterprise-data-wrapper').css('display', 'none')
        $('.private-data-wrapper').css('display', 'block')
    }else{
        $('.private-data-wrapper').css('display', 'none')
        $('.enterprise-data-wrapper').css('display', 'block')
    }
}
$(".person_position").change(function(){
var id = $(this).find("option:selected").attr("id");
console.log(id);
if(id=="privateIndividual"){
    $('.enterprise-data-wrapper').css('display', 'none')
    $('.private-data-wrapper').css('display', 'block')
}else{
    $('.private-data-wrapper').css('display', 'none')
    $('.enterprise-data-wrapper').css('display', 'block')
}
});

$('#enterprise-position-toolkit').select2({
    width: '100%',
});
 $(".open-download-article").on('click', function(){
    if($("#ApplyToopKitFrom").length){
        $("#ApplyToopKitFrom")[0].reset();
    }
    $("#article-form").addClass('active')
    $('body').addClass('over')
});
 $('#iam').select2({
    placeholder: 'select from the list',
    width: '100%'
});
$('#select-toolkit').select2({
    placeholder: 'select from the list',
    width: '100%'
});

$("#iam").change(function(){
    var id = $(this).find("option:selected").attr("id");
    if(id == "privateIndividual"){
        $('.enterprise-data-wrapper').css('display', 'none')
        $('.private-data-wrapper').css('display', 'block')
    }else{
        $('.private-data-wrapper').css('display', 'none')
        $('.enterprise-data-wrapper').css('display', 'block')
    }
});

$(document).bind('ajaxStart', function(){
    $('.loader').removeClass('hide');
}).bind('ajaxStop', function(){
    $('.loader').addClass('hide');
});