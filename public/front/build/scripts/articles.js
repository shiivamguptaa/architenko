$(window).on('load', function(){
    var txt = 'Hi, how are you?';
    var speed = 50;
    var i = 0;
    var textAnimation ="";

    function typeWriter() {
       if (i < txt.length) {
           textAnimation += txt.charAt(i);
           $('.search-input').attr('placeholder',textAnimation)
           i++;
           setTimeout(typeWriter, speed);

       }
       else{
           setTimeout(typeWriterSecond, 2000);
       }
   }

   var txtS = 'Search the articles or go for the Toolkits!';
   var y = 0;
   var textAnimationS ="";

   function typeWriterSecond() {
        textAnimation="";
       if (y < txtS.length) {
           textAnimationS += txtS.charAt(y);
           $('.search-input').attr('placeholder',textAnimationS)
           y++;
           setTimeout(typeWriterSecond, speed);
       }
   }

   typeWriter()

   $('.search-bar-bg').on('click',function(e){
       $('.result-wrapper').removeClass('active')
       $('.search-bar-bg').removeClass('active')
       $('.search-bar').removeClass('active')
       $('.input-wrapper').removeClass('active')
       $('body').removeClass('over')
   })
})

$(function() {
    $('.search-bar-bg').on('click',function(e){
        $('.result-wrapper').removeClass('active')
        $('.search-bar-bg').removeClass('active')
        $('.search-bar').removeClass('active')
        $('.input-wrapper').removeClass('active')
        $('body').removeClass('over')
    })
    // $(".result-padding").niceScroll(".result-inner", {
    //    cursorwidth: "2px",
    //    cursorborder:'none',
    //     cursorborderradius:4,
    //     autohidemode:'leave',
    //     cursoropacitymin:1,
    //  });
    // $(".result-inner").niceScroll({
    //     cursorwidth: "2px",
    //     cursorborder:'none',
    //     cursorborderradius:4,
    //     autohidemode:'leave',
    //     cursoropacitymin:1,
    //     mousescrollstep:10,
    //     scrollspeed:10
    //  });

    var sentData = {};
    var scrollValue=0;
    var liSelected;
     $('.search-input').keyup(function(e) {
         if(e.which !== 40 && e.which !== 38){

             sentData.searchText = $(this).val()
              if (sentData.searchText) {
                  sendToServer(sentData)
              }else{
                  $('.result-wrapper').removeClass('active')
                  $('.search-bar-bg').removeClass('active')
                  $('.search-bar').removeClass('active')
                  $('.input-wrapper').removeClass('active')
                  $('body').removeClass('over')
              }
              scrollValue=0;
              $(".result-inner").scrollTop(scrollValue);
              liSelected="";
          }

    });
    $(document).keyup(function(e) {
         if (e.key === "Escape") { // escape key maps to keycode `27`
             $('.result-wrapper').removeClass('active')
             $('.search-bar-bg').removeClass('active')
             $('.search-input').removeClass('active')
             $('body').removeClass('over')
         }
    });

    $('.article-wrapper .download-btn').on('click', function(){
        $(this).parents('.article-wrapper').find('.download-form').toggleClass('active')
    })


    $(".open-download-article").on('click', function(){
        $("#article-form").addClass('active')
         $('body').addClass('over')
    });

    function sendToServer(sentData){
        // $.ajax({
        //     url: '',
        //     type: 'POST',
        //     data: JSON.stringify(sentData),
        //     contentType: 'application/json; charset=utf-8',
        //     dataType: 'json',
        //     success: function(result) {
        //         bulidDom(result)
        //     }
        // });

        //dummy data
        var result = {
            categories : {
                0: {
                    'name':'category 1',
                    'url':'google.com'
                },
                1: {
                    'name':'category 2',
                    'url':'google.com'
                },
                2: {
                    'name':'category 3',
                    'url':'google.com3'
                },
                3: {
                    'name':'category 1',
                    'url':'google.com'
                },
                4: {
                    'name':'category 2',
                    'url':'google.com'
                },
                5: {
                    'name':'category 3',
                    'url':'google.com'
                }
            },
            articles : {
                0: {
                    'name':'Article 1',
                    'category':'Category 1',
                    'url':'google.com'
                },
                1: {
                    'name':'Article 2',
                    'category':'Category 2',
                    'url':'google.com'
                },
                2: {
                    'name':'Article 3',
                    'category':'Category 3',
                    'url':'google.com'
                },
                3: {
                    'name':'Article 1',
                    'category':'Category 1',
                    'url':'google.com'
                },
                4: {
                    'name':'Article 2',
                    'category':'Category 2',
                    'url':'google.com'
                },
                5: {
                    'name':'Article 3',
                    'category':'Category 3',
                    'url':'google.com'
                },
                6: {
                    'name':'Article 1',
                    'category':'Category 1',
                    'url':'google.com'
                },
                7: {
                    'name':'Article 2',
                    'category':'Category 2',
                    'url':'google.com'
                },
                8: {
                    'name':'Article 3',
                    'category':'Category 3',
                    'url':'google.com'
                },
                9: {
                    'name':'Article 1',
                    'category':'Category 1',
                    'url':'google.com'
                },
                10: {
                    'name':'Article 2',
                    'category':'Category 2',
                    'url':'google.com'
                },
                11: {
                    'name':'Article 3',
                    'category':'Category 3',
                    'url':'google.com'
                }
            }
        }

        bulidDom(result);
    }

    var categories;
    var articles;
    var domC="";
    var domA="";
    var li;
    var max;
    function bulidDom(result){

        categories = result.categories;
        articles = result.articles;
        $(".search-bar .cat-result").empty();
        $(".search-bar .pro-result").empty();


        for (i in categories) {
            domC = '<li>'+
                        '<a href="'+categories[i].url+'">'+categories[i].name+' <span class="hr"></span></a>'+
                    '</li>'

            $(".search-bar .cat-result").append(domC);
        }

        for (i in articles) {
            domA = '<li>'+
                        '<a href="'+articles[i].url+'">'+articles[i].name+'<span class="hr"></span></a><span class="cat">'+articles[i].category+'</span>'+
                    '</li>'

            $(".search-bar .cat-result").append(domA);
        }

        $('.result-wrapper').addClass('active')
        $('.search-bar-bg').addClass('active')
        $('.search-bar').addClass('active')
        $('.input-wrapper').addClass('active')
        $('body').addClass('over')
        $(".result-inner").getNiceScroll().resize();

        li = $('.result-wrapper li');
        // $('.result-inner').attr("tabindex",-1).focus();
        max = $(".result-inner li").length*29
    }


    $(window).keydown(function(e){

        console.log(max);
        if(e.which === 40){
            if(liSelected){
                if (scrollValue>=0 && scrollValue<max) {
                    scrollValue+=29;
                }
                $(".result-inner").scrollTop(scrollValue);
                liSelected.removeClass('selected');
                next = liSelected.next();
                if(next.length > 0){
                    liSelected = next.addClass('selected');
                }else{
                    liSelected = li.last().addClass('selected');
                }
            }else{
                liSelected = li.eq(0).addClass('selected');
            }
        }else if(e.which === 38 ){
            if(liSelected){
                console.log(scrollValue);
                console.log(max);
                if (scrollValue>=29) {
                    scrollValue-=29;
                }
                $(".result-inner").scrollTop(scrollValue);

                liSelected.removeClass('selected');
                next = liSelected.prev();
                if(next.length > 0){
                    liSelected = next.addClass('selected');
                }else{
                    liSelected = li.eq(0).addClass('selected');
                }
            }else{
                liSelected = li.eq(0).addClass('selected');
            }
        }
        else if (e.which === 39 || e.which === 37) {
            return false
        }
        else if(e.which === 13){
            $(liSelected).find('a')[0].click();
        }

    });

    $('#enterprise-position-toolkit').select2({
        placeholder: 'select from the list',
        width: '100%'
    });
    $('#iam').select2({
        placeholder: 'select from the list',
        width: '100%'
    });
    $('#select-toolkit').select2({
        placeholder: 'select from the list',
        width: '100%'
    });

    $("#iam").change(function(){
        var id = $(this).find("option:selected").attr("id");

        if(id == "privateIndividual"){
            $('.enterprise-data-wrapper').css('display', 'none')
            $('.private-data-wrapper').css('display', 'block')
        }else{
            $('.private-data-wrapper').css('display', 'none')
            $('.enterprise-data-wrapper').css('display', 'block')
        }
    });



})
