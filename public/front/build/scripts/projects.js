$(window).on('load', function(){
    var txt = 'Hi, how are you?';
    var speed = 50;
    var i = 0;
    var textAnimation ="";

    function typeWriter() {
       if (i < txt.length) {
           textAnimation += txt.charAt(i);
           $('.search-input').attr('placeholder',textAnimation)
           i++;
           setTimeout(typeWriter, speed);

       }
       else{
           setTimeout(typeWriterSecond, 2000);
       }
   }

   var txtS = 'Surf through the projects and get inspired!';
   var y = 0;
   var textAnimationS ="";

   function typeWriterSecond() {
        textAnimation="";
       if (y < txtS.length) {
           textAnimationS += txtS.charAt(y);
           $('.search-input').attr('placeholder',textAnimationS)
           y++;
           setTimeout(typeWriterSecond, speed);
       }
   }

   typeWriter()
})


$(function() {
    // $(".result-padding").niceScroll(".result-inner", {
    //    cursorwidth: "2px",
    //    cursorborder:'none',
    //     cursorborderradius:4,
    //     autohidemode:'leave',
    //     cursoropacitymin:1,
    //  });
     // $(".result-inner").niceScroll({
     //     cursorwidth: "2px",
     //     cursorborder:'none',
     //      cursorborderradius:4,
     //      autohidemode:'leave',
     //      cursoropacitymin:1,
     //  });

    var sentData = {};
     $('.search-input').keyup(function(e) {
         if(e.which !== 40 && e.which !== 38){

             sentData.searchText = $(this).val()
              if (sentData.searchText) {
                  sendToServer(sentData)
              }else{
                  $('.result-wrapper').removeClass('active')
                  $('.search-bar-bg').removeClass('active')
                  $('.search-bar').removeClass('active')
                  $('.input-wrapper').removeClass('active')
                  $('body').removeClass('over')
              }
              scrollValue=0;
              $(".result-inner").scrollTop(scrollValue);
              liSelected="";
          }

    });

    $(document).keyup(function(e) {
         if (e.key === "Escape") { // escape key maps to keycode `27`
             $('.result-wrapper').removeClass('active')
             $('.search-bar-bg').removeClass('active')
             $('.search-input').removeClass('active')
             $('body').removeClass('over')
         }
    });
    function sendToServer(sentData){
        // $.ajax({
        //     url: '',
        //     type: 'POST',
        //     data: JSON.stringify(sentData),
        //     contentType: 'application/json; charset=utf-8',
        //     dataType: 'json',
        //     success: function(result) {
        //         bulidDom(result)
        //     }
        // });

        //dummy data
        var result = {
            categories : {
                0: {
                    'name':'category 1',
                    'url':'google.com'
                },
                1: {
                    'name':'category 2',
                    'url':'google.com'
                },
                2: {
                    'name':'category 3',
                    'url':'google.com'
                },
                3: {
                    'name':'category 1',
                    'url':'google.com'
                },
                4: {
                    'name':'category 2',
                    'url':'google.com'
                },
                5: {
                    'name':'category 3',
                    'url':'google.com'
                }
            },
            projects : {
                0: {
                    'name':'Project 1',
                    'category':'Category 1',
                    'url':'google.com'
                },
                1: {
                    'name':'Project 2',
                    'category':'Category 2',
                    'url':'google.com'
                },
                2: {
                    'name':'Project 3',
                    'category':'Category 3',
                    'url':'google.com'
                },
                3: {
                    'name':'Project 1',
                    'category':'Category 1',
                    'url':'google.com'
                },
                4: {
                    'name':'Project 2',
                    'category':'Category 2',
                    'url':'google.com'
                },
                5: {
                    'name':'Project 3',
                    'category':'Category 3',
                    'url':'google.com'
                }
            }
        }

        bulidDom(result);
    }

    var categories;
    var projects;
    var domC="";
    var domP="";
    var li;
    var max;

    function bulidDom(result){

        categories = result.categories;
        projects = result.projects;
        $(".search-bar .cat-result").empty();
        $(".search-bar .pro-result").empty();


        for (i in categories) {
            domC = '<li>'+
                        '<a href="'+categories[i].url+'">'+categories[i].name+' <span class="hr"></span></a>'+
                    '</li>'

            $(".search-bar .cat-result").append(domC);
        }

        for (i in projects) {
            domP = '<li>'+
                        '<a href="'+projects[i].url+'">'+projects[i].name+'<span class="hr"></span></a><span class="cat">'+projects[i].category+'</span>'+
                    '</li>'

            $(".search-bar .pro-result").append(domP);
        }

        $('.result-wrapper').addClass('active')
        $('.search-bar-bg').addClass('active')
        $('.search-bar').addClass('active')
        $('.input-wrapper').addClass('active')
        $('body').addClass('over')
        $(".result-inner").getNiceScroll().resize();

        li = $('.result-wrapper li');
        // $('.result-inner').attr("tabindex",-1).focus();
        max = $(".result-inner li").length*29
    }

    $(window).keydown(function(e){

        console.log(max);
        if(e.which === 40){
            if(liSelected){
                if (scrollValue>=0 && scrollValue<max) {
                    scrollValue+=29;
                }
                $(".result-inner").scrollTop(scrollValue);
                liSelected.removeClass('selected');
                next = liSelected.next();
                if(next.length > 0){
                    liSelected = next.addClass('selected');
                }else{
                    liSelected = li.last().addClass('selected');
                }
            }else{
                liSelected = li.eq(0).addClass('selected');
            }
        }else if(e.which === 38 ){
            if(liSelected){
                console.log(scrollValue);
                console.log(max);
                if (scrollValue>=29) {
                    scrollValue-=29;
                }
                $(".result-inner").scrollTop(scrollValue);

                liSelected.removeClass('selected');
                next = liSelected.prev();
                if(next.length > 0){
                    liSelected = next.addClass('selected');
                }else{
                    liSelected = li.eq(0).addClass('selected');
                }
            }else{
                liSelected = li.eq(0).addClass('selected');
            }
        }
        else if (e.which === 39 || e.which === 37) {
            return false
        }
        else if(e.which === 13){
            $(liSelected).find('a')[0].click();
        }

    });
})
