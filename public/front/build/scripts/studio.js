$(document).ready(function() {

    $('.studio-slider').slick({
        // autoplay: true,
        infinite: true,
        autoplaySpeed: 2000,
        speed:1000,
        slidesToShow: 1,
        variableWidth: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        responsive: [
          {
            breakpoint: 992,
            settings: {
                variableWidth: false,
                slidesToShow: 1
            }
        }]
    });
    $(document).keydown(function(e) {
         if (e.key === "ArrowRight") {
             if (!$(".pop-up-big").hasClass('active')) {
                  $(".next").click()
                  console.log('da');
             }
         }
         if (e.key === "ArrowLeft") {
             if (!$(".pop-up-big").hasClass('active')) {
                 $(".prev").click()
             }
         }
     });

    $('#nationality-select-app').select2({
        placeholder: 'Nationality',
        width: '100%'
    });

    $('#start-year').select2({
        placeholder: 'Start Year',
        width: '100%'
    });

    $('#end-year').select2({
        placeholder: 'End Year',
        width: '100%'
    });
    $('#experience-years').select2({
        width: '100%',
        minimumResultsForSearch: -1
    });
    $('#experience-months').select2({
        width: '100%',
        minimumResultsForSearch: -1
    });
    $('#experience-countries').select2({
        width: '100%',
        minimumResultsForSearch: -1
    });

     function generateSelects(){
        var selectStart = $('.select-start');
        for (var i = 0; i < selectStart.length; i++) {
            $('#start-year-'+i+'').select2({
                placeholder: 'Start Year',
                width: '100%'
            });

        }

        var selectEnd = $('.select-end');
        for (var i = 0; i < selectEnd.length; i++) {
            $('#end-year-'+i+'').select2({
                placeholder: 'End Year',
                width: '100%'
            });
        }
    }

    var count=0;
    var education;
    $(".add-education").on("click",function(){
        console.log(count);
        education = '<div class="edu-content"> ' +
            '<label for="">Institution:</label>' +
            '<input type="text">' +
            '<label for="">Degree:</label>' +
            '<input type="text">' +
            '<div class="row years-wrapper">' +
                '<div class="col-l">' +
                    '<select name="" class="select-start" id="start-year-'+count+'">' +
                        '<option value="1991">1991</option>' +
                        '<option value="1992">1992</option>' +
                    '</select>' +
                '</div>' +
                '<div class="col-r">' +
                    '<select name="" class="select-end" id="end-year-'+count+'">' +
                        '<option value="1991">1991</option>' +
                        '<option value="1992">1992</option>' +
                    '</select>' +
                '</div>' +
            '</div>' +
        '</div>';
        $('.edu-content').last().append(education);
        generateSelects()
        count++;
    })

    $("#date_of_birth-app").datepicker({
       dateFormat: "dd-mm-yy",
       changeMonth: true,
       changeYear: true,
       yearRange: '-115:+10',
       beforeShow: function () {
           setTimeout(function (){
           $('.ui-datepicker').css('z-index', 99999999999999);

           }, 0);
       },

   }).on('change', function() {
       if($('#date_of_birth-app').valid()){
           $('#date_of_birth-app').removeClass('errRed');
       }
   });






   $("#apply-wrapper .add-input").on('click', function(){
       $('#nationality-select-app-2').select2({
           placeholder: 'Nationality',
           width: '100%'
       });
       $("#apply-wrapper .add-input").hide();
   });

   $("#apply-wrapper .add-other-lang").on('click', function(){
       $("#apply-wrapper .lang-input").css('display','block');
       $("#apply-wrapper .add-other-lang").hide();
   });

    $(".pop-up .hide").on('click', function(){
         $(this).parents(".pop-up").removeClass('active')
         $('body').removeClass('over')
    });

    $(".studio .open-tc").on('click', function(){
        $(".tc-wrapper").addClass('active')
         $('body').addClass('over')
    });

    $(".studio .open-imprint").on('click', function(){
        $(".imprint-wrapper").addClass('active')
         $('body').addClass('over')
    });

    $(".studio .view-position").on('click', function(){
        $(".position-wrapper").addClass('active')
         $('body').addClass('over')
    });

    $(".open-apply").on('click', function(){
        $(".position-wrapper").removeClass('active')
        $("#apply-wrapper").addClass('active')
         $('body').addClass('over')
    });

    $(".open-pp").on('click', function(){
        $(".pop-up-big .hide").click();
        $(".privacy-poicy-wrapper").addClass('active')
         $('body').addClass('over')
    });

    $(".privacy-poicy-wrapper .hide").on('click', function(){
        $("#apply-wrapper").addClass('active')
        $(".privacy-poicy-wrapper").removeClass('active');
         $('body').addClass('over')
    });

    $("#open-read-team").on('click', function(){
        $("#view-team-wrapper").addClass('active')
         $('body').addClass('over')
    });

    $("#read-more-awards").on('click', function(){
        $("#awards-wrapper").addClass('active')
         $('body').addClass('over')
    });
    $("#open-extended-profile").on('click', function(){
        $(".extended-profile-wrapper").addClass('active')
         $('body').addClass('over')
    });
 });
