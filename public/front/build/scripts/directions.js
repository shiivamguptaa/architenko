$(document).ready(function() {


   $(".directions-wrapper .hide").on('click', function(){
        $("#dw-first").removeClass('active')
        $("#dw-second").removeClass('active')
         $('body').removeClass('over')
   });

   $(".open-direction-first").on('click', function(){
       $("#dw-first").addClass('active')
        $('body').addClass('over')
   });

   $(".open-direction-second").on('click', function(){
       $("#dw-second").addClass('active')
        $('body').addClass('over')
   });
   $(document).keyup(function(e) {
        if (e.key === "Escape") { // escape key maps to keycode `27`
            $("#dw-first").removeClass('active')
            $("#dw-second").removeClass('active')
            $('body').removeClass('over')
        }
    });
});
