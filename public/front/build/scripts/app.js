
$(document).ready(function() {
        var socket_height;
        $(window).on('load', function() {
            if($("#socket").length){
                socket_height = $("#socket").outerHeight(true)+10;
            }
        });
        var top = $(window).scrollTop();
        var direction;
        var widnow_height = $(window).height();
            $(window).scroll(function(){              
                var cur_top = $(window).scrollTop();
                 if($("#header").length){
                    if ( $(window).width() > 992) {                       
                        if (cur_top > 29) {
                            $("#header").addClass('resize');
                        }else{
                            $("#header").removeClass('resize');
                            $("#header").removeClass('move-top');
                        }
                        if (cur_top > 580) {
                            $("#header").addClass('move-top');
                            $("#header").addClass('show');
                        }
                        else{
                            $("#header").removeClass('show');
                        }
                        if (cur_top > widnow_height) {
                            $('#back-to-top').addClass('show');
                        }else{
                            $('#back-to-top').removeClass('show');
                        }
                        if (cur_top==0) {
                            $('#footer').css('transform', 'translateY(0px)');
                        }
                        else{
                            $('#footer').css('transform', 'translateY('+socket_height+'px)');
                        }
                        if(top < cur_top && cur_top > 380){
                            $("#header").removeClass('show');
                        }
                        else{
                            $("#header").addClass('show');
                        }
                        if(!(top < cur_top && cur_top > 380)){
                            $("#header").addClass('show');
                        }
                    }
                    else 
                    {
                        if (cur_top > 0) 
                        {
                        }else{
                            $("#header").removeClass('move-top');
                        }
                        if (cur_top > 0) {
                            $("#header").addClass('move-top');
                            $("#header").addClass('show');
                        }
                        else{
                            $("#header").removeClass('show');
                        }

                        if(top < cur_top && cur_top > 80){
                            $("#header").removeClass('show');
                        }
                        else{
                            $("#header").addClass('show');
                        }
                    }
                }
                top = cur_top;
            });



    $("#header .menu-bar").on('click', function(){
        $("#header .main-nav-wrapper").addClass('active');
         $('body').addClass('over');
    });
    $("#header .close-btn").on('click', function(){
        $("#header .main-nav-wrapper").removeClass('active');
        $('body').removeClass('over');
    });
    $("#back-to-top").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });


    if ( $(window).width() < 992) {
        $("#header .services-link").css('pointer-events','none');
    }
    // $('#main-nav a').click(function(){
    //     $('html, body').animate({
    //         scrollTop: $( $(this).attr('href') ).offset().top
    //     }, 500);
    //     return false;
    // });
});
