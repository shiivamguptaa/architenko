$(function() {

    var $status = $('.project-n');
    var $slickElement = $('.img-slider');

    $slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
        $('.project-slider-wrapper').find('.pic-description').html($('.slick-current').find('.inner').attr('data-description'))
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $status.text(i + '/' + slick.slideCount);
    });
    var observer = lozad();
    observer.observe();
    
    $slickElement.slick({
        lazyLoad: 'ondemand',
        infinite: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        // autoplay: true,
        autoplaySpeed: 2000,
        speed:1000,

    });


    $(document).keydown(function(e) {
         if (e.key === "ArrowRight") {
             $(".next").click()
         }
         if (e.key === "ArrowLeft") {
             $(".prev").click()
         }
     });

    // if ( $(window).width() > 992) {
    // var totalOffset;
    // function setHeightOfSlider(){
    //     if ( $(window).width() < 992) {
    //         totalOffset=119
    //     }else{
    //         totalOffset=154
    //     }
    //     $('.inner-slider-wrapper').height($(window).height()-(totalOffset))
    // }
    //
    // $(window).resize(function(){
    //     setHeightOfSlider();
    // });
    //
    // $(window).on('load', function(){
    //     setHeightOfSlider();
    // })
    // setHeightOfSlider();




        $('.project-summary .read-more').on('click', function(){
            // $(this).parent().next().slideToggle( "slow", function() {});

            $(".project-info").slideToggle( "slow", function() {});
            $('.project-summary .read-more').css('display','none')
            $('.project-summary .hide').css('display','inline-block')
        });
        $('.project-slider-wrapper .hide').on('click', function(){
            $(".project-info").slideToggle( "slow", function() {});
            $('.project-summary .hide').css('display','none')
            $('.project-summary .read-more').css('display','inline-block')
        });


    // }

})
