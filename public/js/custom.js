(function ($) {
    $('#footer').DataTable({
      "paging": true,      
      "ordering": true,
      "info": true,
	  "aoColumnDefs": [
		  { 'bSortable': false, 'aTargets': [ 5 ] }
	  ]	
    });
	$('#citylisting').DataTable({
	  "aoColumnDefs": [
		  { 'bSortable': false, 'aTargets': [ 3 ] }
	  ]	
    });
	$('#sectionlisting').DataTable({
	  "aoColumnDefs": [
		  { 'bSortable': false, 'aTargets': [ 6 ] }
	  ]	
    });
})(jQuery);

