@extends('layouts.login')

@section('content')
<div class="wrapper wrapper-full-page">
	<div class="full-page login-page" filter-color="black" data-image="{{asset('assets/img/login.jpeg')}}">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form method="POST" action="{{ route('login') }}">
							@csrf
                            <div class="card card-login">
                                <div class="card-header text-center" data-background-color="rose">
                                    <h4 class="card-title">Login</h4>                                    
								</div>
                                <p class="category text-center">
                                    
								</p>
                                <div class="card-content">
                                    
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
										</span>
										
                                        <div class="form-group label-floating is-empty">                                            
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email address" required autocomplete="email" autofocus>
											<span class="material-input"></span>
											@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
										</span>
                                        <div class="form-group label-floating is-empty">                                           
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">
											<span class="material-input"></span>
											@error('password')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
								</div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-rose btn-simple btn-wd btn-lg">Let's go</button>
									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
        <footer class="footer">
			<div class="container">				
				<p class="copyright pull-center">
					© <script>document.write(new Date().getFullYear())</script><a href="#"> Architenko. All Rights Reserved. </a>   
				</p>
			</div>
		</footer>		
	<div class="full-page-background" style="background-image: url(../../assets/img/login.jpeg) "></div></div>	
</div>
<style>.pull-center {text-align: center;}</style>
@endsection
