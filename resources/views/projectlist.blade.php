@extends('layouts.front')
@section('content')
<div class="main  projects-page">
     <div class="container project-section" id="projects-wrapper">
        <?php $number=1; $row_number=1; $next_index_no_two = 2; $next_index_no_three=3;
        $inner_arr = count($projects);
        ?>
        <div class="row search-bar-row">
            <div class="col">
                <div class="search-bar">
                    <div class="input-wrapper">
                        <input type="text" name="search" class="search-input" placeholder="Surf through the projects and get inspired!" autocomplete="off" />
                    </div>
                    <div class="result-wrapper">
                        <div class="result-padding">
                            <div class="result-inner" id="result-inner">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @foreach($projects as $project) 
                @if($row_number==$next_index_no_two)
                    <div class="row row{{$number}}">    
                    <div class="col col-l">         
                    <?php $number++;  ?>    
                @elseif($row_number==$next_index_no_three)  
                    <div class="col col-r">
                @else
                    <div class="row row{{$number}}">
                    <div class="col">
                        <?php $number++;  ?>    
                @endif  
                @if($project['type']=="image")  
                 <a href="{{ url('projectdetails/'.$project['projectId']) }}">
                    <div class="h-c-wrapper">                       
                        <div class="cont-wrapper">  
                            <img src="{{URL('images/project/'.$project['datavalue'])}}" />
                        </div>
                       <p class='project-title'>{{$project['title']}}</p>
                    </div>                  
                </a>    
                @elseif($project['type']=="youtube")
                 <a href="{{ url('projectdetails/'.$project['projectId']) }}">
                    <div class="h-c-wrapper">                       
                        <div class="cont-wrapper">                  
                            <div class="youtube-video-wrapper">
                                <iframe src="{{$project['datavalue']}}"  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                            </div>                          
                        </div>
                          <p class='project-title'>{{$project['title']}}</p>                     
                    </div>                                   
                </a>    
                @elseif($project['type']=="video")
                 <a href="{{ url('projectdetails/'.$project['projectId']) }}">
                    <div class="h-c-wrapper">                       
                        <div class="cont-wrapper">
                            <div class="video-wrapper">
                                <video width='100%' controls><source src="{{URL('images/project/'.$project['datavalue'])}}" type="video/mp4"></video>
                            </div>                                  
                        </div>                                      
                     <p class='project-title'>{{$project['title']}}</p>   
                    </div>      
                </a>    
                @elseif($project['type']=="book")
                    <?php  $image_list = json_decode($project['datavalue']);    ?>
                    <a href="{{ url('projectdetails/'.$project['projectId']) }}">
                        <div class="h-c-wrapper">
                            <div class="cont-wrapper">
                                <div class="book-wrapper">
                                    <?php $index=0; foreach($image_list as $image) { 
                                        echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/project/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/project/".$image).'" />';
                                        $index++;   
                                    } ?>
                                </div>
                            </div>
                            <p class='project-title'>{{$project['title']}}</p>                             
                        </div>                                        
                    </a>    
                @elseif($project['type']=="flip")
                    <?php $image_list = json_decode($project['datavalue']);
                        if(count($image_list)>0){ ?>
                             <a href="{{ url('projectdetails/'.$project['projectId']) }}">
                                <div class="h-c-wrapper"><div class="cont-wrapper"><div class="twentytwenty-wrapper twentytwenty-horizontal"><div class="before-after-wrapper twentytwenty-container active">
                            <?php foreach($image_list as $image){
                                echo '<img width="100%"   src="'.URL("images/project/".$image).'" alt="">';
                            } ?>
                            <div class="twentytwenty-handle"><span class="twentytwenty-left-arrow"></span><span class="twentytwenty-right-arrow"></span></div></div></div></div>
                            <p class='project-title'>{{$project['title']}}</p>   
                            </div>                                                        </a>
                            <?php 
                        }
                    ?>
                @endif          
            @if($row_number>=$inner_arr && $row_number==$next_index_no_three)       
                </div></div>
            @elseif($row_number!=$next_index_no_two)
                </div>  
            @elseif($inner_arr==$row_number)
                </div>
                </div>
            @endif 
            @if($row_number==$next_index_no_two)
                <?php $next_index_no_two = $next_index_no_two+4; ?> 
            @endif 
            @if($row_number==$next_index_no_three)  
                    <?php $next_index_no_three = $next_index_no_three+4; ?> 
            @endif
        </div>
        <?php $row_number++; if($row_number>=5){$row_number=1;$next_index_no_two=2;$next_index_no_three=3;$number=1;} ?>
        @endforeach
         {{ $pagination_links }}
        <div class="row subscribe-row-project">
            <div class="col">
                <div class="subscribe-form">
                    <p>{!! $newslettertext['description'] !!}   </p>        
                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                    </div><br />
                    @endif
                    @if (\Session::has('failure'))
                    <div class="alert alert-danger">
                    <p>{{ \Session::get('failure') }}</p>
                    </div><br />
                    @endif
                    <form id="newsletterForm" method="POST" action="{{url('newsletter/store')}}">
                        @csrf
                        <input type="email" class="input email-input" id="input" name="email" placeholder="E-mail:" />
                        <input type="submit" class="submit submit-input" value="{{getLabel('subscribe')}}" /> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>	
<div class="search-bar-bg"></div>
@endsection						


