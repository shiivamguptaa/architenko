<!DOCTYPE html>
<html>
	<head>
		<title></title>
	</head>
	<body>
		<table border="1" style="border: 1px solid black;border-collapse: collapse;width: 75%;">
			<tbody>
				<tr>
					<th>First Name</th>
					<td><?=$full_name;?></td>
				</tr>
				<tr>
					<th>Last Name</th>
					<td><?=$last_name;?></td>
				</tr>
				<tr>
					<th>Email</th>
					<td><?=$email;?></td>
				</tr>
				<tr>
					<th>City</th>
					<td><?=$city;?></td>
				</tr>
				<tr>
					<th>Country</th>
					<td><?=$country;?></td>
				</tr>
				<tr>
					<th>Address</th>
					<td><?=$address;?></td>
				</tr>
				<tr>
					<th>Phone</th>
					<td><?=$phone;?></td>
				</tr>		
			</tbody>
		</table>
	</body>
</html>