@extends('layouts.front')

@section('content')
<div class="main">
    <div class="container" id="article-wrapper">
        <div class="row search-bar-row">
            <div class="col">
                <div class="search-bar">
                    <div class="input-wrapper">
                        <input type="text" class="search-input" id="search-input" placeholder="">
                    </div>

                    <div class="result-wrapper">
                        <div class="result-padding">
                            <div class="result-inner" id='result-inner'>
                                <ul class="cat-result"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		@if(isset($articles[0]->title))
        <div class="row row1">
            <div class="col">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[0]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[0]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[0]->short_description !!}</p>
                    </div>
                    <div class="row">
                        <a href="#" class="read-more">Read more</a>
                    </div>
                </div>
            </div>
        </div>
		@endif
        <div class="row row2">
			@if(isset($articles[1]->title))
            <div class="col-l">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[1]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[1]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[1]->short_description !!}</p>
                    </div>
                </div>
            </div>
			@endif
			@if(isset($articles[2]->title))
            <div class="col-r">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[2]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[2]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[2]->short_description !!}</p>
                    </div>                    
                </div>
            </div>
			@endif
        </div>
		@if(isset($articles[3]->title))
        <div class="row row3">
            <div class="col">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[3]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[3]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[3]->short_description !!}</p>
                    </div>
                </div>
            </div>
        </div>
		@endif
		@if(isset($articles[4]->title))
        <div class="row row4">
            <div class="col">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[4]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[4]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[4]->short_description !!}</p>
                    </div>
                </div>
            </div>
        </div>
		@endif
        <div class="row row2">
			@if(isset($articles[5]->title))
            <div class="col-l">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[5]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[5]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[5]->short_description !!}</p>
                    </div>
                </div>
            </div>
			@endif
			@if(isset($articles[6]->title))
            <div class="col-r">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[6]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[6]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[6]->short_description !!}</p>
                    </div>
                </div>
            </div>
			@endif
        </div>
		@if(isset($articles[7]->title))
        <div class="row row3">
            <div class="col">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[7]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[7]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[7]->short_description !!}</p>
                    </div>
                </div>
            </div>
        </div>
		@endif
		@if(isset($articles[8]->title))
        <div class="row row4">
            <div class="col">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[8]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[8]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[8]->short_description !!}</p>
                    </div>
                </div>
            </div>
        </div>
		@endif
        <div class="row row2">
			@if(isset($articles[9]->title))
            <div class="col-l">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[9]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[9]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[9]->short_description !!}</p>
                    </div>
                </div>
            </div>
			@endif
			@if(isset($articles[10]->title))
            <div class="col-r">
                <div class="article-wrapper">
                    <h3 class="article-title">{{$articles[10]->title}}</h3>
                    <img src="{{asset('images/articles/'.$articles[10]->main_image)}}"  alt="">
                    <div class="text-wrapper">
                        <p>{!! $articles[10]->short_description !!}</p>
                    </div>
                </div>
            </div>
			@endif
        </div>
		@if(isset($articles[11]->title))
        <div class="row row3">
            <div class="col">
                <div class="article-wrapper">
                    <h3 class="article-title">$articles[11]->title</h3>
                    <img src="{{asset('images/articles/'.$articles[11]->main_image)}}"  alt="">
                    <div class="text-wrapper">
					<p>{!! $articles[11]->short_description !!}</p>
                    </div>
                </div>
            </div>
        </div>
		@endif
        <div class="pagination">
            {{ $articles->links() }}
        </div>

    </div>	
    <div class="container">
        <div class="row">
		 <div class="col-lg-12 pd-15">
            <div class="subscribe-form">
					<p>{!! $newslettertext['description'] !!}	</p>		
					@if (\Session::has('success'))
					<div class="alert alert-success">
					<p>{{ \Session::get('success') }}</p>
					</div><br />
					@endif
					@if (\Session::has('failure'))
					<div class="alert alert-danger">
					<p>{{ \Session::get('failure') }}</p>
					</div><br />
					@endif
					<form method="POST" action="{{url('newsletter/store')}}">
						@csrf
						@if (\Session::get('locale')=="en")
                        <input type="email" oninvalid="setCustomValidity('Enter email id')" class="input" name="email" placeholder="E-mail:" required>
                        @else
                        <input type="email" oninvalid="setCustomValidity('Voer e-mail ID in')" class="input" name="email" placeholder="E-mail:" required>
                        @endif
						<input type="submit" class="submit" value="{{getLabel('submit')}}">
					</form>
				</div>
			</div>
        </div>
    </div>
</div>																																		
@endsection						