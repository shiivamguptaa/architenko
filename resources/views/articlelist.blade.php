@extends('layouts.front')
@section('content')
<div class="main articlelist-page">
    <div class="container" id="article-wrapper">
        <div class="row search-bar-row">
            <div class="col">
                <div class="search-bar">
                    <div class="input-wrapper">
                        <input type="text" class="search-input" id="search-input" placeholder="Search the articles or go for the Toolkits!" autocomplete="off" />
                    </div>

                    <div class="result-wrapper">
                        <div class="result-padding">
                            <div class="result-inner" id='result-inner'>
                                <ul class="cat-result"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $number=1; $row_number=1; $next_index_no_two = 2; $next_index_no_three=3;
        $inner_arr = count($articles);
        ?>

        @foreach($articles as $article) 
                @if($row_number==$next_index_no_two)
                    <div class="row row{{$number}}">    
                    <div class="col-l">         
                    <?php $number++;  ?>    
                @elseif($row_number==$next_index_no_three)  
                    <div class="col-r">
                @else
                    <div class="row row{{$number}}">
                    <div class="col">
                        <?php $number++;  ?>    
                @endif  
                @if($article['main_image_type']=="image")  
                  <?php  $image_list = json_decode($article['main_image']);    ?>   
                    <div class="article-wrapper">
                        <h3 class="article-title">{{$article['title']}}</h3>
                        @if(isset($image_list[0]))
                        <img src="{{URL('images/articles/'.$image_list[0])}}" />
                        @endif
                        <div class="text-wrapper">
                            <?php echo $article['short_description']; ?>
                        </div>
                        @if($article['readmore']==1)
                            <div class="row">
                                <a href="{{ url('articleinner/'.$article['id']) }}" class="read-more">{{getLabel('readmore')}}</a>
                            </div>
                        @endif
                    </div>
                @elseif($article['main_image_type']=="youtube")
                    <div class="article-wrapper">
                        <h3 class="article-title">{{$article['title']}}</h3>
                        <iframe src="{{$article['main_image']}}"  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                        <div class="text-wrapper">
                            <?php echo $article['short_description']; ?>
                        </div>
                        @if($article['readmore']==1)
                            <div class="row">
                                <a href="{{ url('articleinner/'.$article['id']) }}" class="read-more">{{getLabel('readmore')}}</a>
                            </div>
                        @endif
                    </div>    
                @elseif($article['main_image_type']=="video")
                 <?php $image_list=json_decode($article['main_image']); ?>   
                    <div class="article-wrapper">
                        <h3 class="article-title">{{$article['title']}}</h3>
                        @if(isset($image_list[0]))
                            <video width='100%' controls>
                                <source src="{{URL('images/articles/'.$image_list[0])}}">
                            </video>
                        @endif
                        <div class="text-wrapper">
                            <?php echo $article['short_description']; ?>
                        </div>
                        @if($article['readmore']==1)
                            <div class="row">
                                <a href="{{ url('articleinner/'.$article['id']) }}" class="read-more">{{getLabel('readmore')}}</a>
                            </div>
                        @endif
                    </div>   
                @elseif($article['main_image_type']=="book")
                    <?php  $image_list = json_decode($article['main_image']);    ?>                     
                    <div class="article-wrapper">
                        <h3 class="article-title">{{$article['title']}}</h3>
                         <div class="book-wrapper">
                            <?php $index=0; foreach($image_list as $image) { 
                                echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/articles/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/articles/".$image).'" />';
                                $index++;   
                            } ?>
                        </div>
                        <div class="text-wrapper">
                            <?php echo $article['short_description']; ?>
                        </div>
                        @if($article['readmore']==1)
                            <div class="row">
                                <a href="{{ url('articleinner/'.$article['id']) }}" class="read-more">{{getLabel('readmore')}}</a>
                            </div>
                        @endif
                    </div>   
                @elseif($article['main_image_type']=="flip")
                    <?php  $image_list = json_decode($article['main_image']);    ?>                     
                    <div class="article-wrapper">
                        <h3 class="article-title">{{$article['title']}}</h3>
                        <div class="twentytwenty-wrapper twentytwenty-horizontal">
                            <div class="before-after-wrapper twentytwenty-container active" style="height: 442.016px;">
                                <?php foreach($image_list as $image){
                                    echo '<img width="100%"   src="'.URL("images/articles/".$image).'" alt="">';
                                } ?>
                                <div class="twentytwenty-handle" style="left: 27.0156px;">
                                    <span class="twentytwenty-left-arrow"></span>
                                    <span class="twentytwenty-right-arrow"></span>
                                </div>
                            </div>
                        </div>
                        <div class="text-wrapper">
                            <?php echo $article['short_description']; ?>
                        </div>
                        @if($article['readmore']==1)
                            <div class="row">
                                <a href="{{ url('articleinner/'.$article['id']) }}" class="read-more">{{getLabel('readmore')}}</a>
                            </div>
                        @endif
                    </div>   
                @endif          
            @if($row_number>=$inner_arr && $row_number==$next_index_no_three)       
                </div></div>
            @elseif($row_number!=$next_index_no_two)
                </div>  
            @elseif($inner_arr==$row_number)
                </div>
                </div>
            @endif 
            @if($row_number==$next_index_no_two)
                <?php $next_index_no_two = $next_index_no_two+4; ?> 
            @endif 
            @if($row_number==$next_index_no_three)  
                    <?php $next_index_no_three = $next_index_no_three+4; ?> 
            @endif
        </div>
        <?php $row_number++; if($row_number>=5){$row_number=1;$next_index_no_two=2;$next_index_no_three=3;$number=1;} ?>
        @endforeach
         {{ $pagination_links }}
    </div>
    <div class="container">
        <div class="row">        
            <div class="col-md-12 pd-15"> 
                <div class="subscribe-form">
    				<p>{!! $newslettertext['description'] !!}	</p>		
    				@if (\Session::has('success'))
    				<div class="alert alert-success">
    				<p>{{ \Session::get('success') }}</p>
    				</div><br />
    				@endif
    				@if (\Session::has('failure'))
    				<div class="alert alert-danger">
    				<p>{{ \Session::get('failure') }}</p>
    				</div><br />
    				@endif
    				<form id="newsletterForm" method="POST" action="{{url('newsletter/store')}}">
                        @csrf
                        <input type="email" class="input email-input" id="input" name="email" placeholder="E-mail:" />
                        <input type="submit" class="submit submit-input" value="{{getLabel('subscribe')}}" /> 
                    </form>
                </div>			
            </div>
        </div>
    </div>
</div>
</div>			
<div class="search-bar-bg"></div>
	
@endsection						