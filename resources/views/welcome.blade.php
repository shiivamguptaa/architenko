@extends('layouts.front')
@section('content')
<div class="main home">
    <div class="container" id="home-main-section">
        <div class="row">
            <div class="col col-right">			
           	<?php echo $right_image; ?>
			</div>
            <div class="col col-left">
					<?php echo $left_image; ?>	
			</div>
		</div>
	</div>	
    <div class="container" id="home-info">
        <div class="row">
            <div class="col-12">
                <h2 class="main-title">{{$homedata['middle_section_title']}}</h2>
			</div>
            <div class="info-wrapper">
            	<?php echo $homedata['middle_section_description']; ?>
                <a class="viewmore-button" href="{{$homedata['middle_section_view_more']}}">
						{{getLabel('viewmore')}}
				 		<img src="{{asset('front/build/static/link-arrow.png')}}" alt="">
				 </a>			
			</div>
		</div>
	</div>
	
    <div class="container">
        <div class="subscribe-form">
			<p>{!! $newslettertext['description'] !!}	</p>		
            @if (\Session::has('success'))
			<div class="alert alert-success">
				<p>{{ \Session::get('success') }}</p>
			</div><br />
			@endif
			@if (\Session::has('failure'))
			<div class="alert alert-danger">
				<p>{{ \Session::get('failure') }}</p>
			</div><br />
			@endif
			<form id="newsletterForm" method="POST" action="{{url('newsletter/store')}}">
				@csrf
				<input type="email" class="input email-input" id="input" name="email" placeholder="E-mail:" />
				<input type="submit" class="submit submit-input" value="{{getLabel('subscribe')}}" /> 
			</form>
		</div>
	</div>
    <div class="container" id="services-wrapper">
		<div class="row">
			<div class="col-md-12">
				<h2 class="main-title">{{$homedata['project_service_title']}}</h2>
			</div>
		</div>
			
		<?php $number=1; $row_number=1; $next_index_no_two = 2; $next_index_no_three=3;
		$inner_arr = count($services);
		?>

		@foreach($services as $service)	
				@if($row_number==$next_index_no_two)
					<div class="row row{{$number}}">	
					<div class="col col-l">			
					<?php $number++;  ?>	
				@elseif($row_number==$next_index_no_three)	
					<div class="col col-r">
				@else
					<div class="row row{{$number}}">
					<div class="col">
						<?php $number++;  ?>	
				@endif	
				@if($service['type']=="image")	
				<a href="{{ url('servicedetails/'.$service['serviceId']) }}">
					<div class="h-c-wrapper">						
						<div class="cont-wrapper">	
							<img src="{{URL('images/service/'.$service['datavalue'])}}" />
						</div>
						<h2 class="r-title">{{$service['title']}}</h2>
					</div>
					<div class="text-b"><p>{{$service['subtitle']}}</p></div>
				</a>	
				@elseif($service['type']=="youtube")
				<a href="{{ url('servicedetails/'.$service['serviceId']) }}">
					<div class="h-c-wrapper">						
						<div class="cont-wrapper">					
							<div class="youtube-video-wrapper">
								<iframe src="{{$service['datavalue']}}"  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
							</div>							
						</div>
						<h2 class="r-title">{{$service['title']}}</h2>						
					</div>			
					<div class="text-b"><p>{{$service['subtitle']}}</p></div>		
				</a>	
				@elseif($service['type']=="video")
				<a href="{{ url('servicedetails/'.$service['serviceId']) }}">
					<div class="h-c-wrapper">						
						<div class="cont-wrapper">
							<div class="video-wrapper">
								<video width='100%' controls><source src="{{URL('images/service/'.$service['datavalue'])}}" type="video/mp4"></video>
							</div>	
								
						</div>	
						<h2 class="r-title">{{$service['title']}}</h2>						
					</div>		
					<div class="text-b"><p>{{$service['subtitle']}}</p></div>
				</a>	
				@elseif($service['type']=="book")
					<?php  $image_list = json_decode($service['datavalue']);	?>
					<a href="{{ url('servicedetails/'.$service['serviceId']) }}">
						<div class="h-c-wrapper">
							<div class="cont-wrapper">
								<div class="book-wrapper">
									<?php $index=0; foreach($image_list as $image) { 
										echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/service/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/service/".$image).'" />';
										$index++;	
									} ?>
								</div>
							</div>
							<h2 class="r-title">{{$service['title']}}</h2>								
						</div>
						<div class="text-b"><p>{{$service['subtitle']}}</p></div>						
					</a>	
				@elseif($service['type']=="flip")
					<?php $image_list = json_decode($service['datavalue']);
						if(count($image_list)>0){ ?>
							<a href="{{ url('servicedetails/'.$service['serviceId']) }}"><div class="h-c-wrapper"><div class="cont-wrapper"><div class="twentytwenty-wrapper twentytwenty-horizontal"><div class="before-after-wrapper twentytwenty-container active">
							<?php foreach($image_list as $image){
								echo '<img width="100%"   src="'.URL("images/service/".$image).'" alt="" >';
							} ?>
							<div class="twentytwenty-handle"><span class="twentytwenty-left-arrow"></span><span class="twentytwenty-right-arrow"></span></div></div></div></div>
							<h2 class="r-title">{{$service['title']}}</h2>	
							</div>
							<div class="text-b"><p>{{$service['subtitle']}}</p></div>
							</a>
							<?php 
						}
					?>
				@endif			
		    @if($row_number>=$inner_arr && $row_number==$next_index_no_three)		
				</div></div>
			@elseif($row_number!=$next_index_no_two)
				</div>	
			@elseif($inner_arr==$row_number)
		 		</div>
				</div>
			@endif 
			@if($row_number==$next_index_no_two)
				<?php $next_index_no_two = $next_index_no_two+4; ?>	
			@endif 
			@if($row_number==$next_index_no_three)	
					<?php $next_index_no_three = $next_index_no_three+4; ?>	
			@endif
		</div>
		<?php $row_number++; if($row_number>=5){$row_number=1;$next_index_no_two=2;$next_index_no_three=3;$number=1;} ?>
		@endforeach
	</div>
	<div class="container service-section" id="projects-wrapper">
		<div class="row">
			<div class="col-md-12">
				<h2 class="main-title">{{$homedata['project_main_title']}}</h2>

			</div>
		</div>
		
		<?php  $number=1;$row_number=1; $next_index_no_two = 2; $next_index_no_three=3;
		$inner_arr = count($projects);
		?>

		@foreach($projects as $project)	
				@if($row_number==$next_index_no_two)
					<div class="row row{{$number}}">	
					<div class="col col-l">				
							<?php $number++;  ?>	
				@elseif($row_number==$next_index_no_three)	
					<div class="col col-r">
				@else
					<div class="row row{{$number}}">
					<div class="col">
							<?php $number++;  ?>	
				@endif	
				@if($project['type']=="image")	
				 <a href="{{ url('projectdetails/'.$project['projectId']) }}">
					<div class="h-c-wrapper">						
						<div class="cont-wrapper">	
							<img src="{{URL('images/project/'.$project['datavalue'])}}" />
						</div>
							<p class='project-title'>{{$project['title']}}</p>	
					</div>
					
				</a>	
				@elseif($project['type']=="youtube")
				 <a href="{{ url('projectdetails/'.$project['projectId']) }}">
					<div class="h-c-wrapper">						
						<div class="cont-wrapper">					
							<div class="youtube-video-wrapper">
								<iframe src="{{$project['datavalue']}}"  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
							</div>							
						</div>
							<p class='project-title'>{{$project['title']}}</p>					
					</div>			
					
				</a>	
				@elseif($project['type']=="video")
				 <a href="{{ url('projectdetails/'.$project['projectId']) }}">
					<div class="h-c-wrapper">						
						<div class="cont-wrapper">
							<div class="video-wrapper">
								<video width='100%' controls><source src="{{URL('images/project/'.$project['datavalue'])}}" type="video/mp4"></video>
							</div>	
								
						</div>	
							<p class='project-title'>{{$project['title']}}</p>						
					</div>		
					
				</a>	
				@elseif($project['type']=="book")
					<?php  $image_list = json_decode($project['datavalue']);	?>
					 <a href="{{ url('projectdetails/'.$project['projectId']) }}">
						<div class="h-c-wrapper">
							<div class="cont-wrapper">
								<div class="book-wrapper">
									<?php $index=0; foreach($image_list as $image) { 
										echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/project/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/project/".$image).'" />';
										$index++;	
									} ?>
								</div>
							</div>
								<p class='project-title'>{{$project['title']}}</p>								
						</div>
							
					</a>	
				@elseif($project['type']=="flip")
					<?php $image_list = json_decode($project['datavalue']);
						if(count($image_list)>0){ ?>
							 <a href="{{ url('projectdetails/'.$project['projectId']) }}"><div class="h-c-wrapper"><div class="cont-wrapper"><div class="twentytwenty-wrapper twentytwenty-horizontal"><div class="before-after-wrapper twentytwenty-container active">
							<?php foreach($image_list as $image){
								echo '<img width="100%"   src="'.URL("images/project/".$image).'" alt="">';
							} ?>
							<div class="twentytwenty-handle" ><span class="twentytwenty-left-arrow"></span><span class="twentytwenty-right-arrow"></span></div></div></div></div>
							<p class='project-title'>{{$project['title']}}</p>	
							</div>
							
							</a>
							<?php 
						}
					?>
				@endif			
		    @if($row_number>=$inner_arr && $row_number==$next_index_no_three)		
				</div></div>
			@elseif($row_number!=$next_index_no_two)
				</div>	
			@elseif($inner_arr==$row_number)
		 		</div>
				</div>
			@endif 
			@if($row_number==$next_index_no_two)
				<?php $next_index_no_two = $next_index_no_two+4; ?>	
			@endif 
			@if($row_number==$next_index_no_three)	
					<?php $next_index_no_three = $next_index_no_three+4; ?>	
			@endif
		</div>
		<?php $row_number++; if($row_number>=5){$row_number=1;$next_index_no_two=2;$next_index_no_three=3;$number=1;} ?>
		@endforeach
	</div>	
</div>
@endsection		