@extends('layouts.front')

@section('content')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha256-UK1EiopXIL+KVhfbFa8xrmAWPeBjMVdvYMYkTAEv/HI=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" integrity="sha256-4hqlsNP9KM6+2eA8VUT0kk4RsMRTeS7QGHIM+MZ5sLY=" crossorigin="anonymous" />
    
<div class="main">
    <div class="container project-inner-single article-page-inner">
        <div class="row">
            <div class="col-lg-12 pd-15">
                <a href="javascript:void(0)" onclick="window.history.back()" class="return"><img src="{{URL('images/link-arrow.png')}}" alt=""> {{getLabel('return')}}</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 pd-15">
                <h4 class="section-title">{{$projectlist->innerpagetitle}}</h4>
            </div>
        </div>       
        @foreach($projectInnerContents as $projectInnerContent)
         <div class="row mt-2">
            <div class="col-md-6 pd-15 text-justify">
                <?php echo $projectInnerContent->description; ?>
            </div>
            <div class="col-md-6 pd-15">
                <img src="{{URL('images/project/'.$projectInnerContent->image)}}">
                <!-- <div class="text-b"><p>{{$projectInnerContent->title}}</p></div> -->
                <p class="r-title content-title">{{$projectInnerContent->title}}</p>
            </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-lg-12 pd-15">
                <div class="subscribe-form">
                    <a class="book-app">Book an appointment</a>, or get for <a class="more-info">more information</a> about service in terms of your project. Follow us on <a class="social" href="">f</a>, <a class="social" href="">in</a>, <a class="social" href="">p</a>,
                    <a href="" class="social">i </a> or stay tuned by a news e-mail
                    <form id="newsletterForm" method="POST" action="{{url('newsletter/store')}}">
                        @csrf
                        <input type="email" class="input email-input" id="input" name="email" placeholder="E-mail:" />
                        <input type="submit" class="submit submit-input" value="{{getLabel('subscribe')}}" /> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection                     