@extends('layouts.front')
@section('content')

<div class="main">
    @if(!empty($articles))
        <div class="container article-page-inner">
            <div class="row">
                <div class="col-lg-12 pd-15">
                    <a href="javascript:void(0)" onclick="window.history.back()" class="return">
                        <img src="{{URL('images/link-arrow.png')}}" alt=""> {{getLabel('return')}}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 pd-15">
                    @if(isset($articles->title))
                    <h4 class="section-title">{{$articles->title}}</h4>
                    @endif
                </div>
            </div>
            @foreach($articlesDetails as $articlesdetail) 
                <div class="row mt-2">
                     <div class="col-lg-6 pd-15">
                        <?php echo $articlesdetail->description; ?><br>
                    </div>
                    <div class="col-lg-6 pd-15">
                         @if($articlesdetail["image_type"]=="image")
                            <?php $image_list=json_decode($articlesdetail["image"]); ?>  
                            @if(isset($image_list[0]))
                                <img src="{{URL('images/articles/'.$image_list[0])}}" />
                            @endif
                         @elseif($articlesdetail["image_type"]=="youtube")
                            <iframe src="{{$articlesdetail['image']}}"  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                         @elseif($articlesdetail["image_type"]=="video")
                            <?php $image_list=json_decode($articlesdetail["image"]); ?>  
                            @if(isset($image_list[0]))
                                <video width='100%' controls>
                                    <source src="{{URL('images/articles/'.$image_list[0])}}">
                                </video>
                            @endif
                        @elseif($articlesdetail["image_type"]=="book")
                            <?php $image_list=json_decode($articlesdetail["image"]); ?>  
                            <div class="book-wrapper">
                                <?php $index=0; foreach($image_list as $image) { 
                                    echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/articles/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/articles/".$image).'" />';
                                    $index++;   
                                } ?>
                            </div>
                        @elseif($articlesdetail["image_type"]=="flip")
                            <?php $image_list=json_decode($articlesdetail["image"]); ?>  
                            <div class="twentytwenty-wrapper twentytwenty-horizontal">
                                <div class="before-after-wrapper twentytwenty-container active" style="height: 442.016px;">
                                    @foreach($image_list as $image)
                                        <img width="100%" src="{{URL('images/articles/'.$image)}}" alt="">
                                    @endforeach
                                    <div class="twentytwenty-handle" style="left: 27.0156px;">
                                        <span class="twentytwenty-left-arrow"></span>
                                        <span class="twentytwenty-right-arrow"></span>
                                    </div>
                                </div>
                            </div>
                        @endif       
                        @if(isset($articlesdetail["image_title"]))
                        <p class="r-title content-title">{{$articlesdetail["image_title"]}}</p>            
                        @endif       

                    </div>                
                </div>                
            @endforeach
            @if($articles->applyfortooltip==1)
                <div class="row">
                    <div class="col-lg-12 pd-15">
                        <p class="download-article open-download-article">
                            {{getLabel('applyfortooltip')}}
                        </p>
                    </div>
                </div>
            @endif
        </div>
    @endif
</div>
@if(isset($applytooltipform))
<div id="article-form" class="pop-up-big 12">
    <div class="container">
        <div class="row">
            <div class="hide close-icon">close [x]</div>
        </div>
        <div class="row">
            <div class="col">
                <h3 class="section-tilte">
                    @if(isset($applytooltipform->title)){{$applytooltipform->title}}@endif
                </h3>
                <form action="" id="ApplyToopKitFrom"  method="POST">
                    @csrf
                    <p class="small-title">@if(isset($applytooltipform->title_name)){{$applytooltipform->title_name}}@endif</p>
                    <select  id="select-toolkit" name="article_id">
                        @if(getArticalList())                            
                            @foreach(getArticalList() as $artical)
                                <option value="{{$artical['id']}}">{{$artical['title']}}</option>
                            @endforeach
                        @endif
                    </select>
                    <p class="small-title">@if(isset($applytooltipform->title_name)){{$applytooltipform->title_name}}@endif</p>
                    <div class="found-us-on">
                        <label class="cont">
                            <input name="you_found_through" type="radio" value="@if(isset($applytooltipform->you_found_through_option1)){{$applytooltipform->you_found_through_option1}}@endif" />  @if(isset($applytooltipform->you_found_through_option1)){{$applytooltipform->you_found_through_option1}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="cont">
                            <input name="you_found_through" type="radio" value="@if(isset($applytooltipform->you_found_through_option2)){{$applytooltipform->you_found_through_option2}}@endif" /> @if(isset($applytooltipform->you_found_through_option2)){{$applytooltipform->you_found_through_option2}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="cont">
                            <input name="you_found_through" type="radio" value="@if(isset($applytooltipform->you_found_through_option3)){{$applytooltipform->you_found_through_option3}}@endif" />@if(isset($applytooltipform->you_found_through_option3)){{$applytooltipform->you_found_through_option3}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="cont">
                            <input name="you_found_through" type="radio" value="@if(isset($applytooltipform->you_found_through_option4)){{$applytooltipform->you_found_through_option4}}@endif" />@if(isset($applytooltipform->you_found_through_option4)){{$applytooltipform->you_found_through_option4}}@endif <input type="text" class="inner-lg" name="you_found_through_other" placeholder="@if(isset($applytooltipform->you_found_through_other)){{$applytooltipform->you_found_through_other}}@endif">
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <p class="small-title">@if(isset($applytooltipform->choose_toolkit_title)){{$applytooltipform->choose_toolkit_title}}@endif</p>
                    <div class="reason-toolkit">
                        <label class="cont">
                            <input name="choose_toolkit" type="radio" value="@if(isset($applytooltipform->choose_toolkit_option1)){{$applytooltipform->choose_toolkit_option1}}@endif" />@if(isset($applytooltipform->choose_toolkit_option1)){{$applytooltipform->choose_toolkit_option1}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="cont">
                            <input name="choose_toolkit" type="radio" value="@if(isset($applytooltipform->choose_toolkit_option2)){{$applytooltipform->choose_toolkit_option2}}@endif" />@if(isset($applytooltipform->choose_toolkit_option2)){{$applytooltipform->choose_toolkit_option2}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="cont">
                            <input name="choose_toolkit" type="radio" value="@if(isset($applytooltipform->choose_toolkit_option3)){{$applytooltipform->choose_toolkit_option3}}@endif" />@if(isset($applytooltipform->choose_toolkit_option3)){{$applytooltipform->choose_toolkit_option3}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="cont">
                            <input name="choose_toolkit" type="radio" value="@if(isset($applytooltipform->choose_toolkit_option4)){{$applytooltipform->choose_toolkit_option4}}@endif" />@if(isset($applytooltipform->choose_toolkit_option4)){{$applytooltipform->choose_toolkit_option4}}@endif <input type="text" class="inner-lg" name="choose_toolkit_other"  placeholder="@if(isset($applytooltipform->choose_toolkit_other)){{$applytooltipform->choose_toolkit_other}}@endif" />
                            <span class="checkmark"></span>
                        </label>
                    </div>


                    <h5 class="sub-title">
                        @if(isset($applytooltipform->personal_contact_title)){{$applytooltipform->personal_contact_title}}@endif
                    </h5>
                    <p class="small-title">@if(isset($applytooltipform->i_am_title)){{$applytooltipform->i_am_title}}@endif</p>
                    <select name="position" id="iam">
                        <option id="privateIndividual" value="@if(isset($applytooltipform->private_individual_title)){{$applytooltipform->private_individual_title}}@endif">@if(isset($applytooltipform->private_individual_title)){{$applytooltipform->private_individual_title}}@endif</option>
                        <option value="@if(isset($applytooltipform->enterprise_title)){{$applytooltipform->enterprise_title}}@endif">@if(isset($applytooltipform->enterprise_title)){{$applytooltipform->enterprise_title}}@endif</option>
                        <option value="@if(isset($applytooltipform->developer_title)){{$applytooltipform->developer_title}}@endif">@if(isset($applytooltipform->developer_title)){{$applytooltipform->developer_title}}@endif</option>
                        <option value="@if(isset($applytooltipform->governmental_title)){{$applytooltipform->governmental_title}}@endif">@if(isset($applytooltipform->governmental_title)){{$applytooltipform->governmental_title}}@endif</option>
                    </select>
                    <div class="private-data-wrapper">
                        <p  class="small-title">@if(isset($applytooltipform->private_individual_address_title)){{$applytooltipform->private_individual_address_title}}@endif</p>
                        <div class="radio-wrapper">
                            <label class="cont">
                                <input name="private_individual_gender" type="radio" value="@if(isset($applytooltipform->private_individual_mr_title)){{$applytooltipform->private_individual_mr_title}}@endif" /> @if(isset($applytooltipform->private_individual_mr_title)){{$applytooltipform->private_individual_mr_title}}@endif
                                <span class="checkmark"></span>
                            </label>
                            <label class="cont">
                                <input name="private_individual_gender" type="radio" value="@if(isset($applytooltipform->private_individual_mrs_title)){{$applytooltipform->private_individual_mrs_title}}@endif" />@if(isset($applytooltipform->private_individual_mrs_title)){{$applytooltipform->private_individual_mrs_title}}@endif
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-l">
                                <input type="text" name="private_individual_first_name" placeholder="@if(isset($applytooltipform->private_individual_first_name)){{$applytooltipform->private_individual_first_name}}@endif" title="@if(isset($applytooltipform->private_individual_first_name)){{$applytooltipform->private_individual_first_name}}@endif" />
                            </div>
                            <div class="col-r">
                                 <input type="text" name="private_individual_middle_name" placeholder="@if(isset($applytooltipform->private_individual_middle_name)){{$applytooltipform->private_individual_middle_name}}@endif" title="@if(isset($applytooltipform->private_individual_middle_name)){{$applytooltipform->private_individual_middle_name}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-l">
                                <input type="text" name="private_individual_last_name" placeholder="@if(isset($applytooltipform->private_individual_last_name)){{$applytooltipform->private_individual_last_name}}@endif" title="@if(isset($applytooltipform->private_individual_last_name)){{$applytooltipform->private_individual_last_name}}@endif" />
                            </div>
                            <div class="col-r">
                                 <input type="text" name="private_individual_address" placeholder="@if(isset($applytooltipform->private_individual_address)){{$applytooltipform->private_individual_address}}@endif" title="@if(isset($applytooltipform->private_individual_address)){{$applytooltipform->private_individual_address}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-l">
                                <input type="text" name="private_individual_city" placeholder="@if(isset($applytooltipform->private_individual_city)){{$applytooltipform->private_individual_city}}@endif" title="@if(isset($applytooltipform->private_individual_city)){{$applytooltipform->private_individual_city}}@endif" />
                            </div>
                            <div class="col-r">
                               <input type="text" name="private_individual_postal_code" placeholder="@if(isset($applytooltipform->private_individual_postal_code)){{$applytooltipform->private_individual_postal_code}}@endif" title="@if(isset($applytooltipform->private_individual_postal_code)){{$applytooltipform->private_individual_postal_code}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-l">
                                  <input type="text" name="private_individual_country" placeholder="@if(isset($applytooltipform->private_individual_country)){{$applytooltipform->private_individual_country}}@endif" title="@if(isset($applytooltipform->private_individual_country)){{$applytooltipform->private_individual_country}}@endif" />
                            </div>
                            <div class="col-r">
                                 <input type="text" name="private_individual_phone" placeholder="@if(isset($applytooltipform->private_individual_phone)){{$applytooltipform->private_individual_phone}}@endif" title="@if(isset($applytooltipform->private_individual_phone)){{$applytooltipform->private_individual_phone}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-l">
                                <input type="text" name="private_individual_email" placeholder="@if(isset($applytooltipform->private_individual_email)){{$applytooltipform->private_individual_email}}@endif" title="@if(isset($applytooltipform->private_individual_email)){{$applytooltipform->private_individual_email}}@endif" />
                            </div>
                            <div class="col-r">
                                <input type="date" name="private_individual_dob" placeholder="@if(isset($applytooltipform->private_individual_dob)){{$applytooltipform->private_individual_dob}}@endif" title="@if(isset($applytooltipform->private_individual_dob)){{$applytooltipform->private_individual_dob}}@endif" />
                            </div>
                        </div>
                    </div>
                    <div class="enterprise-data-wrapper">
                        <div class="row">
                            <div class="col-l">
                                <input type="text" name="enterprise_price_title1" placeholder="@if(isset($applytooltipform->enterprise_price_title1)){{$applytooltipform->enterprise_price_title1}}@endif" title="@if(isset($applytooltipform->enterprise_price_title1)){{$applytooltipform->enterprise_price_title1}}@endif" />
                            </div>
                            <div class="col-r">
                                <input type="text" name="enterprise_price_title2" placeholder="@if(isset($applytooltipform->enterprise_price_title2)){{$applytooltipform->enterprise_price_title2}}@endif" title="@if(isset($applytooltipform->enterprise_price_title2)){{$applytooltipform->enterprise_price_title2}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-l">
                                 <input type="text" name="enterprise_address_title" placeholder="@if(isset($applytooltipform->enterprise_address_title)){{$applytooltipform->enterprise_address_title}}@endif" title="@if(isset($applytooltipform->enterprise_address_title)){{$applytooltipform->enterprise_address_title}}@endif" />
                            </div>
                            <div class="col-r">
                                <input type="text" name="enterprise_city_title" placeholder="@if(isset($applytooltipform->enterprise_city_title)){{$applytooltipform->enterprise_city_title}}@endif" title="@if(isset($applytooltipform->enterprise_city_title)){{$applytooltipform->enterprise_city_title}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-l">
                               <input type="text" name="enterprise_postal_code_title" placeholder="@if(isset($applytooltipform->enterprise_postal_code_title)){{$applytooltipform->enterprise_postal_code_title}}@endif" title="@if(isset($applytooltipform->enterprise_postal_code_title)){{$applytooltipform->enterprise_postal_code_title}}@endif" />
                            </div>
                            <div class="col-r">
                                <input type="text" name="enterprise_country_title" placeholder="@if(isset($applytooltipform->enterprise_country_title)){{$applytooltipform->enterprise_country_title}}@endif" title="@if(isset($applytooltipform->enterprise_country_title)){{$applytooltipform->enterprise_country_title}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-l">
                               <input type="text" name="enterprise_phone_title" placeholder="@if(isset($applytooltipform->enterprise_phone_title)){{$applytooltipform->enterprise_phone_title}}@endif" title="@if(isset($applytooltipform->enterprise_phone_title)){{$applytooltipform->enterprise_phone_title}}@endif" />
                            </div>
                            <div class="col-r">
                               <input type="text" name="enterprise_email_title" placeholder="@if(isset($applytooltipform->enterprise_email_title)){{$applytooltipform->enterprise_email_title}}@endif" title="@if(isset($applytooltipform->enterprise_email_title)){{$applytooltipform->enterprise_email_title}}@endif" />
                            </div>
                        </div>
                        <p  class="small-title">@if(isset($applytooltipform->enterprise_contact_title)){{$applytooltipform->enterprise_contact_title}}@endif</p>
                        <div class="radio-wrapper">
                            <label class="cont">
                                <input name="enterprise_gender" type="radio" value="@if(isset($applytooltipform->enterprise_mr_title)){{$applytooltipform->enterprise_mr_title}}@endif" /> @if(isset($applytooltipform->enterprise_mr_title)){{$applytooltipform->enterprise_mr_title}}@endif
                                <span class="checkmark"></span>
                            </label>
                            <label class="cont">
                                <input name="enterprise_gender" type="radio" value="@if(isset($applytooltipform->enterprise_mrs_title)){{$applytooltipform->enterprise_mrs_title}}@endif" />@if(isset($applytooltipform->enterprise_mrs_title)){{$applytooltipform->enterprise_mrs_title}}@endif
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-l">
                                <input type="text" name="enterprise_contact_first_name_title" placeholder="@if(isset($applytooltipform->enterprise_contact_first_name_title)){{$applytooltipform->enterprise_contact_first_name_title}}@endif" title="@if(isset($applytooltipform->enterprise_contact_first_name_title)){{$applytooltipform->enterprise_contact_first_name_title}}@endif" />
                            </div>
                            <div class="col-r">
                                <input type="text" name="enterprise_contact_last_name_title" placeholder="@if(isset($applytooltipform->enterprise_contact_last_name_title)){{$applytooltipform->enterprise_contact_last_name_title}}@endif" title="@if(isset($applytooltipform->enterprise_contact_last_name_title)){{$applytooltipform->enterprise_contact_last_name_title}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <select name="enterprise_contact_personpositionlist_title" id="enterprise-position-toolkit">
                                @if(isset($applytooltipform->enterprise_contact_personpositionlist_title))
                                    <?php $personpositionlist =  json_encode($applytooltipform->enterprise_contact_personpositionlist_title);     
                                        if(!empty($personpositionlist))
                                            $personpositionlist = substr($personpositionlist,1,strlen($personpositionlist)-2);
                                        $personpositionlist = explode(",", $personpositionlist);
                                    ?>
                                    @foreach($personpositionlist as $personPosition)
                                        <option value="{{$personPosition}}">{{$personPosition}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="row text-right">
                        <input type="submit" value="@if(isset($applytooltipform->apply_title)){{$applytooltipform->apply_title}}@endif" class="submit-cus">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@endsection                     