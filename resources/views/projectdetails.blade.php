@extends('layouts.front')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha256-UK1EiopXIL+KVhfbFa8xrmAWPeBjMVdvYMYkTAEv/HI=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" integrity="sha256-4hqlsNP9KM6+2eA8VUT0kk4RsMRTeS7QGHIM+MZ5sLY=" crossorigin="anonymous" />
                                                                                                                

<!-- <div class="twentytwenty-wrapper twentytwenty-horizontal">
    <div class="before-after-wrapper twentytwenty-container active">
        <img class="lozad" width="100%"   src="https://kretoss.com/project/architenko/public/images/project/1582279507download_(3)_-_copy_-_copy.png" alt="" >
        <img class="lozad" width="100%"   src="https://kretoss.com/project/architenko/public/images/project/1582279507download.jpg" alt="" >
        <div class="twentytwenty-handle">
            <span class="twentytwenty-left-arrow"></span>
            <span class="twentytwenty-right-arrow"></span>
        </div>
    </div>
</div> -->
<div class="main projects project-inner">
    <a href="javascript:void(0)" onclick="window.history.back()"  class="back-button show-on-mobile"><img src="{{URL('images/link-arrow.png')}}" alt=""> Return</a>

    <div class="slider-parent">
        <div class="container inner-slider-wrapper project-inner-slider">
            <div class="row">
                <div class="img-slider">
                    
                    <!-- <div class="slide">
                        <div class="inner" data-description="First Image">
                            <div class="youtube-video-wrapper">
                                <iframe class="youtube-video" width="100%" src="https://www.youtube.com/embed/qUksnWaArko" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>    -->
                    @foreach($projects as $project) 
                        <div class="slide">
                            <div class="inner" data-description="{{$project['moretitle']}}"> 
                                @if($project['type']=="image")  
                                  <!--   <a href="">
                                        <div class="h-c-wrapper">                       
                                            <div class="cont-wrapper">   -->
                                                <img  data-loaded="true" class="lozad" src="{{URL('images/project/'.$project['datavalue'])}}" />
                                            <!-- </div>
                                            <h2 class="r-title">{{$project['title']}}</h2>
                                        </div>
                                        <div class="text-b"><p>{{$project['subtitle']}}</p></div>
                                    </a>   -->  
                                @elseif($project['type']=="youtube")
                                    <!-- <a href="">
                                        <div class="h-c-wrapper">                       
                                            <div class="cont-wrapper">     -->              
                                                <div class="youtube-video-wrapper">
                                                    <iframe class="youtube-video" src="{{$project['datavalue']}}"  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                                                </div>                          
                                            <!-- </div>
                                            <h2 class="r-title">{{$project['title']}}</h2>                      
                                        </div>          
                                        <div class="text-b"><p>{{$project['subtitle']}}</p></div>       
                                    </a>     -->
                                @elseif($project['type']=="video")
                                    <!-- <a href="">
                                        <div class="h-c-wrapper">                       
                                            <div class="cont-wrapper"> -->
                                                <div class="video-wrapper">
                                                    <video width='100%' controls><source src="{{URL('images/project/'.$project['datavalue'])}}" type="video/mp4"></video>
                                                </div>  
                                           <!--          
                                            </div>  
                                            <h2 class="r-title">{{$project['title']}}</h2>                      
                                        </div>      
                                        <div class="text-b"><p>{{$project['subtitle']}}</p></div>
                                    </a>     -->
                                @elseif($project['type']=="book")
                                    <?php  $image_list = json_decode($project['datavalue']);    ?>
                                  <!--   <a href="">
                                        <div class="h-c-wrapper">
                                            <div class="cont-wrapper"> -->
                                                <div class="book-wrapper">
                                                    <?php $index=0; foreach($image_list as $image) { 
                                                        echo  ($index==0) ? '<img class="lozad"  width="100%"  src="'.URL("images/project/".$image).'" class="active" />' : '<img  class="lozad" width="100%" src="'.URL("images/project/".$image).'" />';
                                                        $index++;   
                                                    } ?>
                                                </div>
                                           <!--  </div>
                                            <h2 class="r-title">{{$project['title']}}</h2>                              
                                        </div>
                                        <div class="text-b"><p>{{$project['subtitle']}}</p></div>                       
                                    </a>     -->
                                @elseif($project['type']=="flip")
                                    <?php $image_list = json_decode($project['datavalue']);
                                        if(count($image_list)>0){ ?>
                                           <!--  <a>
                                                <div class="h-c-wrapper">
                                                    <div class="cont-wrapper"> -->
                                                        <div class="twentytwenty-wrapper twentytwenty-horizontal" style="height: 100%; width: 50%;">
                                                            <div class="before-after-wrapper twentytwenty-container active">
                                                            <?php foreach($image_list as $image){
                                                                echo '<img class="lozad" width="100%"   src="'.URL("images/project/".$image).'" alt="" >';
                                                            } ?>
                                                                <div class="twentytwenty-handle">
                                                                    <span class="twentytwenty-left-arrow"></span>
                                                                    <span class="twentytwenty-right-arrow"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <!-- </div>
                                                    <h2 class="r-title">{{$project['title']}}</h2>  
                                                </div>
                                                <div class="text-b"><p>{{$project['subtitle']}}</p></div>
                                            </a> -->
                                            <?php 
                                        }
                                    ?>
                                @endif   
                                </div></div>       
                            @endforeach                 
                </div>
            </div>
            <div class="next"></div>
            <div class="prev"></div>
        </div>
        <div class="project-slider-wrapper">
            <div class="col-lg-12">
                <div class="project-summary">
                    <ul>
                        <li class="name-y"><?php echo $projectlist->title; ?></li>
                        <li class="read-more">{{getLabel('readmore')}}</li>
                        <li class="hide">{{getLabel('hide')}}</li>
                        <li class="project-n"></li>
                        <li class="pic-description"></li>
                    </ul>
                    <div class="project-info">
                        <div class="row">
                            <div class="col-lg-6 left">
                                <p>
                                    <?php echo $projectlist->descriptionleft; ?>
                                </p>
                            </div>
                            <div class="col-lg-6 right">
                                <p>
                                    <?php echo $projectlist->descriptionright; ?>
                                </p>
                            </div>

                        </div>

                        <a href="{{ url('projectinnerpage/'.$projectlist['id']) }}" class="full-text">{{getLabel('fulltext')}}</a>
                        <div class="subscribe-form">
                            <p class="text"><i>Download: <?php echo $projectlist->download_text; ?></i> </p>
                            <form id="projectDetailsForm" method="POST" action="{{url('projectlist/projectDetailsForm')}}">
                                @csrf
                                <input type="email" class="input email-input" id="userEmail" name="userEmail" placeholder="E-mail:" />
                                <input type="hidden" id="projectid" name="projectid" value="{{@$projectlist['id']}}" />
                                <input type="submit" class="submit submit-input" value="{{getLabel('send')}}" /> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection                     