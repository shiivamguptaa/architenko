@extends('layouts.front')

@section('content')
<div class="main" id="services-wrapper">
    <div class="container" >
        <div class="row intro-text">
            <div class="col-h">
                <p>{{ $servicetopdescription->description }}</p>
            </div>
        </div>
		@if(isset($services[0]->title))
        <div class="row row1">
            <div class="col">
                <a href="">
                    <div class="h-c-wrapper">
                        <div class="cont-wrapper">
                            <img src="{{asset('images/service/'.$services[0]->datavalue)}}"  alt="">
                        </div>
                        <h2 class="r-title">{{$services[0]->title}}</h2>
                    </div>
                    <div class="text-b"><p>{{$services[0]->subtitle}}</p></div>
                </a>
            </div>
        </div>
		@endif
        <div class="row row2">
			@if(isset($services[1]->title))
            <div class="col col-l">
                <a href="">
                    <div class="h-c-wrapper">
                        <div class="cont-wrapper">
                            <img src="{{asset('images/service/'.$services[1]->datavalue)}}"  alt="">
                        </div>
                        <h2 class="r-title">{{$services[1]->title}}</h2>
                    </div>
                    <div class="text-b"><p>{{$services[1]->subtitle}}</p></div>
                </a>
            </div>
			@endif
			@if(isset($services[2]->title))
            <div class="col col-r">
                <a href="">
                    <div class="h-c-wrapper">
                        <div class="cont-wrapper">
                            <img src="{{asset('images/service/'.$services[2]->datavalue)}}"  alt="">
                        </div>
                        <h2 class="r-title">{{$services[2]->title}}</h2>
                    </div>
                    <div class="text-b"><p>{{$services[2]->subtitle}}</p></div>
                </a>
            </div>
			@endif
        </div>
		@if(isset($services[3]->title))
        <div class="row row3">
            <div class="col">
                <a href="">
                    <div class="h-c-wrapper">
                        <div class="cont-wrapper">
                            <img src="{{asset('images/service/'.$services[3]->datavalue)}}"  alt="">
                        </div>
                        <h2 class="r-title">{{$services[3]->title}}</h2>
                    </div>
                    <div class="text-b"><p>{{$services[3]->subtitle}}</p></div>
                </a>
            </div>
        </div>
		@endif
		@if(isset($services[4]->title))
        <div class="row row4">
            <div class="col">
                <a href="">
                    <div class="h-c-wrapper">

                        <div class="cont-wrapper">
                            <img src="{{asset('images/service/'.$services[4]->datavalue)}}"  alt="">
                        </div>
                        <h2 class="r-title">{{$services[4]->title}}</h2>
                    </div>
                    <div class="text-b"><p>{{$services[4]->subtitle}}</p></div>
                </a>
            </div>
        </div>
		@endif
        <div class="row row5">
			@if(isset($services[5]->title))
            <div class="col col-l">
                <a href="">
                    <div class="h-c-wrapper">

                        <div class="cont-wrapper">
                            <img src="{{asset('images/service/'.$services[5]->datavalue)}}"  alt="">
                        </div>
                        <h2 class="r-title">{{$services[5]->title}}</h2>
                    </div>
                    <div class="text-b"><p>{{$services[5]->subtitle}}</p></div>
                </a>
            </div>
			@endif
			@if(isset($services[6]->title))
            <div class="col col-r">
                <a href="">
                    <div class="h-c-wrapper">

                        <div class="cont-wrapper">
                            <img src="{{asset('images/service/'.$services[6]->datavalue)}}"  alt="">
                        </div>
                        <h2 class="r-title">{{$services[6]->title}}</h2>
                    </div>
                    <div class="text-b"><p>{{$services[6]->subtitle}}</p></div>
                </a>
            </div>
			@endif
        </div>
		@if(isset($services[7]->title))
		<div class="row row1">
			<div class="col">
				<a href="">
					<div class="h-c-wrapper">
						
						<div class="cont-wrapper">
							
							<div class="book-wrapper">
								<img src="{{asset('images/service/'.$services[7]->datavalue)}}"  alt="">						
								
							</div>
						</div>
						<h2 class="r-title">{{$services[7]->title}}</h2>
					</div>
					<div class="text-b"><p>{{$services[7]->subtitle}}</p></div>
				</a>
			</div>
		</div>
		@endif
		@if(isset($services[8]->title))
		<div class="row row2">
			<div class="col">
				<a href="">
					<div class="h-c-wrapper">
						<div class="cont-wrapper">
							<div class="before-after-wrapper">
								<img src="{{asset('images/service/'.$services[8]->datavalue)}}"  alt="">
							</div>
						</div>
						<h2 class="r-title"> {{$services[8]->title}}</h2>
					</div>
					<div class="text-b"><p>{{$services[8]->subtitle}}</p></div>
				</a>
			</div>
		</div>
		@endif		
		<div class="row row3">
			@if(isset($services[9]->title))
			<div class="col col-l">
				<a href="">
					<div class="h-c-wrapper">
						<div class="cont-wrapper">
							<img src="{{asset('images/service/'.$services[9]->datavalue)}}"  alt="">
						</div>
						<h2 class="r-title">{{$services[9]->title}}</h2>
					</div>
					<div class="text-b"><p>{{$services[9]->subtitle}}</p></div>
				</a>
			</div>
			@endif
			@if(isset($services[10]->title))
			<div class="col col-r">
				<a href="">
					<div class="h-c-wrapper">
						<div class="cont-wrapper">
							<img src="{{asset('images/service/'.$services[10]->datavalue)}}"  alt="">
						</div>
						<h2 class="r-title">{{$services[10]->title}}</h2>
					</div>
					<div class="text-b"><p>{{$services[10]->subtitle}}</p></div>
				</a>
			</div>
			@endif
		</div>
    </div>
    <div class="container">
        <div class="row">
            <div class="subscribe-form">
					<p>{!! $newslettertext['description'] !!}	</p>		
					@if (\Session::has('success'))
					<div class="alert alert-success">
					<p>{{ \Session::get('success') }}</p>
					</div><br />
					@endif
					@if (\Session::has('failure'))
					<div class="alert alert-danger">
					<p>{{ \Session::get('failure') }}</p>
					</div><br />
					@endif
					<form method="POST" action="{{url('newsletter/store')}}">
						@csrf
						@if (\Session::get('locale')=="en")
                        <input type="email" oninvalid="setCustomValidity('Enter email id')" class="input" name="email" placeholder="E-mail:" required>
                        @else
                        <input type="email" oninvalid="setCustomValidity('Voer e-mail ID in')" class="input" name="email" placeholder="E-mail:" required>
                        @endif
						<input type="submit" class="submit" value="{{getLabel('submit')}}" />
					</form>
				</div>
        </div>
    </div>
</div>																																		
@endsection						