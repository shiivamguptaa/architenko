@extends('layouts.front')
@section('content')

<div class="main studio">
    <?php if(!empty($studiolist)) : ?>
    <div class="container">
        <div class="row logo" id="profile">
            <div class="col">
                @if($studiolist->title_img_type=="image")                  
                    <img src="{{URL('images/studio/'.$studiolist->title_img)}}" />                        
                @elseif($studiolist->title_img_type=="youtube")                
                    <iframe src="{{$studiolist->title_img}}"  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                @elseif($studiolist->title_img_type=="video")              
                    <video width='100%' controls><source src="{{URL('images/studio/'.$studiolist->title_img)}}" type="video/mp4"></video>                           
                @elseif($studiolist->title_img_type=="book")
                    <?php  $image_list = json_decode($studiolist->title_img);    ?>
                    <div class="book-wrapper">
                        <?php $index=0; foreach($image_list as $image) { 
                            echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/studio/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/studio/".$image).'" />';
                            $index++;   
                        } ?>
                    </div>                            
                @elseif($studiolist->title_img_type=="flip")
                    <?php $image_list = json_decode($studiolist->title_img);
                        if(count($image_list)>0){ ?>
                         
                        <div class="h-c-wrapper">
                            <div class="cont-wrapper">
                                <div class="twentytwenty-wrapper twentytwenty-horizontal">
                                    <div class="before-after-wrapper twentytwenty-container active" style="height: 442.016px;">
                            <?php foreach($image_list as $image){
                                echo '<img width="100%"   src="'.URL("images/studio/".$image).'" alt="">';
                            } ?>
                                        <div class="twentytwenty-handle" style="left: 27.0156px;">
                                            <span class="twentytwenty-left-arrow"></span>
                                            <span class="twentytwenty-right-arrow"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php 
                        }
                    ?>
                @endif 
            </div>
        </div>
        <div class="row row1" >
            <div id="profile-link"></div>
            <div class="col-l">
                <h3 class="section-title">
                    {{$studiolist->profile_title}}
                </h3>
                <div class="text-wrapper">
                   <?php echo $studiolist->profile_description; ?>                   
                </div>
                <p id="open-extended-profile">{{getLabel('extendedprofile')}}</p>
            </div>
            <div class="col-r">
                @if($studiolist->profile_right_img_type=="image")                  
                    <img src="{{URL('images/studio/'.$studiolist->profile_right_img)}}" />                        
                @elseif($studiolist->profile_right_img_type=="youtube")                
                    <iframe src="{{$studiolist->profile_right_img}}"  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                @elseif($studiolist->profile_right_img_type=="video")              
                    <video width='100%' controls><source src="{{URL('images/studio/'.$studiolist->profile_right_img)}}" type="video/mp4"></video>                           
                @elseif($studiolist->profile_right_img_type=="book")
                    <?php  $image_list = json_decode($studiolist->profile_right_img);    ?>                   
                    <div class="book-wrapper">
                        <?php $index=0; foreach($image_list as $image) { 
                            echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/studio/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/studio/".$image).'" />';
                            $index++;   
                        } ?>
                    </div>                            
                @elseif($studiolist->profile_right_img_type=="flip")
                    <?php $image_list = json_decode($studiolist->profile_right_img);
                        if(count($image_list)>0){ ?>
                         
                        <div class="h-c-wrapper">
                            <div class="cont-wrapper">
                                <div class="twentytwenty-wrapper twentytwenty-horizontal">
                                    <div class="before-after-wrapper twentytwenty-container active" style="height: 442.016px;">
                            <?php foreach($image_list as $image){
                                echo '<img width="100%"   src="'.URL("images/studio/".$image).'" alt="">';
                            } ?>
                                        <div class="twentytwenty-handle" style="left: 27.0156px;">
                                            <span class="twentytwenty-left-arrow"></span>
                                            <span class="twentytwenty-right-arrow"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php 
                        }
                    ?>
                @endif 
            </div>
        </div>
        <div id="founders-link"></div>
        <div class="row row2" id="team">
            <div class="col-l">
               @if($studiolist->founder_right_img_type=="image")                  
                    <img src="{{URL('images/studio/'.$studiolist->founder_right_img)}}" class="hide-on-mobile" />                        
                @elseif($studiolist->founder_right_img_type=="youtube")                
                    <iframe src="{{$studiolist->founder_right_img}}"   class='youtube-video hide-on-mobile' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                @elseif($studiolist->founder_right_img_type=="video")              
                    <video width='100%' class="hide-on-mobile" controls><source src="{{URL('images/studio/'.$studiolist->founder_right_img)}}" type="video/mp4"></video>                           
                @elseif($studiolist->founder_right_img_type=="book")
                    <?php  $image_list = json_decode($studiolist->founder_right_img);    ?>                   
                    <div class="book-wrapper hide-on-mobile">
                        <?php $index=0; foreach($image_list as $image) { 
                            echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/studio/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/studio/".$image).'" />';
                            $index++;   
                        } ?>
                    </div>                            
                @elseif($studiolist->founder_right_img_type=="flip")
                    <?php $image_list = json_decode($studiolist->founder_right_img);
                        if(count($image_list)>0){ ?>
                         
                        <div class="h-c-wrapper hide-on-mobile">
                            <div class="cont-wrapper">
                                <div class="twentytwenty-wrapper twentytwenty-horizontal">
                                    <div class="before-after-wrapper twentytwenty-container active" style="height: 442.016px;">
                            <?php foreach($image_list as $image){
                                echo '<img width="100%"   src="'.URL("images/studio/".$image).'" alt="">';
                            } ?>
                                        <div class="twentytwenty-handle" style="left: 27.0156px;">
                                            <span class="twentytwenty-left-arrow"></span>
                                            <span class="twentytwenty-right-arrow"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php 
                        }
                    ?>
                @endif 
            </div>
            <div class="col-r">
                <h3 class="section-title">
                    {{$studiolist->founder_title}}
                </h3>
                <div class="text-wrapper">
                   <?php echo $studiolist->founder_description; ?> 
                </div>
                <p id="open-read-team">{{getLabel('readmoreabouttheteam')}}</p>
                 @if($studiolist->founder_right_img_type=="image")                  
                    <img src="{{URL('images/studio/'.$studiolist->founder_right_img)}}" class="show-on-mobile" />                        
                @elseif($studiolist->founder_right_img_type=="youtube")                
                    <iframe src="{{$studiolist->founder_right_img}}"   class='youtube-video show-on-mobile' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                @elseif($studiolist->founder_right_img_type=="video")              
                    <video width='100%' class="show-on-mobile" controls><source src="{{URL('images/studio/'.$studiolist->founder_right_img)}}" type="video/mp4"></video>                           
                @elseif($studiolist->founder_right_img_type=="book")
                    <?php  $image_list = json_decode($studiolist->founder_right_img);    ?>                   
                    <div class="book-wrapper show-on-mobile">
                        <?php $index=0; foreach($image_list as $image) { 
                            echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/studio/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/studio/".$image).'" />';
                            $index++;   
                        } ?>
                    </div>                            
                @elseif($studiolist->founder_right_img_type=="flip")
                    <?php $image_list = json_decode($studiolist->founder_right_img);
                        if(count($image_list)>0){ ?>
                         
                        <div class="h-c-wrapper show-on-mobile">
                            <div class="cont-wrapper">
                                <div class="twentytwenty-wrapper twentytwenty-horizontal">
                                    <div class="before-after-wrapper twentytwenty-container active" style="height: 442.016px;">
                            <?php foreach($image_list as $image){
                                echo '<img width="100%"   src="'.URL("images/studio/".$image).'" alt="">';
                            } ?>
                                        <div class="twentytwenty-handle" style="left: 27.0156px;">
                                            <span class="twentytwenty-left-arrow"></span>
                                            <span class="twentytwenty-right-arrow"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php 
                        }
                    ?>
                @endif 
            </div>
        </div>
        <div id="join-link"></div>
        <div class="row row3" id="join">
            <div class="col-l">
                <h3 class="section-title">
                    {{$studiolist->join_architenko_title}}
                </h3>
                <div class="text-wrapper">
                    <?php echo $studiolist->join_architenko_description; ?>
                </div>
            </div>
        </div>
        <div id="positions-link"></div>
        <div class="row row4" id="openposition">
            <div class="col-r">
                <h3 class="section-title">
                   {{$studiolist->opened_positions_title}}
                </h3>
                <table class="opened-positions">                   
                    <?php  $index=1;
                    foreach ($studiopositionlist as $key => $value1)  : ?>
                        <tr>
                            <td>{{$value1->title}}</td>
                            <td><p class="view-position" data-id="{{$index++}}">{{getLabel('view')}}</p></td>
                        </tr>
                    <?php endforeach; ?>                      	
                </table>
                @if(!empty($studiopositionlist))
                <p class="openapply open-apply">{{getLabel('apply')}} <img src="{{URL('front/build/static/link-arrow.png')}}" alt=""></p>
                @endif
            </div>
        </div>
        <div id="awards-link"></div>
        <div class="row row5" id="award">
            <div class="col">
                <h3 class="section-title">{{$studiolist->award_title}}</h3>
                <div class="text-wrapper">
                  <?php echo $studiolist->award_description; ?>
                </div>
                <p id="read-more-awards">{{getLabel('selectedawards')}}</p>
            </div>
        </div>
        <div class="row row6">
            <div class="col">
                <div class="studio-slider">
                <?php 
                    $image_list = json_decode($studiolist->award_slider);
                    function compare($a, $b) {
                       return ($a->display_order > $b->display_order);
                    }
                    usort($image_list, "compare");
                    foreach ($image_list as $key => $value) : ?>
                        <div class="slide">
                            <div class="inner" style="background-image:url(<?=URL('images/studio/'.$value->image)?>)">
                                <img src="<?php echo URL('images/studio/'.$value->image)?>" alt="">
                            </div>
                        </div>
                <?php endforeach; ?>                      
                </div>
                <div class="slider-nav text-center">
                    <div class="prev"></div>
                    <div class="next"></div>
                </div>
            </div>
        </div>
        <div id="terms-link"></div>
        <div class="row row4 bottom-menu-wrapper" id="terms">
            <div class="col-r">
                <h3 class="section-title">
                    {{$studiolist->terms_and_conditions}}
                </h3>
                <ul class="bottom-menu">
                    @foreach ($studioTermConditionList as $value)
                    <li><p class="open-tc" data-id="{{$value->id}}" data-title="{{$value->title}}">{{$value->title}}</p></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pd-15">
                <div class="subscribe-form">
                    <a class="book-app">Book an appointment</a>, or get for <a class="more-info">more information</a> about service in terms of your project. Follow us on <a class="social" href="">f</a>, <a class="social" href="">in</a>, <a class="social" href="">p</a>,
                    <a href="" class="social" >i </a> or stay tuned by a news e-mail
                    <form id="newsletterForm" method="POST" action="{{url('newsletter/store')}}">
                        @csrf
                        <input type="email" class="input email-input" id="input" name="email" placeholder="E-mail:" />
                        <input type="submit" class="submit submit-input" value="{{getLabel('subscribe')}}" /> 
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<?php if(!empty($studiodetailslist)) : ?>
<div class="extended-profile-wrapper pop-up-big">
    <div class="container">
        <div class="row">
            <div class="close-icon hide">close [x]</div>
        </div>
         <div class="row">
            <div class="col-lg-12 pd-15">
                <h3 class="section-title">{{$studiolist->profile_title}}</h3>
            </div>
        </div>  
        <?php foreach ($studiodetailslist as $key => $value) :  
            if($value->section_type=="profilepopup")  : ?>                             
                <div class="row">
                    <div class="col-lg-6 pd-15">
                        <div class="inner-wrapper">
                            <?php echo $value->description; ?>
                        </div><br>
                    </div>
                    <div class="col-lg-6 pd-15 d-block">
                        <img src="<?php echo URL('images/studio/'.$value->image)?>" alt="">
                        <p class="r-title content-title">{{$value->title}}</p><br>
                    </div>
                </div>
        <?php endif; endforeach; ?>
    </div>
</div>



<div id="view-team-wrapper" class="pop-up-big">
    <div class="container">
        <div class="row">
            <div class="close-icon hide">close [x]</div>
        </div>
         <div class="row">
            <div class="col-lg-12 pd-15">
                <h3 class="section-title">{{$studiolist->founder_title}}</h3>
            </div>
        </div> 
         <?php foreach ($studiodetailslist as $key => $value) :  
            if($value->section_type=="founderpopup")  : ?>
            <div class="row">
                <div class="col-lg-6 pd-15 ">
                    <div class="inner-wrapper">
                         <?php echo $value->description; ?>
                    </div><br>
                </div>
                <div class="col-lg-6 pd-15 d-block">
                    <img src="<?php echo URL('images/studio/'.$value->image)?>" alt="">
                    <p class="r-title content-title">{{$value->title}}</p><br>
                </div>
            </div>
         <?php endif; endforeach; ?>
    </div>
</div>
<div id="awards-wrapper" class="pop-up-big">
    <div class="container">
        <div class="row">
            <div class="close-icon hide">close [x]</div>
        </div>
        <div class="row">
            <div class="col-lg-12 pd-15">
                   <h3 class="section-title">{{$studiolist->award_title}}</h3>
            </div>
        </div>
         <?php foreach ($studiodetailslist as $key => $value) :  
            if($value->section_type=="awardpopup")  : ?>                
                <div class="row">
                    <div class="col-lg-6 pd-15">
                        <div class="inner-wrapper">
                            <?php echo $value->description; ?>
                        </div><br>
                    </div>
                    <div class="col-lg-6 pd-15 d-block">
                        <img src="<?php echo URL('images/studio/'.$value->image)?>" alt="">
                        <p class="r-title content-title">{{$value->title}}</p><br>
                    </div>
                </div>
            <?php endif; endforeach; ?>
    </div>
</div>


<?php $index=1;foreach ($studiopositionlist as $key => $value) :  ?>
    <div class="position-wrapper pop-up-big " id="position{{$index++}}">
        <div class="container">
            <div class="row">
                <div class="hide close-icon">close [x]</div>
            </div>
            <div class="row">
                <div class="col-lg-12 pd-15">
                    <h3 class="section-title">{{$value->title}}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 pd-15">
                    <div class="inner-wrapper">
                       <?=$value->description;?>
                    </div><br>
                </div>
                <div class="col-lg-6 pd-15 d-block">
                    <img src="<?php echo URL('images/studio/'.$value->image)?>"/>
                    <p class="content-title">{{$value->image_title}}</p><br>
                </div>
            </div>  
            <div class="row">
	            <div class="col-lg-12 pd-15">
	                 <p class="openapply open-apply">{{getLabel('apply')}} <img src="{{URL('front/build/static/link-arrow.png')}}" alt=""></p>
	            </div>
        	</div>         
        </div>
    </div>
<?php endforeach;?>
@foreach($studioTermConditionDetailsList as $studioTermConditionDetails)
	<div class="tc-wrapper pop-up-big" id="tc-wrapper{{$studioTermConditionDetails[0]->tnc_id}}">
	    <div class="container">
	        <div class="row">
	            <div class="hide close-icon">close [x]</div>
	        </div>
	        <div class="row bg-white">
	            <div class="col-lg-12 pd-15">
	                <h3 class="section-title term_condition_title"></h3>
	            </div>
	        </div>
	        @foreach($studioTermConditionDetails as $value)         
	            <div class="row">
	                <div class="col-lg-6 pd-15">
	                    <div class="inner-wrapper">
	                            <?php echo $value->description; ?>
	                    </div><br>
	                </div>
	                <div class="col-lg-6 pd-15 d-block ">
	                    <img src="{{URL('images/studio',$value->image)}}" />
	                    <p class="r-title content-title">{{$value->title}}</p><br>
	                    <br/>
	                </div>
	            </div>
	        @endforeach
	    </div>
	</div>
@endforeach
<?php endif;?>

<div id="apply-wrapper" class="pop-up-big">
    <div class="container">
        <div class="row">
            <div class="hide close-icon">close [x]</div>
        </div>
        <div class="row">
            <div class="col">
                <h3 class="section-tilte">
                   @if(isset($studioapplyform['privacy_policy_title'])){{$studioapplyform['privacy_policy_title']}}@endif
                </h3>
                <div class="row">
                    <?=isset($studioapplyform['privacy_policy_short_description']) ? $studioapplyform['privacy_policy_short_description'] : ""; ?>
                    <p class="open-pp"> @if(isset($studioapplyform['apply_btn'])){{$studioapplyform['apply_btn']}}@endif <img src="{{URL('front/build/static/link-arrow.png')}}" alt=""></p>
                </div>
                <form id="studioApplyForm" method="POST" enctype="multipart/form-data">
                    @csrf
                    <label class="check-box">
                        <input name="privacy_policy_accept_title" id="privacy_policy_accept_title" placeholder="@if(isset($studioapplyform['privacy_policy_accept_title'])){{$studioapplyform['privacy_policy_accept_title']}}@endif" title="@if(isset($studioapplyform['privacy_policy_accept_title'])){{$studioapplyform['privacy_policy_accept_title']}}@endif" type="checkbox" value="YES" />@if(isset($studioapplyform['privacy_policy_accept_title'])){{$studioapplyform['privacy_policy_accept_title']}}@endif
                        <span class="checkmark"></span>
                    </label>
                    <p class="small-title">@if(isset($studioapplyform['language_correspondence_title'])){{$studioapplyform['language_correspondence_title']}}@endif</p>
                    <div class="radio-wrapper langs">
                        @if(isset($studioapplyform['language_input']))
                            <?php 
                                $language_input =  json_encode($studioapplyform['language_input']);     
                                if(!empty($language_input))
                                    $language_input = substr($language_input,1,strlen($language_input)-2);
                                $languageList = explode(",", $language_input);
                            ?>
                            @foreach($languageList as $language)
                                <label class="cont">
                                    <input name="language_input"  value="{{$language}}" type="radio" title="{{$language}}" placeholder="{{$language}}" />{{$language}}
                                    <span class="checkmark"></span>
                                </label>
                            @endforeach
                        @endif
                    </div>
                    <div class="app-pos-wrapper">
                        <h5 class="sub-title">
                            @if(isset($studioapplyform['position_title'])){{$studioapplyform['position_title']}}@endif
                        </h5>
                        <input type="text" name="position_title" id="position_title" title="Position" />
                    </div>
                    <div class="willing">
                        <label class="check-box">
                            <input name="position1" type="checkbox" title="@if(isset($studioapplyform['position_option1'])){{$studioapplyform['position_option1']}}@endif" placeholder="@if(isset($studioapplyform['position_option1'])){{$studioapplyform['position_option1']}}@endif" value="@if(isset($studioapplyform['position_option1'])){{$studioapplyform['position_option1']}}@endif" />@if(isset($studioapplyform['position_option1'])){{$studioapplyform['position_option1']}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="check-box">
                            <input name="position2" type="checkbox" title="@if(isset($studioapplyform['position_option2'])){{$studioapplyform['position_option2']}}@endif" placeholder="@if(isset($studioapplyform['position_option2'])){{$studioapplyform['position_option2']}}@endif" value="@if(isset($studioapplyform['position_option2'])){{$studioapplyform['position_option2']}}@endif" />@if(isset($studioapplyform['position_option2'])){{$studioapplyform['position_option2']}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="check-box">
                            <input name="position3" type="checkbox" title="@if(isset($studioapplyform['position_option3'])){{$studioapplyform['position_option3']}}@endif" placeholder="@if(isset($studioapplyform['position_option3'])){{$studioapplyform['position_option3']}}@endif" value="@if(isset($studioapplyform['position_option3'])){{$studioapplyform['position_option3']}}@endif" />@if(isset($studioapplyform['position_option3'])){{$studioapplyform['position_option3']}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="check-box">
                            <input name="position4" type="checkbox" title="@if(isset($studioapplyform['position_option4'])){{$studioapplyform['position_option4']}}@endif" placeholder="@if(isset($studioapplyform['position_option4'])){{$studioapplyform['position_option4']}}@endif" value="@if(isset($studioapplyform['position_option4'])){{$studioapplyform['position_option4']}}@endif" />@if(isset($studioapplyform['position_option4'])){{$studioapplyform['position_option4']}}@endif
                            <span class="checkmark"></span>
                        </label>
                    </div>

                    <h5 class="sub-title">
                        @if(isset($studioapplyform['personal_contact_title'])){{$studioapplyform['personal_contact_title']}}@endif
                    </h5>
                    <p class="small-title">@if(isset($studioapplyform['address_title'])){{$studioapplyform['address_title']}}@endif</p>
                    <div class="radio-wrapper">
                        <label class="cont">
                            <input name="gender" type="radio" placeholder="@if(isset($studioapplyform['mr_title'])){{$studioapplyform['mr_title']}}@endif" title="@if(isset($studioapplyform['mr_title'])){{$studioapplyform['mr_title']}}@endif" value="@if(isset($studioapplyform['mr_title'])){{$studioapplyform['mr_title']}}@endif" />@if(isset($studioapplyform['mr_title'])){{$studioapplyform['mr_title']}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="cont">
                            <input name="gender" type="radio" placeholder="@if(isset($studioapplyform['mrs_title'])){{$studioapplyform['mrs_title']}}@endif" title="@if(isset($studioapplyform['mrs_title'])){{$studioapplyform['mrs_title']}}@endif" value="@if(isset($studioapplyform['mrs_title'])){{$studioapplyform['mrs_title']}}@endif" />@if(isset($studioapplyform['mrs_title'])){{$studioapplyform['mrs_title']}}@endif
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-l">
                            <input type="text"  name="first_name" title="@if(isset($studioapplyform['first_name'])){{$studioapplyform['first_name']}}@endif" placeholder="@if(isset($studioapplyform['first_name'])){{$studioapplyform['first_name']}}@endif" />
                        </div>
                        <div class="col-r">
                            <input type="text"  name="middle_name" placeholder="@if(isset($studioapplyform['middle_name'])){{$studioapplyform['middle_name']}}@endif" title="@if(isset($studioapplyform['middle_name'])){{$studioapplyform['middle_name']}}@endif" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-l">
                            <input type="text"  name="last_name" title="@if(isset($studioapplyform['last_name'])){{$studioapplyform['last_name']}}@endif" placeholder="@if(isset($studioapplyform['last_name'])){{$studioapplyform['last_name']}}@endif"/>
                        </div>
                        <div class="col-r">
                            <input type="text" name="address" placeholder="@if(isset($studioapplyform['address'])){{$studioapplyform['address']}}@endif" title="@if(isset($studioapplyform['address'])){{$studioapplyform['address']}}@endif" />
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-l">
                            <input type="text" name="city" title="@if(isset($studioapplyform['city'])){{$studioapplyform['city']}}@endif" placeholder="@if(isset($studioapplyform['city'])){{$studioapplyform['city']}}@endif" />
                        </div>
                        <div class="col-r">
                            <input type="text" name="postal_code" title="@if(isset($studioapplyform['postal_code'])){{$studioapplyform['postal_code']}}@endif" placeholder="@if(isset($studioapplyform['postal_code'])){{$studioapplyform['postal_code']}}@endif" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-l">
                            <select name="nationality1" id="nationality-select-app2" placeholder="@if(isset($studioapplyform['nationality_title'])){{$studioapplyform['nationality_title']}}@endif">
                                <?php $index=0; ?>
                                @foreach(getCountryList() as $nationality)
                                    <option value="{{$nationality['id']}}" data-select2-id="nationality1<?=$index++?>">{{$nationality['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-r">
                            <input type="text"  name="phone" placeholder="@if(isset($studioapplyform['phone'])){{$studioapplyform['phone']}}@endif" title="@if(isset($studioapplyform['phone'])){{$studioapplyform['phone']}}@endif" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-l">
                            <input type="text" name="email"  title="@if(isset($studioapplyform['email'])){{$studioapplyform['email']}}@endif" placeholder="@if(isset($studioapplyform['email'])){{$studioapplyform['email']}}@endif" />
                        </div>
                        <div class="col-r">
                            <input type="date"  name="dob" autocomplete="off" class="hasDatepicker" title="@if(isset($studioapplyform['dob'])){{$studioapplyform['dob']}}@endif" placeholder="@if(isset($studioapplyform['dob'])){{$studioapplyform['dob']}}@endif" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-l">
                            <select name="nationality1" id="nationality-select-app" placeholder="@if(isset($studioapplyform['nationality_title'])){{$studioapplyform['nationality_title']}}@endif">
                                <?php $index=0; ?>
                                @foreach(getCountryList() as $nationality)
                                    <option value="{{$nationality['id']}}" data-select2-id="nationality1<?=$index++?>">{{$nationality['name']}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-r">
                            <p class="add-input1">@if(isset($studioapplyform['another_nationality_title'])){{$studioapplyform['another_nationality_title']}}@endif</p>
                            <select name="nationality2" id="nationality-select-app-2" placeholder="@if(isset($studioapplyform['nationality_title'])){{$studioapplyform['nationality_title']}}@endif">
                                @foreach(getCountryList() as $nationality)
                                    <option value="{{$nationality['id']}}" data-select2-id="nationality2<?=$index++?>">{{$nationality['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="ediucation-wrapper">
                        <p class="sub-title cap">@if(isset($studioapplyform['education_title'])){{$studioapplyform['education_title']}}@endif</p>
                        <div class="edu-content">
                            <label for="institution">@if(isset($studioapplyform['institution_title'])){{$studioapplyform['institution_title']}}@endif</label>
                            <input type="text" name="institution[]" id="institution" placeholder="@if(isset($studioapplyform['institution_title'])){{$studioapplyform['institution_title']}}@endif"  title="@if(isset($studioapplyform['institution_title'])){{$studioapplyform['institution_title']}}@endif" />
                            <label for="">@if(isset($studioapplyform['degree_title'])){{$studioapplyform['degree_title']}}@endif</label>
                            <input type="text" id="degree" name="degree[]" placeholder="@if(isset($studioapplyform['degree_title'])){{$studioapplyform['degree_title']}}@endif" title="@if(isset($studioapplyform['degree_title'])){{$studioapplyform['degree_title']}}@endif" />

                            <div class="row years-wrapper">
                                <div class="col-l">
                                    <select name="start_year[]"  class="start-year">
                                        <?php  $index=0; ?>
                                        @if(isset($studioapplyform['degree_start_year']))
                                            @if(!is_numeric($studioapplyform['degree_start_year']))
                                                <?php $studioapplyform['degree_start_year'] = 1991; ?>
                                            @endif
                                            @for($i=$studioapplyform['degree_start_year'];$i<=date('Y');$i++)
                                                <option value="{{$i}}" data-select2-id="startyear1<?=$index++?>">{{$i}}</option>
                                            @endfor   
                                        @endif
                                    </select>
                                </div>
                                <div class="col-r">
                                   <select name="end_year[]" class="end-year">
                                    <?php  $index=0; ?>
                                    @if(isset($studioapplyform['degree_start_year']))
                                        @if(!is_numeric($studioapplyform['degree_start_year']))
                                            <?php $studioapplyform['degree_start_year'] = 1991; ?>
                                        @endif
                                        @for($i=$studioapplyform['degree_start_year'];$i<=date('Y');$i++)
                                            <option value="{{$i}}" data-select2-id="endyear1<?=$index++?>">{{$i}}</option>
                                        @endfor   
                                    @endif
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <p class="add-education">@if(isset($studioapplyform['another_degree_add_title'])){{$studioapplyform['another_degree_add_title']}}@endif</p>
                        </div>
                    </div>
                    <p class="sub-title">@if(isset($studioapplyform['language_title'])){{$studioapplyform['language_title']}}@endif</p>
                    <div class="radio-wrapper langs">
                        @if(isset($studioapplyform['language_list']))
                            <?php $language_list =  json_encode($studioapplyform['language_list']);     
                                if(!empty($language_list))
                                    $language_list = substr($language_list,1,strlen($language_list)-2);
                                $languageList = explode(",", $language_list);
                            ?>
                            @foreach($languageList as $language)
                                 <label class="cont">
                                    <input name="language_list" type="radio" title="{{$language}}" value="{{$language}}" placeholder="{{$language}}" /> {{$language}}
                                    <span class="checkmark"></span>
                                </label>
                            @endforeach
                        @endif
                        <div class="other-lang-wrapper">
                            <p class="add-other-lang">@if(isset($studioapplyform['language_add_another_title'])){{$studioapplyform['language_add_another_title']}}@endif</p>
                            <input type="text" name="other_language"  class="lang-input" placeholder="@if(isset($studioapplyform['language_add_another_input'])){{$studioapplyform['language_add_another_input']}}@endif" title="@if(isset($studioapplyform['language_add_another_input'])){{$studioapplyform['language_add_another_input']}}@endif" />
                        </div>
                    </div>
                    <div class="profesional-situation-wrapper profile-situation-section">
                        <p class="sub-title cap">@if(isset($studioapplyform['profesional_situation_title'])){{$studioapplyform['profesional_situation_title']}}@endif</p>
                        <label class="cont">
                            <input name="profesional_situation" type="radio" value="@if(isset($studioapplyform['profesional_situation_option1'])){{$studioapplyform['profesional_situation_option1']}}@endif"> @if(isset($studioapplyform['profesional_situation_option1'])){{$studioapplyform['profesional_situation_option1']}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="cont">
                            <input name="profesional_situation" type="radio" value="@if(isset($studioapplyform['profesional_situation_option2'])){{$studioapplyform['profesional_situation_option2']}}@endif"> @if(isset($studioapplyform['profesional_situation_option2'])){{$studioapplyform['profesional_situation_option2']}}@endif <div class="inner-sm">
                                <select name="experience_years" id="experience-years">
                                    @for($i=1;$i<=10;$i++)
                                        <option value="{{$i}}" data-select2-id="experience_years{{$i}}">{{$i}}</option>
                                    @endfor                                    
                                </select>
                            </div>
                             @if(isset($studioapplyform['profesional_situation_option2_year_month'])){{$studioapplyform['profesional_situation_option2_year_month']}}@endif 
                            <span class="checkmark"></span>
                        </label>  
                        <label class="cont">
                            <input name="profesional_situation" type="radio" value=" @if(isset($studioapplyform['profesional_situation_option3'])){{$studioapplyform['profesional_situation_option3']}}@endif" />
                            @if(isset($studioapplyform['profesional_situation_option3'])){{$studioapplyform['profesional_situation_option3']}}@endif
                            <div class="inner-sm">
                                <select name="experience_months" id="experience-months">
                                    @for($i=1;$i<=25;$i++)
                                        <option value="{{$i}}" data-select2-id="experience_months{{$i}}">{{$i}}</option>
                                    @endfor  
                                </select>
                            </div>
                             @if(isset($studioapplyform['profesional_situation_option3_year_month'])){{$studioapplyform['profesional_situation_option3_year_month']}}@endif
                            <span class="checkmark"></span>
                        </label> 
                        <label class="cont">
                            <input name="profesional_situation" type="radio" value="@if(isset($studioapplyform['profesional_situation_option4'])){{$studioapplyform['profesional_situation_option4']}}@endif" />@if(isset($studioapplyform['profesional_situation_option4'])){{$studioapplyform['profesional_situation_option4']}}@endif
                            <div class="inner-lg">
                                <?php $index=0; ?>
                                <select name="experience_countries" id="experience-countries">
                                    @foreach(getCountryList() as $nationality)
                                        <option value="{{$nationality['id']}}" data-select2-id="nationality3<?=$index++?>">{{$nationality['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <span class="checkmark"></span>
                        </label> 
                    </div>                    
                    <div class="prev-apply-wrapper">
                        <p class="sub-title cap">@if(isset($studioapplyform['applied_at_architenko_title'])){{$studioapplyform['applied_at_architenko_title']}}@endif</p>
                        <label class="cont">
                            <input name="applied_at_architenko" type="radio" checked value="@if(isset($studioapplyform['yes_title'])){{$studioapplyform['yes_title']}}@endif" />@if(isset($studioapplyform['yes_title'])){{$studioapplyform['yes_title']}}@endif
                            <span class="checkmark"></span>
                        </label>
                        <label class="cont">
                            <input name="applied_at_architenko" type="radio" value="@if(isset($studioapplyform['no_title'])){{$studioapplyform['no_title']}}@endif" />@if(isset($studioapplyform['no_title'])){{$studioapplyform['no_title']}}@endif
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="how-find-wrapper">
                        <p class="small-title">@if(isset($studioapplyform['aboutus_title'])){{$studioapplyform['aboutus_title']}}@endif</p>
                        <input type="text" name="aboutus" />
                    </div>
                    <div class="attachments-wrapper">
                        <p class="sub-title cap">@if(isset($studioapplyform['attachments_title'])){{$studioapplyform['attachments_title']}}@endif</p>
                        <p>@if(isset($studioapplyform['file_validation_message'])){{$studioapplyform['file_validation_message']}}@endif</p>
                        <table>
                            <tbody><tr>
                                <td>@if(isset($studioapplyform['cover_letter_title'])){{$studioapplyform['cover_letter_title']}}@endif</td>
                                <td><input type="file" name="cover_letter" class="file" placeholder="Choose file"></td>
                            </tr>
                            <tr>
                                <td>@if(isset($studioapplyform['curriculum_vitae_title'])){{$studioapplyform['curriculum_vitae_title']}}@endif</td>
                                <td><input type="file" name="curriculum_vitae" class="file" placeholder="Choose file"></td>
                            </tr>
                        </tbody></table>
                        <p class="portfolio">@if(isset($studioapplyform['portfolio_title'])){{$studioapplyform['portfolio_title']}}@endif
                        <input type="file" name="portfolio" class="file" placeholder="Choose file"></p>
                    </div>

                    <div class="comments">
                        <p class="sub-title">@if(isset($studioapplyform['comment_title'])){{$studioapplyform['comment_title']}}@endif</p>
                        <textarea name="comments"  class="comments"></textarea>
                    </div>
                    <div class="row text-right">
                        <input type="submit" value="@if(isset($studioapplyform['apply_title'])){{$studioapplyform['apply_title']}}@endif" class="submit-cus">
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>
<div class="privacy-poicy-wrapper pop-up-big">    
    @if(isset($termconditionList))             
        <div class="container">
            <div class="row">
                <div class="privacy_policy_close_icon close-icon">close [x]</div>
            </div>
            <div class="row bg-white">
                <div class="col-lg-12 pd-15">
                    <h3 class="section-title">
                        @if(isset($termconditionList->title)){{$termconditionList->title}}@endif
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 pd-15">
                    <div class="inner-wrapper">
                        <?php if(isset($termconditionList->description)){ echo $termconditionList->description; }  ?>
                    </div>
                </div>
                <div class="col-lg-6 pd-15 d-block ">
                    @if(isset($termconditionList->image))
                    <img src="{{URL('images/studio',$termconditionList->image)}}" />
                    @endif
                    <p class="r-title content-title">
                        <?php if(isset($termconditionList->imageTitle)){ echo $termconditionList->imageTitle; }  ?>
                    </p><br>
                    <br/>
                </div>
            </div>                    
        </div>
    @endif</div>
@endsection						
