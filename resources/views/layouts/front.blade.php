<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title }}</title>
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:title" content="Architenko" />
    <meta property="og:type" content="Architenko" />
    <meta property="og:url" content="linktowebsitehere" />
    <meta property="og:image" content="linktobrandinghere" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" integrity="sha256-PIRVsaP4JdV/TIf1FR8UHy4TFh+LiRqeclYXvCPBeiw=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <link href="{{asset('front/build/scripts/lib/twentytwenty.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('front/build/styles/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('front/build/styles/slick.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('front/build/styles/slick.theme.css')}}" rel="stylesheet" type="text/css" />
    @if(Request::is('projectdetails/*'))
    <style type="text/css">
        @media only screen and (max-width: 992px){
            #footer,
            #header.projects-header {
                display: none;
            }/*
            .inner-slider-wrapper .inner{
                position: absolute;
                top: 50%;
                transform: translateY(-50%);
            }
        }
    </style>
    @endif 
    @if(Request::is('articlelist/*'))
    <style type="text/css">
       .subscribe-form form input.submit{
        padding-right:0px !important;
       }
    </style>
    @endif
    @if(Request::is('servicedetails/*'))
        <style type="text/css">
            .main .back-button img {
                margin-bottom: 0;
                -ms-transform: rotate(-180deg);
                transform: rotate(-180deg);
                margin-right: 10px;
            }
            .main .back-button{
                font-weight: bold;
                float: right;
                margin-top: 25px;
                margin-bottom: 15px;
            }  
            .pd-10{
                padding-left: 10px;
                padding-right: 10px; 
            }  
        </style>
     @endif
</head>
<body>
 @if(Request::is('projectdetails/*') || Request::is('servicedetails/*')) 
<div id="header" class="{{ (isset($ishomepage) && $ishomepage != '') ? $ishomepage : '' }} services projects-header">
    <div class="container">
        <div class="row">
            <div class="col-l">
                <a href="" class="logo">
                    <img class="d-logo" src="{{ asset('images/') }}/{{ $logo->logo }}" alt="">
                    <img class="m-logo" src="{{ asset('images/') }}/{{ $logo->transparent_logo }}" alt="">
                </a>
                <div class="menu-bar">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </div>
            </div>
            <div class="col-r">
                <div class="main-nav-wrapper">
                    <h3 class="menu-title">
                        <img class="m-logo" src="{{ asset('images/') }}/{{ $logo->menutoggle_logo }}" alt="">
                    </h3>
                    <div class="close-btn">
                        <i class="fa fa-times-circle"></i>
                    </div>
                    <nav id="main-nav">
                        <ul class="clearfix">                         
                            <?php
                            $checkmenu = array();                           
                            foreach($submenus as $menuid){
                                $checkmenu[] = $menuid['parent'];                               
                            }
                            ?>
                            @foreach($menus as $menu)
                            <li><a  href="{{URL($menu->link)}}" class="@if($menu->link == Request::path()) active @endif">{{$menu->name}}</a>
                            @if(in_array($menu->id,$checkmenu))
                            <ul class="subuldt">
                            @foreach($submenus as $submenu)
                            @if($submenu->parent == $menu->id)
                            <li><a href="{{$submenu->link}}">{{$submenu->name}}</a></li>    
                            @endif
                            @endforeach
                            </ul>
                            @endif
                            </li>
                            @endforeach
                            
                            <li class="lang-selector">
                                <a>{{strtoupper(App::getLocale())}}<span></span></a>
                                <ul>
                                @if(count($languages) > 0)
                                    @foreach($languages as $lang)
                                        <li><a href="{{URL('lang/'.$lang->name)}}">{{strtoupper($lang->name)}}</a></li>
                                    @endforeach
                                @endif
                                    </ul>
                            </li>                            
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@else

<div id="header" class="{{ (isset($ishomepage) && $ishomepage != '') ? $ishomepage : '' }}">
    <div class="container">
        <div class="row">
            <div class="col-l">
                <a href="" class="logo">
                    <img class="d-logo" src="{{ asset('images/') }}/{{ $logo->logo }}" alt="">
                    <img class="m-logo" src="{{ asset('images/') }}/{{ $logo->transparent_logo }}" alt="">
                </a>
                <div class="menu-bar">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </div>
            </div>
            <div class="col-r">
                <div class="main-nav-wrapper">
                    <h3 class="menu-title">
                        <img class="m-logo" src="{{ asset('images/') }}/{{ $logo->menutoggle_logo }}" alt="">
                    </h3>
                    <div class="close-btn">
                        <i class="fa fa-times-circle"></i>
                    </div>
                    <nav id="main-nav">
                        <ul class="clearfix">                         
                            <?php
                            $checkmenu = array();                           
                            foreach($submenus as $menuid){
                                $checkmenu[] = $menuid['parent'];                               
                            }
                            ?>
                            @foreach($menus as $menu)
                            <li>
                                @if($menu->link=="studiolist")
                                    <a href="javascript:void(0)" class="@if($menu->link == Request::path()) active @endif">
                                        {{$menu->name}}
                                    </a>
                                    @if(in_array($menu->id,$checkmenu))
                                        <ul class="subuldt">
                                        @foreach($submenus as $submenu)
                                            @if($submenu->parent == $menu->id)
                                                <li><a href="javascript:void(0)" onclick="redirect('{{$submenu->link}}','{{$menu->link}}')">{{$submenu->name}}</a></li>
                                            @endif
                                        @endforeach
                                        </ul>
                                    @endif
                                @else   
                                    @if($menu->link=="servicelist") 
                                    <a href="javascript:void(0)" class="@if($menu->link == Request::path()) active @endif">
                                        {{$menu->name}}
                                    </a>
                                    @else
                                        <a href="{{URL($menu->link)}}" class="@if($menu->link == Request::path()) active @endif">
                                            {{$menu->name}}
                                        </a>
                                    @endif


                                    @if(in_array($menu->id,$checkmenu))
                                        <ul class="subuldt">
                                        @foreach($submenus as $submenu)
                                            @if($submenu->parent == $menu->id)
                                                <li><a href="{{$submenu->link}}">{{$submenu->name}}</a></li>    
                                            @endif
                                        @endforeach
                                        </ul>
                                    @endif
                                @endif
                            </li>
                            @endforeach
                            
                            <li class="lang-selector">
                                <a>{{strtoupper(App::getLocale())}}<span></span></a>
                                <ul>
                                @if(count($languages) > 0)
                                @foreach($languages as $lang)
                                    <li><a href="{{URL('lang/'.$lang->name)}}">{{strtoupper($lang->name)}}</a></li>
                                    @endforeach
                                    @endif
                                    </ul>
                            </li>
                            
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

 @yield('content')
</body>
 <a href="" id="back-to-top">
 
 <img src="{{asset('front/build/static/link-arrow.png')}}"/> {{getLabel('backtotop')}} </a>
<footer id="footer" class="{{ (isset($ishomepage) && $ishomepage != '') ? 'footer-home' : '' }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <p>{{$config['mobile']}}, {{$config['address']}}</p>
            </div>
            <div class="col-lg-3 text-center">
                <a href="mailto:{{$config['email']}}">{{$config['email']}}</a>
            </div>
            <div class="col-lg-3 text-right">
                <ul>
                @if(count($sociallinks) > 0)
                @foreach($sociallinks as $social)
                    <li><a class="social" href="{{$social->link}}">{{$social->name}}</a></li>
                @endforeach
                @endif
                </ul>
            </div>
        </div>
    </div>
     @if(Request::is('/'))
        <div id="socket">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6" >
                    <?php echo strip_tags($config['description'],'<a><em>');?> </div>
                    <div class="col-lg-6">
                        <?php echo $config['description2'];?>
                    </div>
                </div>
            </div>
        </div>
    @endif
</footer>
<div id="more-info-wrapper" class="pop-up-big" data-select2-id="more-info-wrapper">
    
        <div class="container">
        <div class="row">
            <div class="hide close-icon">close [x]</div>
        </div>
        <div class="row" data-select2-id="9">
            <?php $moreinformation = getMoreInformation();  ?>
            @if(!empty($moreinformation))    
                <div class="col" data-select2-id="8">
                    <h4 class="section-title">@if(isset($moreinformation['title'])) {{$moreinformation['title']}} @endif</h4>
                    <h5 class="sub-title">@if(isset($moreinformation['service_title'])) {{$moreinformation['service_title']}} @endif</h5>
                    <form class="moreInformationForm" id="moreInformationForm" method="POST">
                        <select name="service_id" id="service-select" data-select2-id="service-select" tabindex="-1" class="select2-hidden-accessible" aria-hidden="true">                      
                            @foreach(getServiceList() as $service)
                                <option value="{{$service['id']}}" data-select2-id="{{$service['id']}}">{{$service['title']}}</option>
                            @endforeach                     
                        </select>
                        <label for="address">
                            @if(isset($moreinformation['project_address_title'])) {{$moreinformation['project_address_title']}} @endif
                        </label>
                            <input id="project_address" name="project_address" type="text" placeholder="@if(isset($moreinformation['project_address_input_title'])){{$moreinformation['project_address_input_title']}}@endif" title="@if(isset($moreinformation['project_address_input_title'])){{$moreinformation['project_address_input_title']}}@endif" />

                        <label for="question">
                            @if(isset($moreinformation['description_question_project_title'])) {{$moreinformation['description_question_project_title']}} @endif
                        </label>
                        <textarea id="description_project" name="description_project" title="Question" ></textarea>

                        <h5 class="sub-title">                            
                            @if(isset($moreinformation['personal_contact_title'])) {{$moreinformation['personal_contact_title']}} @endif
                        </h5>
                        <p class="small-title">@if(isset($moreinformation['address_title'])) {{$moreinformation['address_title']}} @endif</p>
                        <div class="radio-wrapper">
                            <label class="cont">
                                <input name="gender" title="@if(isset($moreinformation['mr_title'])){{$moreinformation['mr_title']}}@endif" type="radio" checked="checked" value="@if(isset($moreinformation['mr_title'])){{$moreinformation['mr_title']}}@endif" />@if(isset($moreinformation['mr_title'])){{$moreinformation['mr_title']}}@endif
                                <span class="checkmark"></span>
                            </label>
                            <label class="cont">
                                <input name="gender" title="@if(isset($moreinformation['mrs_title'])){{$moreinformation['mrs_title']}}@endif" value="@if(isset($moreinformation['mrs_title'])){{$moreinformation['mrs_title']}}@endif" type="radio" />@if(isset($moreinformation['mrs_title'])) {{$moreinformation['mrs_title']}} @endif
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="row">
                            <div class="col-l">
                                <input type="text" id="first_name" name="first_name" title="@if(isset($moreinformation['first_name'])){{$moreinformation['first_name']}}@endif"  placeholder="@if(isset($moreinformation['first_name'])){{$moreinformation['first_name']}}@endif" />
                            </div>
                            <div class="col-r">
                                <input type="text" id="middle_name" name="middle_name" title="@if(isset($moreinformation['middle_name'])){{$moreinformation['middle_name']}}@endif" placeholder="@if(isset($moreinformation['middle_name'])){{$moreinformation['middle_name']}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-l">
                                <input type="text" id="last_name" name="last_name" placeholder="@if(isset($moreinformation['last_name'])){{$moreinformation['last_name']}}@endif" title="@if(isset($moreinformation['last_name'])){{$moreinformation['last_name']}}@endif" />
                            </div>
                            <div class="col-r">
                                <input type="text" title="@if(isset($moreinformation['address'])){{$moreinformation['address']}}@endif" id="address" name="address" placeholder="@if(isset($moreinformation['address'])){{$moreinformation['address']}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-l">
                                <input type="text" name="city" id="city"  title="@if(isset($moreinformation['city'])){{$moreinformation['city']}}@endif" placeholder="@if(isset($moreinformation['city'])){{$moreinformation['city']}}@endif" />
                            </div>
                            <div class="col-r">
                                <input type="text" name="postal_code" id="postal_code" title="@if(isset($moreinformation['postal_code'])){{$moreinformation['postal_code']}}@endif" placeholder="@if(isset($moreinformation['postal_code'])){{$moreinformation['postal_code']}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-l">
                                <select name="nationality1" id="nationality-select-app1" placeholder="@if(isset($studioapplyform['nationality_title'])){{$studioapplyform['nationality_title']}}@endif">
                                    <?php $index=0; ?>
                                    @foreach(getCountryList() as $nationality)
                                        <option value="{{$nationality['id']}}" data-select2-id="nationality1<?=$index++?>">{{$nationality['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-r">
                                <input type="text" name="phone" id="phone" title="@if(isset($moreinformation['phone'])){{$moreinformation['phone']}}@endif" placeholder="@if(isset($moreinformation['phone'])){{$moreinformation['phone']}}@endif" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-l">
                                <input type="text" id="email" name="email"  title="@if(isset($moreinformation['email'])){{$moreinformation['email']}}@endif" placeholder="@if(isset($moreinformation['email'])){{$moreinformation['email']}}@endif" />
                            </div>
                            <div class="col-r">
                                <input type="date" id="dob" name="dob" autocomplete="off" class="hasDatepicker" title="@if(isset($moreinformation['dob'])){{$moreinformation['dob']}}@endif" placeholder="@if(isset($moreinformation['dob'])){{$moreinformation['dob']}}@endif">
                            </div>
                        </div>
                        <div class="row">
                            <label for="">@if(isset($moreinformation['iam_title'])){{$moreinformation['iam_title']}}@endif</label>
                            <select name="person_position" id="person-position" data-select2-id="person-position" tabindex="-1" class="select2-hidden-accessible person_position" aria-hidden="true">
                                <option id="privateIndividual" value="@if(isset($moreinformation['private_individual_title'])){{$moreinformation['private_individual_title']}}@endif">
                                    @if(isset($moreinformation['private_individual_title'])){{$moreinformation['private_individual_title']}}@endif
                                </option>
                                <option  value="@if(isset($moreinformation['enterprise_title'])){{$moreinformation['enterprise_title']}}@endif">
                                    @if(isset($moreinformation['enterprise_title'])){{$moreinformation['enterprise_title']}}@endif
                                </option>
                                <option value="@if(isset($moreinformation['developer_title'])){{$moreinformation['developer_title']}}@endif">
                                    @if(isset($moreinformation['developer_title'])){{$moreinformation['developer_title']}}@endif
                                </option> 
                                <option value="@if(isset($moreinformation['governmental_title'])){{$moreinformation['governmental_title']}}@endif">
                                    @if(isset($moreinformation['governmental_title'])){{$moreinformation['governmental_title']}}@endif
                                </option>     
                               
                            </select>
                        </div>
                        <div class="private-data-wrapper">
                            <p class="small-title">@if(isset($moreinformation['private_individual_address_title'])){{$moreinformation['private_individual_address_title']}}@endif</p>
                            <div class="radio-wrapper">
                                <label class="cont">
                                    <input name="private_individual_gender" type="radio" title="@if(isset($moreinformation['private_individual_mr_title'])){{$moreinformation['private_individual_mr_title']}}@endif" placeholder="@if(isset($moreinformation['private_individual_mr_title'])){{$moreinformation['private_individual_mr_title']}}@endif" value="@if(isset($moreinformation['private_individual_mr_title'])){{$moreinformation['private_individual_mr_title']}}@endif" checked="">@if(isset($moreinformation['private_individual_mr_title'])){{$moreinformation['private_individual_mr_title']}}@endif
                                    <span class="checkmark"></span>
                                </label>
                                <label class="cont">
                                    <input name="private_individual_gender" type="radio" title="@if(isset($moreinformation['private_individual_mrs_title'])){{$moreinformation['private_individual_mrs_title']}}@endif" placeholder="@if(isset($moreinformation['private_individual_mrs_title'])){{$moreinformation['private_individual_mrs_title']}}@endif" value="@if(isset($moreinformation['private_individual_mrs_title'])){{$moreinformation['private_individual_mrs_title']}}@endif" />@if(isset($moreinformation['private_individual_mrs_title'])){{$moreinformation['private_individual_mrs_title']}}@endif
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-l">
                                    <input type="text" id="private_individual_first_name" name="private_individual_first_name" placeholder="First Name"  title="@if(isset($moreinformation['private_individual_first_name'])){{$moreinformation['private_individual_first_name']}}@endif" placeholder="@if(isset($moreinformation['private_individual_first_name'])){{$moreinformation['private_individual_first_name']}}@endif" />
                                </div>
                                <div class="col-r">
                                    <input type="text"  id="private_individual_middle_name" name="private_individual_middle_name" placeholder="Midlle Name" title="@if(isset($moreinformation['private_individual_middle_name'])){{$moreinformation['private_individual_middle_name']}}@endif" placeholder="@if(isset($moreinformation['private_individual_middle_name'])){{$moreinformation['private_individual_middle_name']}}@endif" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-l">
                                    <input type="text" id="private_individual_last_name" name="private_individual_last_name" title="@if(isset($moreinformation['private_individual_last_name'])){{$moreinformation['private_individual_last_name']}}@endif" placeholder="@if(isset($moreinformation['private_individual_last_name'])){{$moreinformation['private_individual_last_name']}}@endif" />
                                </div>
                                <div class="col-r">
                                    <input type="text" id="private_individual_address" name="private_individual_address" title="@if(isset($moreinformation['private_individual_address'])){{$moreinformation['private_individual_address']}}@endif" placeholder="@if(isset($moreinformation['private_individual_address'])){{$moreinformation['private_individual_address']}}@endif" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-l">
                                    <input type="text" id="private_individual_city" name="private_individual_city" title="@if(isset($moreinformation['private_individual_city'])){{$moreinformation['private_individual_city']}}@endif" placeholder="@if(isset($moreinformation['private_individual_city'])){{$moreinformation['private_individual_city']}}@endif" />
                                </div>
                                <div class="col-r">
                                    <input type="text" id="private_individual_postal_code" name="private_individual_postal_code" title="@if(isset($moreinformation['private_individual_postal_code'])){{$moreinformation['private_individual_postal_code']}}@endif" placeholder="@if(isset($moreinformation['private_individual_postal_code'])){{$moreinformation['private_individual_postal_code']}}@endif" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-l">
                                    <input type="text" id="private_individual_country" name="private_individual_country" title="@if(isset($moreinformation['private_individual_country'])){{$moreinformation['private_individual_country']}}@endif" placeholder="@if(isset($moreinformation['private_individual_country'])){{$moreinformation['private_individual_country']}}@endif" />
                                </div>
                                <div class="col-r">
                                     <input type="text" id="private_individual_phone" name="private_individual_phone" title="@if(isset($moreinformation['private_individual_phone'])){{$moreinformation['private_individual_phone']}}@endif" placeholder="@if(isset($moreinformation['private_individual_phone'])){{$moreinformation['private_individual_phone']}}@endif" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-l">
                                     <input type="text" id="private_individual_email" name="private_individual_email" title="@if(isset($moreinformation['private_individual_email'])){{$moreinformation['private_individual_email']}}@endif" placeholder="@if(isset($moreinformation['private_individual_email'])){{$moreinformation['private_individual_email']}}@endif" />
                                </div>
                                <div class="col-r">
                                      <input type="date" id="private_individual_dob" name="private_individual_dob" title="@if(isset($moreinformation['private_individual_dob'])){{$moreinformation['private_individual_dob']}}@endif" placeholder="@if(isset($moreinformation['private_individual_dob'])){{$moreinformation['private_individual_dob']}}@endif" />
                                </div>
                            </div>
                        </div>
                        <div class="enterprise-data-wrapper" >
                            <div class="row">
                                <div class="col-l">
                                     <input type="text" id="enterprise_price_title1" name="enterprise_price_title1" title="@if(isset($moreinformation['enterprise_price_title1'])){{$moreinformation['enterprise_price_title1']}}@endif" placeholder="@if(isset($moreinformation['enterprise_price_title1'])){{$moreinformation['enterprise_price_title1']}}@endif" />
                                </div>
                                <div class="col-r">
                                    <input type="text" id="enterprise_price_title2" name="enterprise_price_title2" title="@if(isset($moreinformation['enterprise_price_title2'])){{$moreinformation['enterprise_price_title2']}}@endif" placeholder="@if(isset($moreinformation['enterprise_price_title2'])){{$moreinformation['enterprise_price_title2']}}@endif" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-l">
                                   <input type="text" id="enterprise_address_title" name="enterprise_address_title" title="@if(isset($moreinformation['enterprise_address_title'])){{$moreinformation['enterprise_address_title']}}@endif" placeholder="@if(isset($moreinformation['enterprise_address_title'])){{$moreinformation['enterprise_address_title']}}@endif" />                                
                                </div>
                                <div class="col-r">
                                    <input type="text" id="enterprise_city_title" name="enterprise_city_title" title="@if(isset($moreinformation['enterprise_city_title'])){{$moreinformation['enterprise_city_title']}}@endif" placeholder="@if(isset($moreinformation['enterprise_city_title'])){{$moreinformation['enterprise_city_title']}}@endif" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-l">
                                     <input type="text" id="enterprise_postal_code_title" name="enterprise_postal_code_title" title="@if(isset($moreinformation['enterprise_postal_code_title'])){{$moreinformation['enterprise_postal_code_title']}}@endif" placeholder="@if(isset($moreinformation['enterprise_postal_code_title'])){{$moreinformation['enterprise_postal_code_title']}}@endif" /> 
                                </div>
                                <div class="col-r">
                                    <input type="text" id="enterprise_country_title" name="enterprise_country_title" title="@if(isset($moreinformation['enterprise_country_title'])){{$moreinformation['enterprise_country_title']}}@endif" placeholder="@if(isset($moreinformation['enterprise_country_title'])){{$moreinformation['enterprise_country_title']}}@endif" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-l">                                   
                                     <input type="text" id="enterprise_phone_title" name="enterprise_phone_title" title="@if(isset($moreinformation['enterprise_phone_title'])){{$moreinformation['enterprise_phone_title']}}@endif" placeholder="@if(isset($moreinformation['enterprise_phone_title'])){{$moreinformation['enterprise_phone_title']}}@endif" />
                                </div>
                                <div class="col-r">
                                    <input type="text" id="enterprise_email_title" name="enterprise_email_title" title="@if(isset($moreinformation['enterprise_email_title'])){{$moreinformation['enterprise_email_title']}}@endif" placeholder="@if(isset($moreinformation['enterprise_email_title'])){{$moreinformation['enterprise_email_title']}}@endif" />
                                </div>
                            </div>
                            <p class="small-title">@if(isset($moreinformation['enterprise_contact_title'])){{$moreinformation['enterprise_contact_title']}}@endif</p>
                            <div class="radio-wrapper">
                                <label class="cont">
                                    <input name="enterprise_gender" type="radio" title="@if(isset($moreinformation['enterprise_mr_title'])){{$moreinformation['enterprise_mr_title']}}@endif" placeholder="@if(isset($moreinformation['enterprise_mr_title'])){{$moreinformation['enterprise_mr_title']}}@endif" value="@if(isset($moreinformation['enterprise_mr_title'])){{$moreinformation['enterprise_mr_title']}}@endif" checked/>
                                    <span class="checkmark"></span>@if(isset($moreinformation['enterprise_mr_title'])){{$moreinformation['enterprise_mr_title']}}@endif
                                </label>
                                <label class="cont">
                                    <input name="enterprise_gender" type="radio"  title="@if(isset($moreinformation['enterprise_mrs_title'])){{$moreinformation['enterprise_mrs_title']}}@endif" placeholder="@if(isset($moreinformation['enterprise_mrs_title'])){{$moreinformation['enterprise_mrs_title']}}@endif" value="@if(isset($moreinformation['enterprise_mrs_title'])){{$moreinformation['enterprise_mrs_title']}}@endif" />
                                    <span class="checkmark"></span>@if(isset($moreinformation['enterprise_mrs_title'])){{$moreinformation['enterprise_mrs_title']}}@endif
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-l">
                                    <input type="text" id="enterprise_contact_first_name_title" name="enterprise_contact_first_name_title" title="@if(isset($moreinformation['enterprise_contact_first_name_title'])){{$moreinformation['enterprise_contact_first_name_title']}}@endif" placeholder="@if(isset($moreinformation['enterprise_contact_first_name_title'])){{$moreinformation['enterprise_contact_first_name_title']}}@endif" /> 
                                </div>
                                <div class="col-r">
                                    <input type="text" id="enterprise_contact_last_name_title" name="enterprise_contact_last_name_title" title="@if(isset($moreinformation['enterprise_contact_last_name_title'])){{$moreinformation['enterprise_contact_last_name_title']}}@endif" placeholder="@if(isset($moreinformation['enterprise_contact_last_name_title'])){{$moreinformation['enterprise_contact_last_name_title']}}@endif" />
                                </div>
                            </div>
                            <div class="row">
                                <select name="enterprise_position" id="enterprise-position-toolkit">
                                    @if(isset($moreinformation['enterprise_contact_personpositionlist_title']))
                                    <?php $personpositionlist =  json_encode($moreinformation['enterprise_contact_personpositionlist_title']);     
                                        if(!empty($personpositionlist))
                                            $personpositionlist = substr($personpositionlist,1,strlen($personpositionlist)-2);
                                        $personpositionList = explode(",", $personpositionlist);
                                    ?>
                                    @foreach($personpositionList as $position)
                                        <option value="{{$position}}">{{$position}}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>
                        </div>
                                
                        <div class="row text-center">
                            @csrf
                            <input type="submit" value="@if(isset($moreinformation['submit'])){{$moreinformation['submit']}}@endif" class="submit-cus" />
                        </div>
                    </form>
                </div>
            @endif
        </div>
    </div>    
</div>
  
<div id="loader-wrapper" class="loader hide">
    <img class="loaderimage" src="{{URL('images/1575910459.png')}}" width="100px">
</div>  

<script src="{{asset('front/build/scripts/jquery-3.4.1.js')}}"></script>
<script src="{{asset('front/build/scripts/select2.min.js')}}"></script>
<script src="{{asset('front/build/scripts/jquery.validate.min.js')}}"></script>
<script src="{{asset('front/build/scripts/additional-methods.min.js')}}"></script>
<script src="{{asset('front/build/scripts/jquery-ui.min.js')}}"></script>
<script src="{{asset('front/build/scripts/lib/jquery.event.move.js')}}"></script>
<script src="{{asset('front/build/scripts/lib/jquery.twentytwenty.js')}}"></script>
<script src="{{asset('front/build/scripts/before-after.js')}}"></script>
<script src="{{asset('front/build/scripts/book.js')}}"></script>
<script src="{{asset('front/build/scripts/more-info.js')}}"></script>
<script src="{{asset('front/build/scripts/lozad.min.js')}}"></script>
<script src="{{asset('front/build/scripts/slick.min.js')}}" ></script>
<script src="{{asset('front/build/scripts/app.js')}}"></script>
<script src="{{asset('front/build/scripts/custom.js')}}"></script> 


@if(Request::is('studiolist'))
<script type="text/javascript">    
    $("#studioApplyForm").submit(function(){
        let studioApplyForm =  new FormData($("#studioApplyForm")[0]);
        let formdata = $("#studioApplyForm").serializeArray();
       let form = {};
        $.each(formdata, function(i, field){            
            form[field.name] = field.value;
        });
        
       let data = JSON.stringify({
        email:form["email"],
        first_name:form["first_name"]+" "+form["middle_name"],
        last_name:form["last_name"],
        country:form["country"],
        phone:form["phone"],       
        tag_list:["studioapplyform"],
        custom_fields:{
            "Postal Code":form["postal_code"],
            "dob":form["dob"],
            "Country 2":form["nationality2"],
            "Language of correspondence":form["language_input"],        
            "Person Position":form["position_title"],
            "Position 1":form["position1"],
            "Position 2":form["position2"],
            "Position 3":form["position3"],
            "Position 4":form["position4"],
            "Gender":form["gender"],
            "Languages":form["language"],
            "Nationality 1":form["nationality1"],
            "Nationality 2":form["nationality2"],
            "Institution":form["institution[0]"],
            "Degree":form["degree[0]"],
            "Start Year":form["start_year[0]"],
            "End Year":form["end_year[0]"],
            "Language":form["language_list"],
            "Other Language":form["other_language"],
            "Experience Years":form["experience_years"],
            "Experience Month":form["experience_months"],
            "Experience Countries":form["experience_countries"],
            "Before Applied At Architenko":form["applied_at_architenko"],
            "About":form["aboutus"],
            "Comments":form["comments"]           
          }
        });
        fetch("https://api2.myclients.io/v2/clients?teams_view_filter=all", {
         "credentials": "include",
         "headers": {
          "accept": "application/json, text/plain, */*",
          "accept-language": "en-GB,en-US;q=0.9,en;q=0.8,nl;q=0.7",
          "authorization": "Token b002c819f2ff90916c97422e45d5874ba96a62227b8689271ca4a58f14339008",
          "cache-control": "no-cache",
          "content-type": "application/json;charset=UTF-8",
          "pragma": "no-cache",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "cross-site",
          "x-app-name": "Frontage"
         },
         "referrer": "https://app.vcita.com/app/clients",
         "referrerPolicy": "no-referrer-when-downgrade",
         "body": data,
         "method": "POST",
         "mode": "cors"
        }).then((response) => {
            return response.json();
        }).then((myJson) => {
            let client_id = myJson.id; 
            data = JSON.stringify({"uid":client_id,tags:"privacypolicy"});
            fetch("https://api2.myclients.io/v2/tags?teams_view_filter=all", {
            "credentials": "include",
            "headers": {
            "accept": "application/json, text/plain, */*",
            "accept-language": "en-GB,en-US;q=0.9,en;q=0.8,nl;q=0.7",
            "authorization": "Token b002c819f2ff90916c97422e45d5874ba96a62227b8689271ca4a58f14339008",
            "cache-control": "no-cache",
            "content-type": "application/json;charset=UTF-8",
            "pragma": "no-cache",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site",
            "x-app-name": "Frontage"
            },
            "referrer": "https://app.vcita.com/app/clients",
            "referrerPolicy": "no-referrer-when-downgrade",
            "body": data,
            "method": "POST",
            "mode": "cors"
            });
        });
        $(".loader").removeClass("hide");
        $.ajax({
            url:"{{route('storestudioapplyform')}}",
            type:"POST",
            data:studioApplyForm,
            cache: false,
            contentType: false,
            processData: false,
            success:function(response){
                let data = JSON.parse(JSON.stringify(response));
                $(".loader").addClass("hide");
                if(data.success){
                    alert(data.message);
                }
                $("#apply-wrapper").removeClass("active");
                $("body").removeClass("active");
                $("body").removeClass("over");                
            },
            error:function(error) {
                 $(".loader").addClass("hide");
                console.error(error);
            }
        });
        return false;
    });
</script>
@endif

@if(Request::is('articlelist'))
<script type="text/javascript">
    function getArticleList(){
        $('.result-wrapper').addClass('active');
        data = $('.search-input').val().trim();
        $.ajax({
            url:"{{ route('articlesearch') }}",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:"POST",
            data:{data: data},
            success:function(response){ 
                $('#result-inner').html(response.data);
                $('.result-wrapper').addClass('active');
                $('.search-bar-bg').addClass('active');
                $('.search-bar').addClass('active');
                $('.input-wrapper').addClass('active');
                $('body').addClass('over');
            },
            error:function(error){
                console.log(error);
            }
        });
    }


    $('.search-input').on('click',function(e){
        getArticleList();
    });

    $('.search-input').on('input',function(e){
        getArticleList();
        if($(this).val().trim() == ''){
             $('.result-wrapper').removeClass('active');
        }
    });
    $('.search-input').keypress(function(e) {
        if(e.which == 13) {
            if($(this).val().trim() != ''){
               window.location = '{{ URL("articlelist") }}?search='+$(this).val().trim();
            }
        }
    });

    $(document).keyup(function(e) {
         if (e.key === "Escape") { // escape key maps to keycode `27`
             $('.result-wrapper').removeClass('active')
             $('.search-bar-bg').removeClass('active')
             $('.search-input').removeClass('active')
             $('body').removeClass('over')
         }
    });
     $('.search-bar-bg').on('click',function(e){
            $('.result-wrapper').removeClass('active');
            $('.search-bar-bg').removeClass('active');
            $('.search-bar').removeClass('active');
            $('.input-wrapper').removeClass('active');
            $('body').removeClass('over');
        });
</script>
@endif
@if(Request::is('projectlist'))
<script type="text/javascript">
    function getProjectist(){
        $('.result-wrapper').addClass('active');
        data = $('.search-input').val().trim();
        $.ajax({
            url:"{{ route('searchProject') }}",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:"POST",
            data:{data: data},
            success:function(response){ 
                $('#result-inner').html(response.data);
                $('.result-wrapper').addClass('active');
                $('.search-bar-bg').addClass('active');
                $('.search-bar').addClass('active');
                $('.input-wrapper').addClass('active');
                $('body').addClass('over');
            },
            error:function(error){
                console.log(error);
            }
        });
    }
    $('.search-input').on('click',function(e){
        getProjectist();
    });
    $('.search-input').on('input',function(e){
        getProjectist();
        if($(this).val().trim() == ''){
             $('.result-wrapper').removeClass('active');
        }
    });
    $('.search-input').keypress(function(e) {
        if(e.which == 13) {
            if($(this).val().trim() != ''){
               window.location = '{{ URL("projectlist") }}?search='+$(this).val().trim();
            }
        }
    });
    $(document).keyup(function(e) {
        if (e.key === "Escape") { // escape key maps to keycode `27`
            $('.result-wrapper').removeClass('active')
            $('.search-bar-bg').removeClass('active')
            $('.search-input').removeClass('active')
            $('body').removeClass('over')
        }
    });
    $('.search-bar-bg').on('click',function(e){
        $('.result-wrapper').removeClass('active');
        $('.search-bar-bg').removeClass('active');
        $('.search-bar').removeClass('active');
        $('.input-wrapper').removeClass('active');
        $('body').removeClass('over');
    });
</script>
@endif

@if(Request::is('/'))
    <script type="text/javascript">
        let row_number = 1;
        let offset = 1;
        let isRequestComplete = 0;
        let isLastRecord = false;
        jQuery( window ).scroll(function() {
            if(!isLastRecord) {
                if(($(window).scrollTop() + $(window).height())  > ($(document).height()-50)){
                    if(isRequestComplete==0) {                  
                        isRequestComplete++;
                        $("#loader-wrapper").removeClass("hide");
                        $.ajax({
                            url:"{{route('getProjectList')}}",
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            type:"POST",
                            data:{offset:offset},
                            success:function(response){
                                let data = JSON.parse(JSON.stringify(response));
                                if(data.success){
                                    $(".service-section").append(data.data);
                                    isLastRecord = data.isLastRecord;
                                }
                            },
                            error:function(error){
                                console.log(error);
                            },
                            complete: function() {
                                isRequestComplete = 0;
                                offset++;
                                $("#loader-wrapper").addClass("hide");                                                 
                            }
                        }); 
                    }
                }
            }       
        }); 

    </script> 
@endif       
@if(Request::is('contactus'))
    <script type="text/javascript">
        $(document).keyup(function(e) {
            if (e.key === "Escape") { // escape key maps to keycode `27`
                $(".directions-wrapper").removeClass('active');
                $('body').removeClass('over');
            }
        });
    </script>
@endif   
@if(Request::is('projectlist'))
    <script type="text/javascript">
        // let sentData = {};
        // $('.search-input').keyup(function(e) {
        //     if(e.which !== 40 && e.which !== 38) {
        //         sentData.searchText = $(this).val();
        //         if (sentData.searchText) {
        //             sendToServer();
        //         }
        //         else {
        //             $('.result-wrapper').removeClass('active');
        //             $('.search-bar-bg').removeClass('active');
        //             $('.search-bar').removeClass('active');
        //             $('.input-wrapper').removeClass('active');
        //             $('body').removeClass('over');
        //         }
        //         scrollValue=0;
        //         liSelected="";
        //     }
        // });
        // function sendToServer(){
        //     $('.result-wrapper').addClass('active');
        //     data = $('.search-input').val().trim();
        //     $.ajax({
        //         url:"{{ route('searchProject') }}",
        //         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        //         type:"POST",
        //         data:{data: data},
        //         success:function(response){ 
        //             $('#result-inner').html(response.data);
        //             $('.result-wrapper').addClass('active');
        //             $('.search-bar-bg').addClass('active');
        //             $('.search-bar').addClass('active');
        //             $('.input-wrapper').addClass('active');
        //             $('body').addClass('over');
        //         },
        //         error:function(error){
        //             console.log(error);
        //         }
        //     });                
        //     $('.result-wrapper').addClass('active');
        //     $('.search-bar-bg').addClass('active');
        //     $('.search-bar').addClass('active');
        //     $('.input-wrapper').addClass('active');
        //     $('body').addClass('over');
        // }

        // $('.search-bar-bg').on('click',function(e){
        //     $('.result-wrapper').removeClass('active');
        //     $('.search-bar-bg').removeClass('active');
        //     $('.search-bar').removeClass('active');
        //     $('.input-wrapper').removeClass('active');
        //     $('body').removeClass('over');
        // });
    </script>
@endif
@php $service = getServiceList(); @endphp
<script type="text/javascript">
    let serviceName = JSON.parse('@json($service)');
    $("#moreInformationForm").validate({
        errorClass: "text-danger",          
        rules: {
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
            },
        },
        messages: {
            email: {
                required: 'Please enter your email address',
                email: 'Please enter valid email address'
            },
            phone: {
                required: 'Please enter your phone number',
            }
        },
        submitHandler:function(){           
            let formdata = $("#moreInformationForm").serializeArray();
            let form = {};
            $.each(formdata, function(i, field){            
                form[field.name] = field.value;
            });
            let data = JSON.stringify({
            email:form["email"],
            first_name:form["first_name"]+" "+form["middle_name"],
            last_name:form["last_name"],
            country:form["country"],
            phone:form["phone"],
            tag_list:["moreinformation"],
            custom_fields:{
                "Service Name":serviceName[form["service_id"]],
                "Project Address":form["project_address"],
                "Project Description":form["description_project"],
                "Gender":form["gender"],
                "Address":form["address"],
                "City":form["city"],
                "Postal Code":form["postal_code"],
                "dob":form["dob"],
                "Person Position":form["person_position"],
                "Private Individual Gender":(form["private_individual_gender"]!="") ? form["private_individual_gender"] : "",
                "Private Individual First Name":form["private_individual_first_name"],
                "Private Individual Middle Name":form["private_individual_middle_name"],
                "Private Individual Last Name":form["private_individual_last_name"],
                "Private Individual Address":form["private_individual_address"],
                "Private Individual City":form["private_individual_city"],
                "Private Individual Postal Code":form["private_individual_postal_code"],
                "Private Individual Country":form["private_individual_country"],
                "Private Individual Phone":form["private_individual_phone"],
                "Private Individual Email":form["private_individual_email"],
                "Private Individual dob":form["private_individual_dob"],
                "Enterprise Title":form["enterprise_price_title1"],
                "Enterprise Title1":form["enterprise_price_title2"],
                "Enterprise Address":form["enterprise_address_title"],
                "Enterprise City":form["enterprise_city_title"],
                "Enterprise Postal Code":form["enterprise_postal_code_title"],
                "Enterprise Country":form["enterprise_country_title"],
                "Enterprise Phone":form["enterprise_phone_title"],
                "Enterprise Email":form["enterprise_email_title"],
                "Enterprise Gender":(form["enterprise_gender"]!="") ? form["enterprise_gender"] : "",
                "Enterprise Contact First Name":form["enterprise_contact_first_name_title"],
                "Enterprise Contact Last Name":form["enterprise_contact_last_name_title"],
                "Enterprise Position":form["enterprise_position"]            
              }
            });
            fetch("https://api2.myclients.io/v2/clients?teams_view_filter=all", {
             "credentials": "include",
             "headers": {
              "accept": "application/json, text/plain, */*",
              "accept-language": "en-GB,en-US;q=0.9,en;q=0.8,nl;q=0.7",
              "authorization": "Token b002c819f2ff90916c97422e45d5874ba96a62227b8689271ca4a58f14339008",
              "cache-control": "no-cache",
              "content-type": "application/json;charset=UTF-8",
              "pragma": "no-cache",
              "sec-fetch-mode": "cors",
              "sec-fetch-site": "cross-site",
              "x-app-name": "Frontage"
             },
             "referrer": "https://app.vcita.com/app/clients",
             "referrerPolicy": "no-referrer-when-downgrade",
             "body": data,
             "method": "POST",
             "mode": "cors"
            }).then((response) => {
                return response.json();
            }).then((myJson) => {
                let client_id = myJson.id; 
                data = JSON.stringify({"uid":client_id,tags:"moreinformation"});
                fetch("https://api2.myclients.io/v2/tags?teams_view_filter=all", {
                "credentials": "include",
                "headers": {
                "accept": "application/json, text/plain, */*",
                "accept-language": "en-GB,en-US;q=0.9,en;q=0.8,nl;q=0.7",
                "authorization": "Token b002c819f2ff90916c97422e45d5874ba96a62227b8689271ca4a58f14339008",
                "cache-control": "no-cache",
                "content-type": "application/json;charset=UTF-8",
                "pragma": "no-cache",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "cross-site",
                "x-app-name": "Frontage"
                },
                "referrer": "https://app.vcita.com/app/clients",
                "referrerPolicy": "no-referrer-when-downgrade",
                "body": data,
                "method": "POST",
                "mode": "cors"
                });
            });
            $(".loader").removeClass("hide");
            $.ajax({
                url:"{{route('storemoreinformation')}}",
                type:"POST",
                data:$("#moreInformationForm").serialize(),
                success:function(response){
                    let data = JSON.parse(JSON.stringify(response));
                    $(".loader").addClass("hide");
                    if(data.success){
                        alert(data.message);
                    }
                    $("#more-info-wrapper").removeClass("active");
                    $("body").removeClass("active");
                    $("body").removeClass("over");

                },
                error:function(error) {
                    console.error(error);
                     $(".loader").addClass("hide");
                }
            });
            return false;            
        }
    });
   
</script>
@php $article = articalList(); @endphp

<script type="text/javascript">
    $("#ApplyToopKitFrom").submit(function(){
        let articleList = JSON.parse('@json($article)');
        let formdata = $("#ApplyToopKitFrom").serializeArray();
        let form = {};
        $.each(formdata, function(i, field){            
            form[field.name] = field.value;
        });
        let email = (form["private_individual_email"]!="") ?  form["private_individual_email"] : form["enterprise_email_title"]; 
        let first_name = (form["enterprise_price_title1"]!="") ?  form["enterprise_price_title1"] : (form["private_individual_first_name"]+" "+form["private_individual_middle_name"]); 
        let last_name =   (form["private_individual_last_name"]!="") ?  form["private_individual_last_name"] : form["enterprise_price_title2"];
        let data = JSON.stringify({
        email:email,
        first_name:first_name,
        last_name:last_name,
        tag_list:["applytoopkitfrom"],
        custom_fields:{
            "Article Name":articleList[form["article_id"]],
            "You Found Through":form["you_found_through"],
            "You Found Through Other":form["you_found_through_other"],
            "Choose Toolkit":form["choose_toolkit"],
            "Choose Toolkit Other":form["choose_toolkit_other"],           
            "Person Position":form["position"],
            "Private Individual Gender":(form["private_individual_gender"]!="") ? form["private_individual_gender"] : "",
            "Private Individual First Name":form["private_individual_first_name"],
            "Private Individual Middle Name":form["private_individual_middle_name"],
            "Private Individual Last Name":form["private_individual_last_name"],
            "Private Individual Address":form["private_individual_address"],
            "Private Individual City":form["private_individual_city"],
            "Private Individual Postal Code":form["private_individual_postal_code"],
            "Private Individual Country":form["private_individual_country"],
            "Private Individual Phone":form["private_individual_phone"],
            "Private Individual Email":form["private_individual_email"],
            "Private Individual dob":form["private_individual_dob"],
            "Enterprise Title":form["enterprise_price_title1"],
            "Enterprise Title1":form["enterprise_price_title2"],
            "Enterprise Address":form["enterprise_address_title"],
            "Enterprise City":form["enterprise_city_title"],
            "Enterprise Postal Code":form["enterprise_postal_code_title"],
            "Enterprise Country":form["enterprise_country_title"],
            "Enterprise Phone":form["enterprise_phone_title"],
            "Enterprise Email":form["enterprise_email_title"],
            "Enterprise Gender":(form["enterprise_gender"]!="") ? form["enterprise_gender"] : "",
            "Enterprise Contact First Name":form["enterprise_contact_first_name_title"],
            "Enterprise Contact Last Name":form["enterprise_contact_last_name_title"],
            "Enterprise Position":form["enterprise_position"],            
          }
        });

       // console.log(data);
        fetch("https://api2.myclients.io/v2/clients?teams_view_filter=all", {
         "credentials": "include",
         "headers": {
          "accept": "application/json, text/plain, */*",
          "accept-language": "en-GB,en-US;q=0.9,en;q=0.8,nl;q=0.7",
          "authorization": "Token b002c819f2ff90916c97422e45d5874ba96a62227b8689271ca4a58f14339008",
          "cache-control": "no-cache",
          "content-type": "application/json;charset=UTF-8",
          "pragma": "no-cache",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "cross-site",
          "x-app-name": "Frontage"
         },
         "referrer": "https://app.vcita.com/app/clients",
         "referrerPolicy": "no-referrer-when-downgrade",
         "body": data,
         "method": "POST",
         "mode": "cors"
        }).then((response) => {
            return response.json();
        }).then((myJson) => {
            let client_id = myJson.id; 
               data = JSON.stringify({"uid":client_id,tags:"applytoopkitfrom"});
              fetch("https://api2.myclients.io/v2/tags?teams_view_filter=all", {
                 "credentials": "include",
                 "headers": {
                  "accept": "application/json, text/plain, */*",
                  "accept-language": "en-GB,en-US;q=0.9,en;q=0.8,nl;q=0.7",
                  "authorization": "Token b002c819f2ff90916c97422e45d5874ba96a62227b8689271ca4a58f14339008",
                  "cache-control": "no-cache",
                  "content-type": "application/json;charset=UTF-8",
                  "pragma": "no-cache",
                  "sec-fetch-mode": "cors",
                  "sec-fetch-site": "cross-site",
                  "x-app-name": "Frontage"
                 },
                 "referrer": "https://app.vcita.com/app/clients",
                 "referrerPolicy": "no-referrer-when-downgrade",
                 "body": data,
                 "method": "POST",
                 "mode": "cors"
                });
        });
        $(".loader").removeClass("hide");
        $.ajax({
            url:"{{route('storeapplytoolkitform')}}",
            type:"POST",
            data:$(this).serialize(),
            success:function(response){
                let data = JSON.parse(JSON.stringify(response));
                $(".loader").addClass("hide");
                if(data.success){
                    alert(data.message);
                }
                $("#article-form").removeClass("active");
                $("body").removeClass("active");
                $("body").removeClass("over");

            },
            error:function(error) {
                console.error(error);
                $(".loader").addClass("hide");
            }
        });
        return false;
    });
</script>
<script type="text/javascript">
    let baseurl = "<?php echo URL::to('/'); ?>";
    let currentUrl = "<?php echo Request::path(); ?>";
    function redirect(submenu,menu) {
        if(currentUrl=="studiolist") {
            $(".close-btn").trigger("click");
            $("html, body").animate({
                scrollTop: $(submenu).offset().top - 80
            }, 2000);
        }
        else {
            window.location = baseurl+"/"+menu+submenu;
        }
    }
</script>

@if(Request::is('studiolist'))
    <script type="text/javascript">
        document.onreadystatechange = function () {
            if (document.readyState === 'complete') {
                let currentUrl = window.location.href;
                let slug = currentUrl.split("#"); 
                if(slug[1]){
                    $("html, body").animate({
                        scrollTop: $("#"+slug[1]).offset().top - 100
                    }, 100);
                }
            }
        }    
    </script>
@endif  



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script type="text/javascript">
    $("#newsletterForm").validate({
        errorClass: "text-danger",          
        rules: {
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            email: {
                required: '{{getEmailEnterMessage()}}',
                email: '{{getEmailEnterInvalidMessage()}}'
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.next());
        } ,
        submitHandler: function() { 
            $.ajax({
                url:"{{url('newsletter/store')}}",
                type:"POST",
                data:$("#newsletterForm").serialize(),
                success:function(response){
                    alert(response.responseMessage);                     
                },
                error:function(error) {
                    console.error(error);
                }
            });
        }     
    });
    $("#projectDetailsForm").validate({
        errorClass: "text-danger",          
        rules: {
            userEmail: {
                required: true,
                email: true
            },
        },
        messages: {
            userEmail: {
                required: '{{getEmailEnterMessage()}}',
                email: '{{getEmailEnterInvalidMessage()}}'
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.next());
        },
        submitHandler:function(){           
            $.ajax({
            url:"{{url('projectlist/projectDetailsForm')}}",
            type:"POST",
            data:$("#projectDetailsForm").serialize(),
            success:function(response){
                let data = JSON.parse(JSON.stringify(response));
                if(data.responceCode){
                    $("#projectDetailsForm")[0].reset();    
                }  
                alert(data.responseMessage);                     
            },
            error:function(error) {
                console.error(error);
            }
        });
            return false;
        }
    });
</script>
</html>
