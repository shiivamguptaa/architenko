<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset('/assets/img/sidebar-1.jpg')}}">	
	<div class="logo">
        <a href="#" class="simple-text logo-normal">
			Architenko
		</a>
	</div>
	<div class="sidebar-wrapper">
        <ul class="nav">
			<!--<li class="nav-item @if(Request::segment(1) == 'languages')active @endif">
				<a class="nav-link" href="{{url('/languages')}}">
				<i class="material-icons">content_paste</i>
				<p>Languages</p>
				</a>
			</li>-->
			<li class="nav-item dropdown @if(Request::segment(1) == 'menu' || Request::segment(1) == 'submenu' || Request::segment(1) == 'logo') active @endif">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="material-icons">menu</i>
					Header 
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">            
					<a class="dropdown-item" href="{{url('/logo/'.$logo->id.'/edit')}}">Logo</a>
					<a class="dropdown-item" href="{{url('/menu')}}">Main Menu</a>
					<a class="dropdown-item" href="{{url('/submenu')}}">Sub Menu</a>
				</div>
				
			</li>
			<li class="nav-item @if(Request::segment(1) == 'front')active @endif">
				<a class="nav-link" href="{{url('/front/home')}}">
					<i class="material-icons">home</i>
					<p>Home</p>
				</a>
			</li>
			<li class="nav-item ">
				<a class="nav-link" href="{{url('/studio')}}">
					<i class="material-icons">person</i>
					<p>Architenko</p>
				</a>
			</li>
			<li class="nav-item dropdown @if(Request::segment(1) == 'service' || Request::segment(1) == 'servicetopdescription') active @endif">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="material-icons">laptop</i>
					Services 
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">            
					<a class="dropdown-item" href="{{url('/service')}}">Services List</a>
					<a class="dropdown-item" href="{{url('/servicetopdescription')}}">Services Page top description</a>
				</div>
			</li>
			<li class="nav-item dropdown @if(Request::segment(1) == 'project' || Request::segment(1) == 'projectcatagory') active @endif">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="material-icons">library_books</i>
					Projects 
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">            
					<a class="dropdown-item" href="{{url('/project')}}">Projects List</a>
					<a class="dropdown-item" href="{{url('/projectcatagory')}}">Projects Catagory</a>
				</div>
			</li>
			<li class="nav-item dropdown @if(Request::segment(1) == 'article' || Request::segment(1) == 'articlescategory') active @endif">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="material-icons">bubble_chart</i>
					Articles
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">            
					<a class="dropdown-item" href="{{url('/article')}}">Articles List</a>
					<a class="dropdown-item" href="{{url('/articlescategory')}}">Articles Category List</a>
				</div>
			</li>
			<li class="nav-item  @if(Request::segment(1) == 'contact') active @endif">
				<a class="nav-link" href="{{url('/contact')}}">
					<i class="material-icons">location_ons</i>
					<p>Contact</p>
				</a>
			</li>
			<li class="nav-item  @if(Request::segment(1) == 'newslettertext') active @endif">
				<a class="nav-link" href="{{url('/newslettertext')}}">
					<i class="material-icons">email</i>
					<p>Call to action</p>
				</a>
			</li>
			{{-- <li class="nav-item @if(Request::segment(1) == 'crm')active @endif">
				<a class="nav-link" href="javascript:void(0)">
					<i class="material-icons">dashboard</i>
					<p>CRM</p>
				</a>
			</li> --}}
			<li class="nav-item dropdown @if(Request::segment(1) == 'projects'||Request::segment(1) == 'moreinformation' || Request::segment(1) == 'newsletter') active @endif">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="material-icons">assignment</i>
					Form & Email List
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item" href="{{url('/moreinformation')}}">More Information (Forms)</a>
					<a class="dropdown-item" href="{{url('/moreinformationdetails')}}">More Information (Lists)</a>
					<a class="dropdown-item" href="{{url('/studioapplyform')}}">Studio Apply (Forms)</a>
					<a class="dropdown-item" href="{{url('/studioapplydetails')}}">Studio Apply (Lists)</a>
					<a class="dropdown-item" href="{{url('/applytooltipform')}}">Apply ToolKit (Forms)</a>
					<a class="dropdown-item" href="{{url('/applytooltipformdetails')}}">Apply ToolKit (Lists)</a>
					<a class="dropdown-item" href="{{url('/newsletter')}}">Newsletter list</a>
					<a class="dropdown-item" href="{{url('/projects/download')}}">Project Download list</a>
				</div>				
			</li>
			<li class="nav-item @if(Request::segment(1) == 'label')active @endif">
				<a class="nav-link" href="{{url('/label/')}}">
					<i class="material-icons">label</i>
					<p>Label</p>
				</a>
			</li>
			<li class="nav-item @if(Request::segment(1) == 'social')active @endif">
				<a class="nav-link" href="{{url('/social')}}">
					<i class="material-icons">public</i>
					<p>Social Links</p>
				</a>
			</li>
			<li class="nav-item @if(Request::segment(1) == 'footer')active @endif">
				<a class="nav-link" href="{{url('/footer')}}">
					<i class="material-icons">dashboard</i>
					<p>Footer</p>
				</a>
			</li>
			<li class="nav-item @if(Request::segment(1) == 'Country')active @endif">
				<a class="nav-link" href="{{url('/country')}}">
					<i class="material-icons">public</i>
					<p>Country</p>
				</a>
			</li>
			<li class="nav-item @if(Request::segment(1) == 'errormessage')active @endif">
				<a class="nav-link" href="{{url('/errormessage')}}">
					<i class="material-icons">warning</i>
					<p>Error Message</p>
				</a>
			</li>	
			<li class="nav-item @if(Request::segment(1) == 'emailform')active @endif">
				<a class="nav-link" href="{{url('/emailform')}}">
					<i class="material-icons">mail</i>
					<p>Admin notification</p>
				</a>
			</li>	
			@isset(Auth::user()->role)
				@if(Auth::user()->role == 1)
					<li class="nav-item @if(Request::segment(1) == 'users')active @endif">
						<a class="nav-link" href="{{url('/users')}}">
							<i class="material-icons">supervised_user_circle</i>
							<p>Users</p>
						</a>
					</li>
				@endif
			@endisset
			<!--
			<li class="nav-item @if(Request::segment(1) == 'studioapplyform')active @endif">
				<a class="nav-link" href="{{url('/studioapplyform')}}">
					<i class="material-icons">form</i>
					<p>Studio Apply Form</p>
				</a>
			</li>		
			<li class="nav-item @if(Request::segment(1) == 'studioapplydetails')active @endif">
				<a class="nav-link" href="{{url('/studioapplydetails')}}">
					<i class="material-icons">form</i>
					<p>Studio Apply Details</p>
				</a>
			</li> -->

		</ul>
	</div>
</div>				