<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8" />
		<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/favicons.ico">
		<link rel="icon" type="image/png" href="{{asset('assets/img/favicons.ico')}}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>
			Login Architenko
		</title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
		<!--     Fonts and icons     -->
		<!-- CSS Files -->
		<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
		<link href="{{asset('assets/css/material-login.css?v=1.3.0')}}" rel="stylesheet" />
		<!-- CSS Just for demo purpose, don't include it in your project -->
		<link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" />
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
		rel="stylesheet">
	</head>
	<body class="off-canvas-sidebar">
		@yield('content')
		<div class="fixed-plugin">
		</div>
		<!--   Core JS Files   -->
		<script src="{{asset('assets/js/core/jquery.min.js')}} type="text/javascript"></script>
		<script src="{{asset('assets/js/core/bootstrap-material-design.min.js')}} type="text/javascript"></script>
		<script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}} type="text/javascript"></script>
		<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
		<!-- Library for adding dinamically elements -->
		<script src="{{asset('assets/js/plugins/arrive.min.js')}} type="text/javascript"></script>
		<!-- Forms Validations Plugin -->
		<script src="{{asset('assets/js/plugins/jquery.validate.min.js')}}></script>
		<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
		<script src="{{asset('assets/js/plugins/moment.min.js')}}></script>
		<!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
		<script src="{{asset('assets/js/plugins/chartist.min.js')}}></script>
		<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
		<script src="{{asset('assets/js/plugins/jquery.bootstrap-wizard.js')}}></script>
		<!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
		<script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}></script>
		<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
		<script src="{{asset('assets/js/plugins/bootstrap-datetimepicker.min.js')}}></script>
		<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
		<script src="{{asset('assets/js/plugins/jquery-jvectormap.js')}}></script>
		<!-- Sliders Plugin, full documentation here: https://refreshless.com/nouislider/ -->
		<script src="{{asset('assets/js/plugins/nouislider.min.js')}}></script>
		<!--  Google Maps Plugin    -->
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1_8C5Xz9RpEeJSaJ3E_DeBv8i7j_p6Aw"></script>
		<!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
		<script src="{{asset('assets/js/plugins/bootstrap-selectpicker.js')}}></script>
		<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
		<script src="{{asset('assets/js/plugins/jquery.dataTables.min.js')}}></script>
		<!-- Sweet Alert 2 plugin, full documentation here: https://limonte.github.io/sweetalert2/ -->
		<script src="{{asset('assets/js/plugins/sweetalert2.js')}}></script>
		<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
		<script src="{{asset('assets/js/plugins/jasny-bootstrap.min.js')}}></script>
		<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
		<script src="{{asset('assets/js/plugins/fullcalendar.min.js')}}"></script><style>
			#offerCard {
			position: fixed;
			z-index: 99999;
			/* overflow: hidden; */
			right: -410px;
			bottom: 40px;
			display: block;
			background: #444;
			max-width: 410px;
			min-height: 360px;
			padding: 10px 17px;
			border-radius: 12px;
			box-shadow: 0 5px 25px -5px rgba(0,0,0,.23), 0 10px 31px -8px rgba(0,0,0,.25), 0 7px 40px -14px rgba(0,0,0,.15);
			transition: all 670ms cubic-bezier(0.34, 1.61, 0.7, 1);
			}
			#offerCard.active{
			right: 50px;
			}
			#offerCard b {
			font-size: 16px !important;
			}
			#count-down {
			display: initial;
			padding-left: 10px;
			font-weight: bold;
			}
			#closeBar {
			font-size: 22px;
			color: #3e3947;
			margin-right: 0;
			position: absolute;
			right: 37px;
			background: white;
			opacity: 0.8;
			padding: 0px;
			height: 30px;
			line-height: 27px;
			width: 30px;
			border-radius: 50%;
			text-align: center;
			top: -12px;
			cursor: pointer;
			z-index: 9999999999;
			font-weight: 200;
			}
			#closeBar:hover{
			opacity: 1;
			}
			#offerButton {
			background-color: #fff;
			color: #333;
			border-radius: 4px;
			display: block;
			padding: 10px 20px;
			font-weight: bold;
			text-transform: uppercase;
			font-size: 12px;
			opacity: .95;
			max-width: 180px;
			margin: 10px auto 10px;
			cursor: pointer;
			text-align: center;
			box-shadow: 0 5px 16px -3px rgba(0,0,0,.23), 0 10px 15px -5px rgba(0,0,0,.25);
			}
			#offerButton{
			opacity: 1;
			}
			#fromText{
			font-size: 15px;
			color: #bbb;
			font-weight: 400;
			margin-right: 3px;
			}
			#newPrice{
			color: #ff3638;
			font-size: 24px;
			font-weight: 700;
			}
			#offerImage{
			width: 100%;
			margin-top: -40px;
			border-radius: 8px;
			}
			#offerText{
			width: 100%;
			display: block;
			margin-top: 15px;
			text-align: center;
			line-height: 1.6;
			color: #fff;
			font-size: 16px;
			}
			#oldPrice{
			text-decoration: line-through;
			font-size: 16px;
			color: #bbb;
			display: block;
			}
			@media (max-width: 768px) {
			#offerCard {
			padding: 50px 20px 20px;
			text-align: center;
			font-size: 20px;
			}
			#offerButton{
			display: block;
			margin-top: 20px;
			margin-left: 0;
			}
			#count-down {
			display: block;
			font-size: 25px;
			}
			#offerCard span {
			display: none;
			}
			.pull-center {
			text-align: center;
			}
			}
		</style>
		<script type="text/javascript" id="">var card;function setCookie(c,d,b){var a=new Date;a.setTime(a.getTime()+864E5*b);b="expires\x3d"+a.toUTCString();document.cookie=c+"\x3d"+d+";"+b+";path\x3d/"}function readCookie(c){c+="\x3d";for(var d=document.cookie.split(";"),b=0;b<d.length;b++){for(var a=d[b];" "==a.charAt(0);)a=a.substring(1,a.length);if(0==a.indexOf(c))return a.substring(c.length,a.length)}return null}
			function closeOfferBar(){card=document.getElementById("offerCard");card.classList.remove("active");console.log("closeOffer");setCookie("view_bf_card","true",1)}var value=readCookie("view_bf_card");
		null==value&&(card=document.getElementById("offerCard"),console.log(card,"instant got element"),closeBar=document.getElementById("closeBar"),closeBar.addEventListener("click",closeOfferBar),console.log("close:",closeBar.onclick),setTimeout(function(){console.log("after 4 seconds");card=document.getElementById("offerCard");card.classList.add("active")},4E3));</script>
		<!-- Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
		<script src="{{asset('assets/js/plugins/jquery.tagsinput.js')}}"></script>
		<!-- Material Dashboard javascript methods -->
		<script src="{{asset('assets/js/material-dashboard.js?v=1.3.0')}}"></script>
		<script src="{{asset('assets/demo/demo.js')}}"></script>
		<script type="text/javascript">
			$().ready(function(){
				demo.checkFullPageBackgroundImage();
				setTimeout(function(){
					// after 1000 ms we add the class animated to the login/register card
					$('.card').removeClass('card-hidden');
				}, 700)
			});
		</script>
	</body>
</html>