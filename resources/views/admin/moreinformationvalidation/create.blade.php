@extends('layouts.auth')
@section('content')

<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{URL('moreinformation/validation/store')}}" >
									@csrf	
									<div class="form-group row">
										<div class="col-md-12">
											<input name="service_id" type="text" class="form-control" autocomplete="off" title="Service Error" placeholder="Service Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="project_address" type="text" class="form-control" autocomplete="off" title="Project Address Error" placeholder="Project Address Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="description_project" type="text" class="form-control" autocomplete="off" title="Project description Error" placeholder="Project description Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="first_name" type="text" class="form-control" autocomplete="off" title="First Name Error" placeholder="First Name Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="middle_name" type="text" class="form-control" autocomplete="off" title="Middle Name Error" placeholder="Middle Name Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="last_name" type="text" class="form-control" autocomplete="off" title="Last Name Error" placeholder="Last Name Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="address" type="text" class="form-control" autocomplete="off" title="Address Error" placeholder="Address Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="postal_code" type="text" class="form-control" autocomplete="off" title="Postal Code Error" placeholder="Postal Code Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="phone" type="text" class="form-control" autocomplete="off" title="Phone Error" placeholder="Phone Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="email" type="text" class="form-control" autocomplete="off" title="Email Error" placeholder="Email Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="dob" type="text" class="form-control" autocomplete="off" title="Date of Birth Error" placeholder="Date of Birth Error" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="private_individual_first_name" type="text" class="form-control" autocomplete="off" title="Private Individual First Name Error" placeholder="Private Individual First Name Error" autofocus />			
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="private_individual_middle_name" type="text" class="form-control" autocomplete="off" title="Private Individual Middle Name Error" placeholder="Private Individual Middle Name Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="private_individual_last_name" type="text" class="form-control" autocomplete="off" title="Private Individual Last Name Error" placeholder="Private Individual Last Name Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="private_individual_address" type="text" class="form-control" autocomplete="off" title="Private Individual Address Error" placeholder="Private Individual Address Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="private_individual_postal_code" type="text" class="form-control" autocomplete="off" title="Private Individual Postal Code Error" placeholder="Private Individual Postal Code Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="private_individual_country" type="text" class="form-control" autocomplete="off" title="Private Individual Country Error" placeholder="Private Individual Country Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="private_individual_phone" type="text" class="form-control" autocomplete="off" title="Private Individual Phone Error" placeholder="Private Individual Phone Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="private_individual_dob" type="text" class="form-control" autocomplete="off" title="Private Individual Date of Birth Error" placeholder="Private Individual Date of Birth Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="enterprise_price_title1" type="text" class="form-control" autocomplete="off" title="Private Enterprise Title Error" placeholder="Private Enterprise Title Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="enterprise_address_title" type="text" class="form-control" autocomplete="off" title="Private Enterprise Address Error" placeholder="Private Enterprise Address Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="enterprise_city_title" type="text" class="form-control" autocomplete="off" title="Private Enterprise Address Error" placeholder="Private Enterprise Address Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="enterprise_postal_code_title" type="text" class="form-control" autocomplete="off" title="Private Enterprise Postal Code Error" placeholder="Private Enterprise Postal Code Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="enterprise_country_title" type="text" class="form-control" autocomplete="off" title="Private Enterprise Country Error" placeholder="Private Enterprise Country Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="enterprise_phone_title" type="text" class="form-control" autocomplete="off" title="Private Enterprise Phone Error" placeholder="Private Enterprise Phone Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="enterprise_email_title" type="text" class="form-control" autocomplete="off" title="Private Enterprise Email Error" placeholder="Private Enterprise Email Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="enterprise_contact_first_name_title" type="text" class="form-control" autocomplete="off" title="Private Enterprise Contact First Name Error" placeholder="Private Enterprise First Name Error" autofocus />				
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input name="enterprise_contact_last_name_title" type="text" class="form-control" autocomplete="off" title="Private Enterprise Contact Last Name Error" placeholder="Private Enterprise last Name Error" autofocus />				
										</div>
									</div>

									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="language">
											@if(count($languages)>0)
												@foreach($languages as $language)
													<option value="{{$language->name}}">{{$language->name}}</option>
												@endforeach						  
											@endif
										</select>
									</div>				
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>

@endsection						