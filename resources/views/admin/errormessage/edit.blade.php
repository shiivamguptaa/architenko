@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('errormessage/update') }}" enctype="multipart/form-data">
									@csrf	
									<input id="id" type="hidden" class="form-control " name="id" value="{{$id}}" />
									<div class="form-group row">
										<label for="name" style="position:relative" class="col-md-4 col-form-label text-md-left">{{ __('Enter Email Error Message') }}</label>					
										<div class="col-md-10">
											<input id="email_enter_message" type="text"   class="form-control " name="email_enter_message" value="@if(isset($error['email_enter_message'])){{$error['email_enter_message']}}@endif"  />
										</div>
									</div>	
									<div class="form-group row">
										<label for="name" style="position:relative" class="col-md-4 col-form-label text-md-left">{{ __('Enter Email InValid Message') }}</label>					
										<div class="col-md-10">
											<input id="email_invalid_message" type="text"   class="form-control " name="email_invalid_message" value="@if(isset($error['email_invalid_message'])){{$error['email_invalid_message']}}@endif" />
										</div>
									</div>		
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="language">
											@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}" @if($language->name==$error["language"]) selected @endif>{{$language->name}}</option>
											@endforeach
											@else
											<option value="en">en</option>
											<option value="nl">nl</option>
											@endif
										</select>
									</div>								
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection							