@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">		
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn">
				<a href="{{url('errormessage/create')}}" class="btn btn-success">
				Add New Error Message<div class="ripple-container"></div></a>
				</p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-2 pull-right">
									<label for="select_lang">Select Language</label>
									<select id="select_lang" class="form-control" name="lang">
										<option value="" selected disabled>Select Language</option>
										@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}">{{$language->name}}</option>
											@endforeach						  
										@endif
									</select>
								</div>
							</div>
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#</th>					
											<th>Email  Message</th>				
											<th>Email InValid Message</th>				
											<th>Language</th>															
											<th class="text-right">Action</th>				
										</tr>
									</thead>
									<tbody>		
									<?php $counter=1; ?>	
										@if($errors)		
										@foreach($errors as $error)
											<tr>
												<td>{{$counter++}}</td>						
												<td>{{$error['email_enter_message']}}</td>						
												<td>{{$error['email_invalid_message']}}</td>			
												<td>{{$error['language']}}</td>						
												<td class="td-actions text-right">												
													<a href="{{ url('errormessage/'.$error['id'].'/edit') }}" class="btn btn-success">
														<i class="material-icons">edit</i>
													</a>
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('errormessage/'.$error['id'].'/delete') }}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a>
												</td>
											</tr>
											
										@endforeach					
										@endif					
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection							



@section('extrascript')
<script type="text/javascript">
    $(document).ready(function () {
      $('#dtOrderExample').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
          { className: "td-actions text-right",orderable: false , "targets": [4] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
<script type="text/javascript">
	let BASE_URL = '{{URL("/")}}';
    $(document).ready(function () {
      	var table = $('#dtOrderExample').DataTable();
      	$(document).on('change','#select_lang',function(){
      		let lang = $(this).val();
      		$.ajax({
				url:"{{route('onchangelanguageerror')}}",
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				data:{lang:lang},
				type:"POST",
				success:function(response){
						// console.table(response.data);
					if (response.responseCode == 1) {
						table.clear().draw();
						$.each(response.data,function(key,value){
							let edit = BASE_URL+'/errormessage/'+value.id+'/edit';
							let del = BASE_URL+'/errormessage/'+value.id+'/delete';
							
							let action = `<div class="td-actions text-right"><a href="${edit}" class="btn btn-success">
														<i class="material-icons">edit</i>
													</a>								
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('${del}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a></div>`;
							
							table.row.add([key+1,value.email_enter_message,value.email_invalid_message,value.language,action]).draw();
						});
					}
				},
				error:function(error){	
					console.log(error);
				}
			});
      	});
      	
    });
</script>
@endsection	