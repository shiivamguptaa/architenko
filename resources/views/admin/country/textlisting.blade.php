@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn"><a href="{{ url('newslettertext/create') }}" class="btn btn-success">Add New Newsletter Text</a></p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }}" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach
		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Newsletter Top Text</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th class="text-center">#</th>
											<th>Description</th>
											<th>Language</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>
										@if(!empty($newslettertext) > 0)
										<?php $counter = 1; ?>
										@foreach($newslettertext as $newslettertexts)
										<tr>							
											<td class="text-center">{{$counter}}</td>
											<td>{!! $newslettertexts['description'] !!}</td>
											<td>{{ $newslettertexts['language'] }}</td>											
											<td class="td-actions text-right">												
												<a href="{{ url('newslettertext/'.$newslettertexts['id'].'/edit') }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>
												<a  href="{{ url('newslettertext/'.$newslettertexts['id'].'/delete') }}" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>							
										</tr>
										<?php $counter++; ?>
										@endforeach
										@endif						
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection											