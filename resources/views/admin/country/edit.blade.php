@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('country/update') }}">
									@csrf	
										<input id="id" type="hidden" class="form-control" name="id" value="{{$id}}" />	
									<div class="form-group row">
										<label for="name" style="position:relative" class="col-md-4 col-form-label text-md-left">{{ __('Name') }}</label>					
										<div class="col-md-10">
											<input id="name" type="text" class="form-control" name="name" placeholder="Country Name" title="Country Name" value="@if(isset($country->name)){{$country->name}}@endif" />	
										</div>
									</div>									
									<div class="form-group row">
										<label for="inputState" class="col-md-12 col-form-label text-md-left">Select Language</label>				
										<div class="col-md-2">
											<select id="language" class="form-control" name="language">
												@if(count($languages)>0)
													@foreach($languages as $language)
														<option value="{{$language->name}}" @if($country->language == $language->name) selected @endif >{{$language->name}}</option>
													@endforeach
												@else
												<option value="en">en</option>
												<option value="nl">nl</option>
												@endif
											</select>
										</div>
									</div>

									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection							