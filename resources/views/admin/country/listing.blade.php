@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">		
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn">
				<a href="{{url('country/create')}}" class="btn btn-success">
				Add New Country<div class="ripple-container"></div></a>
				</p>		
			</div>
		</div>
			@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Country List</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-2 pull-right">
									<label for="select_lang">Select Language</label>
									<select id="select_lang" class="form-control" name="lang">
										<option value="" selected disabled>Select Language</option>
										@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}">{{$language->name}}</option>
											@endforeach						  
										@endif
									</select>
								</div>
							</div>
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#</th>
											<th>Name</th>		
											<th>Language</th>				
											<th class="text-right">Action</th>				
										</tr>
									</thead>
									<tbody id="labelList">					
										@if(count($countrys) > 0)
											<?php $counter = 1; ?>
											@foreach($countrys as $country)
												<tr>
													<td>{{$counter}}</td>
													<td>{{$country['name']}}</td>						
													<td>{{$country['language']}}</td>						
													<td class="td-actions text-right">												
														<a href="{{ url('country/edit/'.$country['id']) }}" class="btn btn-success">
															<i class="material-icons">edit</i>
														</a>
														<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('country/destroy/'.$country['id']) }}',this)" class="btn btn-danger">
															<i class="material-icons">close</i>
														</a>
													</td>
												</tr>
												<?php $counter++; ?>
											@endforeach					
										@endif					
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script type="text/javascript">
function onChangeLabelLanguage(language){
	$.ajax({
		url:"{{route('onChangeLabelLanguage')}}",
		headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
		data:{language:language},
		type:"POST",
		success:function(response){
			let data  = JSON.parse(JSON.stringify(response));
			if(data.success){
				console.log(data.data);
				$("#labelList").html(data.data);
			}
		},
		error:function(error){	
			console.log(error);
		}
	});
}
</script>
@endsection					

@section('extrascript')
<script type="text/javascript">
    $(document).ready(function () {
      $('#dtOrderExample').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
          { className: "td-actions text-right",orderable: false , "targets": [3] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
<script type="text/javascript">
	let BASE_URL = '{{URL("/")}}';
    $(document).ready(function () {
      	var table = $('#dtOrderExample').DataTable();
      	$(document).on('change','#select_lang',function(){
      		let lang = $(this).val();
      		$.ajax({
				url:"{{route('onchnagelanguagecountry')}}",
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				data:{lang:lang},
				type:"POST",
				success:function(response){
						// console.table(response.data);
					if (response.responseCode == 1) {
						table.clear().draw();
						$.each(response.data,function(key,value){
							let edit = BASE_URL+'/service/edit/'+value.id;
							let del = BASE_URL+'/service/delete/'+value.id;

							let action = `<div class="td-actions text-right"><a href="${edit}" class="btn btn-success">
														<i class="material-icons">edit</i>
													</a>								
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('${del}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a></div>`;
							table.row.add([key+1,value.name,value.language,action]).draw();
						});
					}
				},
				error:function(error){	
					console.log(error);
				}
			});
      	});
      	
    });
</script>
@endsection	