@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn"><a href="{{ url('servicetopdescription/create') }}" class="btn btn-success">Add New Service Top Description</a></p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Service Top Description Listing</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#<i class="fa fa-sort-up"></i><i class="fa fa-sort-down"></i></th>
											<th class="w625">Description<i class="fa fa-sort-up"></i><i class="fa fa-sort-down"></i></th>
											<th>Order By</th>
											<th>Language<i class="fa fa-sort-up"></i><i class="fa fa-sort-down"></i></th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>										
										@if(count($servicetopdescription) > 0)
										<?php $counter = 1; ?>
										@foreach($servicetopdescription as $servicetopdescriptions)
										<tr>
											<td>{{$counter}}</td>
											<td >{{$servicetopdescriptions['description']}}</td>
											<td><input type="number" id="servicetopdescriptions{{$servicetopdescriptions['id']}}" class="orderby form-control" onfocusout="onChangeOrderBy({{$servicetopdescriptions['id']}})" value="{{$servicetopdescriptions['orderby']}}"  /></td>
											<td>{{$servicetopdescriptions['language']}}</td>
											<td class="td-actions text-right">												
												<a href="{{ url('servicetopdescription/'.$servicetopdescriptions['id'].'/edit') }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>
												<a  href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('servicetopdescription/'.$servicetopdescriptions['id'].'/delete') }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>
										</tr>
										<?php $counter++; ?>
										@endforeach										
										@endif										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>

<script>
function onChangeOrderBy(id) {
	let orderby = $("#servicetopdescriptions"+id).val();
	$.ajax({
		url:"{{route('servicetopdescriptionorderby')}}",
		headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
		data:{id:id,orderby:orderby},
		type:"POST",
		success:function(response){
			console.log(response);
		},
		error:function(error){	
			console.log(error);
		}
	});
}
</script>
@endsection										