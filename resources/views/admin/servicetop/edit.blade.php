@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Edit Service Top Description</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="post" action="{{ url('servicetopdescription/update') }}" enctype="multipart/form-data">
									@csrf				
									<input type="hidden" name="id" value="{{ $id }}" />	
									<div class="form-group row">
										<label for="description" class="col-md-4 col-form-label text-md-left">{{ __('Description') }}</label>										
										<div class="col-md-10">
											<textarea class="form-control" id="description" name="description" rows="5">{{ $servicetopdescription['description'] }}</textarea>
											@error('description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="lang">
											@if(count($languages) > 0)
											@foreach($languages as $language)
											<option value="{{$language->name}}" @if($servicetopdescription->language == $language->name) selected @endif>{{$language->name}}</option>
											@endforeach
											@else
											<option value="en" @if($servicetopdescription->language == "en") selected @endif>en</option>
											<option value="nl" @if($servicetopdescription->language == "nl") selected @endif>nl</option>
											@endif
										</select>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Update') }}
											</button>
											<a class="btn btn-primary" href="{{ url('servicetopdescription') }}">Cancel</a>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>

<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
<script>
	CKEDITOR.editorConfig = function (config) {
		config.language = 'es';
		config.uiColor = '#F7B42C';
		config.height = 300;
		config.toolbarCanCollapse = true;
		
	};
	CKEDITOR.replace('description');
</script>
@endsection