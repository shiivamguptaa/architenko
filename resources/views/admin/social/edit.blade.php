@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Edit Social Link</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="post" action="{{ url('social/update') }}" enctype="multipart/form-data">
									@csrf				
									<input type="hidden" name="id" value="{{ $id }}" />	
									<div class="form-group row">
										<label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Name') }}</label>
										<div class="col-md-6">
											<input id="name" type="text" class="form-control" name="name" value="{{$socialdata['name']}}" required autocomplete="name">
											@error('name')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group row">
										<label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Link') }}</label>
										<div class="col-md-6">
											<input id="link" type="text" class="form-control" name="link" value="{{$socialdata['link']}}" required>
											@error('link')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Update') }}
											</button>
											<a class="btn btn-primary" href="{{ url('social') }}">Cancel</a>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection