@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn"><a href="{{ url('projectcatagory/create') }}" class="btn btn-success">Add New Catagory</a></p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Projects Listing</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-2 pull-right">
									<label for="select_lang">Select Language</label>
									<select id="select_lang" class="form-control" name="lang">
										<option value="" selected disabled>Select Language</option>
										@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}">{{$language->name}}</option>
											@endforeach						  
										@endif
									</select>
								</div>
							</div>
							<div class="table-responsive">
								<table id="dtOrderExample1" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#</th>
											<th>Catagory</th>
											<th>Order By</th>
											<th>Language</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>	
										<?php  $i=1; ?>
										@foreach($caragory as $value)		
										<tr>							
											<td>{{ $i++ }}</td>
											<td>{{ $value->catagory }}</td>
											<td><input type="number" id="projectcategory{{$value->id}}" class="orderby form-control" onfocusout="onChangeOrderBy({{$value->id}})" value="{{$value->orderby}}"  /></td>
											<td>{{ $value->language }}</td>
											<td class="td-actions text-right">		
												<a href="{{ URL('projectcatagory/edit',$value->id) }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('projectcatagory/delete',$value->id) }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>	
										</tr>	
										@endforeach				
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>	
<script type="text/javascript">
function onChangeOrderBy(id) {
	//console.log(id);
	let orderby = $("#projectcategory"+id).val();
	//console.log(orderby);
	$.ajax({
		url:"{{route('projectcategoryorderby')}}",
		headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
		data:{id:id,orderby:orderby},
		type:"POST",
		success:function(response){
			console.log(response);
		},
		error:function(error){	
			console.log(error);
		}
	});
}
</script>
@endsection								
@section('extrascript')
<script type="text/javascript">
	let BASE_URL = '{{URL("/")}}';
    $(document).ready(function () {
      	$('#dtOrderExample1').DataTable({
      		"order": [[ 0, "asc" ]],
      		"autoWidth": false,
      		"columnDefs": [
	          { className: "td-actions text-right",orderable: false , "targets": [4] },
	          { orderable: false , "targets": [2] }
	        ]
      	});
      	$('.dataTables_length').addClass('bs-select');
      	var table = $('#dtOrderExample1').DataTable();
      	$(document).on('change','#select_lang',function(){
      		let lang = $(this).val();
      		$.ajax({
				url:"{{route('pojectcategorybylang')}}",
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				data:{lang:lang},
				type:"POST",
				success:function(response){
					console.table(response.data);
					table.clear().draw();
					$.each(response.data,function(key,value){
						let edit = BASE_URL+'/projectcatagory/edit/'+value.id;
						let del = BASE_URL+'/projectcatagory/delete/'+value.id;
						let input = `<input type="number" id="projectcategory${value.id}" class="orderby form-control" onfocusout="onChangeOrderBy(${value.id})" value="${value.orderby}">`;
						let action = `<div class="td-actions text-right"><a href="${edit}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>								
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('${del}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a></div>`;
						table.row.add([key+1,value.catagory,input,value.language,action]).draw();
					});
				},
				error:function(error){	
					console.log(error);
				}
			});
      	});
      	
    });

</script>

@endsection