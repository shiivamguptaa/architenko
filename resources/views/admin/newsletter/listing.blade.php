@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Newsletter List</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#</th>
											<th>Email</th>						
											<th>Date</th>				
											<th class="td-actions text-right td-actions text-right">Action</th>				
										</tr>
									</thead>
									<tbody>					
										@if(count($newletter) > 0)
										<?php $counter = 1; ?>
										@foreach($newletter as $newletters)
										<tr>
											<td>{{$counter}}</td>
											<td>{{$newletters['email']}}</td>
											<?php $time = strtotime($newletters['created_at']); ?>
											<td><?=date('Y-m-d',$time)?></td>
											<td class="td-actions text-right td-actions text-right">
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{URL("newsletter/delete",$newletters["id"])}}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
											</td>		
										</tr>
										<?php $counter++; ?>
										@endforeach					
										@endif					
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection		


@section('extrascript')
<script type="text/javascript">
    $(document).ready(function () {
      $('#dtOrderExample').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
          { className: "td-actions text-right",orderable: false , "targets": [3] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
@endsection