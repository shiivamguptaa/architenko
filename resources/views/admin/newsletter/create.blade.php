@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Add New Newsletter Text</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('newslettertext/store') }}" enctype="multipart/form-data">
									@csrf					
									<div class="form-group row">
										<label for="description" style="position:relative" class="col-md-4 col-form-label text-md-left">{{ __('Description') }}</label>					
										<div class="col-md-10">
											<textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description"></textarea>						
											@error('description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group row">
										<label for="inputState" class="col-md-12 col-form-label text-md-left">Select Language</label>					
										<div class="col-md-2">
											<select id="inputState" class="form-control" name="lang">
												@if(count($languages)>0)
												@foreach($languages as $language)
												<option value="{{$language->name}}">{{$language->name}}</option>
												@endforeach
												@else
												<option value="en">en</option>
												<option value="nl">nl</option>
												@endif
											</select>
										</div>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script>
	CKEDITOR.editorConfig = function (config) {
	config.language = 'es';
	config.uiColor = '#F7B42C';
	config.height = 300;
	config.toolbarCanCollapse = true;
	
	};
	CKEDITOR.replace('description');
	</script>
@endsection							