@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Add New Email</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('emailform/store') }}" enctype="multipart/form-data">
									@csrf	
									<div class="form-group row">
										<label for="formname"  class="col-md-12 col-form-label text-md-left">Select Form</label>					
										<div class="col-md-2">
											<select id="formname" required class="form-control" name="formname">
												<option value="moreinformation">More Information</option>
												<option value="applytoolkit">Apply Tool Kit</option>
												<option value="privacypolicy">Privacy Policy</option>
											</select>
										</div>
									</div>				
									<div class="form-group row">
										<label for="email" style="position:relative" class="col-md-4 col-form-label text-md-left">{{ __('Email') }}</label>					
										<div class="col-md-10">
											<input id="email" type="email" class="form-control" name="email" required />						
										</div>
									</div>									
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection							