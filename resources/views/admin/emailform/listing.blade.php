@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">		
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn">
				<a href="{{url('emailform/create')}}" class="btn btn-success">
				Add New Email<div class="ripple-container"></div></a>
				</p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
			@if(Session::has('alert-'.$msg))
			<div class="alert alert-{{ $msg }} alertmanage" role="alert"> 
				{{ Session::get('alert-'.$msg) }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
			</div>
			@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Email List</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#</th>
											<th>Form Name</th>						
											<th>Email</th>				
											<th class="text-right">Action</th>				
										</tr>
									</thead>
									<tbody id="labelList">					
										@if(count($emailForm) > 0)
										<?php $counter = 1; ?>
										@foreach($emailForm as $email)
										<tr>												
											<td>{{$counter}}</td>						
											<td>{{$email['formname']}}</td>						
											<td>{{$email['email']}}</td>						
											<td class="td-actions text-right">												
												<a href="{{ url('emailform/edit/'.$email['id']) }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('emailform/delete/'.$email['id']) }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>
										</tr>
										<?php $counter++; ?>
										@endforeach					
										@endif					
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script type="text/javascript">
function onChangeLabelLanguage(language){
	$.ajax({
		url:"{{route('onChangeLabelLanguage')}}",
		headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
		data:{language:language},
		type:"POST",
		success:function(response){
			let data  = JSON.parse(JSON.stringify(response));
			if(data.success){
				console.log(data.data);
				$("#labelList").html(data.data);
			}
		},
		error:function(error){	
			console.log(error);
		}
	});
}
</script>
@endsection		

@section('extrascript')
<script type="text/javascript">
    $(document).ready(function () {
      $('#dtOrderExample').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
          { className: "td-actions text-right",orderable: false , "targets": [3] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
@endsection