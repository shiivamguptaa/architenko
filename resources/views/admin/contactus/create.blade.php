@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('contact/store') }}" enctype="multipart/form-data">
									@csrf	
									<div class="form-group row">	
										<div class="col-md-12">
											<input type="text" name="enquiry_title" id="enquiry_title" placeholder="Enquiry Title" class="form-control" title="Enquiry Title"  required 	/>
										</div>
									</div>
									<div class="form-group row">	
										<div class="col-md-12">
											<input type="text" name="enquiry_email" id="enquiry_email" placeholder="Enquiry Email" class="form-control" title="Enquiry Email" required 	/>
										</div>
									</div>
									<div class="form-group row">	
										<div class="col-md-12">
											<input type="text" name="communication_title" id="communication_title" placeholder="Communication Title" class="form-control" title="Communication Title" required />
										</div>
									</div>
									<div class="form-group row">	
										<div class="col-md-12">
											<input type="text" name="communication_email" id="communication_email" placeholder="Communication Email" class="form-control" title="Communication Email"   required />
										</div>
									</div>
									<div class="form-group row">	
										<div class="col-md-12">
											<input type="file" name="image" id="image" placeholder="Image" class="form-control" title="Image"  required />
										</div>
									</div>
									<div class="form-group row">	
										<div class="col-md-12">
											<textarea name="invoice_information" id="invoice_information" placeholder="Invoice Information" class="form-control" title="Invoice Information" required ></textarea>
										</div>
									</div>
									
									
									<!-- Profile Section End -->

									<!-- Profile Popup Section start -->
									<div class="col-md-12">
										<h3>Office Address</h3>
										<table class="table table-bordered" id="addressPopup">	
											<tr>
												<th>Title</th>
												<th>Address</th>
												<th>Phone Number</th>
												<th>Description</th>
												<th>Popup Image</th>
												<th>Direction</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>		
											<tr>  												 
												<td>
													<input class="form-control" id="address[0][title]" name="address[0][title]" title="Title" placeholder="Title" required />
												</td>	
												<td>
													<textarea class="form-control" id="address[0][address]" name="address[0][address]" title="Address" placeholder="Address" required ></textarea>
												</td>	
												<td>
													<input class="form-control" id="address[0][phone_number]" name="address[0][phone_number]" title="Phone Number" placeholder="Phone Number" required />
												</td>
												<td>
													<textarea class="form-control" id="address[0][description]" name="address[0][description]" title="Description" placeholder="Description" required></textarea>
												</td>
												<td>
													<input type="file" name="address[0][popup_image]" class="form-control" accept="image/*" title="Popup Image" required />
												</td>
												<td>
													<input type="file" name="address[0][direction]" class="form-control" accept="application/pdf" title="Direction" required />
												</td>
												<td>
													<input type="number" name="address[0][display_order]" id="address[0][display_order]" class="form-control" min="0" placeholder="Enter display order" title="Display order" required />
												</td>
												<td>
													<button type="button" name="add" id="add" class="btn btn-success">Add More</button>
												</td>  
											</tr>  
										</table>
									</div>
									<!-- Profile Popup Section End -->

									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="lang">
											<option value="en" selected>en</option>
											<option value="nl">nl</option>
										</select>
									</div>
									<!-- Award Popup Section End -->
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary" title="submit">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	CKEDITOR.editorConfig = function (config) {
		config.language = 'es';
		config.uiColor = '#F7B42C';
		config.height = 300;
		config.toolbarCanCollapse = true;	
	};
	CKEDITOR.replace('invoice_information');
	CKEDITOR.replace('address[0][description]');
	let i=0;
	jQuery("#add").click(function(){	
		++i;	
		jQuery("#addressPopup").append(`<tr>  												 
					<td>
						<input class="form-control" id="address[${i}][title]" name="address[${i}][title]" title="Title" placeholder="Title"  />
					</td>	
					<td>
						<textarea class="form-control" id="address[${i}][address]" name="address[${i}][address]" title="Address" placeholder="Address"></textarea>
					</td>	
					<td>
						<input class="form-control" id="address[${i}][phone_number]" name="address[${i}][phone_number]" title="Phone Number" placeholder="Phone Number"  />
					</td>
					<td>
						<textarea class="form-control" id="address[${i}][description]" name="address[${i}][description]" title="Description" placeholder="Description" ></textarea>
					</td>
					<td>
						<input type="file" name="address[${i}][popup_image]" class="form-control" accept="image/*" title="Popup Image" required />
					</td>
					<td>
						<input type="file" name="address[${i}][direction]" class="form-control" accept="application/pdf" title="Direction" required />
					</td>
					<td>
						<input type="number" name="address[${i}][display_order]"  id="address[${i}][display_order]" class="form-control" min="0" placeholder="Enter display order" title="Display order"  />
					</td>					
					<td>
						<button type="button" class="btn btn btn-danger remove" title="Remove">Remove</button>
					</td>  
				</tr>`);
		let description = 'address['+i+'][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.remove', function(){  
		$(this).parents('tr').remove();
	}); 
	
</script>
@endsection								
