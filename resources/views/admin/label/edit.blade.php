@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Edit Label</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="post" action="{{ url('label/update') }}" enctype="multipart/form-data">
									@csrf				
									<input type="hidden" name="id" value="{{ $id }}" />				
									<div class="form-group row">
										<label for="label" class="col-md-12 col-form-label text-md-left">Select Label</label>					
										<div class="col-md-2">
											<select id="label" class="form-control" name="label">
												<option value="extendedprofile" <?php if("extendedprofile" == $label['label']){echo "selected";} ?> >Extended profile</option>
												<option value="readmoreabouttheteam" <?php if("readmoreabouttheteam" == $label['label']){echo "selected";} ?> >Read more about the team</option>
												<option value="selectedawards" <?php if("selectedawards" == $label['label']){echo "selected";} ?> >Selected awards</option>
												<option value="view" <?php if("view" == $label['label']){echo "selected";} ?>>View</option>
												<option value="viewmore" <?php if("viewmore" == $label['label']){echo "selected";} ?>>View More</option>
												<option value="submit" <?php if("submit" == $label['label']){echo "selected";} ?>>Submit</option>
												<option value="backtotop" <?php if("backtotop"  == $label['label']){echo "selected";} ?>>Back to Top</option>
												<option value="termsandconditions"  <?php if("termsandconditions"  == $label['label']){echo "selected";} ?> >Terms and Conditions</option>
												<option value="openedpositions"  <?php if("openedpositions"  == $label['label']){echo "selected";} ?>>Opened positions</option>
												<option value="subscribe" <?php if("subscribe"  == $label['label']){echo "selected";} ?>>Subscribe</option>
												<option value="apply" <?php if("apply"  == $label['label']){echo "selected";} ?>>Apply</option>
												<option value="send" <?php if("send"  == $label['label']){echo "selected";} ?> >Send</option>
												<option value="fulltext" <?php if("fulltext"  == $label['label']){echo "selected";} ?>>Full Text</option>
												<option value="readmore" <?php if("readmore"  == $label['label']){echo "selected";} ?> >Read More</option>
												<option value="hide"  <?php if("hide"  == $label['label']){echo "selected";} ?> >Hide</option>
												<option value="return"  <?php if("return"  == $label['label']){echo "selected";} ?> >Return</option>
												<option value="applyfortooltip"  <?php if("applyfortooltip"  == $label['label']){echo "selected";} ?> >Apply For This Tooltip</option>
												<option value="direction" <?php if("direction"  == $label['label']){echo "selected";} ?> >Direction</option>
												<option value="downloaddirection" <?php if("downloaddirection"  == $label['label']){echo "selected";} ?>>Download Direction</option>
												<option value="subscribealert" <?php if("subscribealert"  == $label['label']){echo "selected";} ?>>Subscribe Alert Success</option>
												<option value="subscribealerterror" <?php if("subscribealerterror"  == $label['label']){echo "selected";} ?>>Subscribe Alert Error</option>
											</select>
										</div>
									</div>				
									<div class="form-group row">
										<label for="name" style="position:relative" class="col-md-4 col-form-label text-md-left">{{ __('Name') }}</label>					
										<div class="col-md-10">
											<input id="name" type="text" value="{{$label['name']}}"  class="form-control @error('name') is-invalid @enderror" name="name" />						
											@error('name')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>						
									<div class="form-group row">
										<label for="inputState" class="col-md-12 col-form-label text-md-left">Select Language</label>					
										<div class="col-md-2">
											<select id="inputState" class="form-control" name="language">
												@if(count($languages)>0)
												@foreach($languages as $language)
												<option value="{{$language->name}}" <?php if($language->name == $label['language']){echo "selected";} ?>>{{$language->name}}</option>
												@endforeach
												@else
												<option value="en">en</option>
												<option value="nl">nl</option>
												@endif
											</select>
										</div>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Update') }}
											</button>
											<a class="btn btn-primary" href="{{ url('label') }}">Cancel</a>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection