@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">		
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn">
				<a href="{{url('label/create')}}" class="btn btn-success">
				Add New Label<div class="ripple-container"></div></a>
				</p>		
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Label List</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-2 pull-right">
									<label for="select_lang">Select Language</label>
									<select id="select_lang" class="form-control" name="lang">
										<option value="" selected disabled>Select Language</option>
										@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}">{{$language->name}}</option>
											@endforeach						  
										@endif
									</select>
								</div>
							</div>
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#</th>
											<th>Name</th>						
											<th>Label</th>				
											<th>Language</th>				
											<th class="text-right">Action</th>				
										</tr>
									</thead>
									<tbody id="labelList">					
										@if(count($labels) > 0)
										<?php $counter = 1; ?>
										@foreach($labels as $label)
										<tr>
											<td>{{$counter}}</td>
											<td>{{$label['name']}}</td>			
											@if($label['label']=="viewmore")		
												<td>View More</td>						
											@endif		
											@if($label['label']=="submit")		
												<td>Submit</td>				
											@endif			
											@if($label['label']=="backtotop")		
												<td>Back to Top</td>		
											@endif		
											@if($label['label']=="extendedprofile")		
												<td>Extended profile</td>						
											@endif	
											@if($label['label']=="readmoreabouttheteam")		
												<td>Read more about the team</td>						
											@endif	
											@if($label['label']=="selectedawards")		
												<td>Selected awards</td>						
											@endif				
											@if($label['label']=="view")		
												<td>View</td>						
											@endif	
											@if($label['label']=="termsandconditions")		
												<td>Terms and Conditions</td>						
											@endif				
											@if($label['label']=="openedpositions")		
												<td>Opened positions</td>						
											@endif		
											@if($label['label']=="apply")		
												<td>Apply</td>						
											@endif		
											@if($label['label']=="subscribe")		
												<td>subscribe</td>						
											@endif			
											@if($label['label']=="send")		
												<td>Send</td>						
											@endif	
											@if($label['label']=="fulltext")		
												<td>Full Text</td>						
											@endif			
											@if($label['label']=="readmore")		
												<td>Read More</td>						
											@endif	
											@if($label['label']=="hide")		
												<td>Hide</td>						
											@endif		
											@if($label['label']=="return")		
												<td>Return</td>						
											@endif	
											@if($label['label']=="applyfortooltip")		
												<td>Apply For This Tooltip</td>						
											@endif		
											@if($label['label']=="direction")		
												<td>Direction</td>						
											@endif	
											@if($label['label']=="downloaddirection")		
												<td>Download Direction</td>						
											@endif
											@if($label['label']=="subscribealert")		
												<td>Subscribe Alert Success</td>						
											@endif	
											@if($label['label']=="subscribealerterror")		
												<td>Subscribe Alert Error</td>						
											@endif					
											<td>{{$label['language']}}</td>						
											<td class="td-actions text-right">												
												<a href="{{ url('label/'.$label['id'].'/edit') }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('label/'.$label['id'].'/delete') }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>
										</tr>
										<?php $counter++; ?>
										@endforeach					
										@endif					
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>

@endsection		

@section('extrascript')
<script type="text/javascript">
    $(document).ready(function () {
      $('#dtOrderExample').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
          { className: "td-actions text-right",orderable: false , "targets": [4] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
<script type="text/javascript">
	let BASE_URL = '{{URL("/")}}';
    $(document).ready(function () {
      	var table = $('#dtOrderExample').DataTable();
      	$(document).on('change','#select_lang',function(){
      		let lang = $(this).val();
      		$.ajax({
				url:"{{route('onChangeLabelLanguage')}}",
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				data:{lang:lang},
				type:"POST",
				success:function(response){
						// console.table(response.data);
					if (response.responseCode == 1) {
						table.clear().draw();
						$.each(response.data,function(key,value){
							console.log(value);
							let edit = BASE_URL+'/label/'+value.id+'/edit';
							let del = BASE_URL+'/label/'+value.id+'/delete';

							let action = `<div class="td-actions text-right"><a href="${edit}" class="btn btn-success">
														<i class="material-icons">edit</i>
													</a>								
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('${del}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a></div>`;
							table.row.add([key+1,value.name,value.name,value.language,action]).draw();
						});
					}
				},
				error:function(error){	
					console.log(error);
				}
			});
      	});
      	
    });
</script>
@endsection