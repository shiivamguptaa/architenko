@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Add New Label</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('label/store') }}" enctype="multipart/form-data">
									@csrf	
									<div class="form-group row">
										<label for="label" class="col-md-12 col-form-label text-md-left">Select Label</label>					
										<div class="col-md-2">
											<select id="label" class="form-control" name="label">
												<option value="extendedprofile">Extended profile</option>
												<option value="readmoreabouttheteam">Read more about the team</option>
												<option value="selectedawards">Selected awards</option>
												<option value="termsandconditions">Terms and Conditions</option>
												<option value="openedpositions">Opened positions</option>
												<option value="view">View</option>
												<option value="viewmore">View More</option>
												<option value="submit">Submit</option>
												<option value="backtotop">Back to Top</option>
												<option value="subscribe">Subscribe</option>
												<option value="apply">Apply</option>
												<option value="send">Send</option>
												<option value="fulltext">Full Text</option>
												<option value="readmore">Read More</option>
												<option value="hide">Hide</option>	
												<option value="return">Return</option>
												<option value="applyfortooltip">Apply For This Tooltip</option>
												<option value="direction">Direction</option>
												<option value="downloaddirection">Download Direction</option>
												<option value="subscribealert">Subscribe Alert Success</option>
												<option value="subscribealerterror">Subscribe Alert Error</option>
											</select>
										</div>
									</div>				
									<div class="form-group row">
										<label for="name" style="position:relative" class="col-md-4 col-form-label text-md-left">{{ __('Name') }}</label>					
										<div class="col-md-10">
											<input id="name" type="text"   class="form-control @error('name') is-invalid @enderror" name="name" />						
											@error('name')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>									
									<div class="form-group row">
										<label for="inputState" class="col-md-12 col-form-label text-md-left">Select Language</label>					
										<div class="col-md-2">
											<select id="inputState" class="form-control" name="language">
												@if(count($languages)>0)
												@foreach($languages as $language)
												<option value="{{$language->name}}">{{$language->name}}</option>
												@endforeach
												@else
												<option value="en">en</option>
												<option value="nl">nl</option>
												@endif
											</select>
										</div>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection							