@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }}" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Logo Setting</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="post" action="{{ url('logo/update') }}" enctype="multipart/form-data">
									@csrf				
									<input type="hidden" name="id" value="{{ $logoid }}" />				
									<div class="form-group row">
										<div class="fileinput fileinput-new text-left" data-provides="fileinput">
											<div class="fileinput-new thumbnail img-raised">							
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail img-raised"><img class="width-100" src="{{ asset('images/') }}/{{ $logo->logo }}" alt="" /></div>
											<div>
												<span class="btn btn-raised btn-round btn-default btn-file">
													<span class="fileinput-new">Select logo</span>
													
													<input type="file" name="logo" />
												</span>							
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="fileinput fileinput-new text-left" data-provides="fileinput">
											<div class="fileinput-new thumbnail img-raised">							
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail img-raised" style="background:#000;"><img class="width-100" src="{{ asset('images/') }}/{{ $logo->transparent_logo }}" alt="" /></div>
											<div>
												<span class="btn btn-raised btn-round btn-default btn-file">
													<span class="fileinput-new">Select Transparent logo</span>
													
													<input type="file" name="transparent_logo" />
												</span>							
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="fileinput fileinput-new text-left" data-provides="fileinput">
											<div class="fileinput-new thumbnail img-raised">							
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail img-raised" style="background:#000;"><img class="width-100" src="{{ asset('images/') }}/{{ $logo->menutoggle_logo }}" alt="" /></div>
											<div>
												<span class="btn btn-raised btn-round btn-default btn-file">
													<span class="fileinput-new">Select Toggle Menu Logo</span>
													
													<input type="file" name="menutoggle_logo" />
												</span>							
											</div>
										</div>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Update') }}
											</button>						
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection	