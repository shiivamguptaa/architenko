@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Edit Footer</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="post" action="{{ url('footer/update') }}" enctype="multipart/form-data">
									@csrf				
									<input type="hidden" name="id" value="{{ $configurationid }}" />
									<div class="form-group row">
										<label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Name') }}</label>					
										<div class="col-md-8">
											<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $configurationdata->name }}" required autocomplete="name" autofocus>						
											@error('name')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group row">
										<label for="email" class="col-md-4 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>					
										<div class="col-md-8">
											<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $configurationdata->email }}" required autocomplete="email">						
											@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group row">
										<label for="mobile" class="col-md-4 col-form-label text-md-left">Mobile</label>					
										<div class="col-md-8">
											<input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ $configurationdata->mobile }}">						
											@error('mobile')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<div class="form-group row">
										<label for="inputAddress" class="col-md-4 col-form-label text-md-left">Address</label>
										<div class="col-md-8">
											<input type="text" name="address" class="form-control" id="inputAddress" placeholder="1234 Main St" value="{{ $configurationdata->address }}">
										</div>				
									</div>				
									<div class="form-group row">
										<label for="inputAddress" class="col-md-4 col-form-label text-md-left">Footer Description Left</label>
										<div class="col-md-8">
											<textarea class="form-control" name="description" id="description" rows="5">{{ $configurationdata->description }}</textarea>					
										</div>				
									</div>				
									<div class="form-group row">
										<label for="inputAddress" class="col-md-4 col-form-label text-md-left">Footer Description Right</label>
										<div class="col-md-8">
											<textarea class="form-control" name="description2" id="description1" rows="5">{{ $configurationdata->description2 }}</textarea>					
										</div>
									</div>
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="lang">
											@if(count($languages) > 0)
											@foreach($languages as $language)
											<option value="{{$language->name}}" @if($configurationdata->language == $language->name) selected @endif>{{$language->name}}</option>
											@endforeach
											@else
											<option value="en" @if($configurationdata->language == "en") selected @endif>en</option>
											<option value="nl" @if($configurationdata->language == "nl") selected @endif>nl</option>
											@endif
										</select>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-8">
											<button type="submit" class="btn btn-primary">
												{{ __('Update') }}
											</button>
											<a class="btn btn-primary" href="{{ url('footer') }}">Cancel</a>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
			
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script>
	CKEDITOR.editorConfig = function (config) {
	config.language = 'es';
	config.uiColor = '#F7B42C';
	config.height = 300;
	config.toolbarCanCollapse = true;
	
	};
	CKEDITOR.replace('description');
	CKEDITOR.editorConfig = function (config) {
	config.language = 'es';
	config.uiColor = '#F7B42C';
	config.height = 300;
	config.toolbarCanCollapse = true;
	
	};
	CKEDITOR.replace('description1');
</script>
@endsection	