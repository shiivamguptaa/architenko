@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn">
					<a href="{{ url('moreinformation/create') }}" class="btn btn-success">Add New More Information Page</a>
				</p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
								    <thead class="">
								        <tr>
											<th class="text-center">#</th>
											<th>Title</th>
											<th>Service Title</th>
											<th>Project Address Title</th>
											<th>Language</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php $counter = 1; ?>
										@foreach($moreinformations as $moreinformation)
											<tr>							
												<td class="text-center">{{$counter}}</td>										
												<td>{{ $moreinformation->title }}</td>
												<td>{{ $moreinformation->service_title }}</td>
												<td>{{ $moreinformation->project_address_title }}</td>
												<td>{{ $moreinformation->language }}</td>											
												<td class="td-actions text-right">												
													<a href="{{ url('moreinformation/'.$moreinformation->id.'/edit') }}" class="btn btn-success">
														<i class="material-icons">edit</i>
													</a>
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('moreinformation/'.$moreinformation->id.'/delete') }}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a>
												</td>							
											</tr>
										<?php $counter++; ?>
										@endforeach											
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection	

@section('extrascript')
<script type="text/javascript">
    $(document).ready(function () {
      $('#dtOrderExample').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
          { className: "td-actions text-right",orderable: false , "targets": [5] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
@endsection										