@extends('layouts.auth')
@section('content')

<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('moreinformation/store') }}" enctype="multipart/form-data">
									@csrf	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="title" name="title" type="text" class="form-control" autocomplete="off" title="Title" placeholder="Title" autofocus />										
										</div>
									</div>

									<div class="form-group row">
										<!-- <label for="service_title" class="col-md-12 col-form-label text-md-left">{{ __('Service Title') }}</label> -->					
										<div class="col-md-12">
											<input id="service_title" name="service_title" type="text" class="form-control" autocomplete="off" title="Service Title" placeholder="Service Title" />											
										</div>
									</div>

									

									<div class="form-group row">
									<!-- 	<label for="project_address_title" class="col-md-12 col-form-label text-md-left">{{ __('Project Address Title') }}</label>	 -->				
										<div class="col-md-12">
											<input id="project_address_title" name="project_address_title" type="text" class="form-control" autocomplete="off" title="Project Address Title" placeholder="Project Address Title" />
										</div>
									</div>

									<div class="form-group row">
										<!-- <label for="project_address_input_title" class="col-md-12 col-form-label text-md-left">{{ __('Project Address Input Title') }}</label>	 -->				
										<div class="col-md-12">
											<input id="project_address_input_title" name="project_address_input_title" type="text" class="form-control" autocomplete="off" title="Project Address Input Title" placeholder="Project Address Input Title" />
										</div>
									</div>

									<div class="form-group row">
										<!-- <label for="description_question_project_title" class="col-md-12 col-form-label text-md-left">{{ __('Description Question Project Title') }}</label>	 -->				
										<div class="col-md-12">
											<input id="description_question_project_title" name="description_question_project_title" type="text" class="form-control" autocomplete="off" title="Description Question Project Title" placeholder="Description Question Project Title" />
										</div>
									</div>

									<div class="form-group row">
										<!-- <label for="personal_contact_title" class="col-md-12 col-form-label text-md-left">{{ __('Personal Contact Title') }}</label>			 -->		
										<div class="col-md-12">
											<input id="personal_contact_title" name="personal_contact_title" type="text" class="form-control" autocomplete="off" title="Personal Contact Title" placeholder="Personal Contact Title" />
										</div>
									</div>
									<div class="form-group row">
									<!-- 	<label for="address_title" class="col-md-12 col-form-label text-md-left">{{ __('Address Title') }}</label> -->					
										<div class="col-md-12">
											<input id="address_title" name="address_title" type="text" class="form-control" autocomplete="off" title="Address Title" placeholder="Address Title" />
										</div>
									</div>
									<div class="form-group row">
										<!-- <label for="mr_title" class="col-md-12 col-form-label text-md-left">{{ __('Mr Title') }}</label> -->					
										<div class="col-md-12">
											<input id="mr_title" name="mr_title" type="text" class="form-control" autocomplete="off" title="Mr Title" placeholder="Mr Title" />
										</div>
									</div>
									<div class="form-group row">
									<!-- 	<label for="mrs_title" class="col-md-12 col-form-label text-md-left">{{ __('Mrs Title') }}</label>	 -->				
										<div class="col-md-12">
											<input id="mrs_title" name="mrs_title" type="text" class="form-control" autocomplete="off" title="Mrs Title" placeholder="Mrs Title" />
										</div>
									</div>
									<div class="form-group row">
										<!-- <label for="first_name" class="col-md-12 col-form-label text-md-left">{{ __('First Name Title') }}</label>		 -->			
										<div class="col-md-12">
											<input id="first_name" name="first_name" type="text" class="form-control" autocomplete="off" title="First Name Title" placeholder="First Name Title" />
										</div>
									</div>
									<div class="form-group row">
									<!-- 	<label for="middle_name" class="col-md-12 col-form-label text-md-left">{{ __('Middle Name Title') }}</label>		 -->			
										<div class="col-md-12">
											<input id="middle_name" name="middle_name" type="text" class="form-control" autocomplete="off" title="Middle Name Title" placeholder="Middle Name Title" />
										</div>
									</div>
									<div class="form-group row">
										<!-- <label for="last_name" class="col-md-12 col-form-label text-md-left">{{ __('Last Name Title') }}</label> -->					
										<div class="col-md-12">
											<input id="last_name" name="last_name" type="text" class="form-control" autocomplete="off" title="Last Name Title" placeholder="Last Name Title" />
										</div>
									</div>	
									<div class="form-group row">
										<!-- <label for="address" class="col-md-12 col-form-label text-md-left">{{ __('Address Title') }}</label> -->					
										<div class="col-md-12">
											<input id="address" name="address" type="text" class="form-control" autocomplete="off" title="Address Title" placeholder="Address Title" />
										</div>
									</div>
									<div class="form-group row">
										<!-- <label for="city" class="col-md-12 col-form-label text-md-left">{{ __('City Title') }}</label> -->
										<div class="col-md-12">
											<input id="city" name="city" type="text" class="form-control" autocomplete="off" title="City Title" placeholder="City Title" />
										</div>
									</div>	
									<div class="form-group row">
										<!-- <label for="postal_code" class="col-md-12 col-form-label text-md-left">{{ __('Postal Code Title') }}</label> -->
										<div class="col-md-12">
											<input id="postal_code" name="postal_code" type="text" class="form-control" autocomplete="off" title="Postal Code Title" placeholder="Postal Code Title" />
										</div>
									</div>
									<div class="form-group row">
										<!-- <label for="country" class="col-md-12 col-form-label text-md-left">{{ __('Country Title') }}</label> -->
										<div class="col-md-12">
											<input id="country" name="country" type="text" class="form-control" autocomplete="off" title="Country Title" placeholder="Country Title" />
										</div>
									</div>	
									<div class="form-group row">
										<!-- <label for="phone" class="col-md-12 col-form-label text-md-left">{{ __('Phone Title') }}</label> -->
										<div class="col-md-12">
											<input id="phone" name="phone" type="text" class="form-control" autocomplete="off" title="Phone Title" placeholder="Phone Title" />
										</div>
									</div>	
									<div class="form-group row">
										<!-- <label for="email" class="col-md-12 col-form-label text-md-left">{{ __('Email Title') }}</label> -->
										<div class="col-md-12">
											<input id="email" name="email" type="text" class="form-control" autocomplete="off" title="Email Title" placeholder="Email Title" />
										</div>
									</div>	

									<div class="form-group row">
									<!-- 	<label for="dob" class="col-md-12 col-form-label text-md-left">{{ __('Date of Birth Title') }}</label> -->
										<div class="col-md-12">
											<input id="dob" name="dob" type="text" class="form-control" autocomplete="off" title="Date of Birth Title" placeholder="Date of Birth Title" />
										</div>
									</div>	

									<div class="form-group row">
										<!-- <label for="iam_title" class="col-md-12 col-form-label text-md-left">{{ __('Date of Birth Title') }}</label> -->
										<div class="col-md-12">
											<input id="iam_title" name="iam_title" type="text" class="form-control" autocomplete="off" title="I am Title" placeholder="I am Title" />
										</div>
									</div>		
									<div class="form-group row">										
										<div class="col-md-12">
											<input id="enterprise_title" name="enterprise_title" type="text" class="form-control" autocomplete="off" title="Enterprise Title" placeholder="Enterprise Title"   />
										</div>
									</div>	
									<div class="form-group row">										
										<div class="col-md-12">
											<input id="developer_title" name="developer_title" type="text" class="form-control" autocomplete="off" title="Developer Title" placeholder="Developer Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="governmental_title" name="governmental_title" type="text" class="form-control" autocomplete="off" title="Governmental Title" placeholder="Governmental Title"  />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_title" name="private_individual_title" type="text" class="form-control" autocomplete="off" title="Private Individual Title" placeholder="Private Individual Title"  />
										</div>
									</div>
									<!-- PRIVATE INDIVIDUAL Start -->
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_address_title" name="private_individual_address_title" type="text" class="form-control" autocomplete="off" title="Private Individual Address Title" placeholder="Private Individual Address Title"  />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_mr_title" name="private_individual_mr_title" type="text" class="form-control" autocomplete="off" title="Private Individual Mr Title" placeholder="Private Individual Mr Title"  />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_mrs_title" name="private_individual_mrs_title" type="text" class="form-control" autocomplete="off" title="Private Individual Mrs Title" placeholder="Private Individual Mrs Title"  />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_first_name" name="private_individual_first_name" type="text" class="form-control" autocomplete="off" title="Private Individual First Name Title" placeholder="Private Individual First Name Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_middle_name" name="private_individual_middle_name" type="text" class="form-control" autocomplete="off" title="Private Individual Middle Name Title" placeholder="Private Individual Middle Name Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_last_name" name="private_individual_last_name" type="text" class="form-control" autocomplete="off" title="Private Individual Last Name Title" placeholder="Private Individual Last Name Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_address" name="private_individual_address" type="text" class="form-control" autocomplete="off" title="Private Individual Address Title" placeholder="Private Individual Address Title" />
										</div>
									</div>			
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_city" name="private_individual_city" type="text" class="form-control" autocomplete="off" title="Private Individual City Title" placeholder="Private Individual City Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_postal_code" name="private_individual_postal_code" type="text" class="form-control" autocomplete="off" title="Private Individual Postal Code Title" placeholder="Private Individual Postal Code Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_country" name="private_individual_country" type="text" class="form-control" autocomplete="off" title="Private Individual Country Title" placeholder="Private Individual Country Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_phone" name="private_individual_phone" type="text" class="form-control" autocomplete="off" title="Private Individual Phone Title" placeholder="Private Individual Phone Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_email" name="private_individual_email" type="text" class="form-control" autocomplete="off" title="Private Individual Email Title" placeholder="Private Individual Email Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_dob" name="private_individual_dob" type="text" class="form-control" autocomplete="off" title="Private Individual Date of Birth Title" placeholder="Private Individual Date of Birth Title" />
										</div>
									</div>

									<!-- PRIVATE INDIVIDUAL end -->

									<!-- ENTERPRISE Start -->
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_price_title1" name="enterprise_price_title1" type="text" class="form-control" autocomplete="off" title="Enterprise Title" placeholder="Enterprise Title"  />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_price_title2" name="enterprise_price_title2" type="text" class="form-control" autocomplete="off" title="Enterprise Title" placeholder="Enterprise Title"  />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_address_title" name="enterprise_address_title" type="text" class="form-control" autocomplete="off" title="Enterprise Address Name Title" placeholder="Enterprise Address Name Title"  />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_city_title" name="enterprise_city_title" type="text" class="form-control" autocomplete="off" title="Enterprise City Title" placeholder="Enterprise City Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_postal_code_title" name="enterprise_postal_code_title" type="text" class="form-control" autocomplete="off" title="Enterprise Postal Code Title" placeholder="Enterprise Postal Code Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_country_title" name="enterprise_country_title" type="text" class="form-control" autocomplete="off" title="Enterprise Country Title" placeholder="Enterprise Country Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_phone_title" name="enterprise_phone_title" type="text" class="form-control" autocomplete="off" title="Enterprise Phone Title" placeholder="Enterprise Phone Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_email_title" name="enterprise_email_title" type="text" class="form-control" autocomplete="off" title="Enterprise Email Title" placeholder="Enterprise Email Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_contact_title" name="enterprise_contact_title" type="text" class="form-control" autocomplete="off" title="Enterprise Contact Title" placeholder="Enterprise Contact Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_mr_title" name="enterprise_mr_title" type="text" class="form-control" autocomplete="off" title="Enterprise Mr Title" placeholder="Enterprise Mr Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_mrs_title" name="enterprise_mrs_title" type="text" class="form-control" autocomplete="off" title="Enterprise Mrs Title" placeholder="Enterprise Mrs Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_contact_first_name_title" name="enterprise_contact_first_name_title" type="text" class="form-control" autocomplete="off" title="Enterprise Contact First Name Title" placeholder="Enterprise Contact First Name Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_contact_last_name_title" name="enterprise_contact_last_name_title" type="text" class="form-control" autocomplete="off" title="Enterprise Contact Last Name Title" placeholder="Enterprise Contact Last Name Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_contact_personpositionlist_title" name="enterprise_contact_personpositionlist_title" type="text" class="form-control" autocomplete="off" title="Enterprise Contact Person Position List" placeholder="Enterprise Contact Person Position List" data-role="tagsinput"/>
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="submit" name="submit" type="text" class="form-control" autocomplete="off" title="Apply Button Title" placeholder="Apply Button Title" />
										</div>
									</div>			

									

									<!-- PRIVATE INDIVIDUAL end -->

									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="language">
											@if(count($languages)>0)
												@foreach($languages as $language)
													<option value="{{$language->name}}">{{$language->name}}</option>
												@endforeach						  
											@endif
										</select>
									</div>				
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>

@endsection						