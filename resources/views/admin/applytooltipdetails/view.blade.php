@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							@if(!empty($studioApplyDetail))
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th>Name</th>
											<th>Value</th>
										</tr>
										<tr>
											<th>Article Name</th>
											<td>@if(isset($studioApplyDetail['article_title'])){{$studioApplyDetail['article_title']}}@endif</td>
										</tr>
										<tr>
											<th>You found us on/through:</th>
											<td>@if(isset($studioApplyDetail['you_found_through'])){{$studioApplyDetail['you_found_through']}}@endif</td>
										</tr>	
										<tr>
											<th>You found us on/through other</th>
											<td>@if(isset($studioApplyDetail['you_found_through_other'])){{$studioApplyDetail['you_found_through_other']}}@endif</td>
										</tr>		
										<tr>
											<th>Choose Toolkit:</th>
											<td>@if(isset($studioApplyDetail['choose_toolkit'])){{$studioApplyDetail['choose_toolkit']}}@endif</td>
										</tr>		
										<tr>
											<th>Choose Toolkit Other</th>
											<td>@if(isset($studioApplyDetail['choose_toolkit_other'])){{$studioApplyDetail['choose_toolkit_other']}}@endif</td>
										</tr>
										<tr>
											<th>Position</th>
											<td>@if(isset($studioApplyDetail['position'])){{$studioApplyDetail['position']}}@endif</td>
										</tr>
										@if($studioApplyDetail['position']=="Private individual")
										<tr>
											<th>Private Individual Gender</th>
											<td>@if(isset($studioApplyDetail['private_individual_gender'])){{$studioApplyDetail['private_individual_gender']}}@endif</td>
										</tr>
										<tr>
											<th>Private Individual First Name</th>
											<td>@if(isset($studioApplyDetail['private_individual_first_name'])){{$studioApplyDetail['private_individual_first_name']}}@endif</td>
										</tr>
										<tr>
											<th>Private Individual Middle Name</th>
											<td>@if(isset($studioApplyDetail['private_individual_middle_name'])){{$studioApplyDetail['private_individual_middle_name']}}@endif</td>
										</tr>
										<tr>
											<th>Private Individual Last Name</th>
											<td>@if(isset($studioApplyDetail['private_individual_last_name'])){{$studioApplyDetail['private_individual_last_name']}}@endif</td>
										</tr>
										<tr>
											<th>Private Individual Address</th>
											<td>@if(isset($studioApplyDetail['private_individual_address'])){{$studioApplyDetail['private_individual_address']}}@endif</td>
										</tr>
										<tr>
											<th>Private Individual City</th>
											<td>@if(isset($studioApplyDetail['private_individual_city'])){{$studioApplyDetail['private_individual_city']}}@endif</td>
										</tr>
										<tr>
											<th>Private Individual Postal Code</th>
											<td>@if(isset($studioApplyDetail['private_individual_country'])){{$studioApplyDetail['private_individual_country']}}@endif</td>
										</tr>			
										<tr>
											<th>Private Individual Country</th>
											<td>@if(isset($studioApplyDetail['private_individual_country'])){{$studioApplyDetail['private_individual_country']}}@endif</td>
										</tr>		
										<tr>
											<th>Private Individual Phone</th>
											<td>@if(isset($studioApplyDetail['private_individual_phone'])){{$studioApplyDetail['private_individual_phone']}}@endif</td>
										</tr>
										<tr>
											<th>Private Individual Email</th>
											<td>@if(isset($studioApplyDetail['private_individual_email'])){{$studioApplyDetail['private_individual_email']}}@endif</td>
										</tr>
										<tr>
											<th>Private Individual dob</th>
											<td>@if(isset($studioApplyDetail['private_individual_dob'])){{$studioApplyDetail['private_individual_dob']}}@endif</td>
										</tr>
										
										@else
										<tr>
											<th>Enterprise Title1</th>
											<td>@if(isset($studioApplyDetail['enterprise_price_title1'])){{$studioApplyDetail['enterprise_price_title1']}}@endif</td>
										</tr>
										<tr>
											<th>Enterprise Title2</th>
											<td>@if(isset($studioApplyDetail['enterprise_price_title2'])){{$studioApplyDetail['enterprise_price_title2']}}@endif</td>
										</tr>
										<tr>
											<th>Enterprise Address</th>
											<td>@if(isset($studioApplyDetail['enterprise_address_title'])){{$studioApplyDetail['enterprise_address_title']}}@endif</td>
										</tr>
										<tr>
											<th>Enterprise City</th>
											<td>@if(isset($studioApplyDetail['enterprise_city_title'])){{$studioApplyDetail['enterprise_city_title']}}@endif</td>
										</tr>	
										<tr>
											<th>Enterprise Postal Code</th>
											<td>@if(isset($studioApplyDetail['enterprise_postal_code_title'])){{$studioApplyDetail['enterprise_postal_code_title']}}@endif</td>
										</tr>
										<tr>
											<th>Enterprise Country</th>
											<td>@if(isset($studioApplyDetail['enterprise_country_title'])){{$studioApplyDetail['enterprise_country_title']}}@endif</td>
										</tr>
										<tr>
											<th>Enterprise Phone</th>
											<td>@if(isset($studioApplyDetail['enterprise_phone_title'])){{$studioApplyDetail['enterprise_phone_title']}}@endif</td>
										</tr>	
										<tr>
											<th>Enterprise Email</th>
											<td>@if(isset($studioApplyDetail['enterprise_email_title'])){{$studioApplyDetail['enterprise_email_title']}}@endif</td>
										</tr>
										<tr>
											<th>Enterprise Gender</th>
											<td>@if(isset($studioApplyDetail['enterprise_gender'])){{$studioApplyDetail['enterprise_gender']}}@endif</td>
										</tr>
										<tr>
											<th>Enterprise Contact First Name</th>
											<td>@if(isset($studioApplyDetail['enterprise_contact_first_name_title'])){{$studioApplyDetail['enterprise_contact_first_name_title']}}@endif</td>
										</tr>	
										<tr>
											<th>Enterprise Contact Last Name</th>
											<td>@if(isset($studioApplyDetail['enterprise_contact_last_name_title'])){{$studioApplyDetail['enterprise_contact_last_name_title']}}@endif</td>
										</tr>
										<tr>
											<th>Enterprise Person Position</th>
											<td>@if(isset($studioApplyDetail['enterprise_contact_personpositionlist_title'])){{$studioApplyDetail['enterprise_contact_personpositionlist_title']}}@endif</td>
										</tr>
										@endif										
										<tr>
											<th>Language</th>
											<td>@if(isset($studioApplyDetail['language'])){{$studioApplyDetail['language']}}@endif</td>
										</tr>										
									</tbody>
								</table>
							@endif						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection						