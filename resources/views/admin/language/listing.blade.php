@extends('layouts.auth')

@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
		<div class="container-fluid">			
			<p class="top-add-btn"><a href="{{ url('languages/create') }}" class="btn btn-success">Add New Language</a></p>		
		</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }}" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
			@endif
			@endforeach
			
			<div class="container-fluid">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Language</th>
							<th class="text-right">Actions</th>
						</tr>
					</thead>
					<tbody>
						
							@if(count($languages) > 0)
							@foreach($languages as $lan)
						<tr>
							<td>{{$lan['id']}}</td>
							<td>{{$lan['name']}}</td>
							<td class="td-actions text-right">
								
								<a href="{{ url('languages/'.$lan['id'].'/edit') }}" class="btn btn-success">
									<i class="material-icons">edit</i>
								</a>
								<a  href="{{ url('languages/'.$lan['id'].'/delete') }}" class="btn btn-danger">
									<i class="material-icons">close</i>
								</a>
							</td>
							</tr>
							@endforeach
							  	
							@endif
						
						
						
					</tbody>
				</table>
			</div>
		</div>
		@include('layouts.authfooter')
		
	</div>
	
@endsection							