@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<h3> Edit Language</h3>
			<form method="post" action="{{ url('languages/update') }}" enctype="multipart/form-data">
				@csrf				
				<input type="hidden" name="id" value="{{ $id }}" />				
				
				 
				
				 <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$languagedata['name']}}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
				
				 
				
				
				<div class="form-group row mb-0">
					<div class="col-md-6">
						<button type="submit" class="btn btn-primary">
							{{ __('Update') }}
						</button>
						<a class="btn btn-primary" href="{{ url('languages') }}">Cancel</a>
					</div>
				</div>
			</form>
			
		</div>
	</div>
	@include('layouts.authfooter')
	
</div>

@endsection