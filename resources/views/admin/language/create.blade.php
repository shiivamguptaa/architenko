@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<h3> Add New Language</h3>
			<form method="POST" action="{{ url('languages/store') }}" enctype="multipart/form-data">
				@csrf				
				 <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
				
				 
				
				 			
				
				<div class="form-group row mb-0">
					<div class="col-md-6">
						<button type="submit" class="btn btn-primary">
							{{ __('Submit') }}
						</button>
					</div>
				</div>
			</form>			
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection							