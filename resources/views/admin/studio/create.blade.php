@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('studio/store') }}" enctype="multipart/form-data">
									@csrf	
									<div class="form-group row col-md-12">		
										<label for="profile_section_description" class="">{{ __('Top Image') }}</label>	
										<select id="main_image_type" name="main_image_type" class="form-control">
											<option value="image" selected>Image/Gif</option>
											<option value="youtube">Youtube</option>
											<option value="video">Video upload</option>
											<option value="book">Book Images</option>
											<option value="flip">Flip Images</option>
										</select>
									</div>
									<div class=" row left-image">
										<div class="col-md-10 ">
											<input type="file" name="img_left[]"  multiple  class="form-control imageitem" />
										</div>				
									</div>	
									<div class="form-group row left-link hide">	<div class="col-md-10">
									<input type="text" name="img_left" placeholder="Please enter youtube link" class="form-control"	/>
									</div>
									</div>
									<!-- Profile Section start -->
									<div class="form-group row col-md-12">
										<h3>Profile Section</h3>
									</div>
									<div class="form-group row col-md-12">	
										<label for="profile_section_description" class="col-md-12">{{ __('Profile Title') }}</label>	

										<input id="profiletitle" type="text" class="form-control @error('profiletitle') is-invalid @enderror" name="profiletitle" value="{{ old('profiletitle') }}" Placeholder="Profile Title" required>
										@error('profiletitle')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
									
									<div class="form-group row">
										<label for="profile_section_description" class="col-md-4 col-form-label text-md-left">{{ __('Profile Section Description') }}</label>					
										<div class="col-md-10">
											<textarea class="form-control" id="profile_section_description" name="profile_section_description"></textarea>
											@error('profile_section_description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<div class="form-group row col-md-12">	
										<label for="profile_section_description" class="row col-md-4 col-form-label text-md-left">{{ __('Profile Right Side Image') }}</label>	 
										<select id="profile_image_type" name="profile_image_type" class="form-control">
											<option value="image" selected>Image/Gif</option>
											<option value="youtube">Youtube</option>
											<option value="video">Video upload</option>
											<option value="book">Book Images</option>
											<option value="flip">Flip Images</option>
										</select>
									</div>
									<div class=" row profile_right_image">
										<div class="col-md-10 ">
											<input type="file" name="profile_img_left[]"  multiple  class="form-control imageitem" />
										</div>				
									</div>	
									<div class="form-group row profile_right_link hide">	<div class="col-md-10">
									<input type="text" name="profile_img_left" placeholder="Please enter youtube link" class="form-control"	/>
									</div>
									</div>
									<!-- Profile Section End -->
									<div class="form-group row col-md-12">
										<h5>Profile Popup Section</h5>
									</div>
									<!-- Profile Popup Section start -->
									<div class="col-md-12">
										
										<table class="table table-bordered" id="profilePopupDynamicTable">	
											<tr>
												<th>Description</th>
												<th>Data</th>
												<th>Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>		
											<tr>  												 
												<td>
													<textarea class="form-control" id="profilPopup[0][description]" name="profilPopup[0][description]" required></textarea> 
												</td>	
												<td>
													<input type="file" name="profilPopup[0][image]" class="form-control" />
												</td>	
												<td>
													<input type="text" name="profilPopup[0][title]" class="form-control" placeholder="Enter Title" required>
												</td>
												<td>
													<input type="number" name="profilPopup[0][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
												<td>
													<button type="button" name="addPopupSection" id="addPopupSection" class="btn btn-success">Add More</button>
												</td>  
											</tr>  
										</table>
									</div>
									<!-- Profile Popup Section End -->
									<div class="form-group row col-md-12">
										<h3>Founder Section</h3>
									</div>
									<!--- Founder Section Start----->
									<div class="form-group row col-md-12">	
										<label for="profile_section_description" class="col-md-12">{{ __('Founder Title') }}</label>	
										<input id="foundertitle" type="text" class="form-control @error('foundertitle') is-invalid @enderror" name="foundertitle" value="{{ old('foundertitle') }}" Placeholder="Founder Title" required>
										@error('foundertitle')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
									
									
									<div class="form-group row col-md-12">	
										<label for="founder_section_description" class="row col-md-4 col-form-label text-md-left">{{ __('Founder Left Side Image') }}</label>	 
										<select id="founder_image_type" name="founder_image_type" class="form-control">
											<option value="image" selected>Image/Gif</option>
											<option value="youtube">Youtube</option>
											<option value="video">Video upload</option>
											<option value="book">Book Images</option>
											<option value="flip">Flip Images</option>
										</select>
									</div>
									<div class=" row founder_right_image">
										<div class="col-md-10 ">
											<input type="file" name="founder_img_left[]"  multiple  class="form-control imageitem" />
										</div>				
									</div>	
									<div class="form-group row founder_right_link hide">	<div class="col-md-10">
										<input type="text" name="founder_img_left" placeholder="Please enter youtube link" class="form-control"	/>
									</div>
									</div>
									<div class="form-group row">
										<label for="profile_section_description" class="found-desc-title col-md-4 col-form-label text-md-left">{{ __('Founder Section Description') }}</label>					
										<div class="col-md-10">
											<textarea class="form-control" id="founder_section_description" name="founder_section_description"></textarea>
											@error('founder_section_description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<!--- Founder Section End----->
									<!-- Founder Popup Section start -->
									<div class="col-md-12">
										<h5>Founder Popup Page Section</h5>
										<table class="table table-bordered" id="founderPopupDynamicTable">	
											<tr>
												<th>Description</th>
												<th>Data</th>
												<th>Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>		
											<tr>  												 
												<td>
													<textarea class="form-control" id="founderPopup[0][description]" name="founderPopup[0][description]" required></textarea> 
												</td>	
												<td>
													<input type="file" name="founderPopup[0][image]" class="form-control" />
												</td>	
												<td>
													<input type="text" name="founderPopup[0][title]" class="form-control" placeholder="Enter Title" required>
												</td>
												<td>
													<input type="number" name="founderPopup[0][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
												<td>
													<button type="button" name="addPopupFounderSection" id="addPopupFounderSection" class="btn btn-success">Add More</button>
												</td>  
											</tr>  
										</table>
									</div>
									<!-- Founder Popup Section End -->
									<div class="form-group row col-md-12">
										<h3>Join Architenko Section</h3>
									</div>
									<!-- Join Architenko Start--->
									<div class="form-group row col-md-12">	
										<label for="profile_section_description" class="col-md-12">{{ __('Join Architenko Title') }}</label>	
										<input id="joinarchitenkotitle" type="text" class="form-control @error('joinarchitenkotitle') is-invalid @enderror" name="joinarchitenkotitle" value="{{ old('joinarchitenkotitle') }}" Placeholder="Join Architenko Title" required>
										@error('joinarchitenkotitle')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
									
									<div class="form-group row">
										<label for="joinarchitenko_section_description" class="col-md-4 col-form-label text-md-left">{{ __('Join Architenko Section Description') }}</label>					
										<div class="col-md-10">
											<textarea class="form-control" id="joinarchitenko_section_description" name="joinarchitenko_section_description"></textarea>
											@error('joinarchitenko_section_description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<!-- Join Architenko End--->
									<div class="col-md-12">
										<h3>Opened Positions Section </h3>
										<div class="form-group row col-md-12">	
											<label for="profile_section_description" class="col-md-12">{{ __('Opened Positions Title') }}</label>	
											<input id="openedpositionstitle" type="text" class="form-control @error('openedpositionstitle') is-invalid @enderror" name="openedpositionstitle" value="{{ old('openedpositionstitle') }}" Placeholder="Opened Positions Title" required>
											@error('openedpositionstitle')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
										<table class="table table-bordered" id="positionDynamicTable">	
											<tr>
												<th>Description</th>
												<th>Data</th>
												<th>Title</th>
												<th>Image Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>		
											<tr>  												 
												<td>
													<textarea class="form-control" id="position[0][description]" name="position[0][description]" required></textarea> 
												</td>	
												<td>
													<input type="file" name="position[0][image]" class="form-control" />
												</td>	
												<td>
													<input type="text" name="position[0][title]" class="form-control" placeholder="Enter Title" required>
												</td>
												<td>
													<input type="text" name="position[0][imagetitle]" class="form-control" placeholder="Enter Image Title" required>
												</td>
												<td>
													<input type="number" name="position[0][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
												<td>
													<button type="button" name="addPositionSection" id="addPositionSection" class="btn btn-success">Add More</button>
												</td>  
											</tr>  
										</table>
									</div>

									<!-- Award Section ---->
									<div class="form-group row col-md-12">
										<h3>Award Section</h3>
									</div>
									<div class="form-group row col-md-12">	
										<label for="awardstitle" class="col-md-12">{{ __('Award Title') }}</label>			
										<input id="awardstitle" type="text" class="form-control @error('awardstitle') is-invalid @enderror" name="awardstitle" value="{{ old('awardstitle') }}" Placeholder="Awards Title" required>
										@error('profiletitle')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
									
									<div class="form-group row">
										<label for="awards_section_description" class="col-md-4 col-form-label text-md-left">{{ __('Awards Section Description') }}</label>					
										<div class="col-md-10">
											<textarea class="form-control" id="awards_section_description" name="awards_section_description"></textarea>
											@error('awards_section_description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<!-- Award Section End---->
									<!-- Award Popup Section start -->
									<div class="col-md-12">
										<h5>Award Popup Page Section</h5>
										<table class="table table-bordered" id="awardPopupDynamicTable">	
											<tr>
												<th>Description</th>
												<th>Data</th>
												<th>Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>		
											<tr>  												 
												<td>
													<textarea class="form-control" id="awardrPopup[0][description]" name="awardrPopup[0][description]" required></textarea> 
												</td>	
												<td>
													<input type="file" name="awardrPopup[0][image]" class="form-control" />
												</td>	
												<td>
													<input type="text" name="awardrPopup[0][title]" class="form-control" placeholder="Enter Title" required>
												</td>
												<td>
													<input type="number" name="awardrPopup[0][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
												<td>
													<button type="button" name="addPopupAwardSection" id="addPopupAwardSection" class="btn btn-success">Add More</button>
												</td>  
											</tr>  
										</table>
									</div>
									<div class="col-md-12">
										<table class="table table-bordered" id="dynamicTable">
											<h5>Award Slider</h5>
											<tr>												
												<th>Data</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>
											<tr>  
												<td id="imageshow">
													<input type="file" name="addmore[0][image]" class="form-control" />
												</td>  												
												<td id="order">
													<input type="number" name="addmore[0][display_order]" class="form-control" min="0" placeholder="Enter order number" >
												</td>
												<td>
													<button type="button" name="add" id="add" class="btn btn-success">Add More</button>
												</td>  
											</tr>  
										</table>
									</div>
									<div class="col-md-12">
										<h3>Terms and Conditions Section</h3>
										<div class="form-group row col-md-12">	
											<label for="terms_and_conditions" class="col-md-12">{{ __('Terms and Conditions') }}</label>	
											<input id="termsandconditions" type="text" class="form-control @error('termsandconditions') is-invalid @enderror" name="termsandconditions" value="{{ old('termsandconditions') }}" Placeholder="Terms and Conditions" required>
											@error('termsandconditions')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
										<div id="tncSection">
											<table class="table table-bordered" id="tncDynamicTable0">
												<tr>
													<td colspan='3'>
														<input type="text" name="termandcondition[0][title]" placeholder="Enter Title" class="form-control" />
													</td>
													<td colspan='3'>
														<input type="number" name="termandcondition[0][orderby]" class="form-control" placeholder="Enter orderby" required>
													</td>
												</tr>	
												<tr>
													<th>Description</th>
													<th>Data</th>
													<th>Title</th>
													<th>Show in Pravicy Policy</th>
													<th>Display Order</th>
													<th>Action</th>
												</tr>		
												<tr>  												 
													<td>
														<textarea class="form-control" id="termandcondition[0][0][description]" name="termandcondition[0][0][description]" required></textarea> 
													</td>	
													<td>
														<input type="file" name="termandcondition[0][0][image]" class="form-control" />
													</td>	
													<td>
														<input type="text" name="termandcondition[0][0][title]" class="form-control" placeholder="Enter Title" required>
													</td>
													<td class="text-center">
														<input type="checkbox" name="termandcondition[0][0][show_in_privacy_policy]" value="1"  />
													</td>
													<td>
														<input type="number" name="termandcondition[0][0][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
													<td>
														<button type="button" data-id="0" class="btn btn-success addtncsubsection">Add More</button>
													</td>  
												</tr>  
											</table>
										</div>
									</div>
									<div class="col-md-12 form-group">
										<button type="button" name="addtncSection" id="addtncSection" class="btn btn-success pull-right">Add More Section</button>
									</div>
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="lang">
											<option value="en" selected>en</option>
											<option value="nl">nl</option>
										</select>
									</div>
									<!-- Award Popup Section End -->
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	CKEDITOR.editorConfig = function (config) {
	config.language = 'es';
	config.uiColor = '#F7B42C';
	config.height = 300;
	config.toolbarCanCollapse = true;	
	};
	CKEDITOR.replace('profile_section_description');
	CKEDITOR.replace('founder_section_description');
	CKEDITOR.replace('joinarchitenko_section_description');
	CKEDITOR.replace('awards_section_description');
	CKEDITOR.replace('profilPopup[0][description]');
	CKEDITOR.replace('founderPopup[0][description]');
	CKEDITOR.replace('awardrPopup[0][description]');
	CKEDITOR.replace('position[0][description]');
	CKEDITOR.replace('termandcondition[0][0][description]');
	jQuery("#main_image_type").click(function() {
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(".left-image").removeClass("hide");
			jQuery(".left-link").addClass("hide");
		}
		else{
			jQuery(".left-link").removeClass("hide"); 
			jQuery(".left-image").addClass("hide");
		}
	});
	jQuery("#profile_image_type").click(function() {
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(".profile_right_image").removeClass("hide");
			jQuery(".profile_right_link").addClass("hide");
		}
		else{
			jQuery(".profile_right_link").removeClass("hide"); 
			jQuery(".profile_right_image").addClass("hide");
		}
	});

	jQuery("#founder_image_type").click(function() {
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(".founder_right_image").removeClass("hide");
			jQuery(".founder_right_link").addClass("hide");
		}
		else{
			jQuery(".founder_right_link").removeClass("hide"); 
			jQuery(".founder_right_image").addClass("hide");
		}
	});

	var i = 0;
	jQuery("#add").click(function(){
		++i;
		jQuery("#dynamicTable").append('<tr><td id="imageshow'+i+'"><input type="file" name="addmore['+i+'][image]" class="form-control" /></td><td id="order'+i+'"><input type="number" name="addmore['+i+'][display_order]" class="form-control" min="0" required placeholder="Enter order number"></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');	
	});
	jQuery(document).on('click', '.remove-tr', function(){  
		$(this).parents('tr').remove();
	});  

	let innerPage=0;
	jQuery("#addPopupSection").click(function(){	
	++innerPage;	
		jQuery("#profilePopupDynamicTable").append(`<tr>  												 
					<td>
						<textarea class="form-control" id="profilPopup[${innerPage}][description]" name="profilPopup[${innerPage}][description]" required></textarea> 
												</td>
					<td>
						<input type="file" name="profilPopup[${innerPage}][image]" class="form-control" required/>
					</td>	
					<td>
						<input type="text" name="profilPopup[${innerPage}][title]" class="form-control" required placeholder="Enter Title" required>
					</td>
					<td>
						<input type="number" name="profilPopup[${innerPage}][display_order]" class="form-control" min="0" placeholder="Enter order number" required>
					</td>
					<td>
						<button type="button" class="btn btn btn-danger removePopupSection">Remove</button>
					</td>  
				</tr>`);
		let description = 'profilPopup['+innerPage+'][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.removePopupSection', function(){  
		$(this).parents('tr').remove();
	}); 

	let founderPopup=0;
	$("#addPopupFounderSection").click(function(){	
	++founderPopup;	
		jQuery("#founderPopupDynamicTable").append(`<tr>  												 
					<td>
						<textarea class="form-control" id="founderPopup[${founderPopup}][description]" name="founderPopup[${founderPopup}][description]" required></textarea> 
												</td>
					<td>
						<input type="file" name="founderPopup[${founderPopup}][image]" class="form-control" required/>
					</td>	
					<td>
						<input type="text" name="founderPopup[${founderPopup}][title]" class="form-control" required placeholder="Enter Title" required>
					</td>
					<td>
						<input type="number" name="founderPopup[${founderPopup}][display_order]" class="form-control" min="0" placeholder="Enter order number" required>
					</td>
					<td>
						<button type="button" class="btn btn btn-danger removeFounderPopupSection">Remove</button>
					</td>  
				</tr>`);
		let description = 'founderPopup['+founderPopup+'][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.removeFounderPopupSection', function(){  
		$(this).parents('tr').remove();
	}); 


	let awardPopup=0;
	$("#addPopupAwardSection").click(function(){	
	++awardPopup;	
		jQuery("#awardPopupDynamicTable").append(`<tr>  												 
					<td>
						<textarea class="form-control" id="awardrPopup[${awardPopup}][description]" name="awardrPopup[${awardPopup}][description]" required></textarea> 
												</td>
					<td>
						<input type="file" name="awardrPopup[${awardPopup}][image]" class="form-control" required/>
					</td>	
					<td>
						<input type="text" name="awardrPopup[${awardPopup}][title]" class="form-control" required placeholder="Enter Title" required>
					</td>
					<td>
						<input type="number" name="awardrPopup[${awardPopup}][display_order]" class="form-control" min="0" placeholder="Enter order number" required>
					</td>
					<td>
						<button type="button" class="btn btn btn-danger removeAwardPopupSection">Remove</button>
					</td>  
				</tr>`);
		let description = 'awardrPopup['+awardPopup+'][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.removeAwardPopupSection', function(){  
		$(this).parents('tr').remove();
	}); 

	let position=0;
	$("#addPositionSection").click(function(){	
	++position;	
		jQuery("#positionDynamicTable").append(`<tr>  												 
					<td>
						<textarea class="form-control" id="position[${position}][description]" name="position[${position}][description]" required></textarea> 
												</td>
					<td>
						<input type="file" name="position[${position}][image]" class="form-control" required/>
					</td>	
					<td>
						<input type="text" name="position[${position}][title]" class="form-control" required placeholder="Enter Title" required>
					</td>
					<td>
						<input type="text" name="position[${position}][imagetitle]" class="form-control" required placeholder="Enter Image Title" required>
					</td>
					<td>
						<input type="number" name="position[${position}][display_order]" class="form-control" min="0" placeholder="Enter order number" required>
					</td>
					<td>
						<button type="button" class="btn btn btn-danger removePositionSection">Remove</button>
					</td>  
				</tr>`);
		let description = 'position['+position+'][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.removePositionSection', function(){  
		$(this).parents('tr').remove();
	}); 

	let tncsection = 0;
	$('#addtncSection').click(function(){
		++tncsection;
		$('#tncSection').append(`<table class="table table-bordered" id="tncDynamicTable${tncsection}">
						<tr>
							<td colspan='3'>
								<input type="text" name="termandcondition[${tncsection}][title]" class="form-control" placeholder="Enter Title" required>
							</td>
							<td colspan='3'>
								<input type="number" name="termandcondition[${tncsection}][orderby]" class="form-control" placeholder="Enter orderby" required>
							</td>
						</tr>	
						<tr>
							<th>Description</th>
							<th>Data</th>
							<th>Title</th>
							<th>Show In Privacy Policy</th>
							<th>Display Order</th>
							<th>Action</th>
						</tr>		
						<tr>  												 
							<td>
								<textarea class="form-control" id="termandcondition[${tncsection}][0][description]" name="termandcondition[${tncsection}][0][description]" required></textarea> 
							</td>	
							<td>
								<input type="file" name="termandcondition[${tncsection}][0][image]" class="form-control" />
							</td>	
							<td>
								<input type="text" name="termandcondition[${tncsection}][0][title]" class="form-control" placeholder="Enter Title" required>
							</td>
							<td class="text-center">
								<input type="checkbox" name="termandcondition[${tncsection}][0][show_in_privacy_policy]" value="1"  />
							</td>
							<td>
								<input type="number" name="termandcondition[${tncsection}][0][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
							<td>
								<button type="button" data-id="${tncsection}" name="termandcondition${tncsection}" class="btn btn-success addtncsubsection">Add More</button>
							</td>  
						</tr> 
						<tr class="rmtnc">
							<td colspan='6'><button type="button" name="removetncsection"  class="btn btn-danger pull-right removetncsection">Remove Section</button></td>
						<tr> 
					</table>`);
			let description = 'termandcondition['+tncsection+'][0][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.removetncsection', function(){  
		$(this).parents('table').remove();
	}); 
	let tncsubsection = 0;
	$(document).on('click','.addtncsubsection',function(){
		++tncsubsection;
		let id = $(this).data('id');
		console.log($('#tncDynamicTable'+id+' .rmtnc'));
		if($('#tncDynamicTable'+id+' .rmtnc').length == 1){
			$('#tncDynamicTable'+id+' .rmtnc').before(`<tr>  												 
											<td>
												<textarea class="form-control" id="termandcondition[${id}][${tncsubsection}][description]" name="termandcondition[${tncsection}][${tncsubsection}][description]" required></textarea> 
											</td>	
											<td>
												<input type="file" name="termandcondition[${id}][${tncsubsection}][image]" class="form-control" />
											</td>	
											<td>
												<input type="text" name="termandcondition[${id}][${tncsubsection}][title]" class="form-control" placeholder="Enter Title" required>
											</td>
												<td class="text-center">
								<input type="checkbox" name="termandcondition[${id}][${tncsubsection}][show_in_privacy_policy]" value="1"  />
							</td>
											<td>
												<input type="number" name="termandcondition[${id}][${tncsubsection}][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
											<td>
												<button type="button" data-id="${id}" name="termandcondition${id}" class="btn btn-danger removetncsubsection">Remove</button>
											</td>  
										</tr>`);
		}
		else{

			$('#tncDynamicTable'+id).append(`<tr>  												 
											<td>
												<textarea class="form-control" id="termandcondition[${id}][${tncsubsection}][description]" name="termandcondition[${id}][${tncsubsection}][description]" required></textarea> 
											</td>	
											<td>
												<input type="file" name="termandcondition[${id}][${tncsubsection}][image]" class="form-control" />
											</td>	
											<td>
												<input type="text" name="termandcondition[${id}][${tncsubsection}][title]" class="form-control" placeholder="Enter Title" required>
											</td>
											<td class="text-center">
												<input type="checkbox" name="termandcondition[${id}][${tncsubsection}][show_in_privacy_policy]" value="1"  />
											</td>
											<td>
												<input type="number" name="termandcondition[${id}][${tncsubsection}][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
											<td>
												<button type="button" data-id="${id}" name="termandcondition${id}" class="btn btn-danger removetncsubsection">Remove</button>
											</td>  
										</tr>`);
		}
		let description = 'termandcondition['+id+']['+tncsubsection+'][description]';
		CKEDITOR.replace(description);
	});
	$(document).on('click','.removetncsubsection',function(){
		$(this).parents('tr').remove();
	});
</script>
@endsection								
