@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('studio/update') }}" enctype="multipart/form-data">
									@csrf	
									<input type="hidden" name="studio_id" value="{{ $studio->id }}">
									<div class="form-group row col-md-12">		
										<label for="profile_section_description" class="">{{ __('Top Image') }}</label>	
										<select id="main_image_type" name="main_image_type" class="form-control">
											<option value="image" @if($studio->title_img_type == 'image') selected @endif>Image/Gif</option>
											<option value="youtube" @if($studio->title_img_type == 'youtube') selected @endif>Youtube</option>
											<option value="video" @if($studio->title_img_type == 'video') selected @endif>Video upload</option>
											<option value="book" @if($studio->title_img_type == 'book') selected @endif>Book Images</option>
											<option value="flip" @if($studio->title_img_type == 'flip') selected @endif>Flip Images</option>
										</select>
									</div>
									<div class=" row left-image @if($studio->title_img_type == 'youtube') hide @endif">
										<div class="col-md-10 ">
											<input type="file" name="img_left[]"  multiple  class="form-control imageitem" />
										</div>				
									</div>	
									<div class="form-group row left-link @if($studio->title_img_type != 'youtube') hide @endif">	<div class="col-md-10">
									<input type="text" name="img_left" value="@if($studio->title_img_type == 'youtube') {{$studio->title_img}} @endif" placeholder="Please enter youtube link" class="form-control"	/>
									</div>
									</div>
									<?php 
										$img = '';
										if($studio->title_img_type == 'image' || $studio->title_img_type == 'video'){
											$img = URL('images/studio/',$studio->title_img);
										}
										else if ($studio->title_img_type == 'youtube') {
											$img = $studio->title_img;
										}
										else if ($studio->title_img_type == 'book' || $studio->title_img_type == 'flip') {
											$temp_top = json_decode($studio->title_img);
											$t_array1 = array();
											foreach ($temp_top as $value) {
												$t_array1[] = URL('images/studio/',$value);
											}
											$img = json_encode($t_array1);
										}
									?>
									<?=getimagehtmlbytype($studio->title_img_type,$img)?>

									<div class="form-group row col-md-12">
										<h3>Profile Section</h3>
									</div>
									<!-- Profile Section start -->
									<div class="form-group row col-md-12">	
										<label for="profile_section_description" class="col-md-12">{{ __('Profile Title') }}</label>
										<input id="profiletitle" type="text" class="form-control @error('profiletitle') is-invalid @enderror" name="profiletitle" value="{{ $studio->profile_title }}" Placeholder="Profile Title" required>
										@error('profiletitle')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
									
									<div class="form-group row">
										<label for="profile_section_description" class="col-md-4 col-form-label text-md-left">{{ __('Profile Section Description') }}</label>					
										<div class="col-md-10">
											<textarea class="form-control" id="profile_section_description" name="profile_section_description">{{ $studio->profile_description }}</textarea>
											@error('profile_section_description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<div class="form-group row col-md-12">	
										<label for="profile_section_description" class="row col-md-4 col-form-label text-md-left">{{ __('Profile Right Side Image') }}</label>	 
										<select id="profile_image_type" name="profile_image_type" class="form-control">
											<option value="image" @if($studio->profile_right_img_type == 'image') selected @endif>Image/Gif</option>
											<option value="youtube" @if($studio->profile_right_img_type == 'youtube') selected @endif>Youtube</option>
											<option value="video" @if($studio->profile_right_img_type == 'video') selected @endif>Video upload</option>
											<option value="book" @if($studio->profile_right_img_type == 'book') selected @endif>Book Images</option>
											<option value="flip" @if($studio->profile_right_img_type == 'flip') selected @endif>Flip Images</option>
										</select>
									</div>
									<div class=" row profile_right_image @if($studio->profile_right_img_type == 'youtube') hide @endif">
										<div class="col-md-10 ">
											<input type="file" name="profile_img_left[]"  multiple  class="form-control imageitem" />
										</div>				
									</div>	
									<div class="form-group row profile_right_link @if($studio->profile_right_img_type != 'youtube') hide @endif">	<div class="col-md-10">
									<input type="text" name="profile_img_left" value="@if($studio->profile_right_img_type == 'youtube') {{$studio->profile_right_img}}  @endif" placeholder="Please enter youtube link" class="form-control"	/>
									</div>
									</div>
									
									<?php 
										$img = '';
										if($studio->profile_right_img_type == 'image' || $studio->profile_right_img_type == 'video'){
											$img = URL('images/studio/',$studio->profile_right_img);
										}
										else if ($studio->profile_right_img_type == 'youtube') {
											$img = $studio->profile_right_img;
										}
										else if ($studio->profile_right_img_type == 'book' || $studio->profile_right_img_type == 'flip') {
											$temp_top = json_decode($studio->profile_right_img);
											$t_array1 = array();
											foreach ($temp_top as $value) {
												$t_array1[] = URL('images/studio/',$value);
											}
											$img = json_encode($t_array1);
										}
									?>
									<?=getimagehtmlbytype($studio->profile_right_img_type,$img)?>
									<!-- Profile Section End -->

									<!-- Profile Popup Section start -->
									<div class="col-md-12">
										<h5>Profile Popup Page Section</h5>
										<table class="table table-bordered" id="profilePopupDynamicTable">	
											<tr>
												<th>Description</th>
												<th>Data</th>
												<th>Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>		
											<?php $profileIndex = 0; ?>
											@foreach($studio_details as $studio_detail)
												@if($studio_detail->section_type == 'profilepopup')
													<input type="hidden" name="profilepopupid[]" value="{{ $studio_detail->id }}">
													<tr>  												 
														<td>
															<input type="hidden" name="profilPopup[{{ $profileIndex }}][id]" value="{{ $studio_detail->id }}">
															<textarea class="form-control" id="profilPopup[{{ $profileIndex }}][description]" name="profilPopup[{{ $profileIndex }}][description]" required> {{$studio_detail->description}} </textarea> 
														</td>	
														<td>
															<input type="file" name="profilPopup[{{ $profileIndex }}][image]" class="form-control" />
															<?=getimagehtmlbytype('image',URL('images/studio/',$studio_detail->image))?>
														</td>	
														<td>
															<input type="text" name="profilPopup[{{ $profileIndex }}][title]" class="form-control" placeholder="Enter Title" value="{{$studio_detail->title}}" required>
														</td>
														<td>
															<input type="number" name="profilPopup[{{ $profileIndex }}][display_order]" class="form-control" min="0" placeholder="Enter order number" value="{{$studio_detail->display_order}}" required></td>
														<td>
															@if($profileIndex == 0)
																<button type="button" name="addPopupSection" id="addPopupSection" class="btn btn-success">Add More</button>
															@else
																<button type="button" class="btn btn btn-danger removePopupSection">Remove</button>
															@endif
														</td>  
													</tr> 
													<?php $profileIndex++; ?>
												@endif	
											@endforeach
										</table>
									</div>
									<!-- Profile Popup Section End -->
									<div class="form-group row col-md-12">
										<h3>Founder Section</h3>
									</div>
									<!--- Founder Section Start----->
									<div class="form-group row col-md-12">	
										<label for="profile_section_description" class="col-md-12">{{ __('Founder Title') }}</label>	
										<input id="foundertitle" type="text" class="form-control @error('foundertitle') is-invalid @enderror" name="foundertitle" value="{{ $studio->founder_title }}" Placeholder="Founder Title" required>
										@error('foundertitle')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
									<div class="form-group row col-md-12">	
										<label for="founder_section_description" class="row col-md-4 col-form-label text-md-left">{{ __('Founder Left Side Image') }}</label>	 
										<select id="founder_image_type" name="founder_image_type" class="form-control">
											<option value="image" @if($studio->founder_right_img_type == 'image') selected @endif>Image/Gif</option>
											<option value="youtube" @if($studio->founder_right_img_type == 'youtube') selected @endif>Youtube</option>
											<option value="video" @if($studio->founder_right_img_type == 'video') selected @endif>Video upload</option>
											<option value="book" @if($studio->founder_right_img_type == 'book') selected @endif>Book Images</option>
											<option value="flip" @if($studio->founder_right_img_type == 'flip') selected @endif>Flip Images</option>
										</select>
									</div>
									<div class=" row founder_right_image @if($studio->founder_right_img_type == 'youtube') hide @endif">
										<div class="col-md-10 ">
											<input type="file" name="founder_img_left[]"  multiple  class="form-control imageitem" />
										</div>				
									</div>	
									<div class="form-group row founder_right_link @if($studio->founder_right_img_type != 'youtube') hide @endif">	<div class="col-md-10">
										<input type="text" name="founder_img_left" value="@if($studio->founder_right_img_type == 'youtube') {{ $studio->founder_right_img }} @endif" placeholder="Please enter youtube link" class="form-control"	/>
									</div>
									</div>
									
									<?php 
										$img = '';
										if($studio->founder_right_img_type == 'image' || $studio->founder_right_img_type  == 'video'){
											$img = URL('images/studio/',$studio->founder_right_img);
										}
										else if ($studio->founder_right_img_type == 'youtube') {
											$img = $studio->founder_right_img;
										}
										else if ($studio->founder_right_img_type == 'book' || $studio->founder_right_img_type == 'flip') {
											$temp_top = json_decode($studio->founder_right_img);
											$t_array1 = array();
											foreach ($temp_top as $value) {
												$t_array1[] = URL('images/studio/',$value);
											}
											$img = json_encode($t_array1);
										}
									?>
									<?=getimagehtmlbytype($studio->founder_right_img_type,$img)?>
									<div style="margin-top: 20px"></div>
									<div class="form-group row" >
										<label for="profile_section_description" class="found-desc-title col-md-4 col-form-label text-md-left">{{ __('Founder Section Description') }}</label>					
										<div class="col-md-10">
											<textarea class="form-control" id="founder_section_description" name="founder_section_description">  {{ $studio->founder_description }}</textarea>
											@error('founder_section_description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<!--- Founder Section End----->
									<!-- Founder Popup Section start -->
									<div class="col-md-12">
										<h5>Founder Popup Page Section</h5>
										<table class="table table-bordered" id="founderPopupDynamicTable">	
											<tr>
												<th>Description</th>
												<th>Data</th>
												<th>Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>	
											<?php $founderIndex = 0; ?>
											@foreach($studio_details as $studio_detail)
												@if($studio_detail->section_type == 'founderpopup')	
													<tr>  												 
														<td>
															<input type="hidden" name="founderPopup[{{$founderIndex}}][id]" value="{{ $studio_detail->id }}">
															<textarea class="form-control" id="founderPopup[{{$founderIndex}}][description]" name="founderPopup[{{$founderIndex}}][description]" required>{{$studio_detail->description}}</textarea> 
														</td>	
														<td>
															<input type="file" name="founderPopup[{{$founderIndex}}][image]" class="form-control" />
															<?=getimagehtmlbytype('image',URL('images/studio/',$studio_detail->image))?>
														</td>	
														<td>
															<input type="text" name="founderPopup[{{$founderIndex}}][title]" value="{{$studio_detail->title}}" class="form-control" placeholder="Enter Title" required>
														</td>
														<td>
															<input type="number" name="founderPopup[{{$founderIndex}}][display_order]" value="{{$studio_detail->display_order}}" class="form-control" min="0" placeholder="Enter order number" required></td>
														<td>
															@if($founderIndex == 0)
																<button type="button" name="addPopupFounderSection"  id="addPopupFounderSection" class="btn btn-success">Add More</button>
															@else
																<button type="button" class="btn btn btn-danger removeFounderPopupSection">Remove</button>
															@endif
														</td>  
													</tr> 
													<?php $founderIndex++; ?>
												@endif	
											@endforeach 
										</table>
									</div>
									<!-- Founder Popup Section End -->
									<div class="form-group row col-md-12">
										<h3>Join Architenko Section</h3>
									</div>
									<!-- Join Architenko Start--->
									<div class="form-group row col-md-12">	
										<label for="profile_section_description" class="col-md-12">{{ __('Join Architenko Title') }}</label>	
										<input id="joinarchitenkotitle" type="text" class="form-control @error('joinarchitenkotitle') is-invalid @enderror" name="joinarchitenkotitle" value="{{ $studio->join_architenko_title }}" Placeholder="Join Architenko Title" required>
										@error('joinarchitenkotitle')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
									
									<div class="form-group row">
										<label for="joinarchitenko_section_description" class="col-md-4 col-form-label text-md-left">{{ __('Join Architenko Section Description') }}</label>					
										<div class="col-md-10">
											<textarea class="form-control" id="joinarchitenko_section_description" name="joinarchitenko_section_description">{{ $studio->join_architenko_description }}</textarea>
											@error('joinarchitenko_section_description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<!-- Join Architenko End--->
									
									<div class="form-group row col-md-12">
										<h3>Opened Positions Section </h3>
										<div class="form-group row col-md-12">	
											<label for="profile_section_description" class="col-md-12">{{ __('Opened Positions Title') }}</label>	
											<input id="openedpositionstitle" type="text" class="form-control @error('openedpositionstitle') is-invalid @enderror" name="openedpositionstitle" value="{{ $studio->opened_positions_title }}" Placeholder="Opened Positions Title" required>
											@error('openedpositionstitle')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
										<table class="table table-bordered" id="positionDynamicTable">	
											<tr>
												<th>Description</th>
												<th>Data</th>
												<th>Title</th>
												<th>Image Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>	
											<?php $positionIndex = 0; ?>
											@foreach($studio_position as $positionValue)	
												<tr>  												 
													<td>
														<input type="hidden" name="position[{{ $positionIndex }}][id]" value="{{ $positionValue->id }}"/>
														<textarea class="form-control" id="position[{{ $positionIndex }}][description]" name="position[{{ $positionIndex }}][description]" required> {{ $positionValue->description }} </textarea> 
													</td>	
													<td>
														<input type="file" name="position[{{ $positionIndex }}][image]" class="form-control" />
														<?=getimagehtmlbytype('image',URL('images/studio/',$positionValue->image))?>
													</td>	
													<td>
														<input type="text" name="position[{{ $positionIndex }}][title]" value="{{ $positionValue->title }}" class="form-control" placeholder="Enter Title" required>
													</td>
													<td>
														<input type="text" name="position[{{ $positionIndex }}][imagetitle]" value="{{ $positionValue->image_title }}" class="form-control" placeholder="Enter Image Title" required>
													</td>
													<td>
														<input type="number" name="position[{{ $positionIndex }}][display_order]" value="{{ $positionValue->display_order }}" class="form-control" min="0" placeholder="Enter order number" required></td>
													<td>
														@if($positionIndex == 0)
															<button type="button" name="addPositionSection" id="addPositionSection" class="btn btn-success">Add More</button>
														@else
															<button type="button" class="btn btn btn-danger removePositionSection">Remove</button>
														@endif
													</td>  
													<?php $positionIndex++; ?>
												</tr>  
											@endforeach
										</table>
									</div>
									
									<div class="form-group row col-md-12">
										<h3>Award Section</h3>
									</div>
									<!-- Award Section ---->
									<div class="form-group row col-md-12">	
											<label for="awardstitle" class="col-md-12">{{ __('Award Title') }}</label>	
										<input id="awardstitle" type="text" class="form-control @error('awardstitle') is-invalid @enderror" name="awardstitle" value="{{ $studio->award_title }}" Placeholder="Awards Title" required>
										@error('profiletitle')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
									
									<div class="form-group row">
										<label for="awards_section_description" class="col-md-4 col-form-label text-md-left">{{ __('Awards Section Description') }}</label>					
										<div class="col-md-10">
											<textarea class="form-control" id="awards_section_description" name="awards_section_description">{{ $studio->award_description }}</textarea>
											@error('awards_section_description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<div class="col-md-12">
										<table class="table table-bordered" id="dynamicTable">
											<h5>Award Slider</h5>
											<tr>												
												<th>Data</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>
											<?php $awardsliders = json_decode($studio->award_slider); $sliderIndex =0; ?>
											@foreach($awardsliders as $slider)
											<tr>  
												<td id="imageshow">
													<input type="file" name="addmore[{{ $sliderIndex }}][image]" class="form-control"  />
													<input type="hidden" name="addmore[{{ $sliderIndex }}][exist_img]" value="{{ $slider->image }}" class="form-control"  />
													<?=getimagehtmlbytype('image',URL('images/studio/',$slider->image))?>
												</td>  												
												<td id="order">
													<input type="number" name="addmore[{{ $sliderIndex }}][display_order]" class="form-control" value="{{ $slider->display_order }}" min="0" placeholder="Enter order number" >
												</td>
												<td>
													@if($sliderIndex == 0)
														<button type="button" name="add" id="add" class="btn btn-success">Add More</button>
													@else
														<button type="button" class="btn btn-danger remove-tr">Remove</button>
													@endif
												</td>  
											</tr> 
											<?php $sliderIndex++; ?>
											@endforeach 
										</table>
									</div>
									<!-- Award Section End---->
									<!-- Award Popup Section start -->
									<div class="col-md-12">
										<h5>Award Popup Page Section</h5>
										<table class="table table-bordered" id="awardPopupDynamicTable">	
											<tr>
												<th>Description</th>
												<th>Data</th>
												<th>Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>	
											<?php $awardIndex=0; ?>
											@foreach($studio_details as $awardpopup)	
												@if($awardpopup->section_type == 'awardpopup')
													<tr>  												 
														<td>
															<textarea class="form-control" id="awardrPopup[{{ $awardIndex }}][description]" name="awardrPopup[{{ $awardIndex }}][description]" required> {{ $awardpopup->description }} </textarea> 
															<input type="hidden" name="awardrPopup[{{ $awardIndex }}][id]" value="{{ $awardpopup->id }}">
														</td>	
														<td>
															<input type="file" name="awardrPopup[{{ $awardIndex }}][image]" class="form-control" />
															<?=getimagehtmlbytype('image',URL('images/studio/',$awardpopup->image))?>
														</td>	
														<td>
															<input type="text" name="awardrPopup[{{ $awardIndex }}][title]" value="{{ $awardpopup->title }}" class="form-control" placeholder="Enter Title" required>
														</td>
														<td>
															<input type="number" name="awardrPopup[{{ $awardIndex }}][display_order]" value="{{ $awardpopup->display_order }}" class="form-control" min="0" placeholder="Enter order number" required></td>
														<td>
															@if($awardIndex == 0)
																<button type="button" name="addPopupAwardSection" id="addPopupAwardSection" class="btn btn-success">Add More</button>
															@else
																<button type="button" class="btn btn btn-danger removeAwardPopupSection">Remove</button>
															@endif
														</td>  
													</tr> 
													<?php $awardIndex++; ?> 
												@endif
											@endforeach
										</table>
									</div>
									
									<div class="col-md-12">
										<h3>Terms and Conditions Section</h3>
										<div class="form-group row col-md-12">	
											<label for="terms_and_conditions" class="col-md-12">{{ __('Terms and Conditions') }}</label>	
											<input id="termsandconditions" type="text" class="form-control @error('termsandconditions') is-invalid @enderror" name="termsandconditions" value="{{ $studio->terms_and_conditions }}" Placeholder="Terms and Conditions" required>
											@error('termsandconditions')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
										<div id="tncSection">
											<?php $tncIndex = 0; $tnc_dataIndex = 0; ?>
											@foreach($studio_tnc as $tnc)
											<table class="table table-bordered" id="tncDynamicTable{{ $tncIndex }}">
												<tr>
													<td colspan='3'>
														<input type="hidden" name="termandcondition[{{ $tncIndex }}][id]" value="{{ $tnc->id }}">
														<input type="text" name="termandcondition[{{ $tncIndex }}][title]" placeholder="Enter Title" value="{{ $tnc->title }}" class="form-control" />
													</td>
													<td colspan='3'>
														<input type="number" name="termandcondition[{{ $tncIndex }}][orderby]" class="form-control" value="{{ $tnc->orderby }}"  placeholder="Enter orderby" required>
													</td>
												</tr>	
												<tr>
													<th>Description</th>
													<th>Data</th>
													<th>Title</th>
													<th>Show In Privacy Policy</th>
													<th>Display Order</th>
													<th>Action</th>
												</tr>
												<?php $isFirstData = 0; ?>
												@foreach($studio_tnc_details as $tnc_data)
													@if($tnc_data->tnc_id == $tnc->id)		
													<tr>  												 
														<td>
															<input type="hidden" name="termandcondition[{{ $tncIndex }}][{{ $tnc_dataIndex }}][id]" value="{{ $tnc_data->id }}">
															<textarea class="form-control" id="termandcondition[{{ $tncIndex }}][{{ $tnc_dataIndex }}][description]" name="termandcondition[{{ $tncIndex }}][{{ $tnc_dataIndex }}][description]" required>{{ $tnc_data->description }}</textarea> 
														</td>	
														<td>
															<input type="file" name="termandcondition[{{ $tncIndex }}][{{ $tnc_dataIndex }}][image]" class="form-control" />
															<?php if($tnc_data->image != ''){ ?>
															<?=getimagehtmlbytype('image',URL('images/studio/',$tnc_data->image))?>
															<?php } ?>
														</td>	
														<td>
															<input type="text" name="termandcondition[{{ $tncIndex }}][{{ $tnc_dataIndex }}][title]" class="form-control" value="{{ $tnc_data->title }}" placeholder="Enter Title" required>
														</td>
														<td class="text-center">
															<input type="checkbox" value="1" name="termandcondition[{{ $tncIndex }}][{{ $tnc_dataIndex }}][show_in_privacy_policy]" class="" @if($tnc_data->show_in_privacy_policy == 1) checked @endif />		
														</td>
														<td>
															<input type="number" name="termandcondition[{{ $tncIndex }}][{{ $tnc_dataIndex }}][display_order]" class="form-control" value="{{ $tnc_data->display_order }}" min="0" placeholder="Enter order number" required></td>
														<td>
															@if($isFirstData == 0)
																<button type="button" data-id="{{ $tncIndex }}" class="btn btn-success addtncsubsection">Add More</button>
															@else
																<button type="button" class="btn btn-danger removetncsubsection">Remove</button>
															@endif
														</td>  
													</tr>
													<?php $tnc_dataIndex++; $isFirstData++; ?>
													@endif 
												@endforeach
												@if($tncIndex != 0) 
												<tr class="rmtnc">
													<td colspan='6'><button type="button" name="removetncsection"  class="btn btn-danger pull-right removetncsection">Remove Section</button></td>
												<tr>
												@endif
											</table>
											<?php $tncIndex++; ?>
											@endforeach
										</div>
									</div>
									<div class="col-md-12 form-group">
										<button type="button" name="addtncSection" id="addtncSection" class="btn btn-success pull-right">Add More Section</button>
									</div>
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="lang">
											<option value="en" selected>en</option>
											<option value="nl">nl</option>
										</select>
									</div>
									<!-- Award Popup Section End -->
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	CKEDITOR.editorConfig = function (config) {
	config.language = 'es';
	config.uiColor = '#F7B42C';
	config.height = 300;
	config.toolbarCanCollapse = true;	
	};
	CKEDITOR.replace('profile_section_description');
	CKEDITOR.replace('founder_section_description');
	CKEDITOR.replace('joinarchitenko_section_description');
	CKEDITOR.replace('awards_section_description');
	// CKEDITOR.replace('termandcondition[0][0][description]');
	jQuery("#main_image_type").click(function() {
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(".left-image").removeClass("hide");
			jQuery(".left-link").addClass("hide");
		}
		else{
			jQuery(".left-link").removeClass("hide"); 
			jQuery(".left-image").addClass("hide");
		}
	});
	jQuery("#profile_image_type").click(function() {
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(".profile_right_image").removeClass("hide");
			jQuery(".profile_right_link").addClass("hide");
		}
		else{
			jQuery(".profile_right_link").removeClass("hide"); 
			jQuery(".profile_right_image").addClass("hide");
		}
	});

	jQuery("#founder_image_type").click(function() {
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(".founder_right_image").removeClass("hide");
			jQuery(".founder_right_link").addClass("hide");
		}
		else{
			jQuery(".founder_right_link").removeClass("hide"); 
			jQuery(".founder_right_image").addClass("hide");
		}
	});

	var i = "{{ $sliderIndex }}";
	jQuery("#add").click(function(){
		++i;
		jQuery("#dynamicTable").append('<tr><td id="imageshow'+i+'"><input type="file" name="addmore['+i+'][image]" class="form-control" /></td><td id="order'+i+'"><input type="number" name="addmore['+i+'][display_order]" class="form-control" min="0" required placeholder="Enter order number"></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');	
	});
	jQuery(document).on('click', '.remove-tr', function(){  
		$(this).parents('tr').remove();
	});  

	let innerPage="{{ $profileIndex }}";
	@for ($i = 0; $i < $profileIndex; $i++)
	    CKEDITOR.replace('profilPopup[{{$i}}][description]');
	@endfor
	jQuery("#addPopupSection").click(function(){	
	++innerPage;	
		jQuery("#profilePopupDynamicTable").append(`<tr>  												 
					<td>
						<textarea class="form-control" id="profilPopup[${innerPage}][description]" name="profilPopup[${innerPage}][description]" required></textarea> 
												</td>
					<td>
						<input type="file" name="profilPopup[${innerPage}][image]" class="form-control" required/>
					</td>	
					<td>
						<input type="text" name="profilPopup[${innerPage}][title]" class="form-control" required placeholder="Enter Title" required>
					</td>
					<td>
						<input type="number" name="profilPopup[${innerPage}][display_order]" class="form-control" min="0" placeholder="Enter order number" required>
					</td>
					<td>
						<button type="button" class="btn btn btn-danger removePopupSection">Remove</button>
					</td>  
				</tr>`);
		let description = 'profilPopup['+innerPage+'][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.removePopupSection', function(){  
		$(this).parents('tr').remove();
	}); 

	let founderPopup='{{ $founderIndex }}';
	@for ($i = 0; $i < $founderIndex; $i++)
	    CKEDITOR.replace('founderPopup[{{$i}}][description]');
	@endfor
	$("#addPopupFounderSection").click(function(){	
	++founderPopup;	
		jQuery("#founderPopupDynamicTable").append(`<tr>  												 
					<td>
						<textarea class="form-control" id="founderPopup[${founderPopup}][description]" name="founderPopup[${founderPopup}][description]" required></textarea> 
												</td>
					<td>
						<input type="file" name="founderPopup[${founderPopup}][image]" class="form-control" required/>
					</td>	
					<td>
						<input type="text" name="founderPopup[${founderPopup}][title]" class="form-control" required placeholder="Enter Title" required>
					</td>
					<td>
						<input type="number" name="founderPopup[${founderPopup}][display_order]" class="form-control" min="0" placeholder="Enter order number" required>
					</td>
					<td>
						<button type="button" class="btn btn btn-danger removeFounderPopupSection">Remove</button>
					</td>  
				</tr>`);
		let description = 'founderPopup['+founderPopup+'][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.removeFounderPopupSection', function(){  
		$(this).parents('tr').remove();
	}); 


	let awardPopup='{{ $awardIndex }}';

	@for ($i = 0; $i < $awardIndex; $i++)
	    CKEDITOR.replace('awardrPopup[{{$i}}][description]');
	@endfor

	$("#addPopupAwardSection").click(function(){	
	++awardPopup;	
		jQuery("#awardPopupDynamicTable").append(`<tr>  												 
					<td>
						<textarea class="form-control" id="awardrPopup[${awardPopup}][description]" name="awardrPopup[${awardPopup}][description]" required></textarea> 
												</td>
					<td>
						<input type="file" name="awardrPopup[${awardPopup}][image]" class="form-control" required/>
					</td>	
					<td>
						<input type="text" name="awardrPopup[${awardPopup}][title]" class="form-control" required placeholder="Enter Title" required>
					</td>
					<td>
						<input type="number" name="awardrPopup[${awardPopup}][display_order]" class="form-control" min="0" placeholder="Enter order number" required>
					</td>
					<td>
						<button type="button" class="btn btn btn-danger removeAwardPopupSection">Remove</button>
					</td>  
				</tr>`);
		let description = 'awardrPopup['+awardPopup+'][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.removeAwardPopupSection', function(){  
		$(this).parents('tr').remove();
	}); 

	let position= '{{ $positionIndex }}';

	@for ($i = 0; $i < $positionIndex; $i++)
	    CKEDITOR.replace('position[{{$i}}][description]');
	@endfor

	$("#addPositionSection").click(function(){	
	++position;	
		jQuery("#positionDynamicTable").append(`<tr>  												 
					<td>
						<textarea class="form-control" id="position[${position}][description]" name="position[${position}][description]" required></textarea> 
												</td>
					<td>
						<input type="file" name="position[${position}][image]" class="form-control" required/>
					</td>	
					<td>
						<input type="text" name="position[${position}][title]" class="form-control" required placeholder="Enter Title" required>
					</td>
					<td>
						<input type="text" name="position[${position}][imagetitle]" class="form-control" required placeholder="Enter Image Title" required>
					</td>
					<td>
						<input type="number" name="position[${position}][display_order]" class="form-control" min="0" placeholder="Enter order number" required>
					</td>
					<td>
						<button type="button" class="btn btn btn-danger removePositionSection">Remove</button>
					</td>  
				</tr>`);
		let description = 'position['+position+'][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.removePositionSection', function(){  
		$(this).parents('tr').remove();
	}); 
	<?php $tncIndex = 0; $tnc_dataIndex = 0; ?>
	@foreach($studio_tnc as $tnc)
		@foreach($studio_tnc_details as $tnc_data)
			@if($tnc_data->tnc_id == $tnc->id)
				CKEDITOR.replace('termandcondition[{{$tncIndex}}][{{$tnc_dataIndex}}][description]');
				<?php $tnc_dataIndex++; ?>
			@endif
		@endforeach
		<?php $tncIndex++; ?>
	@endforeach
	let tncsection = '{{$tncIndex}}';
	$('#addtncSection').click(function(){
		++tncsection;
		$('#tncSection').append(`<table class="table table-bordered" id="tncDynamicTable${tncsection}">
						<tr>
							<td colspan='3'>
								<input type="text" name="termandcondition[${tncsection}][title]" class="form-control" placeholder="Enter Title" required>
							</td>
							<td colspan='3'>
								<input type="number" name="termandcondition[${tncsection}][orderby]" class="form-control" placeholder="Enter Title" required>
							</td>
						</tr>	
						<tr>
							<th>Description</th>
							<th>Data</th>
							<th>Title</th>
							<th>Show In Privacy Policy</th>
							<th>Display Order</th>
							<th>Action</th>
						</tr>		
						<tr>  												 
							<td>
								<textarea class="form-control" id="termandcondition[${tncsection}][0][description]" name="termandcondition[${tncsection}][0][description]" required></textarea> 
							</td>	
							<td>
								<input type="file" name="termandcondition[${tncsection}][0][image]" class="form-control" />
							</td>	
							<td>
								<input type="text" name="termandcondition[${tncsection}][0][title]" class="form-control" placeholder="Enter Title" required>
							</td>
							<td class="text-center">
								<input type="checkbox" name="termandcondition[${tncsection}][0][show_in_privacy_policy]"  value="1" />
							</td>
							<td>
								<input type="number" name="termandcondition[${tncsection}][0][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
							<td>
								<button type="button" data-id="${tncsection}" name="termandcondition${tncsection}" class="btn btn-success addtncsubsection">Add More</button>
							</td>  
						</tr> 
						<tr class="rmtnc">
							<td colspan='6'><button type="button" name="removetncsection"  class="btn btn-danger pull-right removetncsection">Remove Section</button></td>
						<tr> 
					</table>`);
			let description = 'termandcondition['+tncsection+'][0][description]';
			CKEDITOR.replace(description);
	});
	jQuery(document).on('click', '.removetncsection', function(){  
		$(this).parents('table').remove();
	}); 
	let tncsubsection = '{{$tnc_dataIndex}}';
	$(document).on('click','.addtncsubsection',function(){
		++tncsubsection;
		let id = $(this).data('id');
		console.log($('#tncDynamicTable'+id+' .rmtnc'));
		if($('#tncDynamicTable'+id+' .rmtnc').length == 1){
			$('#tncDynamicTable'+id+' .rmtnc').before(`<tr>  												 
											<td>
												<textarea class="form-control" id="termandcondition[${id}][${tncsubsection}][description]" name="termandcondition[${tncsection}][${tncsubsection}][description]" required></textarea> 
											</td>	
											<td>
												<input type="file" name="termandcondition[${id}][${tncsubsection}][image]" class="form-control" />
											</td>	
											<td>
												<input type="text" name="termandcondition[${id}][${tncsubsection}][title]" class="form-control" placeholder="Enter Title" required>
											</td>
											<td class="text-center">
												<input type="checkbox" name="termandcondition[${id}][${tncsubsection}][show_in_privacy_policy]" value="1"  />
											</td>
											<td>
												<input type="number" name="termandcondition[${id}][${tncsubsection}][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
											<td>
												<button type="button" data-id="${id}" name="termandcondition${id}" class="btn btn-danger removetncsubsection">Remove</button>
											</td>  
										</tr>`);
		}
		else{

			$('#tncDynamicTable'+id).append(`<tr>  												 
											<td>
												<textarea class="form-control" id="termandcondition[${id}][${tncsubsection}][description]" name="termandcondition[${id}][${tncsubsection}][description]" required></textarea> 
											</td>	
											<td>
												<input type="file" name="termandcondition[${id}][${tncsubsection}][image]" class="form-control" />
											</td>	
											<td>
												<input type="text" name="termandcondition[${id}][${tncsubsection}][title]" class="form-control" placeholder="Enter Title" required>
											</td>
											<td class="text-center">
												<input type="checkbox" name="termandcondition[${id}][${tncsubsection}][show_in_privacy_policy]" value="1"  />
											</td>
											<td>
												<input type="number" name="termandcondition[${id}][${tncsubsection}][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
											<td>
												<button type="button" data-id="${id}" name="termandcondition${id}" class="btn btn-danger removetncsubsection">Remove</button>
											</td>  
										</tr>`);
		}
		let description = 'termandcondition['+id+']['+tncsubsection+'][description]';
		CKEDITOR.replace(description);
	});
	$(document).on('click','.removetncsubsection',function(){
		$(this).parents('tr').remove();
	});
</script>
@endsection								
