@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn"><a href="{{ url('studio/create') }}" class="btn btn-success">Add New Studio</a></p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
			@if(Session::has('alert-'.$msg))
			<div class="alert alert-{{ $msg }} alertmanage" role="alert"> 
				{{ Session::get('alert-'.$msg) }}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
			</div>
			@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Studio Listing</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-2 pull-right">
									<label for="select_lang">Select Language</label>
									<select id="select_lang" class="form-control" name="lang">
										<option value="" selected="" disabled="">Select Language</option>
											<option value="en">en</option>
											<option value="nl">nl</option>
									</select>
								</div>
							</div>
							<div class="table-responsive">
								<table id="dtOrderExample1" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#</th>
											<th>Profile Title</th>
											<th>Founder Title</th>
											<th>Join Architenko Title</th>
											<th>Award Title</th>
											<th>Language</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php $index=1; ?>								
										@foreach($studio as $value)
										<tr>
											<td>{{$index++}}</td>
											<td>{{$value->profile_title}}</td>
											<td>{{$value->founder_title}}</td>
											<td>{{$value->join_architenko_title}}</td>
											<td>{{$value->award_title}}</td>
											<td>{{$value->language}}</td>
											<td class="td-actions text-right">
												<a href="{{ URL('studio/edit',$value->id) }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>								
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('studio/delete',$value->id) }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>

										</tr>
										@endforeach									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>	
@endsection								
@section('extrascript')
<script type="text/javascript">
	let BASE_URL = '{{URL("/")}}';
    $(document).ready(function () {
      	$('#dtOrderExample1').DataTable({
      		"order": [[ 3, "desc" ]],
			responsive: true,
			"autoWidth": false,
			"columnDefs": [
			{ className: "td-actions text-right",orderable: false , "targets": [6] }
        ]
      	});
      	$('.dataTables_length').addClass('bs-select');
      	var table = $('#dtOrderExample1').DataTable();
      	$(document).on('change','#select_lang',function(){
      		let lang = $(this).val();
      		$.ajax({
				url:"{{route('studiobylang')}}",
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				data:{lang:lang},
				type:"POST",
				success:function(response){
					table.clear().draw();
					$.each(response.data,function(key,value){
						let edit = BASE_URL+'/studio/edit/'+value.id;
						let del = BASE_URL+'/studio/delete/'+value.id;
						let action = `<div class="td-actions text-right"><a href="${edit}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>								
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('${del}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a></div>`
						table.row.add([key+1,value.profile_title,value.founder_title,value.join_architenko_title,value.award_title,value.language,action ]).draw();
						console.table(value);
					});
				},
				error:function(error){	
					console.log(error);
				}
			});
      	});
      	
    });

</script>

@endsection