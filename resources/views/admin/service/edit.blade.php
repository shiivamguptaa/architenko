@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
        	<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title "> Edit Service</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('service/update') }}" enctype="multipart/form-data">
									@csrf			
									<input id="id" type="hidden" class="form-control" name="id" value="{{$id}}">

									<div class="form-group row">
										<label for="title" class="col-md-4 col-form-label text-md-left">{{ __('Title') }}</label>
										
										<div class="col-md-6">
											<input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{$servicedata->title}}" required>
											
											@error('title')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									
									<div class="form-group row">
										<label for="subtitle" class="col-md-4 col-form-label text-md-left">{{ __('Subtitle') }}</label>
										
										<div class="col-md-6">
											<input id="subtitle" type="subtitle" class="form-control @error('subtitle') is-invalid @enderror" name="subtitle" value="{{$servicedata->subtitle}}" required>
											
											@error('subtitle')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									
									<div class="form-group row">
										<label for="descriptionleft" class="col-md-4 col-form-label text-md-left">{{ __('Left Description') }}</label>
										
										<div class="col-md-10" style="margin-top:20px;">
											<textarea class="form-control" id="descriptionleft" name="descriptionleft" required>{{$servicedata->descriptionleft}}</textarea>      
											
										</div>
									</div>
									
									<div class="form-group row">
										<label for="descriptionright" class="col-md-4 col-form-label text-md-left">{{ __('Right Description') }}</label>
										
										<div class="col-md-10" style="margin-top:20px;">
											<textarea class="form-control" id="descriptionright" name="descriptionright" required>{{$servicedata->descriptionright}}</textarea>      
											
										</div>
									</div>
									
									
									<div class="form-group col-md-3" style="display:none">
										<label for="inputState">Select Home Page Display</label>
										<select name="ishome" class="form-control">      
											<option value="image" @if($servicedata->ishome == "image") selected @endif>Image/Gif</option>
											<option value="youtube" @if($servicedata->ishome == "youtube") selected @endif>Youtube</option>
											<option value="video" @if($servicedata->ishome == "video") selected @endif>Video</option>
											<option value="book" @if($servicedata->ishome == "book") selected @endif>Book</option>
											<option value="flip" @if($servicedata->ishome == "flip") selected @endif>Flip</option>
										</select>
									</div>	
									
									<table class="table table-bordered" id="dynamicTable">  
										<h4>Project Header</h4>
										<tr>
											<th>Type</th>
											<th>Data</th>
											<th>Title</th>
											<th>Display Order</th>
											<th>Action</th>
										</tr>
										
										<!-- <tr>  
											<td>
												<select id="projecttype" name="addmore[0][type]" class="form-control">
													<option value="">Select Type</option>
													<option value="image" >Image</option>
													<option value="youtube">Youtube</option>
													<option value="video">Video upload</option>
													<option value="book">Book Images</option>
													<option value="flip">Flip Images</option>
												</select>
											</td>  
											<td id="imageshow"><input type="file" name="addmore[0][image]" class="form-control" /></td>  
											<td id="youtubeshow"><input type="text" name="addmore[0][youtube]" placeholder="https://www.youtube.com/embed/qUksnWaArko" class="form-control" /></td>  
											<td id="videoshow"><input type="file" name="addmore[0][video]" class="form-control" /></td>  
											<td id="bookshow"><input type="file" name="addmore[0][book][]" class="form-control" multiple></td>  
											<td id="flipshow"><input type="file" name="addmore[0][flip][]" class="form-control" multiple></td>
											<td id="addmoretitle"><input type="text" name="addmore[0][moretitle]" class="form-control" placeholder="Enter Title"></td>
											<td id="order"><input type="number" name="addmore[0][display_order]" class="form-control" min="0" placeholder="Enter order number"></td>
											<td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
										</tr> -->
										<?php  $index=0;?>
										@foreach($servicedetails as $project)
										<tr>  
											<td>
												<input id="serviceId" type="hidden" class="form-control" name="addmore[{{$index}}][serviceId]" value="{{$project->id}}">
												<input id="serviceId" type="hidden" class="form-control" name="serviceId[]" value="{{$project->id}}">

												<select class="projecttype form-control" id="projecttype{{$index}}" name="addmore[{{$index}}][type]"  data-id="{{$index}}">
													<option value="" selected disabled>Select Type</option>
													<option value="image" <?=($project->type=="image") ? 'selected' : '' ?> >Image/Gif</option>
													<option value="youtube" <?=($project->type=="youtube") ? 'selected' : '' ?>>Youtube</option>
													<option value="video" <?=($project->type=="video") ? 'selected' : '' ?>>Video upload</option>
													<option value="book" <?=($project->type=="book") ? 'selected' : '' ?>>Book Images</option>
													<option value="flip" <?=($project->type=="flip") ? 'selected' : '' ?>>Flip Images</option>
												</select>
											</td>  
											<td class="imageshow" <?=($project->type!="image")? "style='display:none'": "" ?> >
												<input id="image{{$index}}" type="file" name="addmore[{{$index}}][image]" class="form-control" value="{{$project->datavalue}}" />
												<?php
													if($project->type=="image"){
														echo getimagehtmlbytype($project->type,URL('images/service/',$project->datavalue));
													}
												?>
											</td>  
											<td class="youtubeshow" <?=($project->type!="youtube")? "style='display:none'": "" ?> >
												<input id="youtube{{$index}}" type="text" name="addmore[{{$index}}][youtube]" placeholder="https://www.youtube.com/embed/qUksnWaArko" class="form-control" value="{{$project->datavalue}}" />
												<?php
													if($project->type=="youtube"){
														echo getimagehtmlbytype($project->type,$project->datavalue);
													}
												?>
											</td>  
											<td class="videoshow" <?=($project->type!="video")? "style='display:none'": "" ?> >
												<input id="video{{$index}}" type="file" name="addmore[{{$index}}][video]" class="form-control" value="{{$project->datavalue}}" />
												<?php
													if($project->type=="video"){
														echo getimagehtmlbytype($project->type,URL('images/service/',$project->datavalue));
													}
												?>
											</td>  
											<td class="bookshow" <?=($project->type!="book")? "style='display:none'": "" ?> >
												<input id="book{{$index}}" type="file" name="addmore[{{$index}}][book][]" class="form-control" multiple value="{{$project->datavalue}}" />
												<?php
													if($project->type=="book"){
														$temp_1 = json_decode($project->datavalue);
														if (!empty($temp_1)) {
															$temp_2 = array();
															foreach ($temp_1 as $value) {
																$temp_2[] = URL('images/service/',$value);
															}
															echo getimagehtmlbytype($project->type,json_encode($temp_2));
														}
													}
												?>
											</td>  
											<td class="flipshow" <?=($project->type!="flip")? "style='display:none'": "" ?> >
												<input id="flip{{$index}}" type="file" name="addmore[{{$index}}][flip][]" class="form-control" multiple value="{{$project->datavalue}}" />
												<?php
													if($project->type=="flip"){
														$temp_3 = json_decode($project->datavalue);
														if (!empty($temp_3)) {
															$temp_4 = array();
															foreach ($temp_3 as $value) {
																$temp_4[] = URL('images/service/',$value);
															}
															echo getimagehtmlbytype($project->type,json_encode($temp_4));
														}
													}
												?>
											</td>

											<td class="addmoretitle">
												<input id="moretitle{{$index}}" type="text" name="addmore[{{$index}}][moretitle]" class="form-control" placeholder="Enter Title" value="{{$project->moretitle}}" ></td>
											<td class="order">
												<input  id="display_order{{$index}}" type="number" name="addmore[{{$index}}][display_order]" class="form-control" min="0" placeholder="Enter order number" value="{{$project->display_order}}" ></td>
											@if($index==0)
											<td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
											@else
											<td><button type="button" class="btn btn-danger remove-tr">Remove</button></td>  
											@endif
											
										</tr>  <?php $index++; ?>
										@endforeach
										<?php $total_row = count($servicedetails); ?>
										<tr>  
											<td>
												<select class="projecttype form-control"  id="projecttype{{$total_row}}" name="addmore[{{$total_row}}][type]" class="form-control" data-id="{{$total_row}}">
													<option value="" selected disabled>Select Type</option>
													<option value="image">Image/Gif</option>
													<option value="youtube">Youtube</option>
													<option value="video">Video upload</option>
													<option value="book">Book Images</option>
													<option value="flip">Flip Images</option>
												</select>
											</td>  
											<td class="imageshow" style="display: none;">
												<input id="image{{$total_row}}" type="file" name="addmore[{{$total_row}}][image]" class="form-control" />
											</td>  
											<td class="youtubeshow"  style="display: none;">
												<input id="youtube{{$total_row}}" type="text" name="addmore[{{$total_row}}][youtube]" placeholder="https://www.youtube.com/embed/qUksnWaArko" class="form-control" />
											</td>  
											<td class="videoshow"  style="display: none;">
												<input  id="video{{$total_row}}" type="file" name="addmore[{{$total_row}}][video]" class="form-control" />
											</td>  
											<td class="bookshow"  style="display: none;">
												<input id="book{{$total_row}}" type="file" name="addmore[{{$total_row}}][book][]" class="form-control" multiple>
											</td>  
											<td class="flipshow"  style="display: none;">
												<input  id="flip{{$total_row}}" type="file" name="addmore[{{$total_row}}][flip][]" class="form-control" multiple>
											</td>
											<td class="addmoretitle"  style="display: none;">
												<input id="moretitle{{$total_row}}" type="text" name="addmore[{{$total_row}}][moretitle]" class="form-control" placeholder="Enter Title" >
											</td>
											<td class="order" style="display: none;">
												<input  id="display_order{{$total_row}}" type="number" name="addmore[{{$total_row}}][display_order]" class="form-control" min="0" placeholder="Enter order number" >
											</td>
											@if($index==0)
											<td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
											@else
											<td><button type="button" class="btn btn-danger remove-tr">Remove</button></td>  
											@endif 
										</tr>    
									</table> 
									
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="lang">
											@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}" @if($servicedata->language == $language->name) selected @endif>{{$language->name}}</option>
											@endforeach						  
											@endif
										</select>
									</div>				
									
									
									
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Update') }}
											</button>
											<a class="btn btn-primary" href="{{ url('service') }}">Cancel</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>		
				</div>	
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>

<script>
	CKEDITOR.editorConfig = function (config) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
	
	};
	CKEDITOR.replace('descriptionleft');
	CKEDITOR.replace('descriptionright');
	
$( document ).ready(function() {
		let i = "<?php echo !empty($servicedetails) ? count($servicedetails->toArray()) : 0; ?>";	
			jQuery(document).on('change', '.projecttype', function(){			
			let row_number = $(this).data('id');
			var selectedval = $(this).val();
			if(selectedval == 'image'){
				$("#image"+row_number).parent().css("display","table-cell");
				$("#youtube"+row_number).parent().parent().css("display","none");
				$("#video"+row_number).parent().css("display","none");
				$("#book"+row_number).parent().css("display","none");
				$("#flip"+row_number).parent().css("display","none");
				$("#moretitle"+row_number).parent().parent().css("display","table-cell");
				$("#display_order"+row_number).parent().parent().css("display","table-cell");
			}
			else if(selectedval == 'youtube'){
				$("#image"+row_number).parent().css("display","none");
				$("#youtube"+row_number).parent().parent().css("display","table-cell");
				$("#video"+row_number).parent().css("display","none");
				$("#book"+row_number).parent().css("display","none");
				$("#flip"+row_number).parent().css("display","none");
				$("#moretitle"+row_number).parent().parent().css("display","table-cell");
				$("#display_order"+row_number).parent().parent().css("display","table-cell");
			}
			else if(selectedval == 'video'){
				$("#image"+row_number).parent().css("display","none");
				$("#youtube"+row_number).parent().parent().css("display","none");
				$("#video"+row_number).parent().css("display","table-cell");
				$("#book"+row_number).parent().css("display","none");
				$("#flip"+row_number).parent().css("display","none");
				$("#moretitle"+row_number).parent().parent().css("display","table-cell");
				$("#display_order"+row_number).parent().parent().css("display","table-cell");
			}
			else if(selectedval == 'book'){
				$("#image"+row_number).parent().css("display","none");
				$("#youtube"+row_number).parent().parent().css("display","none");
				$("#video"+row_number).parent().css("display","none");
				$("#book"+row_number).parent().css("display","table-cell");
				$("#flip"+row_number).parent().css("display","none");
				$("#moretitle"+row_number).parent().parent().css("display","table-cell");
				$("#display_order"+row_number).parent().parent().css("display","table-cell");
			}
			else if(selectedval == 'flip'){
				$("#image"+row_number).parent().css("display","none");
				$("#youtube"+row_number).parent().parent().css("display","none");
				$("#video"+row_number).parent().css("display","none");
				$("#book"+row_number).parent().css("display","none");
				$("#flip"+row_number).parent().css("display","table-cell");
				$("#moretitle"+row_number).parent().parent().css("display","table-cell");
				$("#display_order"+row_number).parent().parent().css("display","table-cell");
			}
			
		});
		jQuery("#add").click(function(){
	console.log("before",i);

			++i;

	console.log("after",i);

			console.log(i);
			jQuery("#dynamicTable").append(
				`<tr>
					<td>
						<select data-id='${i}' class="projecttype" id="projecttype${i}" name="addmore[${i}][type]" class="form-control">
							<option value="" selected disabled>Select Type</option>
							<option value="image">Image/Gif</option>
							<option value="youtube">Youtube</option>
							<option value="video">Video Upload</option>
							<option value="book">Book Images</option>
							<option value="flip">Flip Image</option>
						</select>
					</td>
					<td class="imageshow" style="display: none;">
						<input  id="image${i}" type="file" name="addmore[${i}][image]" class="form-control" />
					</td>
					<td class="youtubeshow" style="display: none;">
						<input id="youtube${i}" type="text" name="addmore[${i}][youtube]" placeholder="https://www.youtube.com/embed/qUksnWaArko" class="form-control" />
					</td>
					<td class="videoshow" style="display: none;">
						<input id="video${i}"  type="file" name="addmore[${i}][video]" class="form-control" />
					</td>
					<td class="bookshow" style="display: none;">
						<input  id="book${i}"  type="file" name="addmore[${i}][book][]" class="form-control" multiple>
					</td>
					<td class="flipshow" style="display: none;">
						<input id="flip${i}" type="file" name="addmore[${i}][flip][]" class="form-control" multiple>
					</td>
					<td class="addmoretitle" style="display: none;">
						<input id="moretitle${i}" type="text"  name="addmore[${i}][moretitle]" class="form-control" placeholder="Enter Title">
					</td>
					<td class="order" style="display: none;">
						<input  id="display_order${i}" type="number"   name="addmore[${i}][display_order]" class="form-control" min="0" placeholder="Enter order number">
					</td>
					<td>
						<button type="button" class="btn btn-danger remove-tr">Remove</button>
					</td>
				</tr>`);
			
		});	
		jQuery(document).on('click', '.remove-tr', function(){  
			$(this).parents('tr').remove();
		});  	
	});
</script>
@endsection							
