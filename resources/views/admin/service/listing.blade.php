@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn"><a href="{{ url('service/create') }}" class="btn btn-success">Add New Service</a></p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Services Listing</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-2 pull-right">
									<label for="select_lang">Select Language</label>
									<select id="select_lang" class="form-control" name="lang">
										<option value="" selected disabled>Select Language</option>
										@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}">{{$language->name}}</option>
											@endforeach						  
										@endif
									</select>
								</div>
							</div>
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#</th>
											<th>Title</th>
											<th>Image</th>
											<th>Order By</th>
											<th style="width: 90px">Language</th>
											<th>Show in Home</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody id="serviceList">
										
										@if(count($services) > 0)
										<?php $counter = 1; ?>
										@foreach($services as $service)
										<tr>
											<td>{{$counter}}</td>
											<td>{{$service['title']}}</td>
											@if($service['type'] == "image")
											<td><img src="{{asset('images/service/'.$service['datavalue'])}}" class="width-100"></td>
											@elseif($service['type'] == "book" || $service['type'] == "flip")
											<td><?php $bookimg = json_decode($service['datavalue']);
											foreach($bookimg as $key => $book):?>
											<img src="{{asset('images/service/'.$book)}}" class="width-100 <?php if($key == 0){echo 'active';}?>" alt=""> 
											<?php endforeach; ?>
											</td>
											@elseif($service['type'] == "youtube")
											<td>{{$service['datavalue']}}</td>
											@else
											<td>{{$service['datavalue']}}</td>
											@endif
																				
											<td>
												<input type="number" id="service{{$service['serviceId']}}" class="orderby form-control" onfocusout="onChangeOrderBy({{$service['serviceId']}})" value="{{$service['orderby']}}"  />
											</td>

											<td>{{$service['language']}}</td>
											<td class="text-center">
												<input type="checkbox" <?php if($service['is_home']==1){ echo "checked"; } ?>  id="checkbox{{$service['serviceId']}}" onclick="showServiceInHome({{$service['serviceId']}})" /></td>
											
											<td class="td-actions text-right">												
												<a href="{{ url('service/'.$service['serviceId'].'/edit') }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>
												<a  href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('service/'.$service['serviceId'].'/delete') }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>
										</tr>
										<?php $counter++; ?>
										@endforeach										
										@endif
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>	

<script>
function onChangeOrderBy(id) {
	let orderby = $("#service"+id).val();
	$.ajax({
		url:"{{route('serviceorderby')}}",
		headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
		data:{id:id,orderby:orderby},
		type:"POST",
		success:function(response){
			console.log(response);
		},
		error:function(error){	
			console.log(error);
		}
	});
}
function showServiceInHome(id) {
	let is_home = $("#checkbox"+id).is(':checked') ? 1 : 0;
	$.ajax({
		url:"{{route('showServiceInHome')}}",
		headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
		data:{id:id,is_home:is_home},
		type:"POST",
		success:function(response){
			console.log(response);
		},
		error:function(error){	
			console.log(error);
		}
	});
}
</script>
@endsection								

@section('extrascript')
<script type="text/javascript">
    $(document).ready(function () {
      $('#dtOrderExample').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
		  { className: "td-actions text-right",orderable: false , "targets": [6] },
		  { orderable: false , "targets": [3] },
		  { orderable: false , "targets": [5] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>

<script type="text/javascript">
	let BASE_URL = '{{URL("/")}}';
    $(document).ready(function () {
      	var table = $('#dtOrderExample').DataTable();
      	$(document).on('change','#select_lang',function(){
      		let lang = $(this).val();
      		$.ajax({
				url:"{{route('onChangeServiceLanguage')}}",
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				data:{lang:lang},
				type:"POST",
				success:function(response){
						// console.table(response.data);
					if (response.responseCode == 1) {
						table.clear().draw();
						$.each(response.data,function(key,value){
							let edit = BASE_URL+'/service/'+value.serviceId+'/edit';
							let del = BASE_URL+'/service/'+value.serviceId+'/delete';
							let img = ``;

							if(value.type == "image"){
							 	img = `<img src="${BASE_URL}/images/service/${value.datavalue}" class="width-100">`;
							}
							else if(value.type == "book" || value.type == "flip"){
								let temp = JSON.parse(value.datavalue);
								$.each(temp ,function(key,value1){
									img += `<img src="${BASE_URL}/images/service/${value1}" class="width-100 ${key == 0 ? 'active' : '' }" alt="">`;
								});
							}
							else if(value.type == "youtube"){
								img = value.datavalue;
							}
							else{
								img = value.datavalue;
							}

							let action = `<div class="td-actions text-right"><a href="${edit}" class="btn btn-success">
														<i class="material-icons">edit</i>
													</a>								
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('${del}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a></div>`;
							let text = `<input type="number" id="menu${value.serviceId}" class="orderby form-control" onfocusout="onChangeOrderBy(${value.serviceId})" data-id ="${value.serviceId}" value="${value.orderby}"  />`;
							let home = `<input type="checkbox" ${value.is_home ? 'checked' : ''}  id="checkbox${value.serviceId}" onclick="showServiceInHome(${value.serviceId})" />`;
							table.row.add([key+1,value.title,img,text,value.language,home,action]).draw();
						});
					}
				},
				error:function(error){	
					console.log(error);
				}
			});
      	});
      	
    });

</script>
@endsection