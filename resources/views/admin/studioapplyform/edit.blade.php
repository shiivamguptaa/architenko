@extends('layouts.auth')
@section('content')

<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('studioapplyform/update') }}" enctype="multipart/form-data">
									@csrf	
									<input id="id" name="id" type="hidden" value="{{$id}}" />
									<div class="form-group row">
										<div class="col-md-12">
											<input id="privacy_policy_title" name="privacy_policy_title" type="text" class="form-control" autocomplete="off" title="Privacy Policy Title" placeholder="Privacy Policy Title" autofocus value="@if(isset($studioapplyform['privacy_policy_title'])){{$studioapplyform['privacy_policy_title']}}@endif" />										
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4 col-form-label text-md-left bmd-label-static">Privacy Policy Short Description</label>
										<div class="col-md-12  mt-3">
											<textarea id="privacy_policy_short_description" name="privacy_policy_short_description"  class="form-control" autocomplete="off" title="Privacy Policy Short Description" placeholder="Privacy Policy Short Description">@if(isset($studioapplyform['privacy_policy_short_description'])){{$studioapplyform['privacy_policy_short_description']}}@endif</textarea>		
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input  name="apply_btn" type="text" class="form-control" autocomplete="off" title="Enter Apply Button title" placeholder="Enter Apply Button title" autofocus />										
										</div>
									</div>

								
									<div class="form-group row">
										<div class="col-md-12">
											<input id="privacy_policy_accept_title" name="privacy_policy_accept_title" type="text" class="form-control" autocomplete="off" title="Privacy Policy Accept Title" placeholder="Privacy Policy Accept Title" autofocus value="@if(isset($studioapplyform['privacy_policy_accept_title'])){{$studioapplyform['privacy_policy_accept_title']}}@endif" />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_correspondence_title" name="language_correspondence_title" type="text" class="form-control" autocomplete="off" title="Language Correspondence Title" placeholder="Language Correspondence Title" autofocus   value="@if(isset($studioapplyform['language_correspondence_title'])){{$studioapplyform['language_correspondence_title']}}@endif"/>	
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_input" name="language_input" type="text" class="form-control" autocomplete="off" title="Language" placeholder="Language" autofocus data-role="tagsinput"  value="@if(isset($studioapplyform['language_input'])){{$studioapplyform['language_input']}}@endif" />	
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="position_title" name="position_title" type="text" class="form-control" autocomplete="off" title="Position Title" placeholder="Position Title" autofocus value="@if(isset($studioapplyform['position_title'])){{$studioapplyform['position_title']}}@endif" />	
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="position_option1" name="position_option1" type="text" class="form-control" autocomplete="off" title="Position Option 1" placeholder="Position Option 1" value="@if(isset($studioapplyform['position_option1'])){{$studioapplyform['position_option1']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="position_option2" name="position_option2" type="text" class="form-control" autocomplete="off" title="Position Option 2" placeholder="Position Option 2" value="@if(isset($studioapplyform['position_option2'])){{$studioapplyform['position_option2']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="position_option3" name="position_option3" type="text" class="form-control" autocomplete="off" title="Position Option 3" placeholder="Position Option 3" value="@if(isset($studioapplyform['position_option3'])){{$studioapplyform['position_option3']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="position_option4" name="position_option4" type="text" class="form-control" autocomplete="off" title="Position Option 4" placeholder="Position Option 4"  value="@if(isset($studioapplyform['position_option4'])){{$studioapplyform['position_option4']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="personal_contact_title" name="personal_contact_title" type="text" class="form-control" autocomplete="off" title="Personal Contact Title" placeholder="Personal Contact Title" autofocus value="@if(isset($studioapplyform['personal_contact_title'])){{$studioapplyform['personal_contact_title']}}@endif" />	
										</div>
									</div>

									<div class="form-group row">
										<div class="col-md-12">
											<input id="address_title" name="address_title" type="text" class="form-control" autocomplete="off" title="Address Title" placeholder="Address Title" value="@if(isset($studioapplyform['address_title'])){{$studioapplyform['address_title']}}@endif" />	
										</div>
									</div>

									

									<div class="form-group row">
										<div class="col-md-12">
											<input id="mr_title" name="mr_title" type="text" class="form-control" autocomplete="off" title="Mr Title" placeholder="Mr Title" value="@if(isset($studioapplyform['mr_title'])){{$studioapplyform['mr_title']}}@endif" />
										</div>
									</div>

									<div class="form-group row">
										<div class="col-md-12">
											<input id="mrs_title" name="mrs_title" type="text" class="form-control" autocomplete="off" title="Mrs Title" placeholder="Mrs Title"  value="@if(isset($studioapplyform['mrs_title'])){{$studioapplyform['mrs_title']}}@endif" />
										</div>
									</div>

									<div class="form-group row">
										<div class="col-md-12">
											<input id="first_name" name="first_name" type="text" class="form-control" autocomplete="off" title="First Name Title" placeholder="First Name Title" value="@if(isset($studioapplyform['first_name'])){{$studioapplyform['first_name']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="middle_name" name="middle_name" type="text" class="form-control" autocomplete="off" title="Middle Name Title" placeholder="Middle Name Title" value="@if(isset($studioapplyform['middle_name'])){{$studioapplyform['middle_name']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="last_name" name="last_name" type="text" class="form-control" autocomplete="off" title="Last Name Title" placeholder="Last Name Title" value="@if(isset($studioapplyform['last_name'])){{$studioapplyform['last_name']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="address" name="address" type="text" class="form-control" autocomplete="off" title="Address Title" placeholder="Address Title" value="@if(isset($studioapplyform['address'])){{$studioapplyform['address']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="city" name="city" type="text" class="form-control" autocomplete="off" title="City Title" placeholder="City Title" value="@if(isset($studioapplyform['city'])){{$studioapplyform['city']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="postal_code" name="postal_code" type="text" class="form-control" autocomplete="off" title="Postal Code Title" placeholder="Postal Code Title" value="@if(isset($studioapplyform['postal_code'])){{$studioapplyform['postal_code']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="country" name="country" type="text" class="form-control" autocomplete="off" title="Country Title" placeholder="Country Title" value="@if(isset($studioapplyform['country'])){{$studioapplyform['country']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="phone" name="phone" type="text" class="form-control" autocomplete="off" title="Phone Title" placeholder="Phone Title" value="@if(isset($studioapplyform['phone'])){{$studioapplyform['phone']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="email" name="email" type="text" class="form-control" autocomplete="off" title="Email Title" placeholder="Email Title" value="@if(isset($studioapplyform['email'])){{$studioapplyform['email']}}@endif" />
										</div>
									</div>	

									<div class="form-group row">
										<div class="col-md-12">
											<input id="dob" name="dob" type="text" class="form-control" autocomplete="off" title="Date of Birth Title" placeholder="Date of Birth Title"  value="@if(isset($studioapplyform['dob'])){{$studioapplyform['dob']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="nationality_title" name="nationality_title" type="text" class="form-control" autocomplete="off" title="Nationality Title" placeholder="Nationality Title" value="@if(isset($studioapplyform['nationality_title'])){{$studioapplyform['nationality_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="another_nationality_title" name="another_nationality_title" type="text" class="form-control" autocomplete="off" title="Another Nationality Title" placeholder="Another Nationality Title" value="@if(isset($studioapplyform['another_nationality_title'])){{$studioapplyform['another_nationality_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="education_title" name="education_title" type="text" class="form-control" autocomplete="off" title="Education Title" placeholder="Education Title"  value="@if(isset($studioapplyform['education_title'])){{$studioapplyform['education_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="institution_title" name="institution_title" type="text" class="form-control" autocomplete="off" title="Institution Title" placeholder="Institution Title" value="@if(isset($studioapplyform['institution_title'])){{$studioapplyform['institution_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="degree_title" name="degree_title" type="text" class="form-control" autocomplete="off" title="Degree Title" placeholder="Degree Title"  value="@if(isset($studioapplyform['degree_title'])){{$studioapplyform['degree_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="degree_start_year" name="degree_start_year" type="text" class="form-control" autocomplete="off" title="Degree Start Year" placeholder="Degree Start Year"   value="@if(isset($studioapplyform['degree_start_year'])){{$studioapplyform['degree_start_year']}}@endif" />
										</div>
									</div>	
									
									<div class="form-group row">
										<div class="col-md-12">
											<input id="another_degree_add_title" name="another_degree_add_title" type="text" class="form-control" autocomplete="off" title="Add Another Institution or Degree" placeholder="Add Another Institution or Degree" value="@if(isset($studioapplyform['another_degree_add_title'])){{$studioapplyform['another_degree_add_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_title" name="language_title" type="text" class="form-control" autocomplete="off" title="Language" placeholder="Language" value="@if(isset($studioapplyform['language_title'])){{$studioapplyform['language_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_list" name="language_list" type="text" class="form-control" autocomplete="off" title="Language List" placeholder="Language List" data-role="tagsinput"
											value="@if(isset($studioapplyform['language_list'])){{$studioapplyform['language_list']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_add_another_title" name="language_add_another_title" type="text" class="form-control" autocomplete="off" title="Add Another Language" placeholder="Add Another Language" value="@if(isset($studioapplyform['language_add_another_title'])){{$studioapplyform['language_add_another_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_add_another_input" name="language_add_another_input" type="text" class="form-control" autocomplete="off" title="Add Another Language Input Title" placeholder="Add Another Language Input Title" value="@if(isset($studioapplyform['language_add_another_input'])){{$studioapplyform['language_add_another_input']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="profesional_situation_title" name="profesional_situation_title" type="text" class="form-control" autocomplete="off" title="Profesional Situation Title" placeholder="Profesional Situation Title"  value="@if(isset($studioapplyform['profesional_situation_title'])){{$studioapplyform['profesional_situation_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="profesional_situation_option1" name="profesional_situation_option1" type="text" class="form-control" autocomplete="off" title="Profesional Situation Option 1" placeholder="Profesional Situation Option 1" value="@if(isset($studioapplyform['profesional_situation_option1'])){{$studioapplyform['profesional_situation_option1']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-8">
											<input id="profesional_situation_option2" name="profesional_situation_option2" type="text" class="form-control" autocomplete="off" title="Profesional Situation Option 2" placeholder="Profesional Situation Option 2" value="@if(isset($studioapplyform['profesional_situation_option2'])){{$studioapplyform['profesional_situation_option2']}}@endif" />
										</div>
										<div class="col-md-4">
											<input id="profesional_situation_option2_year_month" name="profesional_situation_option2_year_month" type="text" class="form-control" autocomplete="off" title="Year/Months" placeholder="Year/Months"  value="@if(isset($studioapplyform['profesional_situation_option2_year_month'])){{$studioapplyform['profesional_situation_option2_year_month']}}@endif"/>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-8">
											<input id="profesional_situation_option3" name="profesional_situation_option3" type="text" class="form-control" autocomplete="off" title="Profesional Situation Option 3" placeholder="Profesional Situation Option 3" value="@if(isset($studioapplyform['profesional_situation_option3'])){{$studioapplyform['profesional_situation_option3']}}@endif" />
										</div>
										<div class="col-md-4">
											<input id="profesional_situation_option3_year_month" name="profesional_situation_option3_year_month" type="text" class="form-control" autocomplete="off" title="Year/Months" placeholder="Year/Months" value="@if(isset($studioapplyform['profesional_situation_option3_year_month'])){{$studioapplyform['profesional_situation_option3_year_month']}}@endif"/>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="profesional_situation_option4" name="profesional_situation_option4" type="text" class="form-control" autocomplete="off" title="Profesional Situation Option 4" placeholder="Profesional Situation Option 4" value="@if(isset($studioapplyform['profesional_situation_option4'])){{$studioapplyform['profesional_situation_option4']}}@endif" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="applied_at_architenko_title" name="applied_at_architenko_title" type="text" class="form-control" autocomplete="off" title="Applied At Architenko Title" placeholder="Applied At Architenko Title"  value="@if(isset($studioapplyform['applied_at_architenko_title'])){{$studioapplyform['applied_at_architenko_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="yes_title" name="yes_title" type="text" class="form-control" autocomplete="off" title="Yes Title" placeholder="Yes Title" value="@if(isset($studioapplyform['yes_title'])){{$studioapplyform['yes_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="no_title" name="no_title" type="text" class="form-control" autocomplete="off" title="No Title" placeholder="No Title" value="@if(isset($studioapplyform['no_title'])){{$studioapplyform['no_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="aboutus_title" name="aboutus_title" type="text" class="form-control" autocomplete="off" title="Aboutus Title" placeholder="Aboutus Title"  value="@if(isset($studioapplyform['aboutus_title'])){{$studioapplyform['aboutus_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="attachments_title" name="attachments_title" type="text" class="form-control" autocomplete="off" title="Attachments Title" placeholder="Attachments Title" value="@if(isset($studioapplyform['attachments_title'])){{$studioapplyform['attachments_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="file_validation_message" name="file_validation_message" type="text" class="form-control" autocomplete="off" title="File Validation Message" placeholder="File Validation Message" value="@if(isset($studioapplyform['file_validation_message'])){{$studioapplyform['file_validation_message']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="cover_letter_title" name="cover_letter_title" type="text" class="form-control" autocomplete="off" title="Cover Letter Title" placeholder="Cover Letter Title" value="@if(isset($studioapplyform['cover_letter_title'])){{$studioapplyform['cover_letter_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="curriculum_vitae_title" name="curriculum_vitae_title" type="text" class="form-control" autocomplete="off" title="Curriculum Vitae Title" placeholder="Curriculum Vitae Title" value="@if(isset($studioapplyform['curriculum_vitae_title'])){{$studioapplyform['curriculum_vitae_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="portfolio_title" name="portfolio_title" type="text" class="form-control" autocomplete="off" title="Portfolio Title" placeholder="Portfolio Title"  value="@if(isset($studioapplyform['portfolio_title'])){{$studioapplyform['portfolio_title']}}@endif" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="comment_title" name="comment_title" type="text" class="form-control" autocomplete="off" title="Comment Title" placeholder="Comment Title"  value="@if(isset($studioapplyform['comment_title'])){{$studioapplyform['comment_title']}}@endif" />
										</div>
									</div>			
									<div class="form-group row">
										<div class="col-md-12">
											<input id="apply_title" name="apply_title" type="text" class="form-control" autocomplete="off" title="Apply Title" placeholder="Apply Title" value="@if(isset($studioapplyform['apply_title'])){{$studioapplyform['apply_title']}}@endif" />
										</div>
									</div>
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="language">
											@if(count($languages)>0)
												@foreach($languages as $language)
													<option value="{{$language->name}}" @if($language->name == $studioapplyform['language']) selected @endif>{{$language->name}}</option>
												@endforeach						  
											@endif
										</select>
									</div>				
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
<script>
	CKEDITOR.editorConfig = function (config) {
		config.language = 'es';
		config.uiColor = '#F7B42C';
		config.height = 300;
		config.toolbarCanCollapse = true;		
	};
	// CKEDITOR.replace('privacy_policy_left_side_description');
	// CKEDITOR.replace('privacy_policy_right_side_description');
	CKEDITOR.replace('privacy_policy_short_description');
</script>
@endsection						