@extends('layouts.auth')
@section('content')

<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('studioapplyform/store') }}" enctype="multipart/form-data">
									@csrf	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="privacy_policy_title" name="privacy_policy_title" type="text" class="form-control" autocomplete="off" title="Privacy Policy Title" placeholder="Privacy Policy Title" autofocus />										
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4 col-form-label text-md-left bmd-label-static">Privacy Policy Short Description</label>
										<div class="col-md-12  mt-3">
											<textarea id="privacy_policy_short_description" name="privacy_policy_short_description"  class="form-control" autocomplete="off" title="Privacy Policy Short Description" placeholder="Privacy Policy Short Description"></textarea>		
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input  name="apply_btn" type="text" class="form-control" autocomplete="off" title="Enter Apply Button title" placeholder="Enter Apply Button title" autofocus />										
										</div>
									</div>
									
										<div class="form-group row">
											<div class="col-md-12">
												<input id="privacy_policy_accept_title" name="privacy_policy_accept_title" type="text" class="form-control" autocomplete="off" title="Privacy Policy Accept Title" placeholder="Privacy Policy Accept Title" autofocus />										
											</div>
										</div>
										<div class="form-group row">
										<div class="col-md-12">
											<input id="language_correspondence_title" name="language_correspondence_title" type="text" class="form-control" autocomplete="off" title="Language Correspondence Title" placeholder="Language Correspondence Title" autofocus />	
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_input" name="language_input" type="text" class="form-control" autocomplete="off" title="Language" placeholder="Language" autofocus data-role="tagsinput" />	
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="position_title" name="position_title" type="text" class="form-control" autocomplete="off" title="Position Title" placeholder="Position Title" autofocus />	
										</div>
									</div>	
									<!-- <div class="form-group row">
										<div class="col-md-12">
											<input id="position_input" name="position_input" type="text" class="form-control" autocomplete="off" title="Position Input" placeholder="Position Input" autofocus />	
										</div>
									</div> -->	
									<!-- <div class="form-group row">
										<div class="col-md-12">
											<input id="position_list" name="position_list" type="text" class="form-control" autocomplete="off" title="Position List" placeholder="Position List" data-role="tagsinput" autofocus />	
										</div>
									</div>	 -->
									<div class="form-group row">
										<div class="col-md-12">
											<input id="position_option1" name="position_option1" type="text" class="form-control" autocomplete="off" title="Position Option 1" placeholder="Position Option 1" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="position_option2" name="position_option2" type="text" class="form-control" autocomplete="off" title="Position Option 2" placeholder="Position Option 2" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="position_option3" name="position_option3" type="text" class="form-control" autocomplete="off" title="Position Option 3" placeholder="Position Option 3" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="position_option4" name="position_option4" type="text" class="form-control" autocomplete="off" title="Position Option 4" placeholder="Position Option 4" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="personal_contact_title" name="personal_contact_title" type="text" class="form-control" autocomplete="off" title="Personal Contact Title" placeholder="Personal Contact Title" autofocus />	
										</div>
									</div>

									<div class="form-group row">
										<div class="col-md-12">
											<input id="address_title" name="address_title" type="text" class="form-control" autocomplete="off" title="Address Title" placeholder="Address Title" />	
										</div>
									</div>

									

									<div class="form-group row">
										<div class="col-md-12">
											<input id="mr_title" name="mr_title" type="text" class="form-control" autocomplete="off" title="Mr Title" placeholder="Mr Title" />
										</div>
									</div>

									<div class="form-group row">
										<div class="col-md-12">
											<input id="mrs_title" name="mrs_title" type="text" class="form-control" autocomplete="off" title="Mrs Title" placeholder="Mrs Title" />
										</div>
									</div>

									<div class="form-group row">
										<div class="col-md-12">
											<input id="first_name" name="first_name" type="text" class="form-control" autocomplete="off" title="First Name Title" placeholder="First Name Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="middle_name" name="middle_name" type="text" class="form-control" autocomplete="off" title="Middle Name Title" placeholder="Middle Name Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="last_name" name="last_name" type="text" class="form-control" autocomplete="off" title="Last Name Title" placeholder="Last Name Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="address" name="address" type="text" class="form-control" autocomplete="off" title="Address Title" placeholder="Address Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="city" name="city" type="text" class="form-control" autocomplete="off" title="City Title" placeholder="City Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="postal_code" name="postal_code" type="text" class="form-control" autocomplete="off" title="Postal Code Title" placeholder="Postal Code Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="country" name="country" type="text" class="form-control" autocomplete="off" title="Country Title" placeholder="Country Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="phone" name="phone" type="text" class="form-control" autocomplete="off" title="Phone Title" placeholder="Phone Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="email" name="email" type="text" class="form-control" autocomplete="off" title="Email Title" placeholder="Email Title" />
										</div>
									</div>	

									<div class="form-group row">
										<div class="col-md-12">
											<input id="dob" name="dob" type="text" class="form-control" autocomplete="off" title="Date of Birth Title" placeholder="Date of Birth Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="nationality_title" name="nationality_title" type="text" class="form-control" autocomplete="off" title="Nationality Title" placeholder="Nationality Title" />
										</div>
									</div>	
												
									<div class="form-group row">
										<div class="col-md-12">
											<input id="another_nationality_title" name="another_nationality_title" type="text" class="form-control" autocomplete="off" title="Another Nationality Title" placeholder="Another Nationality Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="education_title" name="education_title" type="text" class="form-control" autocomplete="off" title="Education Title" placeholder="Education Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="institution_title" name="institution_title" type="text" class="form-control" autocomplete="off" title="Institution Title" placeholder="Institution Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="degree_title" name="degree_title" type="text" class="form-control" autocomplete="off" title="Degree Title" placeholder="Degree Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="degree_start_year" name="degree_start_year" type="text" class="form-control" autocomplete="off" title="Degree Start Year" placeholder="Degree Start Year" />
										</div>
									</div>	
									
									<div class="form-group row">
										<div class="col-md-12">
											<input id="another_degree_add_title" name="another_degree_add_title" type="text" class="form-control" autocomplete="off" title="Add Another Institution or Degree" placeholder="Add Another Institution or Degree" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_title" name="language_title" type="text" class="form-control" autocomplete="off" title="Language" placeholder="Language" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_list" name="language_list" type="text" class="form-control" autocomplete="off" title="Language List" placeholder="Language List" data-role="tagsinput" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_add_another_title" name="language_add_another_title" type="text" class="form-control" autocomplete="off" title="Add Another Language" placeholder="Add Another Language" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="language_add_another_input" name="language_add_another_input" type="text" class="form-control" autocomplete="off" title="Add Another Language Input Title" placeholder="Add Another Language Input Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="profesional_situation_title" name="profesional_situation_title" type="text" class="form-control" autocomplete="off" title="Profesional Situation Title" placeholder="Profesional Situation Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="profesional_situation_option1" name="profesional_situation_option1" type="text" class="form-control" autocomplete="off" title="Profesional Situation Option 1" placeholder="Profesional Situation Option 1" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-8">
											<input id="profesional_situation_option2" name="profesional_situation_option2" type="text" class="form-control" autocomplete="off" title="Profesional Situation Option 2" placeholder="Profesional Situation Option 2" />
										</div>
										<div class="col-md-4">
											<input id="profesional_situation_option2_year_month" name="profesional_situation_option2_year_month" type="text" class="form-control" autocomplete="off" title="Year/Months" placeholder="Year/Months" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-8">
											<input id="profesional_situation_option3" name="profesional_situation_option3" type="text" class="form-control" autocomplete="off" title="Profesional Situation Option 3" placeholder="Profesional Situation Option 3" />
										</div>
										<div class="col-md-4">
											<input id="profesional_situation_option3_year_month" name="profesional_situation_option3_year_month" type="text" class="form-control" autocomplete="off" title="Year/Months" placeholder="Year/Months" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="profesional_situation_option4" name="profesional_situation_option4" type="text" class="form-control" autocomplete="off" title="Profesional Situation Option 4" placeholder="Profesional Situation Option 4" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="applied_at_architenko_title" name="applied_at_architenko_title" type="text" class="form-control" autocomplete="off" title="Applied At Architenko Title" placeholder="Applied At Architenko Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="yes_title" name="yes_title" type="text" class="form-control" autocomplete="off" title="Yes Title" placeholder="Yes Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="no_title" name="no_title" type="text" class="form-control" autocomplete="off" title="No Title" placeholder="No Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="aboutus_title" name="aboutus_title" type="text" class="form-control" autocomplete="off" title="Aboutus Title" placeholder="Aboutus Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="attachments_title" name="attachments_title" type="text" class="form-control" autocomplete="off" title="Attachments Title" placeholder="Attachments Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="file_validation_message" name="file_validation_message" type="text" class="form-control" autocomplete="off" title="File Validation Message" placeholder="File Validation Message" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="cover_letter_title" name="cover_letter_title" type="text" class="form-control" autocomplete="off" title="Cover Letter Title" placeholder="Cover Letter Title" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="curriculum_vitae_title" name="curriculum_vitae_title" type="text" class="form-control" autocomplete="off" title="Curriculum Vitae Title" placeholder="Curriculum Vitae Title" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="portfolio_title" name="portfolio_title" type="text" class="form-control" autocomplete="off" title="Portfolio Title" placeholder="Portfolio Title" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="comment_title" name="comment_title" type="text" class="form-control" autocomplete="off" title="Comment Title" placeholder="Comment Title" />
										</div>
									</div>			
									<div class="form-group row">
										<div class="col-md-12">
											<input id="apply_title" name="apply_title" type="text" class="form-control" autocomplete="off" title="Apply Title" placeholder="Apply Title" />
										</div>
									</div>
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="language">
											@if(count($languages)>0)
												@foreach($languages as $language)
													<option value="{{$language->name}}">{{$language->name}}</option>
												@endforeach						  
											@endif
										</select>
									</div>				
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
<script>
	CKEDITOR.editorConfig = function (config) {
		config.language = 'es';
		config.uiColor = '#F7B42C';
		config.height = 300;
		config.toolbarCanCollapse = true;		
	};
	CKEDITOR.replace('privacy_policy_short_description');
</script>
@endsection						