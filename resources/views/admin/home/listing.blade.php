@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn"><a href="{{ url('front/home/create') }}" class="btn btn-success">Add New Home Page</a></p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Home Listing</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th class="text-center">#<i class="fa fa-sort-up"></i><i class="fa fa-sort-down"></i></th>
											<th>Project Service Title<i class="fa fa-sort-up"></i><i class="fa fa-sort-down"></i></th>
											<th>Middle Section tilte<i class="fa fa-sort-up"></i><i class="fa fa-sort-down"></i></th>
											<th>Language<i class="fa fa-sort-up"></i><i class="fa fa-sort-down"></i></th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>
										@if(!empty($sectiononedata) > 0)
										<?php $counter = 1; ?>
										@foreach($sectiononedata as $homedata)
										<tr>							
											<td class="text-center">{{$counter}}</td>
											<!-- <td><img class="width-100" src="{{ url('/images/home/'.$homedata['img_left']) }}" alt=""></td>
											<td><img class="width-100" src="{{ url('/images/home/'.$homedata['img_right']) }}" alt=""></td> -->
											<td>{{ $homedata['project_service_title'] }}</td>
											<td>{{ $homedata['middle_section_title'] }}</td>
											<td>{{ $homedata['language'] }}</td>											
											<td class="td-actions text-right">												
												<a href="{{ url('front/home/'.$homedata['id'].'/edit') }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>
												<a  href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('front/home/'.$homedata['id'].'/delete') }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>							
										</tr>
										<?php $counter++; ?>
										@endforeach
										@endif						
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection											