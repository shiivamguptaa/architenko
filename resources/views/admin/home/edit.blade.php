@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Edit Home Page</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="post" action="{{ url('front/home/update') }}" enctype="multipart/form-data">
									@csrf				
									<input type="hidden" name="id" value="{{ $sectiononeid }}" />		
									<div class="form-group row">
										<label for="left_item_type" class="col-md-4 col-form-label text-md-left">{{ __('Top Left Side Image') }}</label>	
										<div class="col-md-10">
										<select id="left_item_type" name="left_item_type" class="form-control" autofocus>
										<option value="image" <?=($sectiononedata->left_item_type=="image") ? " selected" : "" ?> >Image/Gif</option>
												<option value="youtube" <?=($sectiononedata->left_item_type=="youtube") ? " selected" : "" ?>>Youtube</option>
												<option value="video" <?=($sectiononedata->left_item_type=="video") ?" selected" : "" ?>>Video upload</option>
												<option value="book" <?=($sectiononedata->left_item_type=="book") ?" selected" : "" ?>>Book Images</option>
												<option value="flip" <?=($sectiononedata->left_item_type=="flip") ?" selected" : "" ?>>Flip Images</option>
										</select>	
										</div>		
									</div>	
									<div class=" row left-image">										
										<div class="col-md-10 ">
											<input type="file" name="img_left[]"  multiple  class="form-control imageitem" />
										</div>														
									</div>	
									<div class="form-group row left-link hide">	
										<div class="col-md-10">
											<input type="text" name="img_left" placeholder="Please enter youtube link" class="form-control"	/>
										</div>
									</div>	
									<input type="hidden" name="img_left_isAvailable" value="{{$sectiononedata->img_left}}" />
									<?php 
										$img = '';
										if($sectiononedata->left_item_type == 'image' || $sectiononedata->left_item_type == 'video'){
											$temp = json_decode($sectiononedata->img_left);
											if(isset($temp[0])){
												$img = $temp[0];
											}
										}
										else{
											$img = $sectiononedata->img_left;
										} ?>
									<?=getimagehtmlbytype($sectiononedata->left_item_type,$img)?>
									<div class="form-group row">
										<label for="left_item_type" class="col-md-4 col-form-label text-md-left">{{ __('Top Right Side Image') }}</label>	
										<div class="col-md-10">
											<select id="right_item_type" name="right_item_type" class="form-control">
												<option value="image" <?=($sectiononedata->right_item_type=="image") ? " selected" : "" ?> >Image/Gif</option>
												<option value="youtube" <?=($sectiononedata->right_item_type=="youtube") ? " selected" : "" ?>>Youtube</option>
												<option value="video" <?=($sectiononedata->right_item_type=="video") ?" selected" : "" ?>>Video upload</option>
												<option value="book" <?=($sectiononedata->right_item_type=="book") ?" selected" : "" ?>>Book Images</option>
												<option value="flip" <?=($sectiononedata->right_item_type=="flip") ?" selected" : "" ?>>Flip Images</option>
											</select>	
										</div>	
									</div>	
									<div class="row right-image">										
										<div class="col-md-10">
											<input type="file" name="img_right[]"  multiple  class="form-control imageitem" />
										</div>	
									</div>			
									<div class="form-group row right-link hide">
										<div class="col-md-10">
											<input type="text" name="img_right" placeholder="Please enter youtube link" class="form-control"	/>
										</div>	
									</div>	
									<input type="hidden" name="img_right_isAvailable" value="{{$sectiononedata->img_left}}" />
									<?php 
										$img = '';
										if($sectiononedata->right_item_type == 'image' || $sectiononedata->right_item_type == 'video'){
											$temp = json_decode($sectiononedata->img_right);
											if(isset($temp[0])){
												$img = $temp[0];
											}
										}
										else{
											$img = $sectiononedata->img_right;
										} ?>
									<?=getimagehtmlbytype($sectiononedata->right_item_type,$img)?>

									<!-- <div class="form-group row">
										<div class="fileinput fileinput-new text-left" data-provides="fileinput">
											<div class="fileinput-new thumbnail img-raised">							
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail img-raised"><img src="{{ asset('images/home') }}/{{ $sectiononedata->img_left }}" alt="" /></div>
											<div>
												<span class="btn btn-raised btn-round btn-default btn-file">
													<span class="fileinput-new">Select Left Image</span>								
													<input type="file" name="img_left" />
												</span>							
											</div>
										</div>
									</div>				 -->
									<!-- <div class="form-group row">
										<div class="fileinput fileinput-new text-left" data-provides="fileinput">
											<div class="fileinput-new thumbnail img-raised">							
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail img-raised"><img src="{{ asset('images/home') }}/{{ $sectiononedata->img_right }}" alt="" /></div>
											<div>
												<span class="btn btn-raised btn-round btn-default btn-file">
													<span class="fileinput-new">Select Right Image</span>								
													<input type="file" name="img_right" />
												</span>							
											</div>
										</div>
									</div>				 -->
									<div class="form-group row">
										<label for="middle_section_title" class="col-md-4 col-form-label text-md-left">{{ __('Middle Section Title') }}</label>					
										<div class="col-md-10">
											<input id="middle_section_title" type="text" class="form-control @error('middle_section_title') is-invalid @enderror" name="middle_section_title" value="{{ $sectiononedata->middle_section_title }}" required autocomplete="middle_section_title" autofocus>						
											@error('middle_section_title')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group row">
										<label for="middle_section_description" class="col-md-4 col-form-label text-md-left">{{ __('Middle Section Description') }}</label>					
										<div class="col-md-10">
											<textarea class="form-control" id="middle_section_description" name="middle_section_description">{{ $sectiononedata->middle_section_description }}</textarea>
											@error('middle_section_description')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group row">
										<label for="middle_section_view_more" class="col-md-4 col-form-label text-md-left">{{ __('View More Title Link') }}</label>						
										<div class="col-md-10">
											<input id="middle_section_view_more" type="text" class="form-control" name="middle_section_view_more" value="{{ $sectiononedata->middle_section_view_more }}" required autocomplete="middle_section_view_more" autofocus>						
										</div>
									</div>				
									
									<div class="form-group row">
										<label for="project_service_title" class="col-md-4 col-form-label text-md-left">{{ __('Service Title') }}</label>					
										<div class="col-md-10">
											<input id="project_service_title" type="text" class="form-control" name="project_service_title" value="{{ $sectiononedata->project_service_title }}"  autocomplete="project_service_title" autofocus>												
										</div>
									</div>
									<div class="form-group row">
										<label for="project_main_title" class="col-md-4 col-form-label text-md-left">{{ __('Project Title') }}</label>					
										<div class="col-md-10">
											<input id="project_main_title" type="text" class="form-control" name="project_main_title" value="{{ $sectiononedata->project_main_title }}"  autocomplete="project_main_title" autofocus>												
										</div>
									</div>						
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="lang">
											<option value="en" @if($sectiononedata->language == "en") selected @endif>en</option>
											<option value="nl" @if($sectiononedata->language == "nl") selected @endif>nl</option>
										</select>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Update') }}
											</button>
											<a class="btn btn-primary" href="{{ url('front/home') }}">Cancel</a>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
	CKEDITOR.editorConfig = function (config) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;
    
	
	};
	CKEDITOR.replace('middle_section_description');
</script>
<script>
	jQuery("#left_item_type").click(function() {
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(".left-image").removeClass("hide");
			jQuery(".left-link").addClass("hide");
		}
		else{
			jQuery(".left-link").removeClass("hide"); 
			jQuery(".left-image").addClass("hide");
		}
	});
	jQuery("#right_item_type").click(function() {
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(".right-image").removeClass("hide");
			jQuery(".right-link").addClass("hide");
		}
		else{
			jQuery(".right-link").removeClass("hide"); 
			jQuery(".right-image").addClass("hide");
		}
	});
</script>

@endsection