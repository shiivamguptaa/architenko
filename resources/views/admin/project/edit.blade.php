@extends('layouts.auth')
@section('content')
<!-- <div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<h3> Edit Project</h3>
			<form method="POST" action="{{ url('project/update') }}" enctype="multipart/form-data">
				@csrf				
				<input id="id" type="hidden" class="form-control" name="id" value="{{$id}}">
		
				<div class="form-group row">
					<label for="title" class="col-md-4 col-form-label text-md-left">{{ __('Title') }}</label>
					
					<div class="col-md-6">
						<input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{$projectdata->title}}" required>
						
						@error('title')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
				</div>
				
				<div class="form-group row">
					<label for="subtitle" class="col-md-4 col-form-label text-md-left">{{ __('Subtitle') }}</label>
					
					<div class="col-md-6">
						<input id="subtitle" type="subtitle" class="form-control @error('subtitle') is-invalid @enderror" name="subtitle" value="{{$projectdata->subtitle}}" required>
						
						@error('subtitle')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
				</div>
				
				<div class="form-group row">
					<label for="descriptionleft" class="col-md-4 col-form-label text-md-left">{{ __('Left Description') }}</label>
					
					<div class="col-md-10" style="margin-top:20px;">
						<textarea class="form-control" id="descriptionleft" name="descriptionleft" required>{{$projectdata->descriptionleft}}</textarea>      
						
					</div>
				</div>
				
				<div class="form-group row">
					<label for="descriptionright" class="col-md-4 col-form-label text-md-left">{{ __('Right Description') }}</label>
					
					<div class="col-md-10" style="margin-top:20px;">
						<textarea class="form-control" id="descriptionright" name="descriptionright" required>{{$projectdata->descriptionright}}</textarea>      
						
					</div>
				</div>
				
				
				<div class="form-group col-md-3" style="display:none">
					<label for="inputState">Select Home Page Display</label>
					<select name="ishome" class="form-control">      
						<option value="image" @if($projectdata->ishome == "image") selected @endif>Image/Gif</option>
						<option value="youtube" @if($projectdata->ishome == "youtube") selected @endif>Youtube</option>
						<option value="video" @if($projectdata->ishome == "video") selected @endif>Video</option>
						<option value="book" @if($projectdata->ishome == "book") selected @endif>Book</option>
						<option value="flip" @if($projectdata->ishome == "flip") selected @endif>Flip</option>
					</select>
				</div>	
				
				<table class="table table-bordered" id="dynamicTable">  
					<h4>Project Header</h4>
					<tr>
						<th>Type</th>
						<th>Data</th>
						<th>Title</th>
						<th>Display Order</th>
						<th>Action</th>
					</tr>
					<?php  $index=0; ?>
					@foreach($projectdetails as $project)
					<tr>  
						<td>
							<input id="projectid" type="hidden" class="form-control" name="addmore[{{$index}}][projectId]" value="{{$project->id}}">
							<input id="projectid" type="hidden" class="form-control" name="projectId[]" value="{{$project->id}}">

							<select class="projecttype form-control" id="projecttype{{$index}}" name="addmore[{{$index}}][type]" class="form-control" data-id="{{$index}}">
								<option value="" selected disabled>Select Type</option>
								<option value="image" <?=($project->type=="image") ? 'selected' : '' ?> >Image/Gif</option>
								<option value="youtube" <?=($project->type=="youtube") ? 'selected' : '' ?>>Youtube</option>
								<option value="video" <?=($project->type=="video") ? 'selected' : '' ?>>Video upload</option>
								<option value="book" <?=($project->type=="book") ? 'selected' : '' ?>>Book Images</option>
								<option value="flip" <?=($project->type=="flip") ? 'selected' : '' ?>>Flip Images</option>
							</select>
						</td>  
						<td class="imageshow" <?=($project->type!="image")? "style='display:none'": "" ?> >
							<input id="image{{$index}}" type="file" name="addmore[{{$index}}][image]" class="form-control" value="{{$project->datavalue}}" />
						</td>  
						<td class="youtubeshow" <?=($project->type!="youtube")? "style='display:none'": "" ?> >
							<input id="youtube{{$index}}" type="text" name="addmore[{{$index}}][youtube]" placeholder="https://www.youtube.com/embed/qUksnWaArko" class="form-control" value="{{$project->datavalue}}" />
						</td>  
						<td class="videoshow" <?=($project->type!="video")? "style='display:none'": "" ?> >
							<input id="video{{$index}}" type="file" name="addmore[{{$index}}][video]" class="form-control" value="{{$project->datavalue}}" />
						</td>  
						<td class="bookshow" <?=($project->type!="book")? "style='display:none'": "" ?> >
							<input id="book{{$index}}" type="file" name="addmore[{{$index}}][book][]" class="form-control" multiple value="{{$project->datavalue}}" />
						</td>  
						<td class="flipshow" <?=($project->type!="flip")? "style='display:none'": "" ?> >
							<input id="flip{{$index}}" type="file" name="addmore[{{$index}}][flip][]" class="form-control" multiple value="{{$project->datavalue}}" />
						</td>

						<td class="addmoretitle">
							<input id="moretitle{{$index}}" type="text" name="addmore[{{$index}}][moretitle]" class="form-control" placeholder="Enter Title" value="{{$project->moretitle}}" ></td>
						<td class="order">
							<input  id="display_order{{$index}}" type="number" name="addmore[{{$index}}][display_order]" class="form-control" min="0" placeholder="Enter order number"  value="{{$project->display_order}}"></td>
						@if($index==0)
						<td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
						@else
						<td><button type="button" class="btn btn-danger remove-tr">Remove</button></td>  
						@endif
						
					</tr>  <?php $index++; ?>
					@endforeach
					
				</table> 				
				

				<div class="form-group row">
					<label for="innerpagetitle" class="col-md-4 col-form-label text-md-left">{{ __('Inner Page Title') }}</label>
					<div class="col-md-6">
						<input id="innerpagetitle" type="text" class="form-control @error('innerpagetitle') is-invalid @enderror" name="innerpagetitle" value="{{$projectdata->innerpagetitle}}" required>
						@error('innerpagetitle')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
				</div>	
				<div class="col-md-12">
					<h3>Inner Page Image</h3>
					<table class="table table-bordered" id="innterPageDynamicTable">	
						<tr>
							<th>Description</th>
							<th>Data</th>
							<th>Title</th>
							<th>Display Order</th>
							<th>Action</th>
						</tr>	
						<?php  $innerPageIndex=0;?>
						@foreach($projectinnercontents as $projectinnercontent)	
						<tr>  												 
							<td>
								<input id="projectInnerContentid" type="hidden" class="form-control" name="innerPage[{{$innerPageIndex}}][projectInnerContentid]" value="{{$projectinnercontent->id}}">
							<input id="projectInnerContentid" type="hidden" class="form-control" name="projectInnerContentid[]" value="{{$projectinnercontent->id}}">
							
								<textarea class="form-control" id="innerPage[{{$innerPageIndex}}][description]" name="innerPage[{{$innerPageIndex}}][description]" required>{{$projectinnercontent->description}}</textarea> 
							</td>	
							<td>
								<input type="file" name="innerPage[{{$innerPageIndex}}][image]"  id="innerPage[{{$innerPageIndex}}][image]"  class="form-control"  />
							</td>	
							<td>
								<input type="text" value="{{$projectinnercontent->title}}" name="innerPage[{{$innerPageIndex}}][title]" class="form-control" placeholder="Enter Title" >
							</td>
							<td>
								<input type="number" value="{{$projectinnercontent->display_order}}" name="innerPage[{{$innerPageIndex}}][display_order]" class="form-control" min="0" placeholder="Enter order number" >
							</td>
							
							@if($innerPageIndex==0)
						<td><button type="button" name="addInnerPageImage" id="addInnerPageImage" class="btn btn-success">Add More</button></td>  
						@else
						<td><button type="button" name="remvoeInnerPageImage" id="remvoeInnerPageImage" class="btn btn btn-danger remvoeInnerPageImage">Remove</button></td>  
						@endif 
						</tr>  <?php $innerPageIndex++; ?>
						@endforeach							
						
					</table>
				</div>
				<div class="form-group col-md-3">
					<label for="inputState">Select Language</label>
					<select id="inputState" class="form-control" name="lang">
						@if(count($languages)>0)
						@foreach($languages as $language)
						<option value="{{$language->name}}" @if($projectdata->language == $language->name) selected @endif>{{$language->name}}</option>
						@endforeach						  
						@endif
					</select>
				</div>	
				<div class="form-group row">
					<hr><hr>
				</div>
				<div class="form-group row mb-0">
					<div class="col-md-6">
						<button type="submit" class="btn btn-primary">
							{{ __('Update') }}
						</button>
						<a class="btn btn-primary" href="{{ url('project') }}">Cancel</a>
					</div>
				</div>
			</form>			
		</div>
	</div>
	@include('layouts.authfooter')	
</div> -->


<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Edit New Project</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('project/update') }}" enctype="multipart/form-data">
									@csrf				
									<input id="id" type="hidden" class="form-control" name="id" value="{{$id}}">
									<div class="form-group col-md-3">
										<label for="select_lang">Select Language</label>
										<select id="select_lang" class="form-control" name="lang">
											@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}"  @if($projectdata->language == $language->name) selected @endif>{{$language->name}}</option>
											@endforeach						  
											@endif
										</select>
									</div>	
									
									<div class="form-group col-md-3">
										<label for="select_cat">Select Catagory</label>
										<select id="select_cat" class="form-control" name="catagory_id" required="">
											<option value="" selected hidden disabled>Select Catagory</option>
											@if(count($catagoryList)>0)
											@foreach($catagoryList as $catagory)
											<option value="{{$catagory->id}}" @if($catagory->id==$projectdata->catagory_id) selected @endif  >{{$catagory->catagory}}</option>
											@endforeach						  
											@endif
										</select>
									</div>
									<div class="form-group row">
										<label for="title" class="col-md-4 col-form-label text-md-left">{{ __('Title') }}</label>
										<div class="col-md-6">
											<input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{$projectdata->title}}" required>
											@error('title')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group row">
										<label for="subtitle" class="col-md-4 col-form-label text-md-left">{{ __('Subtitle') }}</label>
										
										<div class="col-md-6">
											<input id="subtitle" type="subtitle" class="form-control @error('subtitle') is-invalid @enderror" name="subtitle" value="{{$projectdata->subtitle}}" required>
											
											@error('subtitle')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>									
									<div class="form-group row">
										<label for="descriptionleft" class="col-md-4 col-form-label text-md-left">{{ __('Left Description') }}</label>
										
										<div class="col-md-10" style="margin-top:20px;">
											<textarea class="form-control" id="descriptionleft" name="descriptionleft" required>{{$projectdata->descriptionleft}}</textarea>      
											
										</div>
									</div>									
									<div class="form-group row">
										<label for="descriptionright" class="col-md-4 col-form-label text-md-left">{{ __('Right Description') }}</label>
										
										<div class="col-md-10" style="margin-top:20px;">
											<textarea class="form-control" id="descriptionright" name="descriptionright" required>{{$projectdata->descriptionright}}</textarea>      
											
										</div>
									</div>
									<div class="form-group col-md-3" style="display:none">
										<label for="inputState">Select Home Page Display</label>
										<select name="ishome" class="form-control">      
											<option value="image" @if($projectdata->ishome == "image") selected @endif>Image/Gif</option>
											<option value="youtube" @if($projectdata->ishome == "youtube") selected @endif>Youtube</option>
											<option value="video" @if($projectdata->ishome == "video") selected @endif>Video</option>
											<option value="book" @if($projectdata->ishome == "book") selected @endif>Book</option>
											<option value="flip" @if($projectdata->ishome == "flip") selected @endif>Flip</option>
										</select>
									</div>					
									<table class="table table-bordered" id="dynamicTable">  
										<h4>Project Header</h4>
										<tr>
											<th>Type</th>
											<th>Data</th>
											<th>Title</th>
											<th>Display Order</th>
											<th>Action</th>
										</tr>
										<?php  $index=0; ?>
										@foreach($projectdetails as $project)
										<tr>  
											<td>
												<input id="projectid" type="hidden" class="form-control" name="addmore[{{$index}}][projectId]" value="{{$project->id}}">
												<input id="projectid" type="hidden" class="form-control" name="projectId[]" value="{{$project->id}}">

												<select class="projecttype form-control" id="projecttype{{$index}}" name="addmore[{{$index}}][type]" class="form-control" data-id="{{$index}}">
													<option value="" selected disabled>Select Type</option>
													<option value="image" <?=($project->type=="image") ? 'selected' : '' ?> >Image/Gif</option>
													<option value="youtube" <?=($project->type=="youtube") ? 'selected' : '' ?>>Youtube</option>
													<option value="video" <?=($project->type=="video") ? 'selected' : '' ?>>Video upload</option>
													<option value="book" <?=($project->type=="book") ? 'selected' : '' ?>>Book Images</option>
													<option value="flip" <?=($project->type=="flip") ? 'selected' : '' ?>>Flip Images</option>
												</select>
											</td>  
											<td class="imageshow" <?=($project->type!="image")? "style='display:none'": "" ?> >
												<input id="image{{$index}}" type="file" name="addmore[{{$index}}][image]" class="form-control" value="{{$project->datavalue}}" />
												<?php 
													if ($project->type == "image") {
														echo getimagehtmlbytype($project->type,'images/project/'.$project->datavalue);	
													}
												?>
											</td>  
											<td class="youtubeshow" <?=($project->type!="youtube")? "style='display:none'": "" ?> >
												<input id="youtube{{$index}}" type="text" name="addmore[{{$index}}][youtube]" placeholder="https://www.youtube.com/embed/qUksnWaArko" class="form-control" value="{{$project->datavalue}}" />
												<?php 
													if ($project->type == "youtube") {
														echo getimagehtmlbytype($project->type,$project->datavalue);	
													}
												?>
											</td>  
											<td class="videoshow" <?=($project->type!="video")? "style='display:none'": "" ?> >
												<input id="video{{$index}}" type="file" name="addmore[{{$index}}][video]" class="form-control" value="{{$project->datavalue}}" />
												<?php 
													if ($project->type == "video") {
														echo getimagehtmlbytype($project->type,'images/project/'.$project->datavalue);	
													}
												?>
											</td>  
											<td class="bookshow" <?=($project->type!="book")? "style='display:none'": "" ?> >
												<input id="book{{$index}}" type="file" name="addmore[{{$index}}][book][]" class="form-control" multiple value="{{$project->datavalue}}" />
												<?php 
													if ($project->type == "book") {
														$temp_1 = json_decode($project->datavalue);
														if(!empty($temp_1)){
															$temp_2 = array();
															foreach ($temp_1 as $value) {
																$temp_2[] = 'images/project/'.$value;
															}
															echo getimagehtmlbytype($project->type,json_encode($temp_2));	
														}
													}
												?>
											</td>  
											<td class="flipshow" <?=($project->type!="flip")? "style='display:none'": "" ?> >
												<input id="flip{{$index}}" type="file" name="addmore[{{$index}}][flip][]" class="form-control" multiple value="{{$project->datavalue}}" />
												<?php 
													if ($project->type == "flip") {
														$temp_3 = json_decode($project->datavalue);
														if(!empty($temp_3)){
															$temp_4 = array();
															foreach ($temp_3 as $value) {
																$temp_4[] = 'images/project/'.$value;
															}
															echo getimagehtmlbytype($project->type,json_encode($temp_4));	
														}
													}
												?>
											</td>

											<td class="addmoretitle">
												<input id="moretitle{{$index}}" type="text" name="addmore[{{$index}}][moretitle]" class="form-control" placeholder="Enter Title" value="{{$project->moretitle}}" ></td>
											<td class="order">
												<input  id="display_order{{$index}}" type="number" name="addmore[{{$index}}][display_order]" class="form-control" min="0" placeholder="Enter order number"  value="{{$project->display_order}}"></td>
											@if($index==0)
											<td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
											@else
											<td><button type="button" class="btn btn-danger remove-tr">Remove</button></td>  
											@endif
											
										</tr>  <?php $index++; ?>
										@endforeach										
									</table> 
									<div class="form-group row">
										<label for="innerpagetitle" class="col-md-4 col-form-label text-md-left">{{ __('Inner Page Title') }}</label>
										<div class="col-md-6">
											<input id="innerpagetitle" type="text" class="form-control @error('innerpagetitle') is-invalid @enderror" name="innerpagetitle" value="{{$projectdata->innerpagetitle}}" required>
											@error('innerpagetitle')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>	
									<div class="col-md-12">
										<h3>Inner Page Image</h3>
										<table class="table table-bordered" id="innterPageDynamicTable">	
											<tr>
												<th>Description</th>
												<th>Data</th>
												<th>Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>	
											<?php  $innerPageIndex=0;?>
											@foreach($projectinnercontents as $projectinnercontent)	
											<tr>  												 
												<td>
													<input id="projectInnerContentid" type="hidden" class="form-control" name="innerPage[{{$innerPageIndex}}][projectInnerContentid]" value="{{$projectinnercontent->id}}">
												<input id="projectInnerContentid" type="hidden" class="form-control" name="projectInnerContentid[]" value="{{$projectinnercontent->id}}">
												
													<textarea class="form-control" id="innerPage[{{$innerPageIndex}}][description]" name="innerPage[{{$innerPageIndex}}][description]" required>{{$projectinnercontent->description}}</textarea> 
												</td>	
												<td>
													<input type="file" name="innerPage[{{$innerPageIndex}}][image]"  id="innerPage[{{$innerPageIndex}}][image]"  class="form-control"  />
													<?=getimagehtmlbytype('image','images/project/'.$projectinnercontent->image)?>
												</td>	
												<td>
													<input type="text" value="{{$projectinnercontent->title}}" name="innerPage[{{$innerPageIndex}}][title]" class="form-control" placeholder="Enter Title" required="">
												</td>
												<td>
													<input type="number" value="{{$projectinnercontent->display_order}}" name="innerPage[{{$innerPageIndex}}][display_order]" class="form-control" min="0" placeholder="Enter order number" required="">
												</td>
												
												@if($innerPageIndex==0)
											<td><button type="button" name="addInnerPageImage" id="addInnerPageImage" class="btn btn-success">Add More</button></td>  
											@else
											<td><button type="button" name="remvoeInnerPageImage" id="remvoeInnerPageImage" class="btn btn btn-danger remvoeInnerPageImage">Remove</button></td>  
											@endif 
											</tr>  <?php $innerPageIndex++; ?>
											@endforeach							
											
										</table>
									</div>
									<div class="form-group row">
										<label for="pdf" class="col-md-12 col-form-label text-md-left">{{ __('Download Text') }}</label>
										<div class="col-md-12">
											<input  type="text" class="form-control" name="downloadText" required  value="{{$projectdata->download_text}}" />
										</div>
									</div>
									<div class="form-group row">
										<label for="pdf" class="col-md-12 col-form-label text-md-left">{{ __('PDF') }}</label>
										<div class="col-md-12">
											<input id="pdf" type="file" class="form-control" name="pdf" />
										</div>
									</div>		
									<div class="form-group row">
										<hr><hr>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Update') }}
											</button>
											<a class="btn btn-primary" href="{{ url('project') }}">Cancel</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
	CKEDITOR.editorConfig = function (config) {
	    config.language = 'es';
	    config.uiColor = '#F7B42C';
	    config.height = 300;
	    config.toolbarCanCollapse = true;
	};
	CKEDITOR.replace('descriptionleft');
	CKEDITOR.replace('descriptionright');	
	function get_catagory(lang){
		$.ajax({
			url:'{{ route("p_catagory_by_name") }}',
			type:'POST',
			data:{lang:lang, _token:'{{csrf_token()}}'},
			success:function(result) {
				$('#select_cat').html(result.data);
			}
		});	
	}
	$( document ).ready(function() {
		let i = "<?php echo !empty($projectdetails) ? count($projectdetails->toArray()) : 0; ?>";		
		let innerPage = "<?php echo !empty($projectinnercontents) ? count($projectinnercontents->toArray()) : 0; ?>";			
		for (let j = 0; j < innerPage; j++) {
			let description = "innerPage["+j+"][description]";
			console.log(description);
			CKEDITOR.replace(description);
		}
		// let lang = $('#select_lang').val();
	 //    get_catagory(lang);
	    $('#select_lang').on('change',function() {
			var lang = $(this).val();
			get_catagory(lang);
		});	
		jQuery(document).on('change', '.projecttype', function(){			
			let row_number = $(this).data('id');			
			let selectedval = $(this).val();
			if(selectedval == 'image'){
				$("#image"+row_number).parent().css("display","table-cell");
				$("#youtube"+row_number).parent().parent().css("display","none");
				$("#video"+row_number).parent().css("display","none");
				$("#book"+row_number).parent().css("display","none");
				$("#flip"+row_number).parent().css("display","none");
				$("#moretitle"+row_number).parent().parent().css("display","table-cell");
				$("#display_order"+row_number).parent().parent().css("display","table-cell");
			}
			else if(selectedval == 'youtube'){
				$("#image"+row_number).parent().css("display","none");
				$("#youtube"+row_number).parent().parent().css("display","table-cell");
				$("#video"+row_number).parent().css("display","none");
				$("#book"+row_number).parent().css("display","none");
				$("#flip"+row_number).parent().css("display","none");
				$("#moretitle"+row_number).parent().parent().css("display","table-cell");
				$("#display_order"+row_number).parent().parent().css("display","table-cell");
			}
			else if(selectedval == 'video'){
				$("#image"+row_number).parent().css("display","none");
				$("#youtube"+row_number).parent().parent().css("display","none");
				$("#video"+row_number).parent().css("display","table-cell");
				$("#book"+row_number).parent().css("display","none");
				$("#flip"+row_number).parent().css("display","none");
				$("#moretitle"+row_number).parent().parent().css("display","table-cell");
				$("#display_order"+row_number).parent().parent().css("display","table-cell");
			}
			else if(selectedval == 'book'){
				$("#image"+row_number).parent().css("display","none");
				$("#youtube"+row_number).parent().parent().css("display","none");
				$("#video"+row_number).parent().css("display","none");
				$("#book"+row_number).parent().css("display","table-cell");
				$("#flip"+row_number).parent().css("display","none");
				$("#moretitle"+row_number).parent().parent().css("display","table-cell");
				$("#display_order"+row_number).parent().parent().css("display","table-cell");
			}
			else if(selectedval == 'flip'){
				$("#image"+row_number).parent().css("display","none");
				$("#youtube"+row_number).parent().parent().css("display","none");
				$("#video"+row_number).parent().css("display","none");
				$("#book"+row_number).parent().css("display","none");
				$("#flip"+row_number).parent().css("display","table-cell");
				$("#moretitle"+row_number).parent().parent().css("display","table-cell");
				$("#display_order"+row_number).parent().parent().css("display","table-cell");
			}
			
		});
		jQuery("#addInnerPageImage").click(function(){	
			++innerPage;	
			jQuery("#innterPageDynamicTable").append(`<tr>  												 
				<td>
					<textarea class="form-control" id="innerPage[${innerPage}][description]" name="innerPage[${innerPage}][description]" required></textarea> 
											</td>
				<td>
					<input type="file" name="innerPage[${innerPage}][image]" class="form-control" required/>
				</td>	
				<td>
					<input type="text" name="innerPage[${innerPage}][title]" class="form-control"  placeholder="Enter Title" required>
				</td>
				<td>
					<input type="number" name="innerPage[${innerPage}][display_order]" class="form-control" min="0" placeholder="Enter order number" >
				</td>
				<td>
					<button type="button" name="remvoeInnerPageImage" id="remvoeInnerPageImage" class="btn btn btn-danger remvoeInnerPageImage">Remove</button>
				</td>  
			</tr>`);
			let description = 'innerPage['+innerPage+'][description]';
			CKEDITOR.replace(description);
		});
		jQuery("#add").click(function(){
			++i;
			jQuery("#dynamicTable").append(
				`<tr>
					<td>
						<select data-id='${i}' class="projecttype" id="projecttype${i}" name="addmore[${i}][type]" class="form-control">
							<option value="" selected disabled>Select Type</option>
							<option value="image">Image/Gif</option>
							<option value="youtube">Youtube</option>
							<option value="video">Video Upload</option>
							<option value="book">Book Images</option>
							<option value="flip">Flip Image</option>
						</select>
					</td>
					<td class="imageshow" style="display: none;">
						<input  id="image${i}" type="file" name="addmore[${i}][image]" class="form-control" />
					</td>
					<td class="youtubeshow" style="display: none;">
						<input id="youtube${i}" type="text" name="addmore[${i}][youtube]" placeholder="https://www.youtube.com/embed/qUksnWaArko" class="form-control" />
					</td>
					<td class="videoshow" style="display: none;">
						<input id="video${i}"  type="file" name="addmore[${i}][video]" class="form-control" />
					</td>
					<td class="bookshow" style="display: none;">
						<input  id="book${i}"  type="file" name="addmore[${i}][book][]" class="form-control" multiple>
					</td>
					<td class="flipshow" style="display: none;">
						<input id="flip${i}" type="file" name="addmore[${i}][flip][]" class="form-control" multiple>
					</td>
					<td class="addmoretitle" style="display: none;">
						<input id="moretitle${i}" type="text" name="addmore[${i}][moretitle]" class="form-control" placeholder="Enter Title">
					</td>
					<td class="order" style="display: none;">
						<input  id="display_order${i}" type="number" name="addmore[${i}][display_order]" class="form-control" min="0" placeholder="Enter order number">
					</td>
					<td>
						<button type="button" class="btn btn-danger remove-tr">Remove</button>
					</td>
				</tr>`);			
		});	
		jQuery(document).on('click', '.remove-tr', function(){  
			$(this).parents('tr').remove();
		});  	
		jQuery(document).on('click', '.remvoeInnerPageImage', function(){  
			$(this).parents('tr').remove();
		});  	
	});
</script>
@endsection							