@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn"><a href="{{ url('project/create') }}" class="btn btn-success">Add New Project</a></p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Projects Listing</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-2 pull-right">
									<label for="select_lang">Select Language</label>
									<select id="select_lang" class="form-control" name="lang">
										<option value="" selected disabled>Select Language</option>
										@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}">{{$language->name}}</option>
											@endforeach						  
										@endif
									</select>
								</div>
							</div>
							<div class="table-responsive">
								<table id="dtOrderExample1" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#</th>
											<th>Catagory Name</th>
											<th>Title</th>
											<th>Image</th>
											<th>Orderby</th>
											<th style="width: 90px">Language</th>
											<th>Show in Home</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody id="projectList">										
										@if(count($projects) > 0)
										<?php $counter = 1; ?>
										@foreach($projects as $project)
										<tr>
											<td>{{$counter}}</td>
											<td>{{$project['catagory']}}</td>
											<td>{{$project['title']}}</td>
											@if($project['type'] == "image")
											<td><img src="{{asset('images/project/'.$project['datavalue'])}}" class="width-100"></td>
											@elseif($project['type'] == "book" || $project['type'] == "flip")
											<td><?php $bookimg = json_decode($project['datavalue']);
											foreach($bookimg as $key => $book):?>
											<img src="{{asset('images/project/'.$book)}}" class="width-100 <?php if($key == 0){echo 'active';}?>" alt=""> 
											<?php endforeach; ?>
											</td>
											@elseif($project['type'] == "youtube")
											<td>{{$project['datavalue']}}</td>
											@else
											<td>{{$project['datavalue']}}</td>
											@endif
													
											<td><input type="number" id="project{{$project['projectId']}}" class="orderby form-control" onfocusout="onChangeOrderBy({{$project['projectId']}})" value="{{$project['orderby']}}"  /></td>

											<td>{{$project['language']}}</td>
											<td class="text-center"><input type="checkbox" <?php if($project['is_home']==1){ echo "checked"; } ?>  id="checkbox{{$project['projectId']}}" onclick="showProjectInHome({{$project['projectId']}})" /></td>
											

											<td class="td-actions text-right">				
											<a href="{{ url('project/'.$project['projectId'].'/edit') }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>								
												<a  href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('project/'.$project['projectId'].'/delete') }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>
										</tr>
										<?php $counter++; ?>
										@endforeach										
										@endif										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>	
<script>
function onChangeOrderBy(id) {
	let orderby = $("#project"+id).val();
	$.ajax({
		url:"{{route('projectorderby')}}",
		headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
		data:{id:id,orderby:orderby},
		type:"POST",
		success:function(response){
			console.log(response);
		},
		error:function(error){	
			console.log(error);
		}
	});
}
function showProjectInHome(id) {
	let is_home = $("#checkbox"+id).is(':checked') ? 1 : 0;
	$.ajax({
		url:"{{route('showProjectInHome')}}",
		headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
		data:{id:id,is_home:is_home},
		type:"POST",
		success:function(response){
			console.log(response);
		},
		error:function(error){	
			console.log(error);
		}
	});
}

</script>
@endsection								
@section('extrascript')
<script type="text/javascript">
	let BASE_URL = '{{URL("/")}}';
    $(document).ready(function () {
      	$('#dtOrderExample1').DataTable({
      		"order": [[ 0, "asc" ]],
      		"autoWidth": false,
      		"columnDefs": [
		          { className: "td-actions text-right",orderable: false , "targets": [7] },
		          { orderable: false , "targets": [6] },
		          { orderable: false , "targets": [4] },
		          { orderable: false , "targets": [3] }
		        ]
      	});
      	$('.dataTables_length').addClass('bs-select');
      	var table = $('#dtOrderExample1').DataTable();
      	$(document).on('change','#select_lang',function(){
      		let lang = $(this).val();
      		$.ajax({
				url:"{{route('onChangeProjectLanguage')}}",
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				data:{lang:lang},
				type:"POST",
				success:function(response){
					table.clear().draw();
					$.each(response.data,function(key,value){
						let edit = BASE_URL+'/project/'+value.projectId+'/edit';
						let del = BASE_URL+'/project/'+value.projectId+'/delete';
						let img = ``;

						if(value.type == "image"){
						 	img = `<img src="${BASE_URL}/images/project/${value.datavalue}" class="width-100">`;
						}
						else if(value.type == "book" || value.type == "flip"){
							let temp = JSON.parse(value.datavalue);
							$.each(temp ,function(key,value1){
								img += `<img src="${BASE_URL}/images/project/${value1}" class="width-100 ${key == 0 ? 'active' : '' }" alt="">`;
							});
						}
						else if(value.type == "youtube"){
							img = value.datavalue;
						}
						else{
							img = value.datavalue;
						}
						


						let input = `<input type="number" id="project${value.projectId}" class="orderby form-control" onfocusout="onChangeOrderBy(${value.projectId})" value="${value.orderby}">`;
						let home = `<input type="checkbox" ${value.is_home ? 'checked' : ''} id="checkbox${value.projectId}" onclick="showProjectInHome(${value.projectId})" />`;
						let action = `<div class="td-actions text-right"><a href="${edit}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>								
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('${del}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a></div>`;
						table.row.add([key+1,value.catagory,value.title,img,input,value.language,home,action]).draw();
					});
				},
				error:function(error){	
					console.log(error);
				}
			});
      	});
      	
    });

</script>

@endsection