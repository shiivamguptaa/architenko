@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Add New Project</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('project/store') }}" enctype="multipart/form-data">
									@csrf	
									<div class="form-group col-md-3">
										<label for="select_lang">Select Language</label>
										<select id="select_lang" class="form-control" name="lang">
											@if(count($languages)>0)
												@foreach($languages as $language)
													<option value="{{$language->name}}">{{$language->name}}</option>
												@endforeach						  
											@endif
										</select>
									</div>	
									<div class="form-group col-md-3">
										<label for="select_cat">Select Catagory</label>
										<select id="select_cat" class="form-control" name="catagory_id" required="">
											<option value="" selected hidden disabled>Select Catagory</option>
										</select>
									</div>		
									<div class="form-group row">
										<label for="title" class="col-md-4 col-form-label text-md-left">{{ __('Title') }}</label>
										<div class="col-md-6">
											<input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required>
											@error('title')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group row">
										<label for="subtitle" class="col-md-4 col-form-label text-md-left">{{ __('Subtitle') }}</label>
										<div class="col-md-6">
											<input id="subtitle" type="subtitle" class="form-control @error('subtitle') is-invalid @enderror" name="subtitle" value="{{ old('subtitle') }}" required>
											@error('subtitle')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group row">
										<label for="descriptionleft" class="col-md-4 col-form-label text-md-left">{{ __('Left Description') }}</label>
										<div class="col-md-10" style="margin-top:20px;">
											<textarea class="form-control" id="descriptionleft" name="descriptionleft" required></textarea> 
										</div>
									</div>				
									<div class="form-group row">
										<label for="descriptionright" class="col-md-4 col-form-label text-md-left">{{ __('Right Description') }}</label>
										<div class="col-md-10" style="margin-top:20px;">
											<textarea class="form-control" id="descriptionright" name="descriptionright" required></textarea> 
										</div>
									</div>				 
									<div class="form-group col-md-3" style="display:none" >
										<label for="inputState">Select Home Page Display</label>
										<select name="ishome" class="form-control">      
											<option value="image">Image/Gif</option>
											<option value="youtube">Youtube</option>
											<option value="video">Video</option>
											<option value="book">Book</option>
											<option value="flip">Flip</option>
										</select>
									</div>
									<div class="col-md-12">
										<table class="table table-bordered" id="dynamicTable">
											<h3>Project Header</h3>
											<tr>
												<th>Type</th>
												<th>Data</th>
												<th>Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>
											<tr>  
												<td>
													<select id="projecttype" name="addmore[0][type]" class="form-control" required="">
														<option value="">Select Type</option>
														<option value="image">Image/Gif</option>
														<option value="youtube">Youtube</option>
														<option value="video">Video upload</option>
														<option value="book">Book Images</option>
														<option value="flip">Flip Images</option>
													</select>
												</td>  
												<td id="imageshow"><input type="file" name="addmore[0][image]" class="form-control" /></td>  
												<td id="youtubeshow"><input type="text" name="addmore[0][youtube]" placeholder="https://www.youtube.com/embed/qUksnWaArko" class="form-control" /></td>  
												<td id="videoshow"><input type="file" name="addmore[0][video]" class="form-control" /></td>  
												<td id="bookshow"><input type="file" name="addmore[0][book][]" class="form-control" multiple></td>  
												<td id="flipshow"><input type="file" name="addmore[0][flip][]" class="form-control" multiple></td>
												<td id="addmoretitle"><input type="text" name="addmore[0][moretitle]" class="form-control" placeholder="Enter Title"></td>
												<td id="order"><input type="number" name="addmore[0][display_order]" class="form-control" min="0" placeholder="Enter order number" ></td>
												<td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
											</tr>  
										</table>
									</div>
									
									<div class="form-group row">
										<label for="innerpagetitle" class="col-md-4 col-form-label text-md-left">{{ __('Inner Page Title') }}</label>
										<div class="col-md-6">
											<input id="innerpagetitle" type="text" class="form-control @error('innerpagetitle') is-invalid @enderror" name="innerpagetitle" value="{{ old('innerpagetitle') }}" required>
											@error('innerpagetitle')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>	
									<div class="col-md-12">
										<h3>Inner Page Image</h3>
										<table class="table table-bordered" id="innterPageDynamicTable">	
											<tr>
												<th>Description</th>
												<th>Data</th>
												<th>Title</th>
												<th>Display Order</th>
												<th>Action</th>
											</tr>		
											<tr>  												 
												<td>
													<textarea class="form-control" id="innerPage[0][description]" name="innerPage[0][description]" required></textarea> 
												</td>	
												<td>
													<input type="file" name="innerPage[0][image]" class="form-control" required="" />
												</td>	
												<td>
													<input type="text" name="innerPage[0][title]" class="form-control" placeholder="Enter Title" required>
												</td>
												<td>
													<input type="number" name="innerPage[0][display_order]" class="form-control" min="0" placeholder="Enter order number" required></td>
												<td><button type="button" name="addInnerPageImage" id="addInnerPageImage" class="btn btn-success">Add More</button></td>  
											</tr>  
										</table>
									</div>
									<div class="form-group row">
										<label for="pdf" class="col-md-12 col-form-label text-md-left">{{ __('Download Text') }}</label>
										<div class="col-md-12">
											<input  type="text" class="form-control" name="downloadText" required />
										</div>
									</div>
									<div class="form-group row">
										<label for="pdf" class="col-md-12 col-form-label text-md-left">{{ __('PDF') }}</label>
										<div class="col-md-12">
											<input id="pdf" type="file" class="form-control" name="pdf" required />
										</div>
									</div>									
									<div class="form-group row">
										<hr><hr>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
<script>
	CKEDITOR.editorConfig = function (config) {
		config.language = 'es';
		config.uiColor = '#F7B42C';
		config.height = 300;
		config.toolbarCanCollapse = true;		
	};
	CKEDITOR.replace('descriptionleft');
	CKEDITOR.replace('descriptionright');
	CKEDITOR.replace('innerPage[0][description]');	
	var i = 0;
	let innerPage=0;	
	function get_catagory(lang){
		$.ajax({
			url:'{{ route("p_catagory_by_name") }}',
			type:'POST',
			data:{lang:lang, _token:'{{csrf_token()}}'},
			success:function(result){
				$('#select_cat').html(result.data);
			}
		});	
	}
	$(document).ready(function() {
		var lang = $('#select_lang').val();
	    get_catagory(lang);
	});
	$('#select_lang').on('change',function(){
		var lang = $(this).val();
		get_catagory(lang);
	});
	jQuery("#add").click(function(){
		++i;
		jQuery("#dynamicTable").append('<tr><td><select id="projecttype'+i+'" name="addmore['+i+'][type]" class="form-control"><option value="">Select Type</option><option value="image">Image</option><option value="youtube">Youtube</option><option value="video">Video Upload</option><option value="book">Book Images</option><option value="flip">Flip Image</option></select></td><td id="imageshow'+i+'"><input type="file" name="addmore['+i+'][image]" class="form-control" /></td><td id="youtubeshow'+i+'"><input type="text" name="addmore['+i+'][youtube]" placeholder="https://www.youtube.com/embed/qUksnWaArko" class="form-control" /></td><td id="videoshow'+i+'"><input type="file" name="addmore['+i+'][video]" class="form-control" /></td><td id="bookshow'+i+'"><input type="file" name="addmore['+i+'][book][]" class="form-control" multiple></td><td id="flipshow'+i+'"><input type="file" name="addmore['+i+'][flip][]" class="form-control" multiple></td><td id="addmoretitle'+i+'"><input type="text" name="addmore['+i+'][moretitle]" class="form-control" placeholder="Enter Title" required> </td><td id="order'+i+'"><input type="number" name="addmore['+i+'][display_order]" class="form-control" min="0" required placeholder="Enter order number"></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
		hidevaluesmore(i);
	});
	jQuery("#addInnerPageImage").click(function(){	
	++innerPage;	
		jQuery("#innterPageDynamicTable").append(`<tr>  												 
					<td>
						<textarea class="form-control" id="innerPage[${innerPage}][description]" name="innerPage[${innerPage}][description]" required></textarea> 
												</td>
					<td>
						<input type="file" name="innerPage[${innerPage}][image]" class="form-control" required/>
					</td>	
					<td>
						<input type="text" name="innerPage[${innerPage}][title]" class="form-control" required placeholder="Enter Title" required>
					</td>
					<td>
						<input type="number" name="innerPage[${innerPage}][display_order]" class="form-control" min="0" placeholder="Enter order number" required>
					</td>
					<td>
						<button type="button" name="remvoeInnerPageImage" id="remvoeInnerPageImage" class="btn btn btn-danger remvoeInnerPageImage">Remove</button>
					</td>  
				</tr>`);
		let description = 'innerPage['+innerPage+'][description]';
			CKEDITOR.replace(description);
	});
	
    jQuery(document).on('click', '.remove-tr', function(){  
		$(this).parents('tr').remove();
	});  
	 jQuery(document).on('click', '.remvoeInnerPageImage', function(){  
		$(this).parents('tr').remove();
	});  
	
	$(document).ready(function() {
		hidevalues();
	});
	
	function hidevalues(){ 
		$("#imageshow").hide();
		$("#youtubeshow").hide();
		$("#videoshow").hide();
		$("#bookshow").hide();
		$("#flipshow").hide();
		$("#addmoretitle").hide();
		$("#order").hide();
	}
	
	function hidevaluesmore(i){ 
		$("#imageshow"+i).hide();
		$("#youtubeshow"+i).hide();
		$("#videoshow"+i).hide();
		$("#bookshow"+i).hide();
		$("#flipshow"+i).hide();
		$("#addmoretitle"+i).hide();
		$("#order"+i).hide();
		$("#projecttype"+i).change(function(){
			$("#imageshow"+i).hide();
			$("#youtubeshow"+i).hide();
			$("#videoshow"+i).hide();
			$("#bookshow"+i).hide();
			$("#flipshow"+i).hide();
			$("#addmoretitle"+i).hide();
			$("#order"+i).hide();
			
			var selectedval = $(this).val();
			if(selectedval == 'image'){
				$("#imageshow"+i).show();
				$("#addmoretitle"+i).show();
				$("#order"+i).show();
				$("#youtubeshow"+i).hide();
				$("#videoshow"+i).hide();
				$("#bookshow"+i).hide();
				$("#flipshow"+i).hide();
			}
			else if(selectedval == 'youtube'){
				$("#imageshow"+i).hide();
				$("#youtubeshow"+i).show();
				$("#addmoretitle"+i).show();
				$("#order"+i).show();
				$("#videoshow"+i).hide();
				$("#bookshow"+i).hide();
				$("#flipshow"+i).hide(); 
			}
			else if(selectedval == 'video'){
				$("#imageshow"+i).hide();
				$("#youtubeshow"+i).hide();
				$("#videoshow"+i).show();
				$("#addmoretitle"+i).show();
				$("#order"+i).show();
				$("#bookshow"+i).hide();
				$("#flipshow"+i).hide(); 
			}
			else if(selectedval == 'book'){
				$("#imageshow"+i).hide();
				$("#youtubeshow"+i).hide();
				$("#videoshow"+i).hide();
				$("#bookshow"+i).show();
				$("#addmoretitle"+i).show();
				$("#order"+i).show();
				$("#flipshow"+i).hide(); 
			}
			else if(selectedval == 'flip'){
				$("#imageshow"+i).hide();
				$("#youtubeshow"+i).hide();
				$("#videoshow"+i).hide();
				$("#bookshow"+i).hide();
				$("#flipshow"+i).show(); 
				$("#addmoretitle"+i).show(); 
				$("#order"+i).show(); 
			}			
		});		
	}
	
	$("#projecttype").change(function(){
		$("#imageshow").hide();
		$("#youtubeshow").hide();
		$("#videoshow").hide();
		$("#bookshow").hide();
		$("#flipshow").hide();
		$("#addmoretitle").hide();
		$("#order").hide();
		
		var selectedval = $(this).val();
		if(selectedval == 'image'){
			$("#imageshow").show();
			$("#addmoretitle").show();
			$("#order").show();
			$("#youtubeshow").hide();
			$("#videoshow").hide();
			$("#bookshow").hide();
			$("#flipshow").hide();

			$("#imageshow input").attr('required',true);
			$("#youtubeshow input").attr('required',false);
			$("#videoshow input").attr('required',false);
			$("#bookshow input").attr('required',false);
			$("#flipshow input").attr('required',false);
		}
		else if(selectedval == 'youtube'){
			$("#imageshow").hide();
			$("#youtubeshow").show();
			$("#addmoretitle").show();
			$("#order").show();
			$("#videoshow").hide();
			$("#bookshow").hide();
			$("#flipshow").hide(); 

			$("#imageshow input").attr('required',false);
			$("#youtubeshow input").attr('required',true);
			$("#videoshow input").attr('required',false);
			$("#bookshow input").attr('required',false);
			$("#flipshow input").attr('required',false);
		}
		else if(selectedval == 'video'){
			$("#imageshow").hide();
			$("#youtubeshow").hide();
			$("#videoshow").show();
			$("#addmoretitle").show();
			$("#order").show();
			$("#bookshow").hide();
			$("#flipshow").hide(); 

			$("#imageshow input").attr('required',false);
			$("#youtubeshow input").attr('required',false);
			$("#videoshow input").attr('required',true);
			$("#bookshow input").attr('required',false);
			$("#flipshow input").attr('required',false);
		}
		else if(selectedval == 'book'){
			$("#imageshow").hide();
			$("#youtubeshow").hide();
			$("#videoshow").hide();
			$("#bookshow").show();
			$("#addmoretitle").show();
			$("#order").show();
			$("#flipshow").hide(); 

			$("#imageshow input").attr('required',false);
			$("#youtubeshow input").attr('required',false);
			$("#videoshow input").attr('required',false);
			$("#bookshow input").attr('required',true);
			$("#flipshow input").attr('required',false);
		}
		else if(selectedval == 'flip'){
			$("#imageshow").hide();
			$("#youtubeshow").hide();
			$("#videoshow").hide();
			$("#bookshow").hide();
			$("#flipshow").show(); 
			$("#addmoretitle").show(); 
			$("#order").show(); 

			$("#imageshow input").attr('required',false);
			$("#youtubeshow input").attr('required',false);
			$("#videoshow input").attr('required',false);
			$("#bookshow input").attr('required',false);
			$("#flipshow input").attr('required',true);
		}		
	});	
</script>
@endsection								