@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('article/store') }}" enctype="multipart/form-data">
									@csrf	
									<div class="form-group row">
										<div class="col-md-12">
											<label for="language">Select Language</label>
											<select id="language" class="form-control" name="language">
												@if(count($languages)>0)
													@foreach($languages as $language)
														<option value="{{$language->name}}">{{$language->name}}</option>
													@endforeach						  
												@endif
											</select>
										</div>	
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<label for="category_id">Select Category</label>
											<select id="category_id" class="form-control" name="category_id">
												<option value="" selected hidden disabled>Select Category</option>
											</select>
										</div>	
									</div>	
									<div class="form-group row">
										<div class="col-md-12 ">
											<input type="text" name="title"  id="title" placeholder="Title" title="Title"  class="form-control" />
										</div>				
									</div>
									<div class="form-group row col-md-12">		
										<select id="main_image_type" name="main_image_type" class="form-control">
											<option value="image" selected>Image/Gif</option>
											<option value="youtube">Youtube</option>
											<option value="video">Video upload</option>
											<option value="book">Book Images</option>
											<option value="flip">Flip Images</option>
										</select>
									</div>
									<div class=" row main_image">
										<div class="col-md-12">
											<input type="file" name="main_image[]" title="Main Image"  placeholder="Main Image"  multiple  class="form-control imageitem" /></div>
									</div>	
									<div class="row main_image_link hide">
										<div class="col-md-12">
											<input type="text" name="main_image"  title="Youtube Link" placeholder="Youtube Link" multiple  class="form-control imageitem" /></div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<textarea class="form-control" title="Short Description" placeholder="Short Description" id="short_description" name="short_description" required></textarea> 
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12 checkbox">
											<label><input type="checkbox" name="readmore" value="readmore"  id="readmore" placeholder="Read more" title="Read more">&nbsp;Read More</label>
										</div>				
									</div>
									<!-- <div class="read-more-section hide">
										<div class="form-group row col-md-12">		
											<select id="left_image_type" name="left_image_type" class="form-control">
												<option value="image" selected>Image/Gif</option>
												<option value="youtube">Youtube</option>
												<option value="video">Video upload</option>
												<option value="book">Book Images</option>
												<option value="flip">Flip Images</option>
											</select>
										</div>
										<div class=" row left_image">
											<div class="col-md-12">
												<input type="file" name="left_image[]" title="Left Image"  placeholder="Left Image"  multiple  class="form-control imageitem" /></div>
										</div>	
										<div class="row left_image_link hide">
											<div class="col-md-12">
												<input type="text" name="left_image"  title="Left Image" placeholder="Youtube link" multiple  class="form-control imageitem" /></div>
										</div>	
										<div class="form-group row">
											<div class="col-md-12">
												<textarea class="form-control" title="Description" placeholder="Description" id="left_description" name="left_description" required></textarea> 
											</div>
										</div>
										<div class="form-group row col-md-12">		
											<select id="right_image_type" name="right_image_type" class="form-control">
												<option value="image" selected>Image/Gif</option>
												<option value="youtube">Youtube</option>
												<option value="video">Video upload</option>
												<option value="book">Book Images</option>
												<option value="flip">Flip Images</option>
											</select>
										</div>
										<div class=" row right_image">
											<div class="col-md-12">
												<input type="file" name="right_image[]" title="right Image"  placeholder="right Image"  multiple  class="form-control imageitem" /></div>
										</div>	
										<div class="row right_image_link hide">
											<div class="col-md-12">
												<input type="text" name="right_image"  title="right Image" placeholder="Youtube link" multiple  class="form-control imageitem" /></div>
										</div>	
										<div class="form-group row">
											<div class="col-md-12">
												<textarea class="form-control" title="Description" placeholder="Description" id="right_description" name="right_description" required></textarea> 
											</div>
										</div>
										<div class="form-group row">
											<div class="col-md-12 checkbox">
												<label><input type="checkbox" name="applyfortooltip" value="applyfortooltip"  id="applyfortooltip" placeholder="Apply for this Toolkit" title="Apply for this Toolkit">&nbsp;Apply for this Toolkit</label>
											</div>				
										</div>
									</div> -->
									<div class="read-more-section hide">
										<div class="col-md-12">
											<h3>Inner Page Image</h3>
											<table class="table table-bordered" id="innterPageDynamicTable">	
												<tr>
													<th>Description</th>
													<th>Data</th>
													<th>Title</th>
													<th>Image Title</th>
													<th>Display Order</th>
													<th>Action</th>
												</tr>		
												<tr>  												 
													<td>
														<textarea class="form-control" id="innerPage[0][description]" name="innerPage[0][description]" required></textarea> 
													</td>	
													<td>
														<select id="innerPage[0][image_type]" name="innerPage[0][image_type]" class="form-control image_type">
															<option value="image" selected>Image/Gif</option>
															<option value="youtube">Youtube</option>
															<option value="video">Video upload</option>
															<option value="book">Book Images</option>
															<option value="flip">Flip Images</option>
														</select>
													</td>	
													<td>
														<input type="file" name="innerPage[0][image][]" class="form-control" placeholder="File" multiple >
													</td>
													<td class="hide">
														<input type="text" name="innerPage[0][image]" class="form-control" placeholder="Image">
													</td>
													<td>
														<input type="text" name="innerPage[0][image_title]" class="form-control"  placeholder="Enter Image Title">
													</td>
													<td>
														<input type="number" name="innerPage[0][display_order]" class="form-control" min="0" placeholder="Enter order number">
													</td>
													<td>
														<button type="button" name="addInnerPageImage" id="addInnerPageImage" class="btn btn-success">Add More</button>
													</td>  
												</tr>  
											</table>
										</div>
										<div class="form-group row">
											<div class="col-md-12 checkbox">
												<label>
													<input type="checkbox" name="applyfortooltip" value="applyfortooltip"  id="applyfortooltip" placeholder="Apply For this Tooltip" title="Read more">&nbsp;Apply For this Tooltip
												</label>
											</div>				
										</div>
									</div>

									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	CKEDITOR.editorConfig = function (config) {
		config.language = 'es';
		config.uiColor = '#F7B42C';
		config.height = 300;
		config.toolbarCanCollapse = true;	
	};
	CKEDITOR.replace('short_description');
	CKEDITOR.replace('innerPage[0][description]');
	function getCatagory(lang){
		$.ajax({
			url:'{{ route("articleCategoryByLanguage") }}',
			type:'POST',
			data:{lang:lang, _token:'{{csrf_token()}}'},
			success:function(result){
				$('#category_id').html(result.data);
			}
		});	
	}
	$(document).ready(function() {
		let lang = $('#language').val();
	    getCatagory(lang);
	    $('#language').on('change',function(){
			lang = $(this).val();
			getCatagory(lang);
		});
	});

	jQuery('#readmore').click(function() {
    	if($(this).is(':checked')) {
    		jQuery(".read-more-section").removeClass("hide");
    	}
    	else {
			jQuery(".read-more-section").addClass("hide");
    	}
  	});

	jQuery("#main_image_type").click(function() {
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(".main_image").removeClass("hide");
			jQuery(".main_image_link").addClass("hide");
		}
		else{
			jQuery(".main_image_link").removeClass("hide"); 
			jQuery(".main_image").addClass("hide");
		}
	});

	let innerPage =0;
	jQuery("#addInnerPageImage").click(function(){	
		++innerPage;	
		jQuery("#innterPageDynamicTable").append(`<tr>  												 
			<td>
				<textarea class="form-control" id="innerPage[${innerPage}][description]" name="innerPage[${innerPage}][description]"></textarea> 
			</td>	
			<td>
				<select id="innerPage[${innerPage}][image_type]" name="innerPage[${innerPage}][image_type]" class="form-control image_type">
					<option value="image" selected>Image/Gif</option>
					<option value="youtube">Youtube</option>
					<option value="video">Video upload</option>
					<option value="book">Book Images</option>
					<option value="flip">Flip Images</option>
				</select>
			</td>	
			<td>
				<input type="file" name="innerPage[${innerPage}][image][]" class="form-control" placeholder="File" multiple >
			</td>
			<td class="hide">
				<input type="text" name="innerPage[${innerPage}][image]" class="form-control" placeholder="Image">
			</td>
			<td>
				<input type="text" name="innerPage[${innerPage}][image_title]" class="form-control"  placeholder="Enter Image Title">
			</td>
			<td>
				<input type="number" name="innerPage[${innerPage}][display_order]" class="form-control" min="0" placeholder="Enter order number">
			</td>
			<td>
				<button type="button" name="remvoeInnerPageImage" id="remvoeInnerPageImage" class="btn btn btn-danger remvoeInnerPageImage">Remove</button>
			</td>  
		</tr>  `);
		let description = 'innerPage['+innerPage+'][description]';
			CKEDITOR.replace(description);
	});	

	jQuery(document).on('click', '.remvoeInnerPageImage', function(){  
		$(this).parents('tr').remove();
	});  

	jQuery(document).on('change', '.image_type', function(){  
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(this).parent().next().next().addClass("hide");
			jQuery(this).parent().next().removeClass("hide");
		}
		else{
			jQuery(this).parent().next().addClass("hide");
			jQuery(this).parent().next().next().removeClass("hide");
		}
	}); 
</script>
@endsection								
