@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('article/update') }}" enctype="multipart/form-data">
									@csrf	
									<input type="hidden" name="articleid"  id="articleid" value="{{$articleid}}" />
									<div class="form-group row">
										<div class="col-md-12">
											<label for="language">Select Language</label>
											<select id="language" class="form-control" name="language">
												@if(count($languages)>0)
													@foreach($languages as $language)
														<option value="{{$language->name}}" <?php if($articles->language==$language->name){ echo "selected"; } ?> >{{$language->name}}</option>
													@endforeach						  
												@endif
											</select>
										</div>	
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<label for="category_id">Select Category</label>
											<select id="category_id" class="form-control" name="category_id">
												<option value="" selected hidden disabled>Select Category</option>
												@if(count($articlescategory)>0)
													@foreach($articlescategory as $category)
														<option value="{{$category->id}}" >{{$category->category_name}}</option>
													@endforeach						  
												@endif
											</select>
										</div>	
									</div>	
									<div class="form-group row">
										<div class="col-md-12 ">
											<input type="text" name="title"  id="title" placeholder="Title" title="Title"  class="form-control" value="{{$articles->title}}" />
										</div>				
									</div>
									<div class="form-group row col-md-12">		
										<select id="main_image_type" name="main_image_type" class="form-control">
											<option value="image" <?=($articles->main_image_type=='image') ? "selected" : ""; ?> >Image/Gif</option>
											<option value="youtube" <?=($articles->main_image_type=='youtube') ? "selected" : ""; ?>>Youtube</option>
											<option value="video" <?=($articles->main_image_type=='video') ? "selected" : ""; ?> >Video upload</option>
											<option value="book" <?=($articles->main_image_type=='book') ? "selected" : ""; ?> >Book Images</option>
											<option value="flip" <?=($articles->main_image_type=='flip') ? "selected" : ""; ?> >Flip Images</option>
										</select>
									</div>
									<div class=" row main_image <?=($articles->main_image_type=='youtube') ? 'hide' : '';?>">
										<div class="col-md-12">
											<input type="file" name="main_image[]" title="Main Image"  placeholder="Main Image"  multiple  class="form-control imageitem" />
										</div>
									</div>	
									<div class="row main_image_link <?=($articles->main_image_type!='youtube') ? 'hide' : '';?> ">
										<div class="col-md-12">
											<input type="text" name="main_image"  title="Youtube Link" placeholder="Youtube Link"   class="form-control imageitem" value="<?=($articles->main_image_type=='youtube') ? $articles->main_image : '';?>" />
										</div>
									</div>	

									<?php 
										$img = '';
										if($articles->main_image_type == 'image' || $articles->main_image_type  == 'video'){
											$articles->main_image = json_decode($articles->main_image);
											$img = URL('images/articles',$articles->main_image);
										}
										else if ($articles->main_image_type == 'youtube') {
											$img = $articles->main_image;
										}
										else if ($articles->main_image_type == 'book' || $articles->main_image_type == 'flip') {
											$temp_top = json_decode($articles->main_image);
											$t_array1 = array();
											foreach ($temp_top as $value) {
												$t_array1[] = URL('images/articles/',$value);
											}
											$img = json_encode($t_array1);
										}
									?>
									<?=getimagehtmlbytype($articles->main_image_type,$img)?>
									<div class="form-group row">
										<div class="col-md-12">
											<textarea class="form-control" title="Short Description" placeholder="Short Description" id="short_description" name="short_description" required>{{$articles->short_description}}</textarea> 
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12 checkbox">
											<label><input type="checkbox" name="readmore" value="readmore"  id="readmore" placeholder="Read more" title="Read more" <?=($articles->readmore==1) ? "checked" : ""; ?> >&nbsp;Read More</label>
										</div>				
									</div>									
									<div class="read-more-section <?=($articles->readmore==0) ? 'hide' : ''; ?>">
										<div class="col-md-12">
											<h3>Inner Page Image</h3>
											<table class="table table-bordered" id="innterPageDynamicTable">	
												<tr>
													<th>Description</th>
													<th>Data</th>
													<th>Title</th>
													<th>Image Title</th>
													<th>Display Order</th>
													<th>Action</th>
												</tr>
												<?php $innerPage=0; ?>		
												@foreach($articlesdetails as $articlesdetail)
												<tr>  												 
													<td>
														<input type="hidden" class="form-control" name="innerPage[{{$innerPage}}][ariclesInnerContentid]" value="{{$articlesdetail->id}}">
														<input  type="hidden" class="form-control" name="ariclesInnerContentid[]" value="{{$articlesdetail->id}}">

														<textarea class="form-control" id="innerPage[{{$innerPage}}][description]" name="innerPage[{{$innerPage}}][description]">{{$articlesdetail->description}}</textarea> 
													</td>	
													<td>
														<select id="innerPage[{{$innerPage}}][image_type]" name="innerPage[{{$innerPage}}][image_type]" class="form-control image_type">
															<option value="image" <?=($articlesdetail->image_type=='image') ? "selected" : ""; ?> >Image/Gif</option>
															<option value="youtube" <?=($articlesdetail->image_type=='youtube') ? "selected" : ""; ?> >Youtube</option>
															<option value="video" <?=($articlesdetail->image_type=='video') ? "selected" : ""; ?> >Video upload</option>
															<option value="book" <?=($articlesdetail->image_type=='book') ? "selected" : ""; ?>>Book Images</option>
															<option value="flip" <?=($articlesdetail->image_type=='flip') ? "selected" : ""; ?> >Flip Images</option>
														</select>
													</td>	
													<td class="@if($articlesdetail->image_type =='youtube') hide @endif">
														<input type="file" name="innerPage[{{$innerPage}}][image][]"  class="form-control" placeholder="File" multiple >
														<?php 
																$img = '';
																if($articlesdetail->image_type == 'image' || $articlesdetail->image_type == 'video'){
																	$articlesdetail->image = json_decode($articlesdetail->image);
																	if (isset($articlesdetail->image[0])) {
																		$img = URL('images/articles',$articlesdetail->image[0]);
																	}
																}
																else if ($articlesdetail->image_type == 'youtube') {
																	$img = $articlesdetail->image;
																}
																else if ($articlesdetail->image_type == 'book' || $articlesdetail->image_type == 'flip') {
																	$temp_top = json_decode($articlesdetail->image);
																	$t_array1 = array();
																	foreach ($temp_top as $value) {
																		$t_array1[] = URL('images/articles/',$value);
																	}
																	$img = json_encode($t_array1);
																}
															?>
															<?=getimagehtmlbytype($articlesdetail->image_type,$img)?>
													</td>
													<td class="@if($articlesdetail->image_type !='youtube') hide @endif">
														<input type="text" name="innerPage[{{$innerPage}}][image]" class="form-control" placeholder="Image"  value="@if($articlesdetail->image_type =='youtube') {{$articlesdetail->image}} @endif">
													</td>
													<td>
														<input type="text" name="innerPage[{{$innerPage}}][image_title]" class="form-control"  placeholder="Enter Image Title" value="{{$articlesdetail->image_title}}">
													</td>
													<td>
														<input type="number" name="innerPage[{{$innerPage}}][display_order]" id="innerPage[{{$innerPage}}][display_order]" class="form-control" min="0" placeholder="Enter order number" value="{{$articlesdetail->display_order}}">
													</td>
													@if($innerPage==0)
													<td>
														<button type="button" name="addInnerPageImage" id="addInnerPageImage" class="btn btn-success">Add More</button>
													</td> 	
													@else		
													<td>
														<button type="button" name="remvoeInnerPageImage" id="remvoeInnerPageImage" class="btn btn btn-danger remvoeInnerPageImage">Remove</button> 
													</td>
													@endif
												</tr> 
												<?php  $innerPage++; ?>
												@endforeach 
											</table>
										</div>
										<div class="form-group row">
											<div class="col-md-12 checkbox">
												<label>
													<input type="checkbox" <?=($articles->applyfortooltip==1) ? "checked" : ""; ?> name="applyfortooltip" value="applyfortooltip"  id="applyfortooltip" placeholder="Apply For this Tooltip" title="Read more">&nbsp;Apply For this Tooltip
												</label>
											</div>				
										</div>
									</div>

									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
CKEDITOR.editorConfig = function (config) {
	config.language = 'es';
	config.uiColor = '#F7B42C';
	config.height = 300;
	config.toolbarCanCollapse = true;	
};
CKEDITOR.replace('short_description');
let innerPage = "<?php echo !empty($articlesdetails) ? count($articlesdetails->toArray()) : 0; ?>";		
$("#category_id").val(<?=$articles->category_id?>);
for (var i = 0; i < innerPage; i++) {
	CKEDITOR.replace(`innerPage[${i}][description]`);
}
function getCatagory(lang){
	$.ajax({
		url:'{{ route("articleCategoryByLanguage") }}',
		type:'POST',
		data:{lang:lang, _token:'{{csrf_token()}}'},
		success:function(result){
			$('#category_id').html(result.data);
		}
	});	
}

$(document).ready(function() {
	let lang = $('#language').val();
    $('#language').on('change',function(){
		lang = $(this).val();
		getCatagory(lang);
	});
	jQuery('#readmore').click(function() {
		if($(this).is(':checked')) {
			jQuery(".read-more-section").removeClass("hide");
		}
		else {
			jQuery(".read-more-section").addClass("hide");
		}
		});

	jQuery("#main_image_type").click(function() {
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(".main_image").removeClass("hide");
			jQuery(".main_image_link").addClass("hide");
		}
		else{
			jQuery(".main_image_link").removeClass("hide"); 
			jQuery(".main_image").addClass("hide");
		}
	});
	jQuery("#addInnerPageImage").click(function(){	
		jQuery("#innterPageDynamicTable").append(`<tr>  												 
			<td>
				<textarea class="form-control" id="innerPage[${innerPage}][description]" name="innerPage[${innerPage}][description]"></textarea> 
			</td>	
			<td>
				<select id="innerPage[${innerPage}][image_type]" name="innerPage[${innerPage}][image_type]" class="form-control image_type">
					<option value="image" selected>Image/Gif</option>
					<option value="youtube">Youtube</option>
					<option value="video">Video upload</option>
					<option value="book">Book Images</option>
					<option value="flip">Flip Images</option>
				</select>
			</td>	
			<td>
				<input type="file" name="innerPage[${innerPage}][image][]" class="form-control" placeholder="File" multiple >
			</td>
			<td class="hide">
				<input type="text" name="innerPage[${innerPage}][image]" class="form-control" placeholder="Image">
			</td>
			<td>
				<input type="text" name="innerPage[${innerPage}][image_title]" class="form-control" min="0" placeholder="Enter order number">
			</td>
			<td>
				<input type="number" name="innerPage[${innerPage}][display_order]" class="form-control" min="0" placeholder="Enter order number">
			</td>
			<td>
				<button type="button" name="remvoeInnerPageImage" id="remvoeInnerPageImage" class="btn btn btn-danger remvoeInnerPageImage">Remove</button>
			</td>  
		</tr>  `);
		let description = 'innerPage['+innerPage+'][description]';
		CKEDITOR.replace(description);
		innerPage++;	
	});	
	jQuery(document).on('click', '.remvoeInnerPageImage', function(){  
		$(this).parents('tr').remove();
	}); 
	jQuery(document).on('change', '.image_type', function(){  
		let  type = jQuery(this).val();
		if(type!="youtube"){
			jQuery(this).parent().next().next().addClass("hide");
			jQuery(this).parent().next().removeClass("hide");
		}
		else{
			jQuery(this).parent().next().addClass("hide");
			jQuery(this).parent().next().next().removeClass("hide");
		}
	});
}); 

</script>
@endsection								
