@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn"><a href="{{ url('articlescategory/create') }}" class="btn btn-success">Add New Category</a></p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Articles Category Listing</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-2 pull-right">
									<label for="select_lang">Select Language</label>
									<select id="select_lang" class="form-control" name="lang">
										<option value="" selected disabled>Select Language</option>
										@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}">{{$language->name}}</option>
											@endforeach						  
										@endif
									</select>
								</div>
							</div>
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
								    <thead class="">
								        <tr>
											<th>#</th>
											<th>Category</th>
											<th>Order By</th>
											<th>Language</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>	
										<?php  $i=1; ?>
										@foreach($category as $value)		
										<tr>							
											<td>{{ $i++ }}</td>
											<td>{{ $value->category_name }}</td>
											<td><input type="number" id="category{{$value->id}}" class="orderby form-control" onfocusout="onChangeOrderBy({{$value->id}})" value="{{$value->orderby}}"  /></td>
											<td>{{ $value->language }}</td>
											<td class="td-actions text-right">												
												<a href="{{ URL('articlescategory/edit',$value->id) }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('articlescategory/delete',$value->id) }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>	
										</tr>	
										@endforeach				
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>	
<script>
function onChangeOrderBy(id) {
	let orderby = $("#category"+id).val();
	$.ajax({
		url:"{{route('articlescategoryorderby')}}",
		headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
		data:{id:id,orderby:orderby},
		type:"POST",
		success:function(response){
			console.log(response);
		},
		error:function(error){	
			console.log(error);
		}
	});
}
</script>
@endsection			

@section('extrascript')
<script type="text/javascript">
    $(document).ready(function () {
      $('#dtOrderExample').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
          { className: "td-actions text-right",orderable: false , "targets": [4] },
          { orderable: false , "targets": [2] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
<script type="text/javascript">
	let BASE_URL = '{{URL("/")}}';
    $(document).ready(function () {
      	var table = $('#dtOrderExample').DataTable();
      	$(document).on('change','#select_lang',function(){
      		let lang = $(this).val();
      		$.ajax({
				url:"{{route('onchnagelanguage')}}",
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				data:{lang:lang},
				type:"POST",
				success:function(response){
					console.table(response.data);
					if (response.responseCode == 1) {
						table.clear().draw();
						$.each(response.data,function(key,value){
							let edit = BASE_URL+'/articlescategory/edit'+value.id;
							let del = BASE_URL+'/articlescategory/delete'+value.id;

							let action = `<div class="td-actions text-right"><a href="${edit}" class="btn btn-success">
														<i class="material-icons">edit</i>
													</a>								
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('${del}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a></div>`;
							let text = `<input type="number" id="category${value.id}" class="orderby form-control" onfocusout="onChangeOrderBy(${value.id})" data-id ="${value.id}" value="${value.orderby}"  />`;
							
							table.row.add([key+1,value.category_name,text,value.language,action]).draw();
						});
					}
				},
				error:function(error){	
					console.log(error);
				}
			});
      	});
      	
    });
</script>
@endsection