@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Add New Category</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('articlescategory/store') }}" enctype="multipart/form-data">
									@csrf				
									<div class="form-group row">
										<label for="title" class="col-md-4 col-form-label text-md-left">Category Name</label>
										<div class="col-md-6">
											<input id="title" type="text" class="form-control @error('category_name') is-invalid @enderror" name="category_name" value="{{ old('category_name') }}" >
											@error('category_name')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="lang">
											@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}">{{$language->name}}</option>
											@endforeach						  
											@endif
										</select>
									</div>
									<div class="form-group row">
										<hr><hr>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>

@endsection								