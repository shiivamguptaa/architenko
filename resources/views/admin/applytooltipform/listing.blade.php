@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn">
					<a href="{{ url('applytooltipform/create') }}" class="btn btn-success">Add New Apply ToolKit Form</a>
				</p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th class="text-center">#</th>
											<th>Title</th>
											<th>Language</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php $counter = 1; ?>
										@foreach($applytooltipform as $applyform)
											<tr>							
												<td class="text-center">{{$counter}}</td>										
												<td>{{ $applyform->title }}</td>
												<td>{{ $applyform->language }}</td>
												<td class="td-actions text-right">												
													<a href="{{ url('applytooltipform/'.$applyform->id.'/edit') }}" class="btn btn-success">
														<i class="material-icons">edit</i>
													</a>
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('applytooltipform/'.$applyform->id.'/delete') }}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a>
												</td>							
											</tr>
										<?php $counter++; ?>
										@endforeach											
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection			

@section('extrascript')
<script type="text/javascript">
    $(document).ready(function () {
      $('#dtOrderExample').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
          { className: "td-actions text-right",orderable: false , "targets": [3] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
@endsection	