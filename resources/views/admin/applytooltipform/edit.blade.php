@extends('layouts.auth')
@section('content')

<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('applytooltipform/update') }}" enctype="multipart/form-data">
									@csrf	
									<input id="id" name="id" type="hidden" value="@if(isset($id)){{$id}}@endif" />
									<div class="form-group row">
										<div class="col-md-12">
											<input id="title" name="title" type="text" class="form-control" autocomplete="off" title="ToolKit Title" placeholder="ToolKit Title" autofocus value="@if(isset($applytooltipform['title'])){{$applytooltipform['title']}}@endif" />										
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="tooltip_name" name="tooltip_name" type="text" class="form-control" autocomplete="off" title="ToolTip Name" placeholder="ToolKit Name" autofocus value="@if(isset($applytooltipform['tooltip_name'])){{$applytooltipform['tooltip_name']}}@endif" />										
										</div>
									</div>
									
									<div class="form-group row">
										<div class="col-md-12">
											<input id="you_found_through_title" name="you_found_through_title" type="text" class="form-control" autocomplete="off" title="You found us on/through title" placeholder="You found us on/through title" autofocus  value="@if(isset($applytooltipform['you_found_through_title'])){{$applytooltipform['you_found_through_title']}}@endif" />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="you_found_through_option1" name="you_found_through_option1" type="text" class="form-control" autocomplete="off" title="You found us on/through option 1" placeholder="You found us on/through option 1" autofocus value="@if(isset($applytooltipform['you_found_through_option1'])){{$applytooltipform['you_found_through_option1']}}@endif" />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="you_found_through_option2" name="you_found_through_option2" type="text" class="form-control" autocomplete="off" title="You found us on/through option 2" placeholder="You found us on/through option 2" autofocus value="@if(isset($applytooltipform['you_found_through_option2'])){{$applytooltipform['you_found_through_option2']}}@endif" />										
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="you_found_through_option3" name="you_found_through_option3" type="text" class="form-control" autocomplete="off" title="You found us on/through option 3" placeholder="You found us on/through option 3" autofocus value="@if(isset($applytooltipform['you_found_through_option3'])){{$applytooltipform['you_found_through_option3']}}@endif" />										
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="you_found_through_option4" name="you_found_through_option4" type="text" class="form-control" autocomplete="off" title="You found us on/through option 4" placeholder="You found us on/through option 4" autofocus value="@if(isset($applytooltipform['you_found_through_option4'])){{$applytooltipform['you_found_through_option4']}}@endif" />										
										</div>
									</div>
										
									<div class="form-group row">
										<div class="col-md-12">
											<input id="choose_toolkit_title" name="choose_toolkit_title" type="text" class="form-control" autocomplete="off" title="Choose Toolkit Title" placeholder="Choose Toolkit Title" autofocus value="@if(isset($applytooltipform['choose_toolkit_title'])){{$applytooltipform['choose_toolkit_title']}}@endif" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="choose_toolkit_option1" name="choose_toolkit_option1" type="text" class="form-control" autocomplete="off" title="Choose Toolkit Option 1" placeholder="Choose Toolkit Option 1" autofocus value="@if(isset($applytooltipform['choose_toolkit_option1'])){{$applytooltipform['choose_toolkit_option1']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="choose_toolkit_option2" name="choose_toolkit_option2" type="text" class="form-control" autocomplete="off" title="Choose Toolkit Option 2" placeholder="Choose Toolkit Option 2" autofocus value="@if(isset($applytooltipform['choose_toolkit_option2'])){{$applytooltipform['choose_toolkit_option2']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="choose_toolkit_option3" name="choose_toolkit_option3" type="text" class="form-control" autocomplete="off" title="Choose Toolkit Option 3" placeholder="Choose Toolkit Option 3" autofocus  value="@if(isset($applytooltipform['choose_toolkit_option3'])){{$applytooltipform['choose_toolkit_option3']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="choose_toolkit_option4" name="choose_toolkit_option4" type="text" class="form-control" autocomplete="off" title="Choose Toolkit Option 4" placeholder="Choose Toolkit Option 4" autofocus value="@if(isset($applytooltipform['choose_toolkit_option4'])){{$applytooltipform['choose_toolkit_option4']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="personal_contact_title" name="personal_contact_title" type="text" class="form-control" autocomplete="off" title="Personal Contact Title" placeholder="Personal Contact Title" autofocus value="@if(isset($applytooltipform['personal_contact_title'])){{$applytooltipform['personal_contact_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="i_am_title" name="i_am_title" type="text" class="form-control" autocomplete="off" title="I Am Title" placeholder="I Am Title" autofocus value="@if(isset($applytooltipform['i_am_title'])){{$applytooltipform['i_am_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_title" name="enterprise_title" type="text" class="form-control" autocomplete="off" title="Enterprise Title" placeholder="Enterprise Title" autofocus value="@if(isset($applytooltipform['enterprise_title'])){{$applytooltipform['enterprise_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="developer_title" name="developer_title" type="text" class="form-control" autocomplete="off" title="Developer Title" placeholder="Developer Title" autofocus value="@if(isset($applytooltipform['developer_title'])){{$applytooltipform['developer_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_title" name="private_individual_title" type="text" class="form-control" autocomplete="off" title="Private Individual Title" placeholder="Private Individual Title" autofocus value="@if(isset($applytooltipform['private_individual_title'])){{$applytooltipform['private_individual_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="governmental_title" name="governmental_title" type="text" class="form-control" autocomplete="off" title="Governmental Title" placeholder="Governmental Title" autofocus value="@if(isset($applytooltipform['governmental_title'])){{$applytooltipform['governmental_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_address_title" name="private_individual_address_title" type="text" class="form-control" autocomplete="off" title="Private Individual Address Title" placeholder="Private Individual Address Title" autofocus value="@if(isset($applytooltipform['private_individual_address_title'])){{$applytooltipform['private_individual_address_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_mr_title" name="private_individual_mr_title" type="text" class="form-control" autocomplete="off" title="Private Individual Mr Title" placeholder="Private Individual Mr Title" autofocus value="@if(isset($applytooltipform['private_individual_mr_title'])){{$applytooltipform['private_individual_mr_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_mrs_title" name="private_individual_mrs_title" type="text" class="form-control" autocomplete="off" title="Private Individual Mrs Title" placeholder="Private Individual Mrs Title" autofocus  value="@if(isset($applytooltipform['private_individual_mrs_title'])){{$applytooltipform['private_individual_mrs_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_first_name" name="private_individual_first_name" type="text" class="form-control" autocomplete="off" title="Private Individual First Name Title" placeholder="Private Individual First Name Title" autofocus   value="@if(isset($applytooltipform['private_individual_first_name'])){{$applytooltipform['private_individual_first_name']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_middle_name" name="private_individual_middle_name" type="text" class="form-control" autocomplete="off" title="Private Individual Middle Name Title" placeholder="Private Individual Middle Name Title" autofocus  value="@if(isset($applytooltipform['private_individual_middle_name'])){{$applytooltipform['private_individual_middle_name']}}@endif" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_last_name" name="private_individual_last_name" type="text" class="form-control" autocomplete="off" title="Private Individual Last Name Title" placeholder="Private Individual Last Name Title" autofocus value="@if(isset($applytooltipform['private_individual_last_name'])){{$applytooltipform['private_individual_last_name']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_address" name="private_individual_address" type="text" class="form-control" autocomplete="off" title="Private Individual Address Title" placeholder="Private Individual Address Title" autofocus  value="@if(isset($applytooltipform['private_individual_address'])){{$applytooltipform['private_individual_address']}}@endif" />
										</div>
									</div>			
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_city" name="private_individual_city" type="text" class="form-control" autocomplete="off" title="Private Individual City Title" placeholder="Private Individual City Title" autofocus  value="@if(isset($applytooltipform['private_individual_city'])){{$applytooltipform['private_individual_city']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_postal_code" name="private_individual_postal_code" type="text" class="form-control" autocomplete="off" title="Private Individual Postal Code Title" placeholder="Private Individual Postal Code Title" autofocus  value="@if(isset($applytooltipform['private_individual_postal_code'])){{$applytooltipform['private_individual_postal_code']}}@endif" />
										</div>
									</div>

									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_country" name="private_individual_country" type="text" class="form-control" autocomplete="off" title="Private Individual Country Title" placeholder="Private Individual Country Title" autofocus  value="@if(isset($applytooltipform['private_individual_country'])){{$applytooltipform['private_individual_country']}}@endif"  />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_phone" name="private_individual_phone" type="text" class="form-control" autocomplete="off" title="Private Individual Phone Title" placeholder="Private Individual Phone Title" autofocus  value="@if(isset($applytooltipform['private_individual_phone'])){{$applytooltipform['private_individual_phone']}}@endif" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_email" name="private_individual_email" type="text" class="form-control" autocomplete="off" title="Private Individual EmailTitle" placeholder="Private Individual Email Title" autofocus  value="@if(isset($applytooltipform['private_individual_email'])){{$applytooltipform['private_individual_email']}}@endif"  />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="private_individual_dob" name="private_individual_dob" type="text" class="form-control" autocomplete="off" title="Private Individual dob Title" placeholder="Private Individual dob Title" autofocus   value="@if(isset($applytooltipform['private_individual_dob'])){{$applytooltipform['private_individual_dob']}}@endif" />	
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_price_title1" name="enterprise_price_title1" type="text" class="form-control" autocomplete="off" title="Enterprise Title 1" placeholder="Enterprise Title 1" autofocus value="@if(isset($applytooltipform['enterprise_price_title1'])){{$applytooltipform['enterprise_price_title1']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_price_title2" name="enterprise_price_title2" type="text" class="form-control" autocomplete="off" title="Enterprise Title 2" placeholder="Enterprise Title 2" autofocus value="@if(isset($applytooltipform['enterprise_price_title2'])){{$applytooltipform['enterprise_price_title2']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_address_title" name="enterprise_address_title" type="text" class="form-control" autocomplete="off" title="Enterprise Address Title" placeholder="Enterprise Address Title" autofocus  value="@if(isset($applytooltipform['enterprise_address_title'])){{$applytooltipform['enterprise_address_title']}}@endif" />
										</div>
									</div>			
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_city_title" name="enterprise_city_title" type="text" class="form-control" autocomplete="off" title="Enterprise City Title" placeholder="Enterprise City Title" autofocus value="@if(isset($applytooltipform['enterprise_city_title'])){{$applytooltipform['enterprise_city_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_postal_code_title" name="enterprise_postal_code_title" type="text" class="form-control" autocomplete="off" title="Enterprise Postal Code Title" placeholder="Enterprise Postal Code Title" autofocus value="@if(isset($applytooltipform['enterprise_postal_code_title'])){{$applytooltipform['enterprise_postal_code_title']}}@endif" />
										</div>
									</div>			
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_country_title" name="enterprise_country_title" type="text" class="form-control" autocomplete="off" title="Enterprise Country Title" placeholder="Enterprise Country Title" autofocus value="@if(isset($applytooltipform['enterprise_country_title'])){{$applytooltipform['enterprise_country_title']}}@endif" />
										</div>
									</div>		
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_phone_title" name="enterprise_phone_title" type="text" class="form-control" autocomplete="off" title="Enterprise Phone Title" placeholder="Enterprise Phone Title" autofocus value="@if(isset($applytooltipform['enterprise_phone_title'])){{$applytooltipform['enterprise_phone_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_email_title" name="enterprise_email_title" type="text" class="form-control" autocomplete="off" title="Enterprise Email Title" placeholder="Enterprise Email Title" autofocus value="@if(isset($applytooltipform['enterprise_email_title'])){{$applytooltipform['enterprise_email_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_contact_title" name="enterprise_contact_title" type="text" class="form-control" autocomplete="off" title="Enterprise Contact Title" placeholder="Enterprise Contact Title" autofocus  value="@if(isset($applytooltipform['enterprise_contact_title'])){{$applytooltipform['enterprise_contact_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_mr_title" name="enterprise_mr_title" type="text" class="form-control" autocomplete="off" title="Enterprise Mr Title" placeholder="Enterprise Mr Title" autofocus value="@if(isset($applytooltipform['enterprise_mr_title'])){{$applytooltipform['enterprise_mr_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_mrs_title" name="enterprise_mrs_title" type="text" class="form-control" autocomplete="off" title="Enterprise Mrs Title" placeholder="Enterprise Mrs Title" autofocus value="@if(isset($applytooltipform['enterprise_mrs_title'])){{$applytooltipform['enterprise_mrs_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_contact_first_name_title" name="enterprise_contact_first_name_title" type="text" class="form-control" autocomplete="off" title="Enterprise Contact First Name Title" placeholder="Enterprise Contact First Name Title" autofocus value="@if(isset($applytooltipform['enterprise_contact_first_name_title'])){{$applytooltipform['enterprise_contact_first_name_title']}}@endif" />
										</div>
									</div>	
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_contact_last_name_title" name="enterprise_contact_last_name_title" type="text" class="form-control" autocomplete="off" title="Enterprise Contact Last Name Title" placeholder="Enterprise Contact Last Name Title" autofocus value="@if(isset($applytooltipform['enterprise_contact_last_name_title'])){{$applytooltipform['enterprise_contact_last_name_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="enterprise_contact_personpositionlist_title" name="enterprise_contact_personpositionlist_title" type="text" class="form-control" autocomplete="off" title="Enterprise Contact Person Position List Title" placeholder="Enterprise Contact Person Position List Title" autofocus data-role="tagsinput" value="@if(isset($applytooltipform['enterprise_contact_personpositionlist_title'])){{$applytooltipform['enterprise_contact_personpositionlist_title']}}@endif" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12">
											<input id="apply_title" name="apply_title" type="text" class="form-control" autocomplete="off" title="Apply Title" placeholder="Apply Title" value="@if(isset($applytooltipform['apply_title'])){{$applytooltipform['apply_title']}}@endif" />
										</div>
									</div>
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="language">
											@if(count($languages)>0)
												@foreach($languages as $language)
													<option value="{{$language->name}}" @if($language->name == $applytooltipform['language']) selected @endif >{{$language->name}}</option>
												@endforeach						  
											@endif
										</select>
									</div>		
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection						