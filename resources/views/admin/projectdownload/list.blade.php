@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Project Downloads Listing</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="dtOrderExample1" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th class="th-sm">#</th>
											<th class="th-sm">Project Name</th>
											<th class="th-sm">Email</th>
											<th class="th-sm">Date</th>
											<th class="th-sm td-actions text-right">Action</th>
										</tr>
									</thead>
									<tbody>										
										@if(count($downloads) > 0)
										<?php $counter = 1; ?>
										@foreach($downloads as $value)
										<?php  $date = date_create($value['created_at']); ?>
										<tr>
											<td>{{$counter}}</td>
											<td>{{$value['project_name']}}</td>
											<td>{{$value['email']}}</td>
											<td><?=date_format($date,"Y-m-d ")?></td>
											<td class="td-actions text-right">
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('project/download/delete/'.$value["id"]) }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>	
										</tr>
										<?php $counter++; ?>
										@endforeach										
										@endif
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection
@section('extrascript')
<script type="text/javascript">
    
    $(document).ready(function () {
      $('#dtOrderExample1').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
		          { className: "td-actions text-right",orderable: false , "targets": [4] },
		        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
@endsection

