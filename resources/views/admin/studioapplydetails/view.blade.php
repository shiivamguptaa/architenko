@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							@if(!empty($studioApplyDetail))
								<table class="table table-bordered">
									<tbody>
										<tr>
											<th>Name</th>
											<th>Value</th>
										</tr>
										<tr>
											<th>Privacy Policy Accept</th>
											<td>@if(isset($studioApplyDetail['privacy_policy_accept_title'])){{$studioApplyDetail['privacy_policy_accept_title']}}@endif</td>
										</tr>	
										<tr>
											<th>Language of correspondence</th>
											<td>@if(isset($studioApplyDetail['language_input'])){{$studioApplyDetail['language_input']}}@endif</td>
										</tr>		
										<tr>
											<th>My applying position is:</th>
											<td>@if(isset($studioApplyDetail['position_title'])){{$studioApplyDetail['position_title']}}@endif</td>
										</tr>		
										<tr>
											<th>Position 1</th>
											<td>@if(isset($studioApplyDetail['position1'])){{$studioApplyDetail['position1']}}@endif</td>
										</tr>
										<tr>
											<th>Position 2</th>
											<td>@if(isset($studioApplyDetail['position2'])){{$studioApplyDetail['position2']}}@endif</td>
										</tr>
										<tr>
											<th>Position 3</th>
											<td>@if(isset($studioApplyDetail['position3'])){{$studioApplyDetail['position3']}}@endif</td>
										</tr>
										<tr>
											<th>Gender</th>
											<td>@if(isset($studioApplyDetail['gender'])){{$studioApplyDetail['gender']}}@endif</td>
										</tr>
										<tr>
											<th>First Name</th>
											<td>@if(isset($studioApplyDetail['first_name'])){{$studioApplyDetail['first_name']}}@endif</td>
										</tr>
										<tr>
											<th>Middle Name</th>
											<td>@if(isset($studioApplyDetail['middle_name'])){{$studioApplyDetail['middle_name']}}@endif</td>
										</tr>
										<tr>
											<th>Last Name</th>
											<td>@if(isset($studioApplyDetail['last_name'])){{$studioApplyDetail['last_name']}}@endif</td>
										</tr>
										<tr>
											<th>Address</th>
											<td>@if(isset($studioApplyDetail['address'])){{$studioApplyDetail['address']}}@endif</td>
										</tr>
										<tr>
											<th>City</th>
											<td>@if(isset($studioApplyDetail['city'])){{$studioApplyDetail['city']}}@endif</td>
										</tr>			
										<tr>
											<th>Postal Code</th>
											<td>@if(isset($studioApplyDetail['postal_code'])){{$studioApplyDetail['postal_code']}}@endif</td>
										</tr>		
										<tr>
											<th>Country</th>
											<td>@if(isset($studioApplyDetail['country'])){{$studioApplyDetail['country']}}@endif</td>
										</tr>
										<tr>
											<th>Phone</th>
											<td>@if(isset($studioApplyDetail['phone'])){{$studioApplyDetail['phone']}}@endif</td>
										</tr>
										<tr>
											<th>Email</th>
											<td>@if(isset($studioApplyDetail['email'])){{$studioApplyDetail['email']}}@endif</td>
										</tr>
										<tr>
											<th>Date of Birth</th>
											<td>@if(isset($studioApplyDetail['dob'])){{$studioApplyDetail['dob']}}@endif</td>
										</tr>
										<tr>
											<th>Nationality 1</th>
											<td>@if(isset($studioApplyDetail['country1'])){{$studioApplyDetail['country1']}}@endif</td>
										</tr>
										<tr>
											<th>Nationality 2</th>
											<td>@if(isset($studioApplyDetail['country2'])){{$studioApplyDetail['country2']}}@endif</td>
										</tr>
										<?php $index=0; ?>
										@if(isset($studioApplyDetail['institution']))
											@foreach($studioApplyDetail['institution'] as $institution)
												<tr>
													<th>Institution</th>
													<td>@if($institution){{$institution}}@endif</td>
												</tr>
												<tr>
													<th>Degree</th>
													<td>
														@if($studioApplyDetail['degree'][$index]){{$studioApplyDetail['degree'][$index]}}@endif
													</td>
												</tr>
												<tr>
													<th>Start Year</th>
													<td>	
														@if($studioApplyDetail['start_year'][$index]){{$studioApplyDetail['start_year'][$index]}}@endif
													</td>
												</tr>
												<tr>
													<th>End Year</th>
													<td>@if($studioApplyDetail['end_year'][$index]){{$studioApplyDetail['end_year'][$index]}}@endif</td>
												</tr>
												<?php $index++; ?>
											@endforeach
										@endif
										<tr>
											<th>Languages</th>
											<td>@if($studioApplyDetail['language_list']){{$studioApplyDetail['language_list']}}@endif</td>
										</tr>
										@if(isset($studioApplyDetail['other_language']))
											<tr>
												<th>Other Language</th>
												<td>{{$studioApplyDetail['other_language']}}</td>
											</tr>
										@endif	
										@if(isset($studioApplyDetail['profesional_situation']))
											<tr>
												<th>Professional Situation:</th>
												<td>{{$studioApplyDetail['profesional_situation']}}</td>
											</tr>
										@endif
										@if(isset($studioApplyDetail['experience_years']))
											<tr>
												<th>Experience Years:</th>
												<td>{{$studioApplyDetail['experience_years']}}</td>
											</tr>
										@endif	
										@if(isset($studioApplyDetail['experience_months']))
											<tr>
												<th>Experience Months:</th>
												<td>{{$studioApplyDetail['experience_months']}}</td>
											</tr>
										@endif	
										@if(isset($studioApplyDetail['experience_countries']))
											<tr>
												<th>Experience Countries:</th>
												<td>{{$studioApplyDetail['experience_countries']}}</td>
											</tr>
										@endif
										@if(isset($studioApplyDetail['applied_at_architenko']))
											<tr>
												<th>Have You Applied At Architenko – Urban Design And Architecture Before?*</th>
												<td>{{$studioApplyDetail['applied_at_architenko']}}</td>
											</tr>
										@endif
										@if(isset($studioApplyDetail['aboutus']))
											<tr>
												<th>About us</th>
												<td>{{$studioApplyDetail['aboutus']}}</td>
											</tr>
										@endif
										@if(isset($studioApplyDetail['comments']))
											<tr>
												<th>Comments</th>
												<td>{{$studioApplyDetail['comments']}}</td>
											</tr>
										@endif	
										@if(isset($studioApplyDetail['cover_letter']))
											<tr>
												<th>Cover Letter</th>
												<td><a href="{{URL('images/studio',$studioApplyDetail['cover_letter'])}}" download >Download </a></td>
											</tr>
										@endif
										@if(isset($studioApplyDetail['curriculum_vitae']))
											<tr>
												<th>Curriculum Vitae</th>
												<td><a href="{{URL('images/studio',$studioApplyDetail['curriculum_vitae'])}}" download >Download </a></td>
											</tr>
										@endif
										@if(isset($studioApplyDetail['portfolio']))
											<tr>
												<th>Portfolio</th>
												<td><a href="{{URL('images/studio',$studioApplyDetail['portfolio'])}}" download >Download </a></td>
											</tr>
										@endif
									</tbody>
								</table>
							@endif						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection						