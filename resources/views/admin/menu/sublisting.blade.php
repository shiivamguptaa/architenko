@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		<div class="top-section">
			<div class="container-fluid">			
				<p class="top-add-btn"><a href="{{ url('submenu/create') }}" class="btn btn-success">Add New Sub Menu</a></p>		
			</div>
		</div>
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Sub Menu Listing</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-2 pull-right">
									<label for="select_lang">Select Language</label>
									<select id="select_lang"  class="form-control" name="lang">
										<option value="" selected="" disabled="">Select Language</option>
											<option value="en" @isset($_GET['lang']) @if($_GET['lang'] == 'en') selected @endif @endif>en</option>
											<option value="nl" @isset($_GET['lang']) @if($_GET['lang'] == 'nl') selected @endif @endif>nl</option>
									</select>
								</div>
							</div>
							<div class="table-responsive">
								<table id="dtOrderExample1" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th>#</th>
											<th>Name</th>
											<th>Parent Menu</th>
											<th>Link</th>
											<th>Language</th>
											<th>Order by</th>
											<th class="text-right">Actions</th>
										</tr>
									</thead>
									<tbody>										
										@if(count($menu) > 0)
										<?php $counter =1; ?>
										@foreach($menu as $menudata)
										<tr>
											<td>{{$counter}}</td>
											<td>{{$menudata->submenuname}}</td>
											<td>{{$menudata->parentmenuname}}</td>
											<td>{{$menudata->link}}</td>
											<td>{{$menudata->language}}</td>
											<td><input type="number" id="menu{{$menudata->id}}" class="orderby form-control" onfocusout="onChangeOrderBy({{$menudata->id}})" data-id ="{{$menudata->id}}" value="{{$menudata->orderby}}"  /></td>

											<td class="td-actions text-right">												
												<a href="{{ url('submenu/'.$menudata->id.'/edit') }}" class="btn btn-success">
													<i class="material-icons">edit</i>
												</a>
												<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('submenu/'.$menudata->id.'/delete') }}',this)" class="btn btn-danger">
													<i class="material-icons">close</i>
												</a>
											</td>
										</tr>
										<?php $counter++; ?>
										@endforeach										
										@endif
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>						

<script>
function onChangeOrderBy(id) {
	let orderby = $("#menu"+id).val();
	$.ajax({
		url:"{{route('orderby')}}",
		headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
		data:{id:id,orderby:orderby},
		type:"POST",
		success:function(response) {
			console.log(response);
		},
		error:function(error){	
			console.log(error);
		}
	});
}
</script>
@endsection								
@section('extrascript')
<script type="text/javascript">
    
    $(document).ready(function () {
      $('#dtOrderExample1').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
          { className: "td-actions text-right",orderable: false , "targets": [6] },
          { orderable: false , "targets": [5] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
<script type="text/javascript">
	let BASE_URL = '{{URL("/")}}';
    $(document).ready(function () {
      	var table = $('#dtOrderExample1').DataTable();
      	$(document).on('change','#select_lang',function(){
      		let lang = $(this).val();
      		$.ajax({
				url:"{{route('submanubylang')}}",
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				data:{lang:lang},
				type:"POST",
				success:function(response){
					if (response.responseCode == 1) {
						table.clear().draw();
						$.each(response.data,function(key,value){
							let edit = BASE_URL+'/submenu/'+value.id+'/edit';
							let del = BASE_URL+'/submenu/'+value.id+'/delete';
							let action = `<div class="td-actions text-right"><a href="${edit}" class="btn btn-success">
														<i class="material-icons">edit</i>
													</a>								
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('${del}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a></div>`;
							let text = `<input type="number" id="menu${value.id}" class="orderby form-control" onfocusout="onChangeOrderBy(${value.id})" data-id ="${value.id}" value="${value.orderby}"  />`
							table.row.add([key+1,value.submenuname,value.parentmenuname,value.link,value.language,text,action]).draw();
						});
					}
				},
				error:function(error){	
					console.log(error);
				}
			});
      	});
      	
    });

</script>
@endsection