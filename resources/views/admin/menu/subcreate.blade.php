@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Add New Sub Menu</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<form method="POST" action="{{ url('submenu/store') }}" enctype="multipart/form-data">
									@csrf				
									<div class="form-group col-md-3">
										<label for="inputState">Select Parent Menu</label>
										<select id="inputState" class="form-control" name="parentmenu">						
											@foreach($menus as $menu)						
											<option value="{{$menu->id}}">{{$menu->name}} ({{strtoupper($menu->language)}})</option>						
											@endforeach						
										</select>
									</div>				
									<div class="form-group row">
										<label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Menu Name') }}</label>					
										<div class="col-md-6">
											<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>						
											@error('name')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>				
									<div class="form-group row">
										<label for="link" class="col-md-4 col-form-label text-md-left">{{ __('Link') }}</label>				
										<div class="col-md-6">
											<input id="limk" type="text" class="form-control @error('name') is-invalid @enderror" name="link" value="{{ old('link') }}" required>				
											@error('link')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
									<div class="form-group col-md-3">
										<label for="inputState">Select Language</label>
										<select id="inputState" class="form-control" name="lang">
											@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}">{{$language->name}}</option>
											@endforeach
											@else
											<option value="en">en</option>
											<option value="nl">nl</option>
											@endif
										</select>
									</div>
									<div class="form-group row mb-0">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												{{ __('Submit') }}
											</button>
										</div>
									</div>
								</form>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection												