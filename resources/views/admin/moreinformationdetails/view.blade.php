@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
        <div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">

							@if(!empty($moreInformationDetail))
								<table class="table table-bordered">
									<tbody>
											<tr>
												<th>Name</th>
												<th>Value</th>
											</tr>
											<tr>
												<th>Service Name</th>
												<td>@if($moreInformationDetail['title']){{$moreInformationDetail['title']}}@endif</td>
											</tr>	
											<tr>
												<th>Project Address</th>
												<td>@if($moreInformationDetail['project_address']){{$moreInformationDetail['project_address']}}@endif</td>
											</tr>		
											<tr>
												<th>Project Description</th>
												<td>@if($moreInformationDetail['description_project']){{$moreInformationDetail['description_project']}}@endif</td>
											</tr>		
											<tr>
												<th>First Name</th>
												<td>@if($moreInformationDetail['first_name']){{$moreInformationDetail['first_name']}}@endif</td>
											</tr>
											<tr>
												<th>Middle Name</th>
												<td>@if($moreInformationDetail['middle_name']){{$moreInformationDetail['middle_name']}}@endif</td>
											</tr>
											<tr>
												<th>Last Name</th>
												<td>@if($moreInformationDetail['last_name']){{$moreInformationDetail['last_name']}}@endif</td>
											</tr>
											<tr>
												<th>Gender</th>
												<td>@if($moreInformationDetail['gender']){{$moreInformationDetail['gender']}}@endif</td>
											</tr>		
											<tr>
												<th>Address</th>
												<td>@if($moreInformationDetail['address']){{$moreInformationDetail['address']}}@endif</td>
											</tr>		
											<tr>
												<th>City</th>
												<td>@if($moreInformationDetail['city']){{$moreInformationDetail['city']}}@endif</td>
											</tr>			
											<tr>
												<th>Postal Code</th>
												<td>@if($moreInformationDetail['postal_code']){{$moreInformationDetail['postal_code']}}@endif</td>
											</tr>		
											<tr>
												<th>Country</th>
												<td>@if($moreInformationDetail['country']){{$moreInformationDetail['country']}}@endif</td>
											</tr>
											<tr>
												<th>Phone</th>
												<td>@if($moreInformationDetail['phone']){{$moreInformationDetail['phone']}}@endif</td>
											</tr>
											<tr>
												<th>Email</th>
												<td>@if($moreInformationDetail['email']){{$moreInformationDetail['email']}}@endif</td>
											</tr>
											<tr>
												<th>Date of Birth</th>
												<td>@if($moreInformationDetail['dob']){{$moreInformationDetail['dob']}}@endif</td>
											</tr>
											<tr>
												<th>Person Position</th>
												<td>@if($moreInformationDetail['person_position']){{$moreInformationDetail['person_position']}}@endif</td>
											</tr>
											@if($moreInformationDetail['person_position']=="Private individual")
											<tr>
												<th>Private Individual First Name</th>
												<td>@if($moreInformationDetail['private_individual_first_name']){{$moreInformationDetail['private_individual_first_name']}}@endif</td>
											</tr>
											<tr>
												<th>Private Individual Middle Name</th>
												<td>@if($moreInformationDetail['private_individual_middle_name']){{$moreInformationDetail['private_individual_middle_name']}}@endif</td>
											</tr>
											<tr>
												<th>Private Individual Last Name</th>
												<td>@if($moreInformationDetail['private_individual_last_name']){{$moreInformationDetail['private_individual_last_name']}}@endif</td>
											</tr>
											<tr>
												<th>Private Individual Address</th>
												<td>@if($moreInformationDetail['private_individual_address']){{$moreInformationDetail['private_individual_address']}}@endif</td>
											</tr>
												<tr>
												<th>Private Individual City</th>
												<td>@if($moreInformationDetail['private_individual_city']){{$moreInformationDetail['private_individual_city']}}@endif</td>
											</tr>
											<tr>
												<th>Private Individual Postal Code</th>
												<td>@if($moreInformationDetail['private_individual_postal_code']){{$moreInformationDetail['private_individual_postal_code']}}@endif</td>
											</tr>
											<tr>
												<th>Private Individual Country</th>
												<td>@if($moreInformationDetail['private_individual_country']){{$moreInformationDetail['private_individual_country']}}@endif</td>
											</tr>
											<tr>
												<th>Private Individual Phone</th>
												<td>@if($moreInformationDetail['private_individual_phone']){{$moreInformationDetail['private_individual_phone']}}@endif</td>
											</tr>
											<tr>
												<th>Private Individual Email</th>
												<td>@if($moreInformationDetail['private_individual_email']){{$moreInformationDetail['private_individual_email']}}@endif</td>
											</tr>
											<tr>
												<th>Private Individual Date of Birth</th>
												<td>@if($moreInformationDetail['private_individual_dob']){{$moreInformationDetail['private_individual_dob']}}@endif</td>
											</tr>
											@else
											<tr>
												<th>EnterprisePrice Title </th>
												<td>@if($moreInformationDetail['enterprise_price_title1']){{$moreInformationDetail['enterprise_price_title1']}}@endif</td>
											</tr>
											<tr>
												<th>EnterprisePrice Title</th>
												<td>@if($moreInformationDetail['enterprise_price_title2']){{$moreInformationDetail['enterprise_price_title2']}}@endif</td>
											</tr>
											<tr>
												<th>EnterprisePrice Address</th>
												<td>@if($moreInformationDetail['enterprise_address_title']){{$moreInformationDetail['enterprise_address_title']}}@endif</td>
											</tr>	
											<tr>
												<th>EnterprisePrice City</th>
												<td>@if($moreInformationDetail['enterprise_city_title']){{$moreInformationDetail['enterprise_city_title']}}@endif</td>
											</tr>
												<tr>
												<th>EnterprisePrice Postal Code</th>
												<td>@if($moreInformationDetail['enterprise_postal_code_title']){{$moreInformationDetail['enterprise_postal_code_title']}}@endif</td>
											</tr>
											<tr>
												<th>EnterprisePrice Country</th>
												<td>@if($moreInformationDetail['enterprise_country_title']){{$moreInformationDetail['enterprise_country_title']}}@endif</td>
											</tr>	
											<tr>
												<th>EnterprisePrice Phone</th>
												<td>@if($moreInformationDetail['enterprise_phone_title']){{$moreInformationDetail['enterprise_phone_title']}}@endif</td>
											</tr>
											<tr>
												<th>EnterprisePrice Email</th>
												<td>@if($moreInformationDetail['enterprise_email_title']){{$moreInformationDetail['enterprise_email_title']}}@endif</td>
											</tr>
											<tr>
												<th>EnterprisePrice Contact First Name</th>
												<td>@if($moreInformationDetail['enterprise_contact_first_name_title']){{$moreInformationDetail['enterprise_contact_first_name_title']}}@endif</td>
											</tr>
											<tr>
												<th>EnterprisePrice Contact Last Name</th>
												<td>@if($moreInformationDetail['enterprise_contact_last_name_title']){{$moreInformationDetail['enterprise_contact_last_name_title']}}@endif</td>
											</tr>
											<tr>
												<th>EnterprisePrice Contact Position</th>
												<td>@if($moreInformationDetail['enterprise_position']){{$moreInformationDetail['enterprise_position']}}@endif</td>
											</tr>
											@endif
									</tbody>
								</table>
									

							@endif
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>

@endsection						