@extends('layouts.auth')
@section('content')
<div class="main-panel">
	<div class="content">
		@foreach(['danger','warning','success','info'] as $msg)
		@if(Session::has('alert-'.$msg))
		<div class="alert alert-{{ $msg }} alertmanage" role="alert">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
		@endif
		@endforeach		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">{{$title}}</h4>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-2 pull-right">
									<label for="select_lang">Select Language</label>
									<select id="select_lang" class="form-control" name="lang">
										<option value="" selected disabled>Select Language</option>
										@if(count($languages)>0)
											@foreach($languages as $language)
											<option value="{{$language->name}}">{{$language->name}}</option>
											@endforeach						  
										@endif
									</select>
								</div>
							</div>
							<div class="table-responsive">
								<table id="dtOrderExample" class="table dataTable" cellspacing="0" width="100%">
									<thead class="">
										<tr>
											<th class="text-center">#</th>
											<th>First Name</th>
											<th>Middle Name</th>
											<th>Last Name</th>
											<th>Address</th>
											<th>email</th>
											<th>Phone</th>
											<th>Date</th>
											<th>Language</th>
											<th class="td-actions text-right td-actions text-right">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php $counter = 1; ?>
										@foreach($moreInformationDetails as $moreInformationDetail)
											<tr>							
												<td class="text-center">{{$counter}}</td>										
												<td>{{ $moreInformationDetail->first_name }}</td>
												<td>{{ $moreInformationDetail->middle_name }}</td>
												<td>{{ $moreInformationDetail->last_name }}</td>
												<td>{{ $moreInformationDetail->address }}</td>
												<td>{{ $moreInformationDetail->email }}</td>
												<td>{{ $moreInformationDetail->phone }}</td>
												<?php $time = strtotime($moreInformationDetail->created_at); ?>
												<td><?=date('Y-m-d',$time)?></td>										
												<td>{{ $moreInformationDetail->language }}</td>	
												<td class="td-actions text-right td-actions text-right">
													<a href="{{url('moreinformationdetails/'.$moreInformationDetail->id.'/view')}}" class="btn btn-success view">
														<i class="material-icons">remove_red_eye</i>
													</a>
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('{{ url('moreinformationdetails/delete',$moreInformationDetail->id) }}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a>
												</td>											
											</tr>
										<?php $counter++; ?>
										@endforeach											
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.authfooter')	
</div>
@endsection											

@section('extrascript')
<script type="text/javascript">
    $(document).ready(function () {
      $('#dtOrderExample').DataTable({
        "order": [[ 0, "asc" ]],
        responsive: true,
        "autoWidth": false,
        "columnDefs": [
          { className: "td-actions text-right",orderable: false , "targets": [9] }
        ]
      });
      $('.dataTables_length').addClass('bs-select');
    });

</script>
<script type="text/javascript">
	let BASE_URL = '{{URL("/")}}';
    $(document).ready(function () {
      	var table = $('#dtOrderExample').DataTable();
      	$(document).on('change','#select_lang',function(){
      		let lang = $(this).val();
      		$.ajax({
				url:"{{route('contactonchanagelanguage')}}",
				headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
				data:{lang:lang},
				type:"POST",
				success:function(response){
					// console.table(response.data);
					if (response.responseCode == 1) {
						table.clear().draw();
						$.each(response.data,function(key,value){
							let edit = BASE_URL+'/moreinformationdetails/'+value.id+'/view';
							let del = BASE_URL+'/moreinformationdetails/delete/'+value.id;

							var date    = new Date(value.created_at),
						    yr      = date.getFullYear(),
						    month   = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
						    day     = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate(),
						    newDate = yr + '-' + month+1 + '-' + day;

							let action = `<div class="td-actions text-right">
													<a href="${edit}" class="btn btn-success">
														<i class="material-icons">remove_red_eye</i>
													</a>
													<a href="javascript:void(0)" type="button" onclick="deleteRecord('${del}',this)" class="btn btn-danger">
														<i class="material-icons">close</i>
													</a>								
													</div>`;
							table.row.add([key+1,value.first_name,value.middle_name,value.last_name,value.address,value.email,value.phone,newDate,value.language,action]).draw();
						});
					}
				},
				error:function(error){	
					console.log(error);
				}
			});
      	});
      	
    });
</script>
@endsection