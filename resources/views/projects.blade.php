@extends('layouts.front')

@section('content')
<div class="main projects-page">
    <div class="container" id="projects-wrapper">
        <div class="row search-bar-row">
            <div class="col">
                <div class="search-bar">
                    <div class="input-wrapper">
                        <input type="text" class="search-input" placeholder="Search for article">
					</div>
                    <div class="result-wrapper">
                        <div class="result-padding">
                            <div class="result-inner" id="result-inner">
                                <ul class="cat-result">
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        @if(isset($projects[0]->title))
		<div class="row row1">
			<div class="col">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[0]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[0]->title}}</p>
				</a>
			</div>
		</div>@endif
		
		<div class="row row2">
			@if(isset($projects[1]->title))
			<div class="col-l">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[1]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[1]->title}}</p>
				</a>
			</div>@endif
			@if(isset($projects[2]->title))
			<div class="col-r">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[2]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[2]->title}}</p>
				</a>
			</div>@endif
		</div>
		@if(isset($projects[3]->title))
		<div class="row row3">
			<div class="col">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[3]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[3]->title}}</p>
				</a>
			</div>
		</div>@endif
		@if(isset($projects[4]->title))
		<div class="row row4">
			<div class="col">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[4]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[4]->title}}</p>
				</a>
			</div>
		</div>
		@endif
		
		<div class="row row5">
			@if(isset($projects[5]->title))
			<div class="col-l">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[5]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[5]->title}}</p>
				</a>
			</div>@endif
			@if(isset($projects[6]->title))
			<div class="col-r">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[6]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[6]->title}}</p>
				</a>
			</div>@endif
		</div> 
		
		@if(isset($projects[7]->title))
		<div class="row row3">
			<div class="col">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[7]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[7]->title}}</p>
				</a>
			</div>
		</div>@endif
		
		@if(isset($projects[8]->title))
		<div class="row row1">
			<div class="col">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[8]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[8]->title}}</p>
				</a>
			</div>
		</div>@endif
		
		<div class="row row2">
			@if(isset($projects[9]->title))
			<div class="col-l">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[9]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[9]->title}}</p>
				</a>
			</div>@endif
			@if(isset($projects[10]->title))
			<div class="col-r">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[10]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[10]->title}}</p>
				</a>
			</div>@endif
		</div>
        @if(isset($projects[11]->title))
		<div class="row row3">
			<div class="col">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[11]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[11]->title}}</p>
				</a>
			</div>
		</div>@endif
		@if(isset($projects[12]->title))
		<div class="row row4">
			<div class="col">
				<a href="">
					<div class="cont-wrapper">
						<img src="{{asset('images/project/'.$projects[12]->datavalue)}}"  alt="">
					</div>
					<p>{{$projects[12]->title}}</p>
				</a>
			</div>
		</div>
		@endif
        <div class="pagination">
		{{ $projects->links() }}
		</div>
		<div class="row subscribe-row-project">
			<div class="col">
				<div class="subscribe-form">
					<p>{!! $newslettertext['description'] !!}	</p>		
					@if (\Session::has('success'))
					<div class="alert alert-success">
					<p>{{ \Session::get('success') }}</p>
					</div><br />
					@endif
					@if (\Session::has('failure'))
					<div class="alert alert-danger">
					<p>{{ \Session::get('failure') }}</p>
					</div><br />
					@endif
					<form method="POST" action="{{url('newsletter/store')}}">
						@csrf
						@if (\Session::get('locale')=="en")
						<input type="email" oninvalid="setCustomValidity('Enter email id')" class="input" name="email" placeholder="E-mail:" required>
						@else
						<input type="email" oninvalid="setCustomValidity('Voer e-mail ID in')" class="input" name="email" placeholder="E-mail:" required>
						@endif
						<input type="submit" class="submit" value="{{getLabel('submit')}}">
					</form>
				</div>
			</div>
		</div>
	</div>			
</div>																																				
@endsection						