@extends('layouts.front')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha256-UK1EiopXIL+KVhfbFa8xrmAWPeBjMVdvYMYkTAEv/HI=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" integrity="sha256-4hqlsNP9KM6+2eA8VUT0kk4RsMRTeS7QGHIM+MZ5sLY=" crossorigin="anonymous" />
<div class="main services services-inner">  
   <div class="container pd-10">
        <div class="row">
            <div class="col-lg-12">
                <a href="javascript:void(0)" onclick="window.history.back()" class="back-button show-on-mobile"><img src="{{URL('images/link-arrow.png')}}" alt=""> {{getLabel('return')}}</a>
            </div>
        </div>
    </div>
    <div class="slider-parent">
        <div class="container inner-slider-wrapper service-inner-slider">
            <div class="row ">
                <div class="inner-slider">                   
                    @foreach($services as $service) 
                        <div class="slide">
                            <div class="inner" data-description="{{$service['moretitle']}}"> 
                                @if($service['type']=="image")  
                                    <!-- <a href="">
                                        <div class="h-c-wrapper">                       
                                            <div class="cont-wrapper">   -->
                                                <img src="{{URL('images/service/'.$service['datavalue'])}}" />
                                            <!-- </div>
                                            <h2 class="r-title">{{$service['title']}}</h2>
                                        </div>
                                        <div class="text-b"><p>{{$service['subtitle']}}</p></div>
                                    </a>     -->
                                @elseif($service['type']=="youtube")
                                    <!-- <a href=""> -->
                                       <!--  <div class="h-c-wrapper">                       
                                            <div class="cont-wrapper">  -->                 
                                                <div class="youtube-video-wrapper">
                                                    <iframe src="{{$service['datavalue']}}"  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                                                </div>                          
                                            <!-- </div> -->
                                            <!-- <h2 class="r-title">{{$service['title']}}</h2> -->                      
                                      <!--   </div>          --> 
                                        <!-- <div class="text-b"><p>{{$service['subtitle']}}</p></div>        -->
                                   <!--  </a>   -->  
                                @elseif($service['type']=="video")
                                  <!--   <a href="">
                                        <div class="h-c-wrapper">                       
                                            <div class="cont-wrapper"> -->
                                                <div class="video-wrapper">
                                                    <video width='100%' controls><source src="{{URL('images/service/'.$service['datavalue'])}}" type="video/mp4"></video>
                                                </div>  
                                             <!--        
                                            </div>  
                                            <h2 class="r-title">{{$service['title']}}</h2>                      
                                        </div>      
                                        <div class="text-b"><p>{{$service['subtitle']}}</p></div>
                                    </a>     -->
                                @elseif($service['type']=="book")
                                    <?php  $image_list = json_decode($service['datavalue']);    ?>
                                 <!--    <a href="">
                                        <div class="h-c-wrapper">
                                            <div class="cont-wrapper"> -->
                                                <div class="book-wrapper">
                                                    <?php $index=0; foreach($image_list as $image) { 
                                                        echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/service/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/service/".$image).'" />';
                                                        $index++;   
                                                    } ?>
                                                </div>
                                            <!-- </div>
                                            <h2 class="r-title">{{$service['title']}}</h2>                              
                                        </div>
                                        <div class="text-b"><p>{{$service['subtitle']}}</p></div>                       
                                    </a>     -->
                                @elseif($service['type']=="flip")
                                    <?php $image_list = json_decode($service['datavalue']);
                                        if(count($image_list)>0){ ?>
                                       <!--      <a>
                                                <div class="h-c-wrapper">
                                                    <div class="cont-wrapper"> -->
                                                        <div class="twentytwenty-wrapper twentytwenty-horizontal">
                                                            <div class="before-after-wrapper twentytwenty-container active">
                                                            <?php foreach($image_list as $image){
                                                                echo '<img width="100%"   src="'.URL("images/service/".$image).'" alt="">';
                                                            } ?>
                                                                <div class="twentytwenty-handle" >
                                                                    <span class="twentytwenty-left-arrow"></span>
                                                                    <span class="twentytwenty-right-arrow"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   <!--  </div>
                                                    <h2 class="r-title">{{$service['title']}}</h2>  
                                                </div>
                                                <div class="text-b"><p>{{$service['subtitle']}}</p></div>
                                            </a> -->
                                            <?php 
                                        }
                                    ?>
                                @endif   
                                </div></div>       
                            @endforeach
                </div>
            </div>
            <div class="next"></div>
            <div class="prev"></div>
        </div>
      
        <div class="container text-services slider-title">
            <div class="row">
                <div class="col-7 pd-15">
                    <h2 class="service-main-title main-title">
                    {{$servicelist->title}}
                    </h2>
                </div>
                  <div class="col-5 pd-15">
                    <a href="#" id="pic-description">First Image</a>
                </div>
            </div>

        </div>
    </div>  
    <div class="container text-services">
        <div class="row">
            <div class="col-lg-6 pd-15">
                <h4 class="section-title"><?php echo $servicelist->subtitle;  ?></h4>
                <div class="text-wrapper">
                   <?php echo $servicelist->descriptionleft;  ?>
                </div>
            </div>
            <div class="col-lg-6 pd-15">
                <h4 class="section-title"><?php echo $servicelist->subtitle;  ?></h4>
                <div class="text-wrapper">
                   <?php echo $servicelist->descriptionright;  ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
    	 <div class="row">
    	 	 <div class="col-md-12 pd-15">

        <div class="subscribe-form">
            <p>{!! $newslettertext['description'] !!}   </p>        
            @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
            @endif
            @if (\Session::has('failure'))
            <div class="alert alert-danger">
                <p>{{ \Session::get('failure') }}</p>
            </div><br />
            @endif
            <form id="newsletterForm" method="POST" action="{{url('newsletter/store')}}">
                @csrf
                <input type="email" class="input email-input" id="input" name="email" placeholder="E-mail:" />
                <input type="submit" class="submit submit-input" value="{{getLabel('subscribe')}}" /> 
            </form>
        </div>
    </div>
</div> </div>
    </div>



@endsection						