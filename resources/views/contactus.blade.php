@extends('layouts.front')
@section('content')


<div class="main contacts">
    <div class="container">
        <div class="contact-page-wrapper">
            <?php if(!empty($contactus)) : ?>
            <div class="row">
                <div class="col-lg-6 dots">
                    <img src="{{URL('images/contactus',$contactus->image)}}" class="section-logo m" alt="">
                    <div class="mail-section">
                        <h4 class="section-title">{{$contactus->enquiry_title}}</h4>
                        <a href="mailto:{{$contactus->enquiry_email}}">{{$contactus->enquiry_email}}</a>
                        <h4 class="section-title">{{$contactus->communication_title}}</h4>
                        <a href="mailto:{{$contactus->communication_email}}">{{$contactus->communication_email}}</a>
                    </div>
                    @foreach($contactusDetails as $contact)
                        <div class="address-section">
                            <h4 class="section-title">{{$contact->title}}</h4>
                            <p>{{$contact->address}}</p>
                            <p>Tel:{{$contact->phone_number}}</p>
                            <a target="_blank" href="https://www.google.com/maps?q={{$contact->address}}">Open in Google Maps</a>
                            <a class="open-direction" type="button" data-id="{{$contact->id}}"  >{{getLabel('direction')}}</a>
                        </div>
                    @endforeach
                </div>
                <div class="col-lg-6">
                    <img src="{{URL('images/contactus',$contactus->image)}}" class="section-logo" alt="">
                    <div class="invoice-wrapper">
                        <?php echo $contactus->invoice_information; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-12">
                     <div class="subscribe-form">
                        <p>{!! $newslettertext['description'] !!}   </p>        
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div><br />
                        @endif
                        @if (\Session::has('failure'))
                        <div class="alert alert-danger">
                            <p>{{ \Session::get('failure') }}</p>
                        </div><br />
                        @endif
                        <form id="newsletterForm" method="POST" action="{{url('newsletter/store')}}">
                            @csrf
                            <input type="email" class="input email-input" id="input" name="email" placeholder="E-mail:" />
                            <input type="submit" class="submit submit-input" value="{{getLabel('send')}}" /> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>	
@if(!empty($contactusDetails))
@foreach($contactusDetails as $contactusDetail)		
<div id="directions{{$contactusDetail->id}}" class="directions-wrapper">
    <div class="col">
        <div class="hide close-icon close-direction" data-id="{{$contactusDetail->id}}">close [x]</div>
        <div class="inner-wrapper">
            <img src="{{URL('images/contactus',$contactusDetail->pop_image)}}" class="img" alt="">
            <div class="address-section">
                <h4 class="section-title">{{$contactusDetail->enquiry_title}}</h4>
                <p>{{$contactusDetail->address}}</p>
              <p>Tel:{{$contactusDetail->phone_number}}</p>
            </div>
            <p><?php echo $contactusDetail->description; ?></p>
        </div>
        <div class="text-center">
            <a type="button" download href="{{URL('images/contactus',$contactusDetail->direction)}}" class="download-direction" value="Download Direction">{{getLabel('downloaddirection')}}</a>
        </div>
    </div>
</div>
@endforeach
@endif

@endsection						