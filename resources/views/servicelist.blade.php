@extends('layouts.front')

@section('content')
<div class="main">
     <div class="container " id="services-wrapper">
        <div class="row intro-text">
            <div class="col-h">
               <?php 
                if(!empty($serviceTopDescription)){
                    echo $serviceTopDescription->description;
                }
               ?>
            </div>
        </div>
            
        <?php $number=1; $row_number=1; $next_index_no_two = 2; $next_index_no_three=3;
        $inner_arr = count($services);
        ?>

        @foreach($services as $service) 
                @if($row_number==$next_index_no_two)
                    <div class="row row{{$number}}">    
                    <div class="col col-l">         
                    <?php $number++;  ?>    
                @elseif($row_number==$next_index_no_three)  
                    <div class="col col-r">
                @else
                    <div class="row row{{$number}}">
                    <div class="col">
                        <?php $number++;  ?>    
                @endif  
                @if($service['type']=="image")  
               <a href="{{ url('servicedetails/'.$service['serviceId']) }}">
                    <div class="h-c-wrapper">                       
                        <div class="cont-wrapper">  
                            <img src="{{URL('images/service/'.$service['datavalue'])}}" />
                        </div>
                        <h2 class="r-title">{{$service['title']}}</h2>
                    </div>
                    <div class="text-b"><p>{{$service['subtitle']}}</p></div>
                </a>    
                @elseif($service['type']=="youtube")
                <a href="{{ url('servicedetails/'.$service['serviceId']) }}">
                    <div class="h-c-wrapper">                       
                        <div class="cont-wrapper">                  
                            <div class="youtube-video-wrapper">
                                <iframe src="{{$service['datavalue']}}"  width='100%' class='youtube-video' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                            </div>                          
                        </div>
                        <h2 class="r-title">{{$service['title']}}</h2>                      
                    </div>          
                    <div class="text-b"><p>{{$service['subtitle']}}</p></div>       
                </a>    
                @elseif($service['type']=="video")
               <a href="{{ url('servicedetails/'.$service['serviceId']) }}">
                    <div class="h-c-wrapper">                       
                        <div class="cont-wrapper">
                            <div class="video-wrapper">
                                <video width='100%' controls><source src="{{URL('images/service/'.$service['datavalue'])}}" type="video/mp4"></video>
                            </div>  
                                
                        </div>  
                        <h2 class="r-title">{{$service['title']}}</h2>                      
                    </div>      
                    <div class="text-b"><p>{{$service['subtitle']}}</p></div>
                </a>    
                @elseif($service['type']=="book")
                    <?php  $image_list = json_decode($service['datavalue']);    ?>
                    <a href="{{ url('servicedetails/'.$service['serviceId']) }}">
                        <div class="h-c-wrapper">
                            <div class="cont-wrapper">
                                <div class="book-wrapper">
                                    <?php $index=0; foreach($image_list as $image) { 
                                        echo  ($index==0) ? '<img  width="100%"  src="'.URL("images/service/".$image).'" class="active" />' : '<img width="100%" src="'.URL("images/service/".$image).'" />';
                                        $index++;   
                                    } ?>
                                </div>
                            </div>
                            <h2 class="r-title">{{$service['title']}}</h2>                              
                        </div>
                        <div class="text-b"><p>{{$service['subtitle']}}</p></div>                       
                    </a>    
                @elseif($service['type']=="flip")
                    <?php $image_list = json_decode($service['datavalue']);
                        if(count($image_list)>0){ ?>
                            <a href="{{ url('servicedetails/'.$service['serviceId']) }}">
                                <div class="h-c-wrapper"><div class="cont-wrapper"><div class="twentytwenty-wrapper twentytwenty-horizontal"><div class="before-after-wrapper twentytwenty-container active" style="height: 442.016px;">
                            <?php foreach($image_list as $image){
                                echo '<img width="100%"   src="'.URL("images/service/".$image).'" alt="">';
                            } ?>
                            <div class="twentytwenty-handle" style="left: 27.0156px;"><span class="twentytwenty-left-arrow"></span><span class="twentytwenty-right-arrow"></span></div></div></div></div>
                            <h2 class="r-title">{{$service['title']}}</h2>  
                            </div>
                            <div class="text-b"><p>{{$service['subtitle']}}</p></div>
                            </a>
                            <?php 
                        }
                    ?>
                @endif          
            @if($row_number>=$inner_arr && $row_number==$next_index_no_three)       
                </div></div>
            @elseif($row_number!=$next_index_no_two)
                </div>  
            @elseif($inner_arr==$row_number)
                </div>
                </div>
            @endif 
            @if($row_number==$next_index_no_two)
                <?php $next_index_no_two = $next_index_no_two+4; ?> 
            @endif 
            @if($row_number==$next_index_no_three)  
                    <?php $next_index_no_three = $next_index_no_three+4; ?> 
            @endif
        </div>
        <?php $row_number++; if($row_number>=5){$row_number=1;$next_index_no_two=2;$next_index_no_three=3;$number=1;} ?>
        @endforeach
    <div class="container service-subscribe-form-outer">
        <div class="row">
        
            <div class="subscribe-form">
					<p>{!! $newslettertext['description'] !!}	</p>		
					@if (\Session::has('success'))
					<div class="alert alert-success">
					<p>{{ \Session::get('success') }}</p>
					</div><br />
					@endif
					@if (\Session::has('failure'))
					<div class="alert alert-danger">
					<p>{{ \Session::get('failure') }}</p>
					</div><br />
					@endif
					<form id="newsletterForm" method="POST" action="{{url('newsletter/store')}}">
                        @csrf
                        <input type="email" class="input email-input" id="input" name="email" placeholder="E-mail:" />
                        <input type="submit" class="submit submit-input" value="{{getLabel('subscribe')}}" /> 
                    </form>
                </div>
			
        </div>
    </div>
    </div>
</div>				
@endsection						