<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   // return view('welcome');
});

Auth::routes();

Route::get('/', 'CmsController@index')->name('home');
Route::post('/getProjectList', 'CmsController@getProjectList')->name('getProjectList');

Route::get('/users', 'Usercontroller@index');
Route::get('/users/create', 'Usercontroller@create');
Route::post('/users/store', 'Usercontroller@store');
Route::get('/users/edit/{id}', 'Usercontroller@edit');
Route::post('/users/update', 'Usercontroller@update');
Route::post('/users/delete/{id}', 'Usercontroller@delete');



Route::get('/projects', 'FrontProjectController@index');
Route::get('/services', 'FrontServiceController@index');
Route::get('/articles', 'FrontArticleController@index');
Route::get('lang/{locale}', 'LocalizationController@lang');

Route::get('newsletter/create','admin\NewsletterController@create');
Route::post('newsletter/store','admin\NewsletterController@store');
Route::post('newsletter/delete/{id}','admin\NewsletterController@delete');

Route::get('servicelist','ServiceListController@index');
Route::post('getServicelist', 'ServiceListController@getServicelist')->name('getServicelist');
Route::get('servicedetails/{id}', 'ServiceListController@servicedetails')->name('servicedetails');


Route::get('projectlist','ProjectListController@index');
Route::post('getProject', 'ProjectListController@getProject')->name('getProject');
Route::post('searchproject', 'ProjectListController@searchProject')->name('searchProject');
Route::get('projectdetails/{id}', 'ProjectListController@projectDetails')->name('projectdetails');
Route::get('projectinnerpage/{id}', 'ProjectListController@projectInnerPage')->name('projectinnerpage');
Route::post('projectlist/projectDetailsForm','ProjectListController@projectDetailsForm');



Route::get('studiolist','StudioListController@index')->name('studiolist');


Route::get('studio','admin\StudioController@index')->name('studio');
Route::post('studiolistbylang','admin\StudioController@listByLang')->name('studiobylang');
Route::get('studio/create','admin\StudioController@create')->name('studio_create');
Route::post('studio/store','admin\StudioController@store');
Route::get('studio/edit/{id}','admin\StudioController@edit');
Route::post('studio/update','admin\StudioController@update');
Route::post('studio/delete/{id}','admin\StudioController@delete');
Route::get('contactus','ContactUsController@index');



Route::get('articlelist','ArticleListController@index');
Route::post('article/search','ArticleListController@articleSearch')->name('articlesearch');
Route::get('articleinner/{id}','ArticleListController@articleInnerContent');


Route::post('storemoreinformation','MoreInformationController@store')->name('storemoreinformation');
Route::post('storestudioapplyform','StudioListController@storeStudioApplyForm')->name('storestudioapplyform');
Route::post('storeapplytoolkitform','ArticleListController@storeApplyToolkitForm')->name('storeapplytoolkitform');




Route::group(['middleware' => ['auth']],function(){

Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('contact','admin\ContactUsController@index')->name('contact');
Route::get('contact/create','admin\ContactUsController@create')->name('contact');
Route::post('contact/store','admin\ContactUsController@store');
Route::get('contact/edit/{id}','admin\ContactUsController@edit');
Route::post('contact/update','admin\ContactUsController@update');
Route::post('contact/delete/{id}','admin\ContactUsController@delete');
Route::post('contact/orderby','admin\ContactUsController@orderby')->name('contactorderby');
Route::post('contact/onchnagelanguage','admin\ContactUsController@onchnagelanguage')->name('contactonchnagelanguage');


Route::get('country','admin\CountryController@index');
Route::get('country/create','admin\CountryController@create');
Route::post('country/store','admin\CountryController@store');
Route::get('country/edit/{id}','admin\CountryController@edit');
Route::post('country/update','admin\CountryController@update');
Route::post('country/destroy/{id}','admin\CountryController@destroy');
Route::post('country/onchnagelanguage','admin\CountryController@onchnagelanguage')->name('onchnagelanguagecountry');



Route::get('emailform','admin\EmailFormController@index');
Route::get('emailform/create','admin\EmailFormController@create');
Route::post('emailform/store','admin\EmailFormController@store');
Route::get('emailform/edit/{id}','admin\EmailFormController@edit');
Route::post('emailform/update','admin\EmailFormController@update');
Route::post('emailform/delete/{id}','admin\EmailFormController@destroy');






Route::resource('footer', 'ConfigurationController',['except' => ['destroy']]);
Route::post('footer/store','ConfigurationController@store');
Route::post('footer/{id}/delete', 'ConfigurationController@destroy');
Route::get('footer/{id}/edit', 'ConfigurationController@edit');
Route::get('logo/{id}/edit', 'ConfigurationController@editlogo');
Route::post('footer/update', 'ConfigurationController@update');
Route::post('logo/update', 'ConfigurationController@updatelogo');




Route::get('social','admin\SocialController@index');
Route::get('social/create', 'admin\SocialController@create');
Route::post('social/store', 'admin\SocialController@store');
Route::get('social/{id}/edit', 'admin\SocialController@edit');
Route::post('social/update', 'admin\SocialController@update');
Route::post('social/{id}/delete', 'admin\SocialController@destroy');


Route::get('moreinformation','admin\MoreInformationController@index');
Route::get('moreinformation/create', 'admin\MoreInformationController@create');
Route::post('moreinformation/store', 'admin\MoreInformationController@store');
Route::get('moreinformation/{id}/edit', 'admin\MoreInformationController@edit');
Route::post('moreinformation/update', 'admin\MoreInformationController@update');
Route::post('moreinformation/{id}/delete', 'admin\MoreInformationController@destroy');
Route::get('moreinformationdetails','admin\MoreInformationController@moreInformationDetailsList');
Route::get('moreinformationdetails/{id}/view', 'admin\MoreInformationController@moreInformationDetailsById');
Route::post('moreinformation/onchanagelanguage', 'admin\MoreInformationController@onchanagelanguage')->name('contactonchanagelanguage');
Route::post('moreinformationdetails/delete/{id}', 'admin\MoreInformationController@delete');


Route::get('applytooltipform','admin\ApplyToolTipFormController@index');
Route::get('applytooltipform/create', 'admin\ApplyToolTipFormController@create');
Route::post('applytooltipform/store', 'admin\ApplyToolTipFormController@store');
Route::get('applytooltipform/{id}/edit', 'admin\ApplyToolTipFormController@edit');
Route::post('applytooltipform/update', 'admin\ApplyToolTipFormController@update');
Route::post('applytooltipform/{id}/delete', 'admin\ApplyToolTipFormController@destroy');
Route::get('applytooltipformdetails','admin\ApplyToolTipFormController@applyToolkitFormDetails');
Route::get('applytooltipformdetails/{id}/view', 'admin\ApplyToolTipFormController@applyToolkitFormDetailById');
Route::post('applytooltipform/onchnagelanguage', 'admin\ApplyToolTipFormController@onchnagelanguage')->name('applytoolonchnagelanguage');
Route::post('applytooltipformdetails/delete/{id}', 'admin\ApplyToolTipFormController@delete');


Route::get('studioapplydetails','admin\StudioApplyFormController@studioApplyFormDetailsList');
Route::get('studioapplydetails/{id}/view', 'admin\StudioApplyFormController@studioApplyFormDetailById');
Route::post('studioapplydetails/delete/{id}', 'admin\StudioApplyFormController@delete');
Route::get('studioapplyform','admin\StudioApplyFormController@index');
Route::get('studioapplyform/create', 'admin\StudioApplyFormController@create');
Route::post('studioapplyform/store', 'admin\StudioApplyFormController@store');
Route::get('studioapplyform/{id}/edit', 'admin\StudioApplyFormController@edit');
Route::post('studioapplyform/update', 'admin\StudioApplyFormController@update');
Route::post('studioapplyform/{id}/delete', 'admin\StudioApplyFormController@destroy');
Route::post('studioapplyform/onchnagelanguage', 'admin\StudioApplyFormController@onchnagelanguage')->name('studioformonchangelanguage');




Route::get('servicetopdescription','admin\ServiceTopdescriptionController@index');
Route::get('servicetopdescription/create', 'admin\ServiceTopdescriptionController@create');
Route::post('servicetopdescription/store', 'admin\ServiceTopdescriptionController@store');
Route::get('servicetopdescription/{id}/edit', 'admin\ServiceTopdescriptionController@edit');
Route::post('servicetopdescription/update', 'admin\ServiceTopdescriptionController@update');
Route::post('servicetopdescription/{id}/delete', 'admin\ServiceTopdescriptionController@destroy');
Route::post('servicetopdescription/orderby','admin\ServiceTopdescriptionController@orderby')->name('servicetopdescriptionorderby');


Route::get('project','admin\ProjectController@index');
Route::get('project/create', 'admin\ProjectController@create');
Route::post('project/store', 'admin\ProjectController@store');
Route::get('project/{id}/edit', 'admin\ProjectController@edit');
Route::post('project/update', 'admin\ProjectController@update');
Route::post('project/{id}/delete', 'admin\ProjectController@destroy');
Route::get('projectcatagory', 'admin\ProjectController@Catagory');
Route::post('project/orderby', 'admin\ProjectController@orderby')->name("projectorderby");
Route::get('projectcatagory/create', 'admin\ProjectController@CatagoryCreate');
Route::post('projectcatagory/store', 'admin\ProjectController@CatagoryStore');
Route::post('projectcatagory/delete/{id}', 'admin\ProjectController@CatagoryDelete');
Route::get('projectcatagory/edit/{id}', 'admin\ProjectController@CatagoryEdit');
Route::post('projectcatagory/update', 'admin\ProjectController@CatagoryUpdate');
Route::post('projectcatagory/catgaorybylang', 'admin\ProjectController@CatagoryByLang')->name('p_catagory_by_name');
Route::post('project/projectcategoryorderby', 'admin\ProjectController@projectcategoryorderby')->name("projectcategoryorderby");
Route::post('project/showProjectInHome', 'admin\ProjectController@showProjectInHome')->name("showProjectInHome");
Route::post('project/onChangeProjectLanguage', 'admin\ProjectController@onChangeProjectLanguage')->name("onChangeProjectLanguage");
Route::post('project/pojectcategorybylang', 'admin\ProjectController@pojectcategorybylang')->name("pojectcategorybylang");


Route::get('projects/download','admin\ProjectDownloadController@index');
Route::post('project/download/delete/{id}','admin\ProjectDownloadController@delete');


Route::get('service','admin\ServiceController@index');
Route::get('service/create', 'admin\ServiceController@create');
Route::post('service/store', 'admin\ServiceController@store');
Route::get('service/{id}/edit', 'admin\ServiceController@edit');
Route::post('service/update', 'admin\ServiceController@update');
Route::post('service/{id}/delete', 'admin\ServiceController@destroy');
Route::post('service/orderby', 'admin\ServiceController@orderby')->name("serviceorderby");
Route::post('service/showServiceInHome', 'admin\ServiceController@showServiceInHome')->name("showServiceInHome");
Route::post('service/onChangeServiceLanguage', 'admin\ServiceController@onChangeServiceLanguage')->name("onChangeServiceLanguage");



Route::get('article','admin\ArticlesController@index');
Route::get('article/create', 'admin\ArticlesController@create');
Route::post('article/store', 'admin\ArticlesController@store');
Route::get('article/edit/{id}', 'admin\ArticlesController@edit');
Route::post('article/update', 'admin\ArticlesController@update');
Route::post('article/delete/{id}', 'admin\ArticlesController@destroy');
Route::post('article/articleCategoryByLanguage', 'admin\ArticlesController@articleCategoryByLanguage')->name("articleCategoryByLanguage");
Route::post('article/orderby', 'admin\ArticlesController@orderby')->name("articlesorderby");
Route::post('article/onChangeArticlesLanguage', 'admin\ArticlesController@onChangeArticlesLanguage')->name("onChangeArticlesLanguage");





Route::get('languages','admin\LanguagesController@index');
Route::get('languages/create', 'admin\LanguagesController@create');
Route::post('languages/store', 'admin\LanguagesController@store');
Route::get('languages/{id}/edit', 'admin\LanguagesController@edit');
Route::post('languages/update', 'admin\LanguagesController@update');
Route::get('languages/{id}/delete', 'admin\LanguagesController@destroy');

Route::get('menu','admin\MenuController@index');
Route::post('menu/menubylang','admin\MenuController@manubylang')->name('manubylang');
Route::get('menu/create', 'admin\MenuController@create');
Route::post('menu/store', 'admin\MenuController@store');
Route::get('menu/{id}/edit', 'admin\MenuController@edit');
Route::post('menu/update', 'admin\MenuController@update');
Route::post('menu/{id}/delete', 'admin\MenuController@destroy');
Route::post('menu/orderby', 'admin\MenuController@orderby')->name("orderby");


Route::get('submenu','admin\MenuController@subindex');
Route::post('submenubylang','admin\MenuController@submanubylang')->name('submanubylang');
Route::get('submenu/create', 'admin\MenuController@subcreate');
Route::post('submenu/store', 'admin\MenuController@substore');
Route::get('submenu/{id}/edit', 'admin\MenuController@subedit');
Route::post('submenu/update', 'admin\MenuController@subupdate');
Route::post('submenu/{id}/delete', 'admin\MenuController@subdestroy');

Route::get('front/home','FrontController@index');
Route::get('front/home/create', 'FrontController@create');
Route::post('front/home/store', 'FrontController@store');
Route::get('front/home/{id}/edit', 'FrontController@edit');
Route::post('front/home/update', 'FrontController@update');
Route::post('front/home/{id}/delete', 'FrontController@destroy');


Route::get('label','admin\LabelController@index');
Route::get('label/create', 'admin\LabelController@create');
Route::post('label/store', 'admin\LabelController@store');
Route::get('label/{id}/edit', 'admin\LabelController@edit');
Route::post('label/update', 'admin\LabelController@update');
Route::post('label/{id}/delete', 'admin\LabelController@destroy');
Route::post('label/onChangeLabelLanguage', 'admin\LabelController@onChangeLabelLanguage')->name("onChangeLabelLanguage");



Route::get('errormessage','admin\ErrorMessageController@index');
Route::get('errormessage/create', 'admin\ErrorMessageController@create');
Route::post('errormessage/store', 'admin\ErrorMessageController@store');
Route::get('errormessage/{id}/edit', 'admin\ErrorMessageController@edit');
Route::post('errormessage/update', 'admin\ErrorMessageController@update');
Route::post('errormessage/{id}/delete', 'admin\ErrorMessageController@destroy');
Route::post('errormessage/onchangelanguage', 'admin\ErrorMessageController@onchangelanguage')->name('onchangelanguageerror');




Route::get('articlescategory','admin\ArticlesCategoryController@index');
Route::get('articlescategory/create', 'admin\ArticlesCategoryController@create');
Route::post('articlescategory/store', 'admin\ArticlesCategoryController@store');
Route::get('articlescategory/edit/{id}', 'admin\ArticlesCategoryController@edit');
Route::post('articlescategory/update', 'admin\ArticlesCategoryController@update');
Route::post('articlescategory/delete/{id}', 'admin\ArticlesCategoryController@destroy');
Route::post('articlescategory/orderby', 'admin\ArticlesCategoryController@orderby')->name("articlescategoryorderby");
Route::post('articlescategory/onchnagelanguage', 'admin\ArticlesCategoryController@onchnagelanguage')->name("onchnagelanguage");







Route::get('newsletter','admin\NewsletterController@index');
Route::get('newsletter/export', 'admin\NewsletterController@download');
// Route::post('newsletter/store','admin\NewsletterTextController@store');


Route::get('newslettertext','admin\NewsletterTextController@index');
Route::get('newslettertext/create','admin\NewsletterTextController@create');
Route::post('newslettertext/store','admin\NewsletterTextController@store');
Route::get('newslettertext/{id}/edit', 'admin\NewsletterTextController@edit');
Route::post('newslettertext/update', 'admin\NewsletterTextController@update');
Route::get('newslettertext/{id}/delete', 'admin\NewsletterTextController@destroy');


Route::get('moreinformation/validation/create','admin\MoreInformationValidationController@create');
Route::post('moreinformation/validation/store','admin\MoreInformationValidationController@store');
});
